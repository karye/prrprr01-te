---
description: Träna inför prov
---

# Provuppgifter

## Enklare

### PortoApp

Denna uppgift går ut på att skapa en app som räknar ut portokostnaden för ett brev. Appen ska ha följande utseende:

![alt text](../.gitbook/assets/image-155.png)

* I rutan **Vikt** anger användaren brevets vikt i gram.\
När användaren klickar på knappen **Beräkna porto** ska programmet räkna ut portokostnaden enligt följande:

![alt text](../.gitbook/assets/image-156.png)

Portotabellen ser ut så här:

| Max vikt (gram) | Pris (kronor) | Antal valörlösa frimärken |
|-----------------|---------------|---------------------------|
| 50              | 22            | 1                         |
| 100             | 44            | 2                         |
| 250             | 66            | 3                         |
| 500             | 88            | 4                         |
| 1 000           | 132           | 6                         |
| 2 000           | 154           | 7                         |

### AritmetikApp

Denna uppgift går ut på att skapa en app som utför enkel aritmetik. Appen ska ha följande utseende:

![alt text](../.gitbook/assets/image-157.png)

* I rutorna **Tal 1** och **Tal 2** anger användaren två tal.\
* I rutan **Operator** väljer användaren en operator: +, -, \*, /.\
* När användaren klickar på knappen **Beräkna** ska programmet utföra beräkningen och visa resultatet i rutan **Resultat**.\
* Tips: Rensa input från mellanslag såhär: ```string tal1Text = txbTal1.Text.Trim();```

![alt text](../.gitbook/assets/image-158.png)

* Om användaren skriver in felaktiga operator ska programmet visa ett felmeddelande:

![alt text](../.gitbook/assets/image-159.png)

* Om användaren skriver in felaktiga tal ska programmet visa ett felmeddelande:

![alt text](../.gitbook/assets/image-160.png)

### SwishaApp

Denna uppgift går ut på att skapa en app som simulerar en swish-tjänst. Appen ska ha följande utseende:

![alt text](../.gitbook/assets/image-151.png)

I rutan *Kontobalans* ser användaren sitt saldo på kontot.\
**Programmet slumpar fram detta belopp: 100-300 kr.**

I rutan **Önskat belopp** anger användaren hur mycket pengar hen vill ta ut.

* Om beloppet är mindre än kontobalansen ska programmet dra av beloppet från kontobalansen och visa det nya saldot.

![alt text](../.gitbook/assets/image-152.png)

* Om beloppet är större än kontobalansen ska programmet visa ett felmeddelande.

![alt text](../.gitbook/assets/image-153.png)

* Om användaren skriver in felaktiga tecken ska programmet visa ett felmeddelande:

![alt text](../.gitbook/assets/image-154.png)