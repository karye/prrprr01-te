# Labbar för WPF  

## 1. Melloomröstning  

![alt text](../../.gitbook/assets/image-132.png)

Skapa en applikation där användaren kan rösta på ett av flera alternativ, till exempel deras favoritfärg eller favoritmat. Resultaten visas efter att användaren har röstat.  

### Skapa projektet  

Öppna en terminal/konsol och kör:  

```powershell
dotnet new wpf -o ValApp
cd ValApp
dotnet run
```

### Använda bilder

För att använda bilder i WPF behöver vi lägga till dem i projektet. Skapa en mapp `bilder` i projektets rotmapp och lägg till en bild som heter `mello.png` från [WikiMedia](https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Melodifestivalen_Logo.svg/645px-Melodifestivalen_Logo.svg.png).

> OBS! Bilden måste vara i PNG-format.

```plaintext
ValApp
├── bilder
│   └── mello.png
├── obj
│   └── ...
├── bin
│   └── ...
├── App.xaml
├── App.xaml.cs
├── MainWindow.xaml
├── MainWindow.xaml.cs
└── ValApp.csproj
```

Öppna projektets `.csproj`-fil och lägg till följande kod för att inkludera bilderna i projektet:

```xml
<Project Sdk="Microsoft.NET.Sdk">

  <PropertyGroup>
    ...
  </PropertyGroup>
  <ItemGroup>
    <Resource Include="./bilder/*.png" />
  </ItemGroup>

</Project>
```

### Skapa gränssnittet i XAML  

Öppna `MainWindow.xaml` och testa med följande:  

![alt text](../../.gitbook/assets/image-135.png)

Här ger vi varje knapp ett namn och en klickhändelse som anropar en metod när knappen trycks. Med tex `name="RostRöd"` kan vi referera till knappen **Röd** i C#-koden.

### Lägg till C#-logik

Öppna `MainWindow.xaml.cs` och lägg till följande kod:  

![alt text](../../.gitbook/assets/image-135.png)

### Testa programmet  

Kör nu programmet igen:  

```powershell
dotnet run
```

Klicka på en av knapparna och se att rösterna räknas upp korrekt.  

### Utmaningar  
- Lägg till fler alternativ, t.ex. "Gul", "Lila".  
- Lägg till en knapp för att återställa rösterna.  

## 2. Textformatterare  

![alt text](../../.gitbook/assets/image-144.png)
  
Användaren kan skriva in en text och sedan formatera den genom att ändra storlek, färg eller göra den fetstil.  

### Skapa gränssnittet i XAML  

Öppna `MainWindow.xaml` och testa med:  

![alt text](../../.gitbook/assets/image-145.png)

### Lägg till C#-logik  

Öppna `MainWindow.xaml.cs` och testa med:  

![alt text](../../.gitbook/assets/image-146.png)

### Testa programmet  

```powershell
dotnet run
```

Mata in text och klicka på knapparna för att ändra format.  

### Utmaningar  
- Lägg till en knapp för att ändra färg på texten.  
- Lägg till en knapp för att ändra textstorlek.  

## 3. Gissa talet  

![alt text](../../.gitbook/assets/image-150.png)

Skapa ett enkelt spel där applikationen genererar ett slumpmässigt tal mellan 1 och 100. Användaren ska gissa vilket tal det är.  

### Skapa gränssnittet i XAML  

Öppna `MainWindow.xaml` och testa med:  

![alt text](../../.gitbook/assets/image-136.png)

### Lägg till C#-logik  

Öppna `MainWindow.xaml.cs` och testa med:  

![alt text](../../.gitbook/assets/image-137.png)

Prova att sätta fokus på textfältet när fönstret laddas:  

```csharp
public MainWindow()
{
    InitializeComponent();
    GissaTalet.Focus();
}
```

### Testa programmet  

Kör nu programmet igen:  

```powershell
dotnet run
```

Mata in ett tal i textfältet och klicka på knappen för att se om du gissar rätt.  

### Utmaningar  
- Lägg till en "Starta om"-knapp.\
  Tips: Flytta genereringen av det slumpade talet till en egen metod.
- Låt användaren få fler försök och visa antal gissningar.\
  Tips: Skapa en räknare för antal gissningar och visa den i textfältet.
- Hantera felaktig av tal - t.ex. om användaren skriver in bokstäver.

## 4. Enkel att göra-lista  

![alt text](../../.gitbook/assets/image-138.png)

Användaren kan lägga till, visa och ta bort uppgifter i en lista.  

### Skapa projektet  

```powershell
dotnet new wpf -o TodoApp
cd TodoApp
dotnet run
```

### Skapa gränssnittet i XAML  

Öppna `MainWindow.xaml` och testa med:  

![alt text](../../.gitbook/assets/image-139.png)

Den här gången använder vi en `TextBlock` för att visa uppgifterna. Jämfört med en `Label` kan en `TextBlock` visa flera rader text, och vi kan enkelt lägga till och ta bort text från den.

### Lägg till C#-logik  

Öppna `MainWindow.xaml.cs` och testa med:  

![alt text](../../.gitbook/assets/image-143.png)

### Testa programmet  

```powershell
dotnet run
```

Mata in en uppgift och klicka på "Lägg till" för att se den läggas till i `TextBlock`.  

### Utmaningar  
- Lägg till en knapp för att rensa alla uppgifter.  
- Lägg till en funktion för att ta bort enskilda uppgifter.  

## 5. Enkel kontaktbok  

![alt text](../../.gitbook/assets/image-147.png)

Användaren kan spara namn och telefonnummer i en enkel kontaktbok.  

### Skapa gränssnittet i XAML  

Öppna `MainWindow.xaml` och testa med:  

![alt text](../../.gitbook/assets/image-148.png)

### Lägg till C#-logik  

Öppna `MainWindow.xaml.cs` och testa med:  

![alt text](../../.gitbook/assets/image-149.png)

### Testa programmet  

```powershell
dotnet run
```

Mata in ett namn och telefonnummer och klicka på "Lägg till" för att se kontakten läggas till i `TextBlock`.  

### Utmaningar  
- Lägg till en knapp för att radera alla kontakter.  
- Lägg till en funktion för att ta bort en enskild kontakt.  
- Gör så att kontaktboken sparas i en textfil.\
  Tips: Använd `File.WriteAlltext` för att skriva till en textfil.\
  Tips: Använd `File.ReadAlltext` för att läsa från en textfil när programmet startar.