# Skapa din första WPF-app

Vi ska lära oss hur man skapar en enkel WPF-applikation med **XAML**, språket som används för att bygga grafiska användargränssnitt i Windows. Den här guiden är skriven för dig som är nybörjare och kanske bara har provat på att skriva lite HTML tidigare.

## Steg 1: Starta ditt projekt
### Gör så här:
Börja med att skapa ett nytt projekt **XamlIntro**:

I konsolen kör sedan följande kommandon:

```powershell
dotnet new wpf
dotnet run
```

## Filerna i projektet

I projektet finns följande filer:

* **MainWindow.xaml** liknar HTML. Den beskriver hur ett fönster är uppbyggt.
* **MainWindow.xaml.cs** är för C#-kod som styr fönstret.
* **App.xaml** är för globala inställningar.
* **App.xaml.cs** är för globala inställningar i C#.

## Steg 2: Lägg till en `Label`
En `Label` används för att visa text.

| Kontroll | Attribut | Beskrivning |
| :--- | :--- | :--- |
| `Label` | `Content` | Texten som ska visas. |

### Ändra koden:
1. Öppna **MainWindow.xaml**.

   ```xml
   <Window x:Class="WpfIntro.MainWindow"
         xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
         xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
         xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
         xmlns:mc="http://schemas.openxmlformats.org/xml-compatibility/2006"
         xmlns:local="clr-namespace:wpf2"
         mc:Ignorable="d"
         Title="MainWindow" Height="450" Width="800">
      <Grid>
      </Grid>
   </Window>
   ```
   Denna fil beskriver hur fönstret ska se ut. Det är en **XAML**-fil som liknar HTML:

   * `Window` beskriver fönstret, dess storlek och titel.
   * `Grid` är en layout som kan innehålla andra element.


1. Byt ut `<Grid></Grid>` mot detta:
   ```xml
   <StackPanel>
       <Label>Hej världen!</Label>
   </StackPanel>
   ```
1. Kör programmet igen:
   ```bash
   dotnet run
   ```
   Nu ser du texten "Hej världen!" i fönstret.

## Steg 3: Ändra textens utseende
Du kan göra texten större och lägga till mellanrum.

### Ändra koden:
* Ändra din `Label` till:
   ```xml
   <Label FontSize="20" Margin="10">Hej världen!</Label>
   ```
* Kör programmet igen. Ser du skillnaden?

## Steg 4: Lägg till en `TextBox`
En `TextBox` låter användaren skriva text.

| Kontroll | Attribut | Beskrivning |
| :--- | :--- | :--- |
| `Label` | `Content` | Texten som ska visas. |
| `TextBox` | `Text` | Texten som användaren skriver. |

### Ändra koden:
* Lägg till en `TextBox` under `Label`:
   ```xml
   <StackPanel>
       <Label FontSize="20" Margin="10">Ange ditt namn:</Label>
       <TextBox></TextBox>
   </StackPanel>
   ```
* Kör programmet. Nu kan du skriva i en ruta!

## Steg 5: Lägg till en `Button`
En `Button` används för att klicka och göra något.

| Kontroll | Attribut | Beskrivning |
| :--- | :--- | :--- |
| `Label` | `Content` | Texten som ska visas. |
| `TextBox` | `Text` | Texten som användaren skriver. |
| `Button` | `Content` | Texten på knappen. |

### Ändra koden:
* Lägg till en knapp under `TextBox`:
   ```xml
   <StackPanel>
       <Label FontSize="20" Margin="10">Ange ditt namn:</Label>
       <TextBox></TextBox>
       <Button>Klicka här</Button>
   </StackPanel>
   ```
* Kör programmet och prova knappen.

## Steg 6: Justera layouten
Nu ska vi göra knappen och texten snyggare.

### Ändra koden:
* Lägg till egenskaper för att justera mellanrum och storlek:
   ```xml
   <StackPanel>
       <Label FontSize="20" Margin="10">Ange ditt namn:</Label>
       <TextBox Margin="10" Padding="5"></TextBox>
       <Button Margin="10" Padding="10" Width="100" Height="40">Klicka här</Button>
   </StackPanel>
   ```
* Kör programmet och se hur det ser ut nu.

## Tips: Vad är `Margin` och `Padding`?
- `Margin`: Utrymme `utanför` komponenten (som en ram runt den).
- `Padding`: Utrymme `inuti` komponenten (runt texten i den).

Prova att ändra `Margin` och `Padding` till olika värden och se vad som händer!

## 7. Ändra fönstret

Nu ska vi ändra fönstrets utseende och beteende. Vi ska ändra titeln, färgen, storleken och var fönstret visas.

### Ändra koden:

1. Lägg till en `Title` för att ändra fönstrets titel:
   ```xml
   <Window x:Class="WpfApp1.MainWindow"
           xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
           xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
           Title="Min första WPF-app" Height="450" Width="800">
   ```

2. Lägg till en `Background` för att ändra fönstrets färg:
   ```xml
   <Window ...
            Background="LightGray">
    ```
3. Lägg till en `MinWidth` och `MinHeight` för att sätta minsta storlek på fönstret\
   och `MaxWidth` och `MaxHeight` för att sätta största storlek:
   ```xml
   <Window ...
            MinWidth="400" MinHeight="300"
            MaxWidth="800" MaxHeight="600">
   ```
4. Lägg till en `SizeToContent` för att ändra storleken på fönstret:
   ```xml
   <Window ...
            SizeToContent="WidthAndHeight">
   ```
   Körs programmet nu ser du att fönstret anpassar sig efter innehållet.
5. Lägg till en `WindowStartupLocation` för att ändra var fönstret visas:
   ```xml
   <Window ...
            WindowStartupLocation="CenterScreen">
   ```

## Övningar
1. Byt ut texten i knappen till "Spara".
2. Lägg till en andra `Label` som säger "Hej!" när knappen trycks.
3. Testa olika `FontFamily` för att ändra typsnittet i knappen.
   ```xml
   <Button FontFamily="Arial">Klicka här</Button>
   ```
4. Experimentera med färger:
   ```xml
   <Button Background="#FF0000" Foreground="#FFFFFF">Klicka här</Button>
   ```
