# Labb: Skapa en WPF-app för att räkna ut area

![](<../../.gitbook/assets/image-57 copy.png>)

I denna labb ska vi skapa en enkel WPF-app som låter användaren ange bredd och höjd, klicka på en knapp och få arean beräknad.

Vi kommer att:

- Skapa en ny WPF-app med .NET  
- Skriva XAML-kod för att skapa appens gränssnitt  
- Lägga till en klickhändelse i C# för att räkna ut arean  

## 1. Skapa ett nytt WPF-projekt

Först skapar vi en ny WPF-app i en konsol eller terminal. Öppna en mapp där du vill skapa projektet och kör:

```powershell
dotnet new wpf -o AreaApp
cd AreaApp
dotnet run
```

När du kör `dotnet run` ska ett tomt WPF-fönster dyka upp.

## 2. Skapa gränssnittet i XAML

Nu ska vi skapa själva appens utseende i filen `MainWindow.xaml`.

Öppna `MainWindow.xaml` i en kodeditor och ersätt innehållet med detta:

![alt text](../../.gitbook/assets/image-133.png)

### Förklaring av koden
- `<StackPanel></StackPanel>` används för att lägga elementen i en vertikal lista.  
- `<Label></Label>` används för att ge instruktioner till användaren. Texten skrivs mellan start- och sluttaggen.  
- `<TextBox></TextBox>` låter användaren ange bredd och höjd.  
- `<Button></Button>` används för att räkna ut arean (och har en `Click`-händelse). Texten för knappen skrivs mellan start- och sluttaggen.  
- `<TextBox></TextBox>` längst ner visar resultatet (`IsReadOnly="True"` gör att man inte kan skriva i den).  

## 3. Lägga till C#-kod för knapptryck

Nu ska vi lägga till logiken för att räkna ut arean när knappen klickas.

Öppna `MainWindow.xaml.cs` och ersätt innehållet med följande kod:

![alt text](../../.gitbook/assets/image-134.png)

### Förklaring av koden
- `KlickRäknaUt` är en metod som körs när knappen klickas.  
- `int.Parse` används för att omvandla text från `<TextBox></TextBox>` till siffror.  
- Arean beräknas genom att multiplicera bredden och höjden.  
- Resultatet visas i `<TextBox></TextBox>`.  

## 4. Testa programmet

Kör nu programmet igen:

```powershell
dotnet run
```

- Ange en bredd och en höjd i textfälten.  
- Klicka på "Räkna ut".  
- Arean visas i det nedre textfältet.  

## 5. Utmaningar

### 1. Lägg till ett valfritt färgtema
- Testa att ändra bakgrundsfärgen på fönstret och textfälten.  
- **Ändra** fontstorlek och typsnitt för att göra programmet mer stilrent.  

| Attribut | Beskrivning | Exempel |
|----------|------------|---------|
| `Background` | Ändrar bakgrundsfärg | `Background="LightGray"` |
| `Foreground` | Ändrar textfärg | `Foreground="DarkBlue"` |
| `FontSize` | Ändrar textstorlek | `FontSize="18"` |
| `FontFamily` | Ändrar typsnitt | `FontFamily="Arial"` |
| `MinHeight` / `MaxHeight` | Begränsar fönsterstorlek | `MinHeight="200"` |

### 2. Lägg till en reset-knapp
- Lägg till en ny knapp som **rensar** alla textfält när man klickar på den.

### 3. Lägg till felhantering
Om användaren skriver in bokstäver eller lämnar ett fält tomt **kraschar** programmet.  
- Se till att programmet kontrollerar inmatningen innan den räknar ut arean.  
- Om inmatningen är ogiltig ska en varning visas istället för att krascha.  

## Sammanfattning

I denna labb har vi:

- Skapat en WPF-app från grunden  
- Byggt gränssnittet med XAML  
- Skrivit C#-kod för att räkna ut arean  
- Testat och förbättrat programmet  
