---
description: Hur man skapar en Windows-gui med WP
---

# WPF-app med eventhantering

I denna guide skapar vi en enkel WPF-applikation med **XAML** och **C#**. Vi lägger till knappar och händelsehantering (events) för att göra appen interaktiv.

## Resultatet

![](<../../.gitbook/assets/image-60 copy.png>)

## Grundkoden

Börja med att skapa ett nytt projekt **WpfIntro**:

I konsolen kör sedan följande kommandon:

```powershell
dotnet new wpf
dotnet run
```

Nu ser vi ett fönster på skärmen!

![](<../../.gitbook/assets/image-61 copy.png>)

## Filerna i projektet

I projektet finns följande filer:

* **MainWindow.xaml** liknar HTML. Den beskriver hur ett fönster är uppbyggt.
* **MainWindow.xaml.cs** är för C#-kod som styr fönstret.

## MainWindow.xaml

Öppna filen **MainWindow.xaml** och titta på koden:

```xml
<Window x:Class="WpfIntro.MainWindow"
        xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
        xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
        xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
        xmlns:mc="http://schemas.openxmlformats.org/xml-compatibility/2006"
        xmlns:local="clr-namespace:wpf2"
        mc:Ignorable="d"
        Title="MainWindow" Height="450" Width="800">
    <Grid>
    </Grid>
</Window>
```

### Ändra fönstrets utseende

Nu skall vi ändra fönstreets utseende. Ändra titeln till "Min första app" och storleken till 150x200. Det gör vi genom att ändra `Title` och `Height` och `Width`:

```xml
<Window x:Class="WpfIntro.MainWindow" ...
        Title="Min första app" Height="200" Width="150">
        ...
</Window>
```

Vi ändrar också bakgrundsfärgen till ljusgrå med `Background="#EEE"`:

```xml
<Window x:Class="WpfIntro.MainWindow" ...
        Title="Min första app" Height="200" Width="150"
        Background="#EEE">
        ...
</Window>
```

Fönstret ser nu ut så här:

![](<../../.gitbook/assets/image-62 copy.png>)

## Skapa layouten

Ta bort `Grid` och infoga en `Label` istället för att skriva ut en text. En `Label` är en text som inte går att redigera:

```xml
<Window ...>
    <Label>Ange en text</Label>
</Window>
```

![](<../../.gitbook/assets/image-63 copy.png>)

Infoga nu en knapp `Button`. En knapp är ett element som användaren kan klicka på:

```xml
<Window ...>
    <Label>Ange en text</Label>
    <Button>OK</Button>
</Window>
```

Prova köra programmet. Du får ett felmeddelande! Du måste bygga en layout! \
Om du inte har en layout så kan du inte placera flera element på fönstret. 

### StackPanel

Vi kommer att använda följande kontroller:

| Kontroll | Attribut | Beskrivning |
| :--- | :--- | :--- |
| StackPanel | Margin | Luft runt elementen |
| Label | | Text som inte går att redigera |
| TextBox | Name | Namn på textrutan |
| Button | Click | Vilken metod som skall köras när knappen klickas på |

För att bygga en layout kan man använda en `StackPanel` för att stapla element:

```xml
<StackPanel>
    <Label>Ange en text</Label>
    <Button>OK</Button>
    <Button>Avbryt</Button>
</StackPanel>
```

Nu kan du köra programmet.

![](<../../.gitbook/assets/image-64 copy.png>)

### Infoga en textruta

För att användaren ska kunna skriva in en text behöver vi en textruta. I HTML används:

```html
<input type="text"...>
```

I XAML används istället `TextBox`:

```xml
<StackPanel>
    <Label>Ange en text</Label>
    <TextBox />
    <Button>OK</Button>
    <Button>Avbryt</Button>
</StackPanel>
```

![](<../../.gitbook/assets/image-65 copy.png>)

### Snygga till layouten

Vi har redan använt attributet `Background` för att byta bakgrundsfärg. Vi ändrar höjden med `Height`:

```xml
<TextBox Height="50" />
```

![](<../../.gitbook/assets/image-66 copy.png>)

Med attribut `Margin` blir layouten luftigare:

```xml
<Window ...>
    <StackPanel Margin="5">
        <Label>Ange en text</Label>
        <TextBox Margin="5" Height="50" />
        <Button Margin="5" Padding="5">OK</Button>
        <Button Margin="5" Padding="5">Avbryt</Button>
    </StackPanel>
</Window>
```

![](<../../.gitbook/assets/image-67 copy.png>)

### Fler attribut

Det finns såklart många fler attribut man kan använd för att bygga en gui, testa:

* Prova `FontSize`, `FontFamily` mm
* Prova `MinHeight`, `MaxHeight` på `<Window>`

### Övningar

1. Ändra fönstrets storlek till 400x300
1. Ändra textfärgen på knapparna

## Händelsehantering (events)

WPF-appar är händelsestyrda (events). När man tex klickar på en knapp så skall programmet göra något. Detta kallas för att hantera ett event (event handling).

För att skapa interaktivitet måste vi koppla händelser till metoder. Det gör vi genom att skriva kod i C#.

Det behövs två nya attribut `Click` och `Name`:

| Kontroll | Attribut | Beskrivning |
| :--- | :--- | :--- |
| TextBox | Name | Namn på textrutan så att vi kan läsa av texten i koden |
| Button | Click | Vilken metod som skall köras när knappen klickas på |

### MainWindow.xaml

För att lyssna på knapparnas `Click`-event skriver man:

* `Click="KlickOk"`

```xml
<Window ...>
    <StackPanel Margin="5">
        <Label>Ange en text</Label>
        <TextBox Name="tbxText" Margin="5" Height="50" />
        <Button Click="KlickOk" Margin="5">OK</Button>
        <Button Margin="5">Avbryt</Button>
    </StackPanel>
</Window>
```

### MainWindow.xaml.cs

För att fånga upp "klicket" på knapparna skapar man sedan metoder:

* `private void KlickOk(...)` för knappen OK

```csharp
...
public partial class MainWindow : Window
{
    public MainWindow()
    {
        InitializeComponent();
    }

    // Ny metod för att hantera klicksignal från knappen Ok
    private void KlickOk(object sender, RoutedEventArgs e)
    {
        MessageBox.Show("Du klickade på OK");
    }
}
```

### Hantera knappen Avbryt

För att hantera knappen Avbryt skapar vi en ny metod `KlickAvbryt`:

```xml
<Button Click="KlickAvbryt" Margin="5">Avbryt</Button>
```

```csharp
private void KlickAvbryt(object sender, RoutedEventArgs e)
{
    MessageBox.Show("Du klickade på Avbryt");
}
```

### Läs av texten i textrutan

För att läsa av texten i textrutan måste vi ge den ett namn `Name="tbxText"`:

```xml
<TextBox Name="tbxText" Margin="5" Height="50" />
```

Nu kan vi läsa av texten i metoden `KlickOk`:

```csharp
private void KlickOk(object sender, RoutedEventArgs e)
{
    // Läsa av texten i textrutan
    string text = tbxText.Text;

    // Visa texten i en dialogruta
    MessageBox.Show($"Du skrev: {text}");
}
```

### Skriva ut i en textruta

Vi lägger till en ny textruta längst ned:
    
```xml
<TextBox Name="tbxResultat" Margin="5" Height="50" />
```

Nu kan vi skriva ut texten i textrutan `tbxResultat`:

```csharp
private void KlickOk(object sender, RoutedEventArgs e)
{
    // Läsa av texten i textrutan tbxText
    string text = tbxText.Text;

    // Visa texten i textrutan tbxResultat
    tbxResultat.Text = text;
}
```

### Övningar

1. Skapa en ny knapp "Rensa" som rensar textrutan
2. Skapa en ny knapp "Avsluta" som stänger fönstret
   Tips: `Close()` stänger fönstret

## Mer info

* Läs mer om [WPF Button](https://wpf-tutorial.com/basic-controls/the-button-control/)
* Youtube-tutorial: [C# WPF Tutorial](https://youtube.com/playlist?list=PLih2KERbY1HHOOJ2C6FOrVXIwg4AZ-hk1&si=K6SPMNN45M5cgvy9)

## Uppgift

### Uppgift 1: Ändra bakgrundsfärg

![alt text](../../.gitbook/assets/image-128.png)

* Skapa en WPF-app med två knappar.
* En knapp som ändrar bakgrundsfärgen till röd.
* En knapp som ändrar bakgrundsfärgen till Grön.
* Tips: Använd tex `this.Background = Brushes.Blue;`

### Uppgift 2: Dölj och visa text

![alt text](../../.gitbook/assets/image-129.png)

Skapa en WPF-app med två knappar:  

* En knapp som döljer text i en `Label`.  
* En knapp som visar texten igen.  
* Tips: Använd `lblText.Visibility = Visibility.Hidden;` för att dölja text.

### Uppgift 3: Ändra storlek på fönstret

![alt text](../../.gitbook/assets/image-130.png)

Skapa en WPF-app med fyra knappar:

* En knapp som ökar fönstrets bredd med 10 pixlar.  
* En knapp som minskar fönstrets bredd med 10 pixlar.
* En knapp som ökar fönstrets höjd med 50 pixlar.
* En knapp som minskar fönstrets höjd med 50 pixlar. 
* Tips: Använd `this.Width += 50;` eller `this.Width -= 50;`.

