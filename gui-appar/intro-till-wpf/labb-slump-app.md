# Labb: Skapa en WPF-app för att slumpa fram ett tal  

![](<../../.gitbook/assets/image-58 copy.png>)

I denna labb ska vi skapa en enkel WPF-app där användaren kan klicka på en knapp för att slumpa fram ett tal mellan 1 och 100.  

Vi kommer att:  

- Skapa en ny WPF-app med .NET  
- Skriva XAML-kod för att skapa appens gränssnitt  
- Lägga till en klickhändelse i C# för att generera ett slumptal  

## 1. Skapa ett nytt WPF-projekt  

Först skapar vi en ny WPF-app i en konsol eller terminal. Öppna en mapp där du vill skapa projektet och kör:  

```powershell
dotnet new wpf -o SlumpTalApp
cd SlumpTalApp
dotnet run
```

När du kör `dotnet run` ska ett tomt WPF-fönster dyka upp.  

## 2. Skapa gränssnittet i XAML  

Nu ska vi skapa själva appens utseende i filen `MainWindow.xaml`.  

Öppna `MainWindow.xaml` i en kodeditor och ersätt innehållet med detta:  

![alt text](../../.gitbook/assets/image-141.png)

- `<StackPanel></StackPanel>` används för att placera elementen i en vertikal lista.  
- `<Label></Label>` används för att ge användaren en instruktion.  
- `<Button></Button>` används för att starta slumptalsgenereringen vid klick.  
- `<TextBox></TextBox>` visar det slumpade talet och är endast för läsning (`IsReadOnly="True"`). Vi ger den ett **namn** så att vi kan referera till den i C#-koden.

## 3. Lägga till C#-kod för knapptryck  

Nu ska vi lägga till logiken för att generera ett slumptal när knappen klickas.  

Vi kopplar en händelse till knappen genom att lägga till `Click="..."` i XAML-koden. 

![alt text](../../.gitbook/assets/image-142.png)

## 4. Testa programmet  

Kör nu programmet igen:  

```powershell
dotnet run
```

- Klicka på "Slumpa tal".  
- Ett nytt slumptal visas i textfältet varje gång knappen klickas.  

## 5. Anpassa och experimentera  

### Styling-tips  
Testa att lägga till eller ändra dessa attribut i XAML:  

| Attribut | Beskrivning | Exempel |
|----------|------------|---------|
| `Background` | Ändrar bakgrundsfärg | `Background="LightGray"` |
| `Foreground` | Ändrar textfärg | `Foreground="DarkBlue"` |
| `FontSize` | Ändrar textstorlek | `FontSize="30"` |
| `FontFamily` | Ändrar typsnitt | `FontFamily="Verdana"` |
| `MinHeight` / `MaxHeight` | Begränsar fönsterstorlek | `MinHeight="200"` |

## 6. Utmaningar  

![](../../.gitbook/assets/labb-2c.png)

### 1. Låt användaren ange ett maxvärde  
- Lägg till en `TextBox` där användaren kan mata in ett maxvärde.  
- Se till att programmet använder detta värde istället för 100.  

### 2. Standardvärde om användaren skriver fel  
- Om användaren inte matar in ett giltigt tal ska programmet använda 100 som standard.  

### 3. Lägg till en statusrad för felmeddelanden  
- Lägg till en `Label` längst ned i fönstret där felmeddelanden kan visas.  
- Om användaren skriver en ogiltig maxgräns, visa ett felmeddelande istället för att krascha.  

