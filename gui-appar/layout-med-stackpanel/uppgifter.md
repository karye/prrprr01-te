---
description: Träna på att skapa gui-appar med WPF
---

# Uppgifter

## Uppgift 1

* Skapa en fönsterapp som krypterar en text med Ceasar-krypto.
* Användaren matar in texten i en ruta och anger nyckel i en annan.
* Det krypterade resultatet visas i en tredje ruta.

![](../../.gitbook/assets/image-49%20copy.png)

I koden från Prog 1 hittar du uppslag på hur du löser uppgiften:

```csharp
using System;

Console.Write("Ange en text som skall krypteras: ");
string text = Console.ReadLine();

Console.Write("Ange en nyckel (1-9): ");
string nyckelString = Console.ReadLine();

int nyckel = 0;
while (!int.TryParse(nyckelString, out nyckel))
{
    Console.WriteLine("Du måste mata in ett tal (1-9). Försök igen: ");
    nyckelString = Console.ReadLine();
}

string kryptadText = "";
for (int i = 0; i < text.Length; i++)
{
    char bokstav = text[i];
    Console.WriteLine($"Bokstaven på position {i} är {bokstav}");

    int ascii = (int)bokstav;
    Console.WriteLine($"{bokstav} är {ascii}");

    int kryptadAscii = ascii + nyckel;

    if (kryptadAscii >= 90)
    {
        kryptadAscii -= 26;
    }

    char kryptadBokstav = (char)(kryptadAscii);
    Console.WriteLine($"{bokstav} blir {kryptadBokstav}");

    kryptadText += kryptadBokstav.ToString();
}

Console.WriteLine($"Den krypterade texten blir: {kryptadText}");
```

## Uppgift 2

* Skapa en fönsterapp som är en miniräknaren som klarar räknesätten + - \* /.
* Appen skall ha två textrutor och fyra knappar samt en svarsruta.
* Användaren skall kunna mata in heltal eller decimaltal.
* För raden med knappar testa `DockPanel` istället för horisontell `StackPanel:`

```xml
<DockPanel HorizontalAlignment="Stretch" >..</DockPanel>
```

![](../../.gitbook/assets/image-50%20copy.png)

## Uppgift 3

* Fortsätt med miniräknaren.
* Om man matar in något ogiltigt visas en popup. Använd [TryParse ](https://www.progsharp.se/kapitel/6/#ett-battre-satt-for-konvertering-av-text-till-tal)för att fånga upp fel:

```csharp
MessageBox.Show("Felaktig inmatning.\nFormatera talen som tex 12 eller 12,5");
```

![Tal2 blev fel](../../.gitbook/assets/image-51%20copy.png)

![](../../.gitbook/assets/image-52%20copy.png)

## Uppgift 4

* Skapa en fönsterapp som visar innehållet i en textfil.
* Användaren får mata in namnet på textfilen.
* Finns inte textfilen visas en varningsruta.
* För en textruta med rullningslist använd:

```xml
<TextBox Name="resultatRuta" Margin="5" Height="150" Background="#FFF" 
FontStyle="italic" TextWrapping="WrapWithOverflow" 
VerticalScrollBarVisibility="Auto" AcceptsReturn="True" IsReadOnly="True" />
```

![](../../.gitbook/assets/image-53%20copy.png)

## Uppgift 5

* Fortsättningen på föregående uppgift.
* Användaren väljer filen på datorn med dialogrutan filväljaren.
* Se [https://wpf-tutorial.com/dialogs/the-openfiledialog](https://wpf-tutorial.com/dialogs/the-openfiledialog/)

![Filen index.html är vald](../../.gitbook/assets/image-54%20copy.png)

![.. och visas i TextBox](../../.gitbook/assets/image-55%20copy.png)

## Uppgift 6

* Skapa en fönsterapp som löpande visar summa och medelvärdet på inmatade tal.
* Användaren matar in ett tal och klickar på knappen **Addera**, summa och medelvärdet visas
* Användaren matar in ett nytt tal och summan ökas och ett nytt medelvärde räknas ut, osv.

![](../../.gitbook/assets/image-56%20copy.png)
