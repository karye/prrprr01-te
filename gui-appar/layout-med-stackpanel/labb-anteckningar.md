---
description: Nu bygger vi färdiga fönsterappar
---

# Labb: anteckningar

![](<../../.gitbook/assets/image-32 copy.png>)

Skapa en ny projektmapp, och kör i konsolen:

```powershell
dotnet new wpf
```

## Layouten i MainWindow.xaml

Följande kontroller skall finnas i fönstret:

* En label med texten "Anteckningar"
* En textbox för att skriva in anteckningar
* En knapp för att skicka anteckningen
* En större textbox för att visa alla anteckningar

### MainWindow.xaml

Ge kontrollerna lämpliga namn och egenskaper.\
`Name`-egenskapen är viktig för att kunna referera till kontrollerna i koden.

```xml
<TextBox Name="txbInput" ...></TextBox>
```

För knappen kan vi koppla en metod till `Click`-eventet:

```xml
<Button Click="KlickSkicka" ...>Skicka!</Button>
```

### MainWindow.xaml.cs



```csharp
public partial class MainWindow : Window
{
    public MainWindow()
    {
        InitializeComponent();
    }

    private void KlickSkicka(object sender, RoutedEventArgs e)
    {
        // Skriv ut i den stora textrutan med +=
        ..
    }
}
```

Såhär kan det se ut:

![](<../../.gitbook/assets/image-32 copy.png>)

## Utmaningar

* Gör så att alla anteckningar samlas i den stora rutan - en per rad.
* Lägg till klockslag för varje anteckning.\
  Tips: `DateTime.Now.ToString("HH:mm:ss")`
* Spara ned alla anteckningar i en textfil.
  Tips: `File.AppendAllText("anteckningar.txt", anteckning + Environment.NewLine);`
* Lägg till en knapp som rensar alla anteckningar.