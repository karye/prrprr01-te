---
description: Hur man får kontroll på layouten
---

# Brännbollsräknaren

![](../../.gitbook/assets/image-45%20copy.png)

Vi bygger en app som räknar poäng i brännboll.\
Programmet ska kunna registrera poäng för två lag: inne och ute.

![](../../.gitbook/assets/image-46%20copy.png)

Poäng räknas enligt följande:

* 1 poäng för varje insprungen person
* 5 poäng för frivarv
* 2 poäng för bränning
* 3 poäng för lyra (enhandslyra räknas inte)

## MainWindow.xaml

XAML-koden ser som följer:

```xml
<StackPanel Margin="10">
    <Label>
    <StackPanel Orientation="Horizontal" Margin="10">
        <Label>>
        <TextBox>
        <Label>
        <TextBox>
    </StackPanel>
    <StackPanel Orientation="Horizontal" Margin="5">
        <Button>
        <Button>
        <Button>
        <Button>
    </StackPanel>
</StackPanel>
```

### Fönstrets storlek

Vi kan anpassa fönstrets storlek till innehållet med `SizeToContent="WidthAndHeight"`:

```xml
<Window ... 
MinHeight="350" MinWidth="370" SizeToContent="WidthAndHeight">
...
</Window>
```

### MainWindow.xaml.cs

Vi skapar en metod för varje knapp. Metoderna skall öka poängen för inne respektive ute.  
När klickar på knappen **Frivarv** ökar vi poängen med 5. Vi kopplar knappen till metoden `KlickFrivarv()`:

```csharp
private void KlickFrivarv(object sender, RoutedEventArgs e)
{
    tbxLagInne.Text = (int.Parse(tbxLagInne.Text) + 5).ToString();
}
```

## Lagens poäng

Vi har två lag, inne och ute. Varje lag har en poängsumma. 
Efter varje klicka ska vi öka poängen för det lag som knappen gäller.  
Vi skapar två variabler, en för varje lag:

```csharp
int poangInne = 0;
int poangUte = 0;
```

Men var ska vi lägga variablerna?  
Vi lägger dem ute i klassen `MainWindow`, dvs utanför metoderna.  
Variablerna är på så vis **globala** variabler, dvs de kan nås från alla metoder i klassen. Vi kan läsa och skriva till variablerna från alla metoder i klassen: 

```csharp
public partial class MainWindow : Window
{
    int poangInne = 0;
    int poangUte = 0;

    public MainWindow()
    {
        InitializeComponent();
    }

    private void KlickFrivarv(object sender, RoutedEventArgs e)
    {
        poangInne += 5;
        tbxLagInne.Text = poangInne.ToString();
    }
}
```

## Utmaningar

### Fönstret

* Se till att fönstret inte kan förstoras eller förminskas för mycket, dvs använd `MinHeight` och `MinWidth` och `MaxHeight` och `MaxWidth`.

### Knapparna

* Se till att alla knappar funkar som dom skall, dvs **Frivarv**, **Bränning**, **Lyra** och **Varv**.

### Historiken

* Skriv ut senaste handling i en historikruta:

![](../../.gitbook/assets/image-47%20copy.png)

* Se till så att **senaste** handling är **överst**
* Skriv ut [klockslaget ](https://csharp.progdocs.se/csharp-ref/grundlaeggande/datum-och-tid#datetime.now) i början på varje rad, se:

```csharp
DateTime tid = DateTime.Now;
Console.WriteLine(tid.ToString("HH:mm:ss"));
```

![](../../.gitbook/assets/image-48%20copy.png)

### Spara ned historiken

* Skapa en knapp **Spara**
* Spara ned spelets historik i en textfil se [Läsa och skriva](https://csharp.progdocs.se/filhantering/laesa-och-skriva)

