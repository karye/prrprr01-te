---
description: Nu bygger vi färdiga fönsterappar
---

# Labb: app för omvänd text

Vi skapar en fönsterapp med två textrutor och en knapp.\
Användaren skriver i en text i en ruta. Programmet skriver ut den baklänges i den andra ruta.

![](<../../.gitbook/assets/image-34 copy.png>)

Skapa en ny projektmapp, och kör i konsolen:

```powershell
dotnet new wpf
```

## MainWindow.xaml

Här XAML-koden för appens utseende:

```xml
<Window x:Class="Labb4.MainWindow"
        xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
        xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
        xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
        xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
        xmlns:local="clr-namespace:Labb4"
        mc:Ignorable="d"
        Title="Omvänd text" Width="300" Height="320"
        SizeToContent="WidthAndHeight"
        Background="#EEE">
    <StackPanel Margin="10">
        <Label FontSize="25">Omvänd text</Label>
        <Label>Ange text</Label>
        <TextBox Name="tbxInput" Margin="5" Height="50" TextWrapping="Wrap" AcceptsReturn="True"/>
        <Button Margin="5">Kör</Button>
        <Label>Sparad text</Label>
        <TextBox Name="txbOutput" Margin="5" Height="100" IsReadOnly="True" TextWrapping="Wrap"/>
        <TextBox Name="txbTotalTecken" Margin="5" IsReadOnly="True" FontStyle="italic"/>
    </StackPanel>
</Window>

```

## MainWindow.xaml.cs

Grundkoden som genereras ser ut såhär:

```csharp
/// <summary>
/// Interaction logic for MainWindow.xaml
/// </summary>
public partial class MainWindow : Window
{
    public MainWindow()
    {
        InitializeComponent();
    }
}
```

Koppla nu knappens `Click` event till koden:

```xml
<Button Click="KlickVänd" ...>Skicka!</Button>
```

## Kod för omvänd text

Skapa en metod `OmvändSträng(string text)` som vänder på en text:

```csharp
public partial class MainWindow : Window
{
    // Klassvariabeln för spara antal tecken som vänds
    static int antalTecken = 0;

    public MainWindow()
    {
        InitializeComponent();
    }

    private void KlickVänd(Object sender, RoutedEventArgs e)
    {
        // Läs in texten från formuläret
        string text = tbxInput.Text;

        // Vänd på texten
        ...
        
        // Skriv ut i textruta
        ...
        
        // Räkna upp antal total tecken
        ...
        
        // Skriv ut totalt antal tecken
    }
    
    // Vänder på textens ordning med en for-loop
    private string OmvändText(string text)
    {
        ...
    }
}
```

## Utmaningar

* Kontrollera att inmatade texten inte är tom. Använd `MessageBox.Show()`:

![](<../../.gitbook/assets/image-35 copy.png>)

* Lägg till en knapp för att spara ned resultatet, se [filhantering](https://csharp.progdocs.se/csharp-ref/filhantering/laesa-och-skriva#att-skriva-data-till-en-fil)

```csharp
// Hanterar klick på knappen Spara
private void KlickSpara(Object sender, RoutedEventArgs e)
{
    // Sparar texten i en fil
    File.WriteAllText("texten.txt", omvändText);
}
```

## Mer info

* Läs mer om `Button`[ http://www.blackwasp.co.uk/WPFButton.aspx](https://github.com/karye/PRRPRR02-TE/tree/204b427e8d7629378601a047369fc260ddb19a13/WPFButton.aspx)
* Läs mer om `TextBox` [http://www.blackwasp.co.uk/WPFTextBox.aspx](http://www.blackwasp.co.uk/WPFTextBox.aspx)
* Läs mer om `StackPanel` [http://www.blackwasp.co.uk/WPFStackPanel.aspx](http://www.blackwasp.co.uk/WPFStackPanel.aspx)
* Läs med om `TextBlock` [http://www.blackwasp.co.uk/TextBlock.aspx](http://www.blackwasp.co.uk/TextBlock.aspx)
