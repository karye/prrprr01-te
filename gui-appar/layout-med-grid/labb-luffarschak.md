# Labb: luffarschack

## Inledning

Luffarschack eller "Tic Tac Toe" som det kallas på engelska är en klassisk programmeringsuppgift.  
I WPF är det dock inte så lätt att använda befintliga komponenter utan vi måste bygga våra egna. Vi tänker oss att vi har en spelplan utav 3 x 3 knappar. När en spelare trycker på en knapp så skrivs "X" eller "O" på knappen. Det är alltså spelaren som trycker på knappen som får skriva "X" eller "O". Detta är en viktig del av uppgiften.

![](../../.gitbook/assets/luffarschack.png)

### Regler

* Två spelare spelar mot varandra.
* Den som får 3 i rad vinner.
* Det finns 8 olika möjligheter att vinna: 3 rader, 3 kolumner och 2 diagonaler.

### Implementation

Spelplanen är en 3 x 3 `grid` med knappar (`Button`). När en spelare trycker på en knapp så skrivs "X" eller "O" på knappen. 

Ett `Click`-event på knappen triggar en metod som skriver "X" eller "O" på knappen. 
Vi kollar sedan om någon har vunnit, dvs om någon har 3 i rad. Om ingen har vunnit så är det den andra spelarens tur. Vi meddelar spelaren mha en `MessageBox` om spelaren har vunnit.

Vi använder en 2d-array för att hålla reda på alla knappar. Vi kan då enkelt kolla om någon har vunnit genom att kolla om någon av de 8 möjligheterna har 3 knappar med samma text. Genom att använda en array kan vi också enkelt ändra storleken på spelplanen från 3 x 3 till 4 x 4 eller 5 x 5.

Här kan du läsa på om [arrayer](https://csharpskolan.se/article/falt-array/).

## Kod

### MainWindow.xaml

```xml
<Window x:Class="Luffarschack.MainWindow" 
    xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation" 
    xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml" 
    xmlns:d="http://schemas.microsoft.com/expression/blend/2008" 
    xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" 
    xmlns:local="clr-namespace:Luffarschack" mc:Ignorable="d" 
    Title="Luffarscahck" 
    Height="450" Width="450" 
    MinWidth="450" MinHeight="450" 
    MaxWidth="500" MaxHeight="500"
    SizeToContent="WidthAndHeight">
    <Grid Margin="10">
        <Grid.RowDefinitions>
            <RowDefinition Height="Auto"/>
            <RowDefinition Height="*"/>
        </Grid.RowDefinitions>
        <Grid.ColumnDefinitions>
            <ColumnDefinition Width="*"/>
        </Grid.ColumnDefinitions>
        <DockPanel Margin="5">
        <Label Grid.Row="0" Grid.Column="0">Spelare 1</Label>
        <TextBox Width="100" Grid.Row="0" Grid.Column="1" Name="txbSpelare1" />
        <Label Grid.Row="1" Grid.Column="0">Spelare 2</Label>
        <TextBox Width="100" Grid.Row="1" Grid.Column="1" Name="txbSpelare2" />
        <Button Margin="20, 0, 0, 0" Grid.Row="2" Grid.Column="0" Content="Starta" Click="KlickStarta" />
        </DockPanel>
        <Grid Grid.Row="1" Grid.Column="0">
            <Grid.RowDefinitions>
                <RowDefinition Height="*"/>
                <RowDefinition Height="*"/>
                <RowDefinition Height="*"/>
            </Grid.RowDefinitions>
            <Grid.ColumnDefinitions>
                <ColumnDefinition Width="*"/>
                <ColumnDefinition Width="*"/>
                <ColumnDefinition Width="*"/>
            </Grid.ColumnDefinitions>
            <Button x:Name="knapp1" Content="1" Grid.Row="0" Grid.Column="0" Click="KlickKnapp"/>
            <Button x:Name="knapp2" Content="2" Grid.Row="0" Grid.Column="1" Click="KlickKnapp"/>
            <Button x:Name="knapp3" Content="3" Grid.Row="0" Grid.Column="2" Click="KlickKnapp"/>
            <Button x:Name="knapp4" Content="4" Grid.Row="1" Grid.Column="0" Click="KlickKnapp"/>
            <Button x:Name="knapp5" Content="5" Grid.Row="1" Grid.Column="1" Click="KlickKnapp"/>
            <Button x:Name="knapp6" Content="6" Grid.Row="1" Grid.Column="2" Click="KlickKnapp"/>
            <Button x:Name="knapp7" Content="7" Grid.Row="2" Grid.Column="0" Click="KlickKnapp"/>
            <Button x:Name="knapp8" Content="8" Grid.Row="2" Grid.Column="1" Click="KlickKnapp"/>
            <Button x:Name="knapp9" Content="9" Grid.Row="2" Grid.Column="2" Click="KlickKnapp"/>
        </Grid>
    </Grid>
</Window>
```

### MainWindow.xaml.cs

```csharp
public partial class MainWindow : Window
{
    // Vems tur det är
    private bool spelare1Tur = true;

    // En lista i 2 dimensioner som håller koll på vilka knappar som är tryckta
    private List<List<Button>> bräde;

    public MainWindow()
    {
        InitializeComponent();

        // Lagra knapparna i arrayen

    }

    private void KlickKnapp(object sender, RoutedEventArgs e)
    {
        Button button = (Button)sender;
        button.FontSize = 90;
        button.IsEnabled = false;

        // Registrera vilken knapp som trycktes


        // Kolla om en rad är full

    }

    private void KlickStarta(object sender, RoutedEventArgs e)
    {
        Reset();
    }

    private void Reset()
    {
        // Återställ knapparna

    }
}
```

## Utmaningar

### Del 1

* I exemplet ovan så är alla 9 knappar utplacerade manuellt. Detta är inte särskilt effektivt ifall vi vill utöka storleken på brädet.
* Uppgiften går ut på att istället skapa de 9 knapparna i kod. Du måste alltså ställa in alla egenskaper som tex storlek, bakgrundsfärg, `Click`-event etc. Använd en loop som baseras på storleken.

Exempel:

```csharp
Button knapp = new Button();
knappen.Background = Brushes.LightSalmon;

// Koppla eventet Click till metoden KlickKnapp
knapp.Click += KlickKnapp;

// Ange position
Grid.SetRow(knapp, rad);
Grid.SetColumn(knapp, kol);

// Rita ut knappen
grid.Children.Add(knapp);
```

### Del 2

* Gör så att du kan ställa in storleken på brädet via en variabel i koden. Anpassa gärna storleken på knapparna samt storleken på typsnittet efter storleken på brädet.
* Inför också en variabel som bestämmer "hur många i rad" som krävs för att vinna.
* Du måste alltså justera algoritmerna för att bestämma vinnaren.

## Facit

Facit finns i projektet [LuffarschackFacit](facit-labb-luffarschack.md).
