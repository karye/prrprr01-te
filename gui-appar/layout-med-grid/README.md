---
description: Hur man skapar en responsiv layout i XAML
---

# Layout med Grid

## Resultatet

![](../../.gitbook/assets/image-24%20copy.png)

![](../../.gitbook/assets/image-25%20copy.png)

## Grundkoden

I konsolen kör:

```powershell
dotnet new wpf
```

## Anpassat appfönstret

* Vi kan sätta gränser för hur mycket fönstret (`Window`) kan förändras av användaren:
  * `MinWidth="250" MinHeight="200"`
  * `MaxWidth="800" MaxHeight="600"`

För att fönstret ska anpassas till innehållet använder vi:

* SizeToContent="WidthAndHeight" på `Window`

```xml
<Window ...
        Title="MainWindow" 
        MinHeight="200" MinWidth="250" MaxHeight="600" MaxWidth="650" 
        SizeToContent="WidthAndHeight"
        Background="#EEE">
```

## Definiera en Grid

Första bestämmer vi hur `Grid`:et ser ut. Här 2 rader och 2 kolumner:

```xml
<Grid Margin="10">
    <Grid.RowDefinitions>
        <RowDefinition Height="Auto" />
        <RowDefinition Height="Auto" />
    </Grid.RowDefinitions>
    <Grid.ColumnDefinitions>
        <ColumnDefinition Width="Auto" />
        <ColumnDefinition Width="Auto" />
    </Grid.ColumnDefinitions>
</Grid>
```

Det ger en layout som ser ut så här:

|   | Kolumn 1 | Kolumn 2 |
| :-: | :-: | :-: |
| **Rad 1** | A | B |
| **Rad 2** | C | D |

### Måttförklaring

Måtten kan anges på olika sätt:

| **Mått** | Förklaring                  |
| :------: | --------------------------- |
| **Fast** | Fast storlek i pixlar       |
| **Auto** | Ta upp bara nödvändig plats |
|  **\***  | Ta upp all plats            |

### Infoga element i Grid

Nu infogar vi en `Label` och en `TextBox` i `Grid`:en:

```xml
<Grid Margin="10">
    <Grid.RowDefinitions>
        <RowDefinition Height="Auto" />
        <RowDefinition Height="Auto" />
    </Grid.RowDefinitions>
    <Grid.ColumnDefinitions>
        <ColumnDefinition Width="Auto" />
        <ColumnDefinition Width="*" />
    </Grid.ColumnDefinitions>

    <Label Grid.Row="0" Grid.Column="0" >Ange epostadress</Label>
    <TextBox Grid.Row="0" Grid.Column="1" Margin="5" />
</Grid>
```

Vår layout ser nu ut så här:

|   | Kolumn 1 | Kolumn 2 |
| :-: | :-: | :-: |
| **Rad 1** | Label | TextBox |
| **Rad 2** |  |  |

Nu syns första raden:

![](../../.gitbook/assets/image-21%20copy.png)

Nu infogar vi en `Label` och en `TextBox` till i nästa raden:

```xml
<Grid Margin="10">
    <Grid.RowDefinitions>
        <RowDefinition Height="Auto" />
        <RowDefinition Height="Auto" />
    </Grid.RowDefinitions>
    <Grid.ColumnDefinitions>
        <ColumnDefinition Width="Auto" />
        <ColumnDefinition Width="*" />
    </Grid.ColumnDefinitions>

    <Label Grid.Row="0" Grid.Column="0" >Ange epostadress</Label>
    <TextBox Grid.Row="0" Grid.Column="1" Margin="5" />
    <Label Grid.Row="1" Grid.Column="0" >Ange texten</Label>
    <TextBox Grid.Row="1" Grid.Column="1" Margin="5" />
</Grid>
```

Vår layout ser nu ut så här:

|   | Kolumn 1 | Kolumn 2 |
| :-: | :-: | :-: |
| **Rad 1** | Label | TextBox |
| **Rad 2** | Label | TextBox |

Nu syns 2 rader, bra!

![](../../.gitbook/assets/image-22%20copy.png)

### Använda span

Vi behöver få rum med knappen underst. Vi skapar en rad till:

```xml
<Grid.RowDefinitions>
    <RowDefinition Height="Auto" />
    <RowDefinition Height="Auto" />
    <RowDefinition Height="Auto" />
</Grid.RowDefinitions>
```

Sen infogar vi en knapp som sträcker sig över 2 kolumner:

```xml
<Label Grid.Row="0" Grid.Column="0" >Ange epostadress</Label>
<TextBox Grid.Row="0" Grid.Column="1" Margin="5" />
<Label Grid.Row="1" Grid.Column="0" >Ange texten</Label>
<TextBox Grid.Row="1" Grid.Column="1" Margin="5" />
<Button Grid.Row="1" Grid.ColumnSpan="2" Margin="5" Height="30">Skicka!</Button>
```

Layouten ser nu ut så här:

|   | Kolumn 1 | Kolumn 2 |
| :-: | :-: | :-: |
| **Rad 1** | Label | TextBox |
| **Rad 2** | Label | TextBox |
| **Rad 3** | Button | ~~ |

Så där ja:

![](../../.gitbook/assets/image-23%20copy.png)

### Ruta att redigera text i

Vi gör meddelanderutan större så att det går att skriva flera rader text i:

```xml
<TextBox Grid.Row="2" Grid.Column="1" Margin="5" 
TextWrapping="Wrap" AcceptsReturn="True" Name="txbTexten" />
```

För att göra `TextBox` responsiv lägger vi till `MinHeight="90"` och ändrar i `RowDefinition`:

```xml
<Grid.RowDefinitions>
  <RowDefinition Height="Auto" /> 
  <RowDefinition Height="Auto" /> 
  <RowDefinition Height="*" /> 
  <RowDefinition Height="Auto" /> 
  <RowDefinition Height="Auto" /> 
</Grid.RowDefinitions>
<Grid.ColumnDefinitions>
   <ColumnDefinition Width="Auto" />
   <ColumnDefinition Width="*" />
</Grid.ColumnDefinitions>
```

### Rubrik med bild

På samma sätt infogar vi nu rubriken överst.

Vi infogar en bild. Ladda ned en png-bild på 32px från:

{% embed url="https://www.flaticon.com/search?type=icon&word=mailchimp" %}

Bilden måste också infogas i projektets proj-fil:

```xml
<Project Sdk="Microsoft.NET.Sdk">
  <PropertyGroup>
    <OutputType>WinExe</OutputType>
    <TargetFramework>net5.0-windows</TargetFramework>
    <UseWPF>true</UseWPF>
  </PropertyGroup>
  <ItemGroup>
    <Resource Include="mailchimp-brands.png"/>
  </ItemGroup>
</Project>
```

Vi infogar en ny rad överst. \
Till vänster `Image` för bilden och till höger `Label`, vilket flyttar ned övriga element ett snäpp:

```xml
<Image Grid.Row="0" Grid.Column="0" Source="mailchimp-brands.png" Height="32" />
<Label Grid.Row="0" Grid.Column="1" Margin="5" FontSize="24">Meddelanden</Label>
```

Efter några justering med `Height` blir det bra:

![](../../.gitbook/assets/image-27%20copy.png)

## Utmaningar

* Vi lägger nu till en `TextBox` med `ReadOnly="True"` och `Background="#EEE"`
* Fixa så att knappen Skicka funkar
* Varna om textfälten är tomma
* Varna om epost inte följer korrekt format se [regex](https://uibakery.io/regex-library/email-regex-csharp)

![](../../.gitbook/assets/image-28%20copy.png)

## Mer info

* Läs mer om `Grid` [https://wpf-tutorial.com/panels/grid](https://wpf-tutorial.com/panels/grid/)
