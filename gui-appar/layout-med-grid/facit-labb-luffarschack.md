# Facit för labb Luffarschack

Här är en möjlig lösning på labb Luffarschack.

```csharp
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Luffarschack
{
    public partial class MainWindow : Window
    {
        // Variabel för att hålla reda på vems tur det är
        private bool spelare1Tur = true;

        // En lista av listor för att hålla koll på knapparna
        private List<List<Button>> bräde;

        // Storleken på brädet (3x3)
        private int storlek = 3;

        // Hur många i rad som krävs för att vinna
        private int vinstKrav = 3;

        public MainWindow()
        {
            InitializeComponent();
            // Initiera brädet när fönstret skapas
            InitieraBräde();
        }

        // Metod för att initiera brädet
        private void InitieraBräde()
        {
            // Skapa en ny lista av listor för knapparna
            bräde = new List<List<Button>>();

            // Rensa tidigare knappar från Grid
            spelplan.Children.Clear();

            // Skapa rader och kolumner i Grid
            spelplan.RowDefinitions.Clear();
            spelplan.ColumnDefinitions.Clear();
            for (int i = 0; i < storlek; i++)
            {
                spelplan.RowDefinitions.Add(new RowDefinition());
                spelplan.ColumnDefinitions.Add(new ColumnDefinition());
            }

            // Skapa knapparna och lägg till dem i Grid
            for (int rad = 0; rad < storlek; rad++)
            {
                var radLista = new List<Button>();
                for (int kol = 0; kol < storlek; kol++)
                {
                    Button knapp = new Button
                    {
                        FontSize = 90 / storlek, // Anpassa typsnittets storlek efter brädets storlek
                        Background = Brushes.LightSalmon // Sätt bakgrundsfärg
                    };
                    knapp.Click += KlickKnapp; // Koppla Click-eventet till KlickKnapp-metoden
                    Grid.SetRow(knapp, rad); // Sätt knappens radposition
                    Grid.SetColumn(knapp, kol); // Sätt knappens kolumnposition
                    spelplan.Children.Add(knapp); // Lägg till knappen i Grid
                    radLista.Add(knapp); // Lagra knappen i listan
                }
                bräde.Add(radLista); // Lägg till raden i brädet
            }
        }

        // Metod som körs när en knapp klickas
        private void KlickKnapp(object sender, RoutedEventArgs e)
        {
            Button knapp = (Button)sender;
            knapp.IsEnabled = false; // Inaktivera knappen efter att den har klickats
            knapp.Content = spelare1Tur ? "X" : "O"; // Sätt knappens innehåll till "X" eller "O" beroende på vems tur det är

            // Kontrollera om någon har vunnit
            if (KontrolleraVinst())
            {
                MessageBox.Show($"Spelare {(spelare1Tur ? 1 : 2)} vinner!"); // Visa meddelande om någon har vunnit
                Reset(); // Återställ spelet
                return;
            }

            // Växla tur
            spelare1Tur = !spelare1Tur;
        }

        // Metod för att kontrollera om någon har vunnit
        private bool KontrolleraVinst()
        {
            // Kontrollera rader och kolumner
            for (int i = 0; i < storlek; i++)
            {
                if (KontrolleraRad(i) || KontrolleraKolumn(i))
                    return true;
            }
            // Kontrollera diagonaler
            return KontrolleraDiagonaler();
        }

        // Metod för att kontrollera en rad
        private bool KontrolleraRad(int rad)
        {
            string första = bräde[rad][0].Content?.ToString();
            if (string.IsNullOrEmpty(första)) return false;

            for (int kol = 1; kol < storlek; kol++)
            {
                if (bräde[rad][kol].Content?.ToString() != första)
                    return false;
            }
            return true;
        }

        // Metod för att kontrollera en kolumn
        private bool KontrolleraKolumn(int kol)
        {
            string första = bräde[0][kol].Content?.ToString();
            if (string.IsNullOrEmpty(första)) return false;

            for (int rad = 1; rad < storlek; rad++)
            {
                if (bräde[rad][kol].Content?.ToString() != första)
                    return false;
            }
            return true;
        }

        // Metod för att kontrollera diagonaler
        private bool KontrolleraDiagonaler()
        {
            // Kontrollera huvuddiagonalen
            string första = bräde[0][0].Content?.ToString();
            if (!string.IsNullOrEmpty(första))
            {
                bool vinst = true;
                for (int i = 1; i < storlek; i++)
                {
                    if (bräde[i][i].Content?.ToString() != första)
                    {
                        vinst = false;
                        break;
                    }
                }
                if (vinst) return true;
            }

            // Kontrollera anti-diagonalen
            första = bräde[0][storlek - 1].Content?.ToString();
            if (!string.IsNullOrEmpty(första))
            {
                for (int i = 1; i < storlek; i++)
                {
                    if (bräde[i][storlek - 1 - i].Content?.ToString() != första)
                        return false;
                }
                return true;
            }

            return false;
        }

        // Metod som körs när "Starta" knappen klickas
        private void KlickStarta(object sender, RoutedEventArgs e)
        {
            Reset();
        }

        // Metod för att återställa spelet
        private void Reset()
        {
            spelare1Tur = true; // Återställ turen till spelare 1
            InitieraBräde(); // Initiera brädet på nytt
        }
    }
}
```