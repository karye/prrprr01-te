---
description: Hur man skapar en Windows-gui med WPF
---

# Intro till WPF

![](../.gitbook/assets/image-68%20copy.png)

## WPF

[WPF](https://docs.microsoft.com/en-us/dotnet/desktop/wpf/overview/) står för Windows Presentation Foundation.  
Det är ett ramverk för att skapa grafiska användargränssnitt (GUI) i Windows. Det är ett ramverk som är skapat av Microsoft och som är en del av .NET Core. 

### Beskrivning

WPF består av två delar:

* XAML - ett språk för att beskriva hur en grafisk användargränssnitt ser ut
* C# - ett ramverk för att skapa logik för en grafisk användargränssnitt

## VS Code

### Tillägg

För att göra det lättare att skriva XAML i VS Code installera följande tillägg:

* [XML av Red Hat](https://marketplace.visualstudio.com/items?itemName=redhat.vscode-xml)

## VS Code inställningar

* Slå **Ctrl + Skift + P** och **Open User Settings (JSON)**
* Klistra in följande:

```JSON
,
"files.associations": {
    "*.xaml": "xml"
},
"xml.fileAssociations": [
    {
        "pattern": "**/*.xaml",
        "systemId": "https://raw.githubusercontent.com/karye/xamelot/refs/heads/master/syntax/xaml.xsd"
    }
],
```

### VS Code "snippet"

* Slå **Ctrl + Skift + P** och **Snippets: Configure Snippets**
* Välj **New Global Snippets File ...**
* Klistra in följande:

```JSON
{
    "XAML event": {
		"prefix": "event",
		"body": [
			"private void $1(object sender, RoutedEventArgs e)",
			"{",
			"\t$2",
			"}"
		],
		"description": "Metod för att fånga event från XAML"
	}
}
```

### Resurser för att lära sig WPF

* [Tutorial: Create a new WPF app with .NET](https://learn.microsoft.com/sv-se/dotnet/desktop/wpf/get-started/create-app-visual-studio?view=netdesktop-8.0)

{% embed url="https://youtu.be/t9ivUosw_iI?si=6LOzWZ_QknDKtHgZ" %}

## Andra ramverk

Det finns flera andra ramverk för att skapa grafiska användargränssnitt i Windows. Det finns till exempel:

* [WinForms](https://docs.microsoft.com/en-us/dotnet/desktop/winforms/?view=netdesktop-5.0) - ett äldre ramverk av Microsoft **Windows** och som är en del av .NET Framework
* [Avalonia](https://avaloniaui.net/platforms) - ett cross-platform ramverk för att skapa grafiska användargränssnitt i **Windows**, **macOS** och **Linux**
* [Uno](https://platform.uno/) - ett cross-platform ramverk för att skapa grafiska användargränssnitt i **Windows**, **macOS**, **Linux**, Android och iOS
* [Flutter](https://flutter.dev/) - ett ramverk av Google för **Android**, **iOS**, **Windows**, **macOS** och **Linux**
* [AppKit](https://developer.apple.com/documentation/appkit) - ett ramverk för att skapa grafiska användargränssnitt i **macOS**
* [GTK](https://www.gtk.org/) - ett ramverk för att skapa grafiska användargränssnitt i **Linux**
* [Qt](https://www.qt.io/) - liksom Gtk är QT ett ramverk för att skapa grafiska användargränssnitt i **Linux**
* [Electron](https://www.electronjs.org/) - ett ramverk för att skapa cross-platform grafiska användargränssnitt i **Windows**, **macOS** och **Linux** med hjälp av JavaScript, HTML och CSS