#!/bin/bash

# This script is used to publish the site to the web server.

# Run the python script to generate the site
python3 gitbook2mkdocs.py

# Run mkdocs to build the static site
mkdocs build -d /var/www/prog1.smutje.se

# Remove link to the git repo
rm docs/.git

