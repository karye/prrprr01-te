# Del 4: Skapa flera fiender och poängsystem

## Steg 1: Skapa en fiende prefab

`enemyPrefab` är en referens till den fiende vi vill skapa flera instanser av. I Unity är en `Prefab` en typ av komponent som kan skapas, konfigureras och lagras i redigeraren, och sedan kan användas för att skapa nya instanser med samma konfiguration i spelet vid runtime. 

För att skapa `enemyPrefab` kan du följa dessa steg:

1. Skapa en fiendeobjekt i Unity-redigeraren. Detta kan vara så enkelt som ett 2D-objekt med en sprite, men du kan också lägga till komponenter som `Rigidbody2D` och `BoxCollider2D`, och skript som `EnemyController` som vi har diskuterat tidigare.

2. Konfigurera fiendeobjektet som du vill. Du kan justera dess sprite, storlek, position, etc.

3. Dra och släpp detta objekt från **hierarchy**-vyn till **project**-vyn. Det kommer att skapa en `Prefab` av objektet. 

4. Nu kan du radera originalobjektet från scenen. Vi kommer att skapa nya instanser av det vid runtime från `Prefab`-objektet.

5. I ditt skript, deklarera en `public` variabel av typen `GameObject` för att lagra referensen till fiende `Prefab`:

```csharp
public GameObject enemyPrefab;
```

6. I Unity-redigeraren, dra och släpp prefaben från projektvyn till det tomma fältet i inspektorvyn som motsvarar `enemyPrefab` variabeln i ditt skript.

Nu när `enemyPrefab` är satt, kan du använda `Instantiate(enemyPrefab)` i ditt skript för att skapa nya instanser av fienden vid runtime.

Observera att eftersom `enemyPrefab` är deklarerad som `public`, kan den sättas direkt i Unity-redigeraren genom att dra och släppa objektet från projekt-vyn till det tomma fältet i inspektorvyn.

## Steg 2: Skapa flera fiender

Nu när vi har en lista för att hålla koll på alla våra fiender, kan vi skapa flera fiender när spelet startar. För att göra detta behöver vi en referens till fiendens prefab, vilket är en mall för hur en fiende ska se ut och bete sig. Med denna prefab kan vi skapa flera kopior av fienden.

Skapa ett nytt spelobjekt i Unity-redigeraren och ge det namnet **EnemyGenerator**. Lägg till en ny komponent av typen **EnemyGenerator** på spelobjektet. Öppna **EnemyGenerator.cs** och lägg till en ny statisk lista av **EnemyController** på toppen av klassen.

![](../../.gitbook/assets/image-86.png)

```csharp
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGenerator : MonoBehaviour
{
    // Länka till fiende prefab
    public GameObject enemyPrefab;

    // Start is called before the first frame update
    void Start()
    {
        // Skapa 5 fiender
        for (int i = 0; i < 5; i++)
        {
            Instantiate(enemyPrefab);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
```

Funktionen `Instantiate` används för att skapa en kopia av det angivna GameObject-objektet. I detta fall kommer vi att skapa kopior av vår fiende prefab.

## Steg 3: Skapa ett poängsystem

För att skapa ett poängsystem behöver vi hålla koll på spelarens poäng och visa det på skärmen. Detta kan göras genom att använda Unitys UI-system. 

Först, skapa en ny UI-Text GameObject genom att gå till **GameObject** -> **UI** -> **Text**. Döp det till **ScoreText**.

![](../../.gitbook/assets/image-90.png)

![](../../.gitbook/assets/image-87.png)

Lägg till följande kod i ditt **GameManager** skript för att hantera poäng:

```csharp
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public Text scoreText;
    public static int score = 0;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        scoreText.text = $"Score: {score}";
        
    }
}
```

Glöm inte att dra din **ScoreText** GameObject till **scoreText** fältet i inspektören.

![](../../.gitbook/assets/image-89.png)

Slutligen, för att öka poängen när en fiende besegras, lägg till följande kod i din **EnemyController** klass:

```csharp
void OnDestroy()
{
    // Öka poängen med 1
    GameManager.score++;
}
```

Nu bör ditt spel ha flera fiender och ett fungerande poängsystem! Varje gång en fiende besegras kommer din poäng att öka med ett och uppdateras på skärmen.

## Uppgifter

### Uppgift 1: Ändra antalet fiender
Ändra antalet fiender som skapas vid starten av spelet. Prova olika värden och se hur det påverkar spelet. Varför händer det som händer när du ändrar antalet?

### Uppgift 2: Ändra startpositionen för fiender
För närvarande skapas alla fiender på samma position eftersom vi inte anger någon specifik position när vi kallar på `Instantiate`. Ändra detta genom att ge varje fiende en slumpmässig startposition. Använd `Random.Range` för att välja ett slumpmässigt x-värde för varje fiendes startposition.

### Uppgift 3: Lägg till poäng vid samling av power-ups
För tillfället ökar poängen bara när en fiende besegras. Lägg till ett power-up objekt som kan samlas in av spelaren för att få extra poäng. Du kommer att behöva skapa ett nytt prefab för power-up och ett nytt tag för att identifiera power-ups i kollisioner.

### Uppgift 4: Visa antal kvarvarande fiender
Förutom att visa spelarens poäng, lägg till en annan UI Text för att visa antalet kvarvarande fiender. Du kan hitta detta antal genom att använda `Count`-egenskapen på listan av fiender: `EnemyController.enemies.Count`.

### Uppgift 5: Reset poäng när spelet börjar om
Just nu, om du stoppar och sedan kör om spelet igen kommer poängen fortsätta där den slutade i föregående omgång. För att lösa detta, se till att poängen återställs till 0 när spelet börjar om. Fundera över var denna kod borde placeras.