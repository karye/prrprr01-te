# Del 3: Kollisioner mellan spelare och fiender

Nästa steg i att utveckla vårt spel är att lägga till kollisioner mellan spelaren och fienderna. För att göra det kommer vi att använda Unitys inbyggda fysiksystem.

## Steg 1: Lägg till en "Box Collider 2D"

För att kunna upptäcka kollisioner behöver vi lägga till en kollisionsdetektor, eller **collider**, till både spelaren och fienden. Eftersom vi arbetar med 2D-objekt, kommer vi att använda en **Box Collider 2D**.

Välj `Player`-objektet i hierarkin, klicka på **Add Component** i inspektorn och sök efter **Box Collider 2D**. Klicka på den för att lägga till komponenten till ditt objekt.

![](../../.gitbook/assets/image-89.png)

Gör detsamma för `Enemy`-objektet.

**Box Collider 2D** är en rektangulär kollider som du kan justera för att passa din bild. Kollidern är det område som kommer att upptäcka när det kommer i kontakt med en annan kollider.

## Steg 2: Lägg till en **Rigidbody 2D** på spelaren

För att fysikmotorn ska kunna interagera med ett objekt behöver det ha en **Rigidbody**-komponent. Vi har redan lagt till en **Rigidbody2D** till `Enemy`-objektet, så nu behöver vi lägga till en till `Player`-objektet.

Välj `Player`-objektet i hierarkin, klicka på **Add Component** i inspektorn och sök efter **Rigidbody 2D**. Klicka på den för att lägga till komponenten till ditt objekt.

![](../../.gitbook/assets/image-90.png)

I inställningarna för **Rigidbody2D**-komponenten, ställ in **Gravity Scale** till 0. Detta hindrar spelaren från att falla neråt på grund av gravitationen.

## Steg 3: Lägg till kollisionsdetektion till **PlayerController**-skriptet

Nu när vi har lagt till **Rigidbody2D** och **Box Collider 2D** till våra objekt, kan vi börja upptäcka kollisioner i vårt skript. För detta kommer vi att använda metoden **OnCollisionEnter2D**.

Öppna **PlayerController**-skriptet och lägg till följande metod:

```csharp
void OnCollisionEnter2D(Collision2D collision)
{
    // Kontrollera om vi kolliderade med en fiende
    if (collision.gameObject.CompareTag("Enemy"))
    {
        // Vi kolliderade med en fiende, så vi gör något här
        Debug.Log("Player hit an enemy!");
    }
}
```

Förklaring:

- `OnCollisionEnter2D` är en speciell metod i Unity som körs när kollidern på samma GameObject som detta skript kommer i kontakt med en annan kollider.
- `Collision2D`-objektet som skickas till metoden innehåller information om kollisionen, inklusive vilket GameObject det kolliderade med.
- `gameObject.CompareTag("Enemy")` kollar om GameObject vi kolliderade med har taggen `Enemy`. Vi kan sätta denna tagg i Unitys inspektor.
- `Debug.Log` skriver ut ett meddelande till konsolen. Just nu använder vi det bara för att testa, men senare kan vi byta ut det mot kod som hanterar vad som händer när spelaren kolliderar med en fiende.

Spara skriptet och gå tillbaka till Unity. Kör spelet och flytta spelaren till fienden. När spelaren kolliderar med fienden, kommer du att se ett meddelande i konsolen.

Taggen `Enemy` är en identifierare som du kan sätta på dina GameObjects i Unity för att enkelt kunna skilja mellan olika typer av objekt. För att ställa in en tagg gör du följande:

1. Välj `Enemy`-objektet i hierarkin.
2. I inspektorn, klicka på **Tag** dropdown-menyn.
3. Välj **Add Tag...**.
4. Klicka på **+**-knappen för att lägga till en ny tagg.
5. Skriv `Enemy` i textfältet som visas och klicka på **Save**.
6. Nu kan du välja `Enemy`-taggen från dropdown-menyn för alla dina fiendeobjekt.

![](../../.gitbook/assets/image-91.png)

När `OnCollisionEnter2D` metoden upptäcker en kollision mellan spelaren och en fiende (dvs. ett objekt med taggen `Enemy`), kommer den just nu bara skriva ut ett meddelande i konsolen. För att göra spelet mer interaktivt, kan du ändra detta beteende. Här är några exempel:

- Du kan minska spelarens hälsa.
- Du kan förstöra fienden.
- Du kan stänga av kontrollerna för en kort stund.

Hur du väljer att hantera kollisionen beror på vilken typ av spel du skapar. För enkelhetens skull kan vi börja med att bara förstöra fienden när den kolliderar med spelaren. För att göra det, ändra din `OnCollisionEnter2D` metod till följande:

```csharp
void OnCollisionEnter2D(Collision2D collision)
{
    // Kontrollera om vi kolliderade med en fiende
    if (collision.gameObject.CompareTag("Enemy"))
    {
        // Förstör fienden
        Destroy(collision.gameObject);
    }
}
```

`Destroy`-metoden förstör det GameObject som skickas till den. I detta fall skickar vi fienden, så den kommer att försvinna från scenen när den kolliderar med spelaren.

## Uppgifter

### Uppgift 1: Ändra beteendet vid kollision
För närvarande, när spelaren kolliderar med en fiende, förstörs fienden. Testa olika beteenden vid kollision. Till exempel:

- Gör så att spelaren studsar tillbaka när den träffar en fiende.
- Lägg till en kort fördröjning efter kollisionen innan fienden försvinner.
- Låt fienden byta riktning istället för att försvinna.

### Uppgift 2: Lägg till fler taggar
För tillfället använder vi bara `Enemy`-taggen, men du kan lägga till fler taggar för att identifiera olika typer av objekt. Till exempel:

- Skapa en ny `Powerup`-tagg och lägg till ett nytt objekt med den taggen.
- Ändra kollisionskoden så att något annat händer när spelaren träffar en `Powerup`.

### Uppgift 3: Lägg till ljud
Unity har inbyggda funktioner för att spela upp ljud. Testa att lägga till ett ljud som spelas upp när spelaren kolliderar med en fiende. Du kan använda `AudioSource.PlayClipAtPoint()` metoden för att spela ett ljud på en specifik position.

### Uppgift 4: Ändra utseendet på fienden när den kolliderar
För att göra kollisionen mer visuellt tydlig kan du ändra utseendet på fienden när den kolliderar med spelaren. Till exempel:

- Ändra färgen på fienden när den kolliderar.
- Ändra storleken på fienden när den kolliderar.
- Lägg till en animerad sprite som spelar när en kollision inträffar.

### Uppgift 5: Använd OnCollisionExit2D
Det finns också en metod som kallas `OnCollisionExit2D` som kallas när två objekt slutar att kollidera. Testa att använda denna metod för att ändra beteendet när spelaren slutar att kollidera med en fiende. Till exempel:

- Ändra tillbaka fiendens färg till original när kollisionen är över.
- Låt spelaren få tillbaka kontrollen efter en viss tid efter kollisionen.
- Lägg till en cooldown-timer så att spelaren är immun mot nya kollisioner för en kort tid efter en kollision.