# Del 5: Skapa skott och skjuta

## Steg 1: Skapa ett skott-GameObject

Precis som vi tidigare skapade en spelare och en fiende i Unity, börjar vi med att skapa ett nytt GameObject för skottet/kulan. Du kan göra detta genom att gå till menyn `GameObject > Create Empty`. Namnge det nya GameObject till något som "Bullet".

Nu vill vi ge vår kula en form. Detta kan göras genom att lägga till en komponent som kallas `Sprite Renderer` och välj en passande bild (sprite). Om du inte har någon passande bild kan du ladda ner en från internet eller skapa en enkel egen i ett program som Photoshop eller GIMP.

Vi kommer också att behöva lägga till en `Rigidbody2D` komponent till vårt skott GameObject, eftersom det kommer att behöva interagera med Unitys fysiksystem. Eftersom vi vill att skottet ska röra sig uppåt snarare än nedåt (som fienden gör), ställ in "Gravity Scale" till 0 för att förhindra att skottet faller nedåt på grund av gravitation.

Slutligen kommer vi att behöva lägga till en `Box Collider 2D` eller `Circle Collider 2D` komponent till vårt skott GameObject. Detta gör att skottet kan upptäcka kollisioner med andra GameObjects.

## Steg 2: Skapa ett BulletController skript

Nu när vi har ett skott GameObject, behöver vi ett skript för att kontrollera dess beteende. Skapa ett nytt C# skript, namnge det "BulletController" och öppna det i din kodredigerare.

Vi behöver göra två saker i detta skript. För det första vill vi att skottet ska flytta sig uppåt över tiden. För det andra vill vi att skottet ska förstöras när det når toppen av skärmen (för att undvika att ha för många skott GameObjects i spelet samtidigt).

Här är ett exempel på hur "BulletController" skriptet kan se ut:

```csharp
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour
{
    // Hastigheten som kulan ska röra sig med
    public float speed = 5.0f;

    void Update()
    {
        // Flytta kulan uppåt
        transform.Translate(Vector2.up * speed * Time.deltaTime);

        // Om kulan är utanför skärmen, förstör den
        if (transform.position.y > 6.0f)
        {
            Destroy(gameObject);
        }
    }
}
```

I detta skript används `transform.Translate` för att flytta kulan uppåt över tiden. `Vector2.up` representerar en vektor som pekar uppåt, `speed` är hastigheten vi vill att kulan ska röra sig med, och `Time.deltaTime` är den tid som har gått sedan förra ramen, vilket gör att rörelsen blir mjuk.

If-satsen `if (transform.position.y > 6.0f)` kontrollerar om kulans y-position är större än 6 (vilket skulle innebära att den är utanför skärmen, om vi antar att skärmen höjd är mindre än 6 enheter). Om detta är fallet, kallas `Destroy(gameObject)`, vilket förstör kulan GameObject.

Nästa steg kommer att vara att lyssna på input från spelaren för att skapa en ny kula GameObject när spelaren trycker på en specifik knapp, till exempel mellanslagstangenten. Men låt oss pausa här för nu och låta dig experimentera med det vi har täckt så här långt.

## Steg 3: Kollisioner

Vi börjar med att lägga till en `Collider` till vår fiende. För detta öppnar du Unity editor och väljer din enemy prefab. I inspektorfönstret, klicka på "Add Component"-knappen och skriv "Box Collider 2D" i sökrutan, och välj den. Nu har din fiende en `BoxCollider2D`-komponent som gör att den kan detektera när det kolliderar med andra objekt.

Vi behöver också lägga till en `Collider` till våra skott. Följ samma steg som ovan, men välj din bullet prefab istället.

Nu behöver vi koda vad som händer när en kollision inträffar mellan en fiende och ett skott. För detta, öppna ditt `EnemyController`-skript.

Lägg till följande kod inuti klassen:

```csharp
void OnCollisionEnter2D(Collision2D collision)
{
    // Om objektet som denna fiende kolliderar med har taggen "Bullet"
    if (collision.gameObject.tag == "Bullet")
    {
        // Koden här kommer att köras när en fiende kolliderar med ett skott
    }
}
```

`OnCollisionEnter2D` är en metod som automatiskt kallas av Unity när objektet detta skript är kopplat till kolliderar med ett annat objekt. `Collision2D`-parametern innehåller information om kollisionen, inklusive vilket objekt som detta objekt kolliderade med.

I den här koden kontrollerar vi om objektet som fienden kolliderade med har taggen "Bullet". Om det har det, vet vi att en fiende har träffats av ett skott.

Nu kan vi bestämma vad som ska hända när en fiende kolliderar med ett skott. En typisk åtgärd i många spel skulle vara att "förstöra" både fienden och skottet. Vi kan göra detta med hjälp av `Destroy`-metoden i Unity. Modifiera `OnCollisionEnter2D`-metoden som följer:

```csharp
void OnCollisionEnter2D(Collision2D collision)
{
    // Om objektet som denna fiende kolliderar med har taggen "Bullet"
    if (collision.gameObject.tag == "Bullet")
    {
        // Förstör både fienden och skottet
        Destroy(collision.gameObject);
        Destroy(gameObject);
    }
}
```

Nu, när du kör ditt spel, borde fienden försvinna när den träffas av ett skott, och skottet borde också försvinna.

Observera att för att detta ska fungera måste du sätta taggen "Bullet" på din bullet prefab i Unity editorn. Du kan göra detta genom att välja bullet prefab i Unity, gå till toppen av inspektorfönstret, klicka på tag dropdown-menyn, välja "Add Tag", klicka på "+"-knappen för att lägga till en ny tagg, skriv "Bullet" i namnfältet och klicka på "Save". Sen gå tillbaka till bullet prefab, klicka på tag dropdown-menyn igen, och välj den nya "Bullet"-taggen.

## Uppgifter

### Uppgift 1
Modifiera hastigheten på skotten. Du kan göra detta genom att ändra värdet på `speed` variabeln i `BulletController`-skriptet. Testa olika värden för att se hur det påverkar spelets känsla.

### Uppgift 2
För tillfället förstörs både fienden och skottet vid kollision. Testa att ändra så att endast skottet förstörs och fienden fortsätter vara intakt. För att göra det, radera `Destroy(gameObject);` raden i `OnCollisionEnter2D` metoden i `EnemyController` skriptet.

### Uppgift 3
Variera storleken på fiendens och skottets kollisionsboxar. För att göra det, gå till Unity-editorn, välj fienden eller bullet prefab, och i inspektorn, justera värdena för "Size" under "Box Collider 2D"-komponenten. Observera hur detta påverkar när kollisioner inträffar.

### Uppgift 4
Just nu startar spelet med att fienden genast börjar röra sig. Testa att ändra så att fienden börjar stå stilla och bara börjar röra sig när spelaren skjuter sitt första skott. För att göra det, kan du lägga till en boolean variabel till `EnemyController` skriptet som håller reda på om spelaren har skjutit än. I början av spelet är denna variabel `false`. I `Update`-metoden, om denna variabel är `false`, rör sig inte fienden. Men när `OnCollisionEnter2D`-metoden kallas, sätt denna variabel till `true`.

### Uppgift 5
Lägg till en funktion så att fienden kan röra sig i sidled, inte bara vertikalt. För att göra det, lägg till en `horizontalSpeed` variabel till `EnemyController` skriptet och ändra `Update`-metoden att också uppdatera fiendens x-position baserat på `horizontalSpeed`.

Kom ihåg att spela ditt spel efter varje ändring för att se hur det påverkar spelupplevelsen!