# Del 1: Skapa en spelare

## Steg 1: Skapa en GameObject för spelaren

Börja med att skapa en ny `GameObject` som representerar spelaren. Gå till menyn **GameObject -> Create Empty**. Döp detta nya objekt till `Player`. Ett GameObject i Unity är en grundläggande enhet i din spelvärld, ett objekt som kan innehålla olika komponenter som ger det beteenden och egenskaper.

![](../../.gitbook/assets/image-82.png)

## Steg 2: Ge form till spelaren

Nästa steg är att ge en form till din spelare. För att göra detta behöver vi lägga till en **Sprite Renderer** till vårt `Player` GameObject. En Sprite Renderer gör det möjligt för en bild, en **Sprite**, att visas i 2D-spelvärlden.

Välj `Player` objektet, klicka på **Add Component** i Inspector fönstret och välj **Sprite Renderer**. Nu kan du klicka på **Sprite** fältet i **Sprite Renderer** komponenten och välj en rektangulär sprite.

![](../../.gitbook/assets/image-83.png)

Välj nu en bild som du vill använda som spelare. Du kan använda en av de bilder som finns i mappen **Assets** -> **Sprites** eller importera en egen bild. För att importera en egen bild, högerklicka i **Project** fönstret och välj **Import New Asset**. Välj en bild från din dator och klicka på **Import**.

![](../../.gitbook/assets/image-84.png)

## Steg 3: Skapa ett skript för spelkontroll

Nu behöver vi ett sätt att styra spelaren. Detta gör vi genom att skriva ett skript. Skapa ett nytt C#-skript genom att högerklicka i **Project** fönstret, välj **Create** -> **C# Script** och döp det till `PlayerController`. Dubbelklicka på skriptet för att öppna det i kodredigeraren.

När du skapar ett nytt C#-skript i Unity kommer det automatiskt med några rader kod:

```csharp
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
```

Här är vad varje del betyder:

- `using System.Collections;` och `using System.Collections.Generic;` är standardbibliotek i C# för att hantera olika typer av datastrukturer.

- `using UnityEngine;` ger oss tillgång till Unitys API (Application Programming Interface), vilket gör att vi kan använda de inbyggda klasserna och metoderna i Unity.

- `public class PlayerController : MonoBehaviour` här deklarerar vi vår klass, `PlayerController`. När vi säger `: MonoBehaviour` menar vi att vår klass ärver från MonoBehaviour-klassen, en bas klass i Unity som alla skript måste ärva från för att kunna fästas på ett GameObject.

- `void Start()` och `void Update()` är funktioner som är en del av varje MonoBehaviour. `Start` körs en gång, precis när spelet startar, eller mer specifikt, när scriptet aktiveras. `Update` körs en gång per bildruta, vilket betyder att det körs kontinuerligt och ofta.

## Steg 4: Lyssna på spelarinput

Nästa steg är att skriva kod som lyssnar på spelarinput. Vi börjar med att kontrollera rörelsen längs X-axeln (vänster och höger). För detta behöver vi skapa en variabel `horizontalInput` av typen `float` i vår klass. En `float` är en decimalvariabel, som kan hålla både hela och bråktal. Lägg till följande kod i `Update`-metoden:

```csharp
void Update()
{
    // Läs av spelarinput från tangentbordets V/H-piltangenter eller A/D-tangenterna
    float horizontalInput = Input.GetAxis("Horizontal");
}
```

`Input.GetAxis("Horizontal")` är en inbyggd Unity-funktion som returnerar ett värde mellan -1 och 1 beroende på om vänster eller höger piltangent (eller A/D-tangenten) hålls nedtryckt. Om ingen tangent trycks ner, är värdet 0.

## Steg 5: Flytta spelaren

Nu när vi har kontroll över vårt horizontalInput, är det dags att faktiskt flytta spelaren med hjälp av detta värde. För att göra det kommer vi att använda `transform.Translate()`. `Transform` är en inbyggd klass i Unity som representerar ett objekts position, rotation och skala i spelvärlden. 

`Translate` är en metod på `Transform`-klassen som flyttar objektet baserat på de angivna parametrarna. Vi kommer att använda vår `horizontalInput` för att flytta spelaren i x-axeln. 

För att göra rörelsen mjukare, multiplicerar vi vårt inputvärde med Time.deltaTime. `Time.deltaTime` är tiden i sekunder sedan förra bildrutan renderades, vilket kan variera beroende på datorns prestanda. Genom att multiplicera vår rörelse med denna tidsskillnad, kan vi säkerställa att rörelsen blir jämn oavsett bildfrekvens.

Lägg till följande kod i `Update`-metoden:

```csharp
void Update()
{
    // Läs av spelarinput från tangentbordets V/H-piltangenter eller A/D-tangenterna
    float horizontalInput = Input.GetAxis("Horizontal");

    // Skapa en förflyttningen baserat på spelarinput
    Vector2 movement = new Vector2(horizontalInput, 0);

    // Flytta spelaren
    transform.Translate(movement * Time.deltaTime);
}
```

`Vector2` är en klass i Unity som representerar ett par av `float`-värden, ofta används för att representera 2D-koordinater eller vektorer. 

## Steg 6: Lägga till hastighet

Just nu kommer spelaren att röra sig väldigt långsamt eftersom värdet av `horizontalInput` är väldigt lågt (-1, 0 eller 1). Vi kan få spelaren att röra sig snabbare genom att införa en hastighetsvariabel. Lägg till följande kod i början av klassen:

```csharp
private float speed = 5.0f;
```

Och ändra sedan din `Translate`-kod till följande:

```csharp
transform.Translate(movement * speed * Time.deltaTime);
```

Detta kommer att multiplicera spelarens rörelse med hastighetsvärdet, vilket får spelaren att röra sig snabbare.

## Steg 7: Lägg till vertikal rörelse

För att styra spelaren vertikalt (upp och ner), kan vi lägga till en liknande kod för vertikal input och rörelse. 

```csharp
void Update()
{
    // Läs av spelarinput från tangentbordets V/H-piltangenter eller A/D-tangenterna
    float horizontalInput = Input.GetAxis("Horizontal");

    // Läs av spelarinput från tangentbordets U/N-piltangenter eller W/S-tangenterna
    float verticalInput = Input.GetAxis("Vertical");

    // Skapa en förflyttningen baserat på spelarinput
    Vector2 movement = new Vector2(horizontalInput, verticalInput);

    // Flytta spelaren
    transform.Translate(movement * speed * Time.deltaTime);
}
```

`Input.GetAxis("Vertical")` fungerar precis som `Input.GetAxis("Horizontal")`, men den returnerar -1, 0 eller 1 baserat på om upp- eller nerpiltangenten (eller W/S-tangenten) hålls nedtryckt. 

Nu kan du styra spelaren både horisontellt och vertikalt med tangentbordet.

![](../../.gitbook/assets/image-86.png)

## Steg 8: Begränsa spelarens rörelse

Just nu kan spelaren röra sig utanför skärmen, vilket vi vill undvika. Vi kan lösa detta genom att begränsa spelarens position med hjälp av `Mathf.Clamp`-metoden. 

`Mathf.Clamp(value, min, max)` är en inbyggd Unity-metod som begränsar ett värde till ett specificerat intervall. Vi kan använda detta för att begränsa spelarens position så att den inte går utanför skärmen.

```csharp
void Update()
{
    // Läs av spelarinput från tangentbordets V/H-piltangenter eller A/D-tangenterna
    float horizontalInput = Input.GetAxis("Horizontal");
    // Läs av spelarinput från tangentbordets U/N-piltangenter eller W/S-tangenterna
    float verticalInput = Input.GetAxis("Vertical");

    // Skapa en förflyttningen baserat på spelarinput
    Vector2 movement = new Vector2(horizontalInput, verticalInput);

    // Flytta spelaren
    transform.Translate(movement * speed * Time.deltaTime);

    // Begränsa spelarens position
    transform.position = new Vector2(Mathf.Clamp(transform.position.x, -4.5f, 4.5f),
                                     Mathf.Clamp(transform.position.y, -4.5f, 4.5f));
}
```

Nu kommer spelaren att stanna på skärmens kanter istället för att gå utanför.

Du bör nu ha en fungerande spelarcontroller som kan styras med tangentbordet och inte går utanför skärmen! Nästa steg kommer att vara att skapa en fiende, men det kan vara bra att testa och förstå allt vi har gjort hittills innan vi går vidare.

## Uppgifter

### Uppgift 1: Ändra spelarens hastighet

Förändra värdet av `speed`-variabeln i `PlayerController` skriptet. Prova olika värden och se hur det påverkar spelarens rörelse. 

- Hur ändras spelupplevelsen med högre hastigheter?
- Vilken effekt har mycket låga hastigheter?

### Uppgift 2: Begränsa spelarens rörelse

För närvarande kan spelaren röra sig fritt inom gränserna vi satt (-4.5, 4.5) för både x och y axlarna. Försök ändra dessa gränser i `Mathf.Clamp` funktionen och se hur det påverkar spelarens rörelse.

- Vad händer om du sätter samma gräns för både x och y? 
- Hur förändras spelupplevelsen om du tillåter spelaren att endast röra sig i en mycket smal remsa av skärmen?

### Uppgift 3: Prova olika indata-axlar

Byt "Horizontal" och "Vertical" i `Input.GetAxis` metoden. Hur ändras spelarens rörelse när du trycker på vänster/höger eller upp/ner piltangenterna?

### Uppgift 4: Ändra formen på spelarobjektet

För närvarande använder vi en enkel rektangel sprite för spelaren. Du kan prova att byta ut denna sprite för en annan form eller till och med en bild. Du kan göra detta genom att ändra "Sprite" inställningen i "Sprite Renderer" komponenten på spelarobjektet.

### Uppgift 5: Förstå "Update" metoden

"Update" metoden i Unity kallas en gång per bildruta. Prova att lägga till en `Debug.Log` uttalande i "Update" metoden för att se hur ofta den körs. Du kan göra detta genom att lägga till följande rad i "Update" metoden:

```csharp
Debug.Log("Update is being called.");
```

När du kör spelet bör du se detta meddelande upprepade gånger i Unitys konsolfönster. Detta hjälper till att illustrera hur ofta "Update" metoden körs. 

