# Spelare och fiende

## Shmup-spel i Unity

I denna labb ska vi skapa ett enkelt shoot 'em up spel i Unity. Spelet kommer att bestå av en spelare som kan styras med piltangenterna och en fiende som faller ner från toppen av skärmen. Målet är att göra så att spelaren kan skjuta fienden med mellanslagstangenten.

### Steg 1: Skapa en spelare

Börja med att skapa en ny `GameObject` som representerar spelaren. I Unity, gå till "GameObject" -> "Create Empty". Döp det nya objektet till "Player".

Lägg till en form till din spelare. Du kan använda en "Sprite Renderer" för att lägga till en 2D-bild eller en "Mesh Renderer" och "Mesh Filter" för att lägga till en 3D-modell. För detta exempel ska vi använda en enkel rektangulär "Sprite". 

Välj "Player" objektet, klicka på "Add Component" i Inspector och lägg till en "Sprite Renderer". Klicka sedan på "Sprite" fältet i "Sprite Renderer" komponenten och välj en rektangel sprite.

Nu behöver vi ett sätt att styra spelaren. Detta kan vi göra genom att skriva ett skript. Högerklicka i din "Project" vy, välj "Create" -> "C# Script" och döp det till "PlayerController". Dubbelklicka på skriptet för att öppna det i din kodredigerare.

Skriv följande kod:

```csharp
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private float speed = 5.0f;  // hastigheten på spelarens rörelse
    private float horizontalInput;  // indata för horisontell rörelse (vänster/höger)
    private float verticalInput;  // indata för vertikal rörelse (upp/ner)

    void Update()
    {
        // Läs indata från tangentbordet
        horizontalInput = Input.GetAxis("Horizontal");
        verticalInput = Input.GetAxis("Vertical");

        // Använd indata för att flytta spelaren
        Vector2 movement = new Vector2(horizontalInput, verticalInput);
        transform.Translate(movement * speed * Time.deltaTime);

        // Begränsa rörelsen till fönstret
        transform.position = new Vector2(Mathf.Clamp(transform.position.x, -4.5f, 4.5f),
                                         Mathf.Clamp(transform.position.y, -4.5f, 4.5f));
    }
}
```
Förklaring: 
- `Input.GetAxis("Horizontal")` och `Input.GetAxis("Vertical")` returnerar värden mellan -1 och 1 beroende på om användaren trycker på vänster/höger eller upp/ner piltangenter. 
- `transform.Translate(movement * speed * Time.deltaTime)` flyttar objektet baserat på indata, hastighet och tiden sedan förra bilden renderades (för att göra rörelsen jämn oavsett bildfrekvens).
- `Mathf.Clamp(value, min, max)` är en metod som begränsar ett värde mellan en min och max gräns. Detta ser till att spelaren inte rör sig utanför skärmen.

Spara och stäng skriptet, gå tillbaka till Unity och dra "PlayerController" skriptet till din "Player" GameObject.

### Steg 2: Skapa en fiende

Skapa en ny `GameObject` som representerar fienden, och lägg till en form på samma sätt som du gjorde för spelaren.

Lägg till en "Rigidbody2D" komponent till din fiende. Detta gör att objektet kan påverkas av fysik och gravitation. I "Rigidbody2D" komponenten, ställ in "Gravity Scale" till det värde du vill ha för din fiendes fallhastighet.

Skapa ett nytt skript och döp det till "EnemyController". Skriv följande kod:

```csharp
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    private Rigidbody2D rb;  // referens till Rigidbody2D-komponenten

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();  // hämta referensen

        // Startposition ovanför skärmen
        ResetPositionAndVelocity();
    }

    void Update()
    {
        // Om fienden är under skärmen, börja om
        if (transform.position.y < -6.0f)
        {
            ResetPositionAndVelocity();
        }
    }

    // Återställ position och hastighet
    private void ResetPositionAndVelocity()
    {
        transform.position = new Vector2(Random.Range(-4.5f, 4.5f), 6.0f);
        rb.velocity = Vector2.zero;  // återställ hastigheten
    }
}
```

Förklaring:
- `GetComponent<Rigidbody2D>()` returnerar en referens till "Rigidbody2D" komponenten på samma GameObject som skriptet är fäst vid. 
- `ResetPositionAndVelocity()` är en metod vi skapade för att återställa positionen och hastigheten på fienden.

Spara och stäng skriptet, gå tillbaka till Unity och dra "EnemyController" skriptet till din "Enemy" GameObject. Nu ska fienden börja falla från toppen av skärmen och återgå till toppen när den passerar botten.

Nu har du en spelare som kan styras med piltangenterna och en fiende som faller ner från toppen av skärmen! Du kan justera hastigheten på spelaren och fallhastigheten på fienden genom att ändra "speed" variabeln i "PlayerController" skriptet och "Gravity Scale" i "Rigidbody2D" komponenten på fienden.

## Uppgifter

### Uppgift 1: Ändra hastigheten för spelaren och fienden. 

- Spelarens hastighet kan ändras genom att ändra värdet av variabeln `speed` i `PlayerController` skriptet.
- Fiendens fallhastighet kan ändras genom att justera "Gravity Scale" i "Rigidbody2D" komponenten på fienden.
- Prova olika värden och se hur de påverkar spelets känsla och svårighetsgrad.

### Uppgift 2: Lägg till fler fiender.

- Kopiera fiendens GameObject, och ändra startpositioner och hastigheter för att skapa mer variation i spelet.
- Prova att ha olika typer av fiender med olika beteenden, t.ex. fiender som rör sig horisontellt istället för att bara falla ner.

### Uppgift 3: Ändra spelarens rörelsebegränsningar.

- I `PlayerController` skriptet, justera värdena i `Mathf.Clamp` funktionen som används för att begränsa spelarens position.
- Prova att tillåta spelaren att röra sig mer fritt, eller gör rörelseområdet mer begränsat.

### Uppgift 4: Ändra hur fienden återställs när den når botten av skärmen.

- I `EnemyController` skriptet, ändra positionen där fienden återställs. Prova att göra det mer varierat genom att ändra `Random.Range` funktionens argument.
- Du kan även ändra fördröjningen innan fienden återställs genom att införa en `yield return new WaitForSeconds(seconds)` i `ResetPositionAndVelocity` metoden och förvandla metoden till en IEnumerator.

### Uppgift 5: Variera storleken på dina fiender.

- Lägg till kod i ditt `EnemyController` skript som slumpmässigt ändrar storleken på din fiende vid återställning. Använd `transform.localScale` för att ändra storleken.
