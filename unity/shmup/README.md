# Spelet Shoot 'em up (Shmup)

![](../../.gitbook/assets/image-81.png)

Här är steg vi kommer att gå igenom för att skapa ett grundläggande Shoot 'em up (Shmup) spel i Unity.

## Steg 1: Installera Unity
Först behöver du installera Unity på din dator. Gå till Unitys hemsida och ladda ner den senaste versionen av Unity Hub. Genom Unity Hub kan du installera och hantera olika versioner av Unity Editor. 

## Steg 2: Skapa ett nytt projekt
Starta Unity Hub och klicka på "New" för att skapa ett nytt projekt. Du kommer att bli ombedd att välja en plats att spara ditt projekt, samt ge det ett namn. För det här projektet, välj "2D" från temat för att göra det enklare att arbeta med 2D-grafik.

## Steg 3: Skapa Spelaren
Först behöver vi skapa spelarobjektet. Gå till menyn "GameObject" och välj "2D Object -> Sprite". Detta kommer att skapa ett nytt 2D spelobjekt som vi kan använda som spelare. Vi kan kalla det "Player". 

## Steg 4: Lägg till rörelse
För att lägga till rörelse till spelaren, behöver vi skapa ett skript. Skapa ett nytt C# skript och kalla det "PlayerMovement". I detta skript kommer vi att använda Unitys `Input` klass för att läsa in spelarens inmatning och flytta spelaren beroende på den inmatningen.

## Steg 5: Skapa Fienden
På liknande sätt som med spelaren, skapa ett nytt spelobjekt för fienden. Detta objekt kommer också att behöva ett skript för att kontrollera dess beteende.

## Steg 6: Skapa Skott
Vi kommer också att behöva ett skott objekt, som spelaren kommer att avfyra mot fienderna. Detta objekt kommer att behöva ett skript för att kontrollera dess rörelse.

## Steg 7: Lägg till Kollision
För att det ska bli konsekvenser av att skjuta fienden eller att bli träffad av fienden, behöver vi lägga till kollision. Vi kan göra detta genom att lägga till en "Collider" komponent till både spelar- och fiendeobjekten, samt skottobjektet.

## Steg 8: Lägg till Poäng
Slutligen, för att göra spelet mer intressant, kan vi lägga till ett poängsystem. Varje gång spelaren träffar en fiende, kan vi öka spelarens poäng. Vi kan visa poängen på skärmen med hjälp av en "Text" UI-komponent.

Detta är bara en grundläggande genomgång av hur man skapar ett Shmup spel i Unity. Det finns många andra funktioner du kan lägga till för att göra spelet mer intressant, som power-ups, olika typer av fiender, bossar, och så vidare. Men detta bör ge dig en bra start!