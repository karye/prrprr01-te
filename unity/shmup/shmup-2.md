# Del 2: Skapa en fiende

Vi har nu en spelare som vi kan kontrollera. Dags att introducera en utmaning i spelet genom att skapa en fiende. 

## Steg 1: Skapa fiendens GameObject

I Unity, gå till **GameObject** -> **Create Empty** för att skapa en ny tom GameObject. Döp det nya objektet till `Enemy`.

Precis som för spelaren behöver vi lägga till en form till fienden. För att göra detta, välj `Enemy`-objektet, klicka på **Add Component** i Inspector-fönstret och lägg till en **Sprite Renderer**. 

Klicka sedan på **Sprite** fältet i **Sprite Renderer** komponenten och välj en sprite som representerar din fiende.

Vår fiende finns nu i scenen men är fortfarande statisk, precis som vår spelare var initialt.

![](../../.gitbook/assets/image-87.png)

## Steg 2: Låt fienden falla

För att få vår fiende att röra sig behöver vi lägga till en `Rigidbody2D`-komponent till den. `Rigidbody2D` gör att vi kan använda Unitys inbyggda fysikmotor för att påverka objektet.

Medan `Enemy`-objektet är valt, klicka på **Add Component** och lägg till en `Rigidbody2D`.

I inställningarna för `Rigidbody2D`, ändra **Gravity Scale** till ett värde som inte är noll. Detta är den kraft som kommer att få vår fiende att falla nedåt. Du kan experimentera med olika värden för att se hur de påverkar hastigheten på fiendens fall.

![](../../.gitbook/assets/image-88.png)

Nu borde vår fiende börja falla när vi spelar scenen. Problemet är att den bara fortsätter att falla och vi vill att den ska börja om när den når botten av skärmen.

## Steg 3: Få fienden att börja om

För att vår fiende ska börja om när den når botten av skärmen behöver vi lägga till lite kod. 

Skapa ett nytt C#-skript och döp det till `EnemyController`.

Öppna skriptet i din kodredigerare och skriv följande kod:

```csharp
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    // Rigidbody2D-komponenten som är fäst till detta GameObject
    private Rigidbody2D rb;

    // Start is called before the first frame update
    void Start()
    {
        // Hämta referens till Rigidbody2D-komponenten
        rb = GetComponent<Rigidbody2D>();

        // Starta fienden på en slumpmässig position på toppen av skärmen
        ResetEnemy();
    }

    // Update is called once per frame
    void Update()
    {
        // Om fienden har fallit under -6.0 på y-axeln, återställ den
        if (transform.position.y < -6.0f)
        {
            ResetEnemy();
        }
    }

    // Återställ fienden till toppen av skärmen
    private void ResetEnemy()
    {
        // Placera fienden på en slumpmässig position på toppen av skärmen
        transform.position = new Vector2(Random.Range(-4.5f, 4.5f), 6.0f);

        // Nollställ fiendens hastighet
        rb.velocity = Vector2.zero;
    }
}
```

Förklaring:

- `GetComponent<Rigidbody2D>()` är en metod som returnerar en referens till `Rigidbody2D`-komponenten som är fäst till samma GameObject som detta skriptet (i detta fall, vår fiende).
- `Update()`-metoden kallas varje bildruta, och här kollar vi om fiendens position är under -6.0 på y-axeln. Om den är det kallar vi på `ResetEnemy()`.
- `ResetEnemy()`-metoden flyttar fienden till en slumpmässig position längs x-axeln och på toppen av skärmen. Vi sätter också fiendens hastighet till noll för att säkerställa att den inte fortsätter att röra sig i samma riktning när den återställs.
- `Random.Range()` är en inbyggd funktion i Unity som genererar ett slumpmässigt tal inom ett specifikt intervall. Den används för att lägga till slumpmässighet i ditt spel, vilket kan vara användbart för många saker som att bestämma fiendernas beteende, generera slumpmässiga belöningar och så vidare.

När du använder `Random.Range()`, anger du två värden: ett lägre gränsvärde och ett övre gränsvärde. Funktionen ger tillbaka ett tal som ligger inom (eller mellan) dessa två gränser. 

Till exempel, om du skulle skriva `Random.Range(1, 10)`, skulle du få ett tal någonstans mellan 1 och 10.

I koden jag visade tidigare:

```csharp
transform.position = new Vector2(Random.Range(-4.5f, 4.5f), 6.0f);
```

Detta innebär att vi placerar vår fiende på en position längs x-axeln som är någonstans mellan -4,5 och 4,5 (dvs., slumpmässigt på skärmen längs x-axeln). Y-värdet är satt till 6, vilket skulle vara någonstans ovanför synfältet på skärmen (beroende på din kamera), vilket gör att det ser ut som om fienden faller in i skärmen från toppen. 

Det är också värt att notera att `Random.Range()` kommer att ge olika värden om de två argumenten är integers (heltal) eller floats (decimaltal). Om de är integers, kommer resultatet att vara ett heltal och det övre gränsvärdet är exkluderat. Om de är floats, kommer resultatet att vara ett flyttal och båda gränserna är inkluderade.

Gå tillbaka till Unity och dra `EnemyController`-skriptet till "Enemy"-objektet.

Nu bör din fiende börja falla när du spelar scenen och sedan återställas till toppen av skärmen när den når botten. Du kan justera värdena i koden för att anpassa spelupplevelsen.

I nästa steg kommer vi att introducera kollisioner mellan spelaren och fienden, vilket gör spelet lite mer interaktivt.

## Uppgifter

### Uppgift 1: Ändra fiendens fallhastighet

Först och främst, experimentera med att ändra "Gravity Scale" inställningen i fiendens Rigidbody2D-komponenten. Detta kommer att påverka hastigheten på fiendens fall.

- Hur påverkas spelets svårighetsgrad när fiendens fallhastighet ökas eller minskas?
- Vad händer om du ställer in "Gravity Scale" till 0?

### Uppgift 2: Ändra fiendens startposition

I `EnemyController` skriptet, modifiera värdena i `Random.Range` funktionen för att ändra var fienden börjar falla. 

- Vad händer om du begränsar fienden att bara starta från mitten av skärmen? 
- Hur påverkas spelet om fienden alltid börjar från samma plats?

### Uppgift 3: Testa olika återställningspunkter

Prova att ändra y-värdet där fienden återställs när den når botten av skärmen. 

- Vad händer om du ställer in detta värde till något mycket högt, till exempel 20? 
- Vad händer om du ställer in det till ett negativt värde?

### Uppgift 4: Lägg till fler fiender

Du kan lägga till fler fiender till scenen genom att duplicera din befintliga fiende-GameObject. 

- Hur påverkas spelet när antalet fiender ökar?
- Kan du hantera att ha flera fiender fallande samtidigt?

### Uppgift 5: Variera fiendens storlek

Du kan lägga till en liten förändring i ditt `EnemyController` skript som slumpmässigt ändrar storleken på din fiende varje gång den återställs. Använd `transform.localScale` för att ändra storleken på fienden.

- Hur påverkas spelets svårighetsgrad när fienden är större eller mindre?
- Vad händer om du har fiender av olika storlekar fallande samtidigt?
