# Introduktion till Unity

Unity är en kraftfull och mycket använd spel- och applikationsutvecklingsplattform. Det stödjer många olika plattformar, inklusive Windows, MacOS, Android, iOS, och många fler. Unity har också stöd för att skapa VR- och AR-applikationer.

### Unitys gränssnitt

![](../.gitbook/assets/image-85.png)

När du först öppnar Unity kommer du att se flera olika fönster, vilka alla har olika syften:

1. **Scenfönstret (Scene View)**: Detta är det fönster där du direkt kommer att interagera med dina spelobjekt. Du kan placera, flytta, skala och rotera objekt i detta fönster.

![](../.gitbook/assets/image.png)

2. **Spelfönstret (Game View)**: Detta är en förhandsvisning av vad ditt spel kommer att se ut när det körs. 

![](../.gitbook/assets/image-35.png)

3. **Hierarkifönstret (Hierarchy View)**: Detta fönster listar alla objekt som finns i den nuvarande scenen. 

![](../.gitbook/assets/image-78.png)

4. **Inspektionsfönstret (Inspector View)**: Detta fönster visar detaljerade information om ett valt objekt i Hierarkifönstret eller Scenfönstret. Här kan du modifiera egenskaper för objektet och tilldela skript till det.

![](../.gitbook/assets/image-79.png)

5. **Projektfönstret (Project View)**: Detta fönster visar alla filer och tillgångar som ingår i ditt projekt.

![](../.gitbook/assets/image-80.png)

### Unitys huvudloop och metoder

Unitys arbetsflöde styrs av en huvudloop, också känd som spel-loopen. Spel-loopen är en kontinuerlig cykel där Unity uppdaterar sina interna system och spelobjekt.

De mest grundläggande metoderna i denna loop är `Start` och `Update`. 

- **Start**: Den här metoden kallas en gång när ett script aktiveras första gången. Detta är ett bra ställe att initialisera variabler och andra saker som bara behöver köras en gång.

- **Update**: Den här metoden kallas en gång per bildruta, vilket betyder att den kan kallas många gånger per sekund och frekvensen kan variera beroende på hur snabbt spelet körs. `Update` används oftast för att uppdatera spellogik, ta hand om användarinmatning och uppdatera AI-beteenden.

Det finns flera andra viktiga metoder också, såsom `OnCollisionEnter` och `OnTriggerEnter`, som används för att hantera kollisioner mellan spelobjekt.

### Komponenter och GameObjects

Allt i en Unity-scen är ett GameObject. Ett GameObject är i grunden en behållare som kan hålla ett antal olika komponenter. En komponent kan vara en mängd olika saker, från transform (som håller reda på objektets position, rotation och skala) till ljus, ljud, fysik, skript, och mycket mer.

Skript i Unity är bara en typ av komponent, och de skrivs oftast i C#. De används för att lägga till beteenden och logik till dina spelobjekt.

Det här är bara en grundläggande introduktion till Unity. Det finns mycket mer att lära om plattformen, men detta ger dig en bra grund att bygga på.

## Koordinatsystem och axlar

Innan vi börjar med Unity är det viktigt att förstå grundläggande koncept som koordinatsystem och axlar. Unity använder ett 3D-kartesiskt koordinatsystem, med x-axeln representerande vänster och höger, y-axeln representerande upp och ned, och z-axeln representerande fram och tillbaka.

I ett 2D-spel kommer du mestadels att arbeta med x- och y-axlarna. Varje objekt i spelvärlden har en position som representeras av en Vector3, vilket är ett objekt som innehåller tre värden: x, y och z.

## Skapa ett spel i Unity

När du startar Unity kommer du att mötas av projektväljaren. Härifrån kan du skapa ett nytt projekt, vilket skapar en ny mapp på din dator där allt relaterat till ditt spel kommer att lagras. Du kan välja om du vill börja med en 3D- eller 2D-mall, men du kan ändra detta senare om du vill.

När du har skapat ett projekt kommer du att se Unitys huvudgränssnitt. Detta inkluderar en scenvy, där du kan se och manipulera objekt i ditt spel, en hierarkivisning, som visar alla objekt i den aktuella scenen, och en inspektör, där du kan se och ändra egenskaper för det valda objektet.

Du skapar spel i Unity genom att lägga till och manipulera objekt i dina scener. Ett spelobjekt kan vara något som en spelare, en fiende, en power-up, en bit av miljön, eller något annat du kan tänka på. Varje spelobjekt kan ha en rad olika komponenter som påverkar dess beteende, till exempel kollisionsdetektorer, fysikkomponenter eller anpassade skript.
