---
description: Bloggapp som sparar inlägg i en fil.
---

# Labb: bloggappen

![](../.gitbook/assets/image-111.png)

I denna laboration kommer du att skapa en enkel konsolapplikation som simulerar en blogg. Applikationen kommer att tillåta användaren att skriva nya blogginlägg, skriva ut alla blogginlägg, radera bloggen och slutligen avsluta applikationen. All bloggdata kommer att sparas i en textfil.

## Mål

Målet med denna laboration är att öva på:

- Hantering av textfiler.
- Användning av loopar och menysystem i konsolapplikationer.
- Skapa och använda metoder för att strukturera koden.

## Instruktioner

1. **Starta ett nytt konsolprojekt** i din utvecklingsmiljö och namnge det till **Bloggappen**.

2. **Skapa grundläggande meny** som låter användaren välja mellan de olika funktionerna i applikationen.

3. **Implementera funktionaliteten** för varje menyval enligt specifikationerna.

4. **Testa applikationen** för att säkerställa att all funktionalitet fungerar som den ska.

## Pseudokod

Här är en översiktlig pseudokod för **Bloggappen**:

```text
Starta Program
    Skapa en lista för att lagra blogginlägg

Visa Meny
    Loopa tills användaren väljer att avsluta
        Visa menyval
            1. Skriv blogginlägg
            2. Skriv ut bloggen
            3. Radera bloggen
            4. Avsluta
        Läs in användarens val

        Om val är 'Skriv blogginlägg'
            Anropa metod för att skriva nytt blogginlägg
        Annars om val är 'Skriv ut bloggen'
            Anropa metod för att skriva ut alla blogginlägg
        Annars om val är 'Radera bloggen'
            Anropa metod för att radera alla blogginlägg
        Annars om val är 'Avsluta'
            Avsluta programmet

Metod för att skriva blogginlägg
    Be användaren mata in inlägg
    Lägg till inlägg i listan
    Spara listan till fil

Metod för att skriva ut bloggen
    Om det finns inlägg
        Skriv ut alla inlägg
    Annars
        Meddela att bloggen är tom

Metod för att radera bloggen
    Töm listan med inlägg
    Töm filen
```

## Tips

* För att rensa en `List<>` kan du använda `Clear()`-metoden.
* För att skriva till en fil kan du använda `File.WriteAllLines()`-metoden.
* För att läsa från en fil kan du använda `File.ReadAllLines()`-metoden.

## Startkod

Här är en översiktlig struktur för **Bloggappen**:

```csharp
/* *************************************************
 * Namn: bloggappen
 * Beskrivning: En enkel bloggapp som sparar inlägg i en fil
 * Av: [Ditt namn]
 * Datum: 2021-09-01
 *********************************************** */
// Presentation

// Programloop
    // Meny

    // Hantera användarens val

/* ************************************************
 * **************** Metoder ***********************
 ************************************************* */
```

## Utmaningar

* Läs in blogginlägg från filen när programmet startar. Om det finns inlägg i filen, läs in dem och lägg till dem i listan med blogginlägg.

* Skriv ut blogginläggen i omvänd ordning, så att det senaste inlägget visas först.\
  Tips: Använd en `for`-loop och börja från sista inlägget i listan.\
  Tips2: Eller använd `Reverse()`-metoden på listan.

* Skapa en metod `RaderaInlägg()` som tar bort ett inlägg från listan.\
  Tips: Använd `RemoveAt()` se [dokumentationen](https://csharp.progdocs.se/grundlaeggande/listor-och-arrayer#removeat).

* **Lägg till tidsstämplar**: Modifera applikationen så att varje blogginlägg sparas med en tidsstämpel. \
  Tips: Använd `DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")` se [dokumentationen](https://csharp.progdocs.se/grundlaeggande/datum-och-tid).

* **Sökfunktionalitet**: Implementera en funktion för att söka efter ord eller fraser i blogginläggen. Visa alla inlägg som innehåller söktermen.

* **Spara inlägg som individuella filer**: Istället för att spara alla blogginlägg i en enda fil, spara varje inlägg som en separat fil. Namnge filerna baserat på tidsstämpeln för när inlägget skapades.\
  Tips: Skapa en mapp med `Directory.CreateDirectory()`, se [dokumentationen](https://csharp.progdocs.se/filhantering/filer-och-mappar#mappar).\
  Tips2: Använd `filnamn = Path.Combine("mappnamn", "....txt")` för att skapa filens fullständiga sökväg.\

* **Kommentarsfunktion**: Lägg till möjligheten för användaren att skriva kommentarer till varje blogginlägg. Kommentarerna ska sparas tillsammans med inlägget.\
  Tips: Skapa en ny lista för kommentarer och koppla ihop kommentarerna med inläggen.

* **Inloggningssystem**: Implementera ett enkelt inloggningssystem som kräver att användaren skriver in ett lösenord för att komma åt bloggen.