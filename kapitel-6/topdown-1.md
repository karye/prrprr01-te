---
description: Topdown-strategi för programutveckling där ett större problem bryts ner i mindre delar eller moduler.
---
# Topdown-strategin i programmering

![](../.gitbook/assets/image-106.png)

**Topdown**-strategi är en strategi för programutveckling där ett större problem bryts ner i mindre delar eller moduler. Strategi går ut på att starta med den **övergripande** funktionaliteten i ett program och sedan gradvis **detaljera** och implementera de enskilda delarna. 

Här är en tydlig jämförelse mellan **topdown-metoden** och att **"börja koda direkt"** i en tabell:

| **Aspekt**            | **Topdown-metoden** ✅ | **Börja koda direkt** ❌ |
|----------------------|-------------------|-------------------|
| **Struktur och planering** | Börjar med en plan och bryter ner problemet i mindre delar. | Ingen eller minimal planering, hoppar direkt till att skriva kod. |
| **Läsbarhet** | Koden är organiserad i små metoder och lätt att förstå. | Koden blir ofta rörig och svår att följa. |
| **Felsökning och testning** | Lätt att hitta och åtgärda fel eftersom varje del är separat. | Svårt att hitta fel när all kod är sammanblandad. |
| **Återanvändning av kod** | Metoder kan enkelt återanvändas i andra program. | Ofta svårt att återanvända kod eftersom den är skriven utan struktur. |
| **Samarbete i team** | Flera personer kan arbeta parallellt på olika delar av programmet. | Svårt att dela upp arbetet eftersom allt skrivs ihop utan tydliga moduler. |
| **Effektivitet på lång sikt** | Tar längre tid i början men sparar mycket tid senare. | Snabb start men blir snabbt svårhanterat och tidskrävande att ändra. |

### 🏆 **Slutsats:**  
Topdown-metoden är **bättre** för alla större program och ger **mer hållbar och lättläst kod**. 🚀

## Exempel: Ett menyprogram

I detta exempel skapar vi ett enkelt menyprogram som demonstrerar användningen av topdown-strategi. Programmet erbjuder användaren tre alternativ:

* Spara meddelande - låter användaren skriva in ett meddelande som sparas i en fil
* Läsa meddelande - läser innehållet från en fil och skriver ut det på skärmen
* Avsluta programmet

### Pseudokod

Här är en översiktlig pseudokod för programmet:

```
Visa presentation

Starta en loop som körs tills användaren väljer att avsluta programmet

    Visa meny och läs in användarens val
        1. Spara meddelande
        2. Läsa meddelande
        3. Avsluta programmet
    
    OM användaren väljer att spara meddelande
        Spara meddelande
    ANNARS OM användaren väljer att läsa meddelande
        Läsa meddelande
    ANNARS OM användaren väljer att avsluta programmet
        Avsluta programmet
    ANNARS
        Visa felmeddelande
```

### Grundstruktur

Här är en grundstruktur för programmet:

```csharp
/* *************************************************
 * Namn: Meddelandeprogram
 * Beskrivning: Ett program för att spara och läsa meddelanden
 * Datum: 2025-01-01
 *********************************************** */

VisaPresentation();

string filnamn = "meddelanden.txt";
while (true)
{
    VisaMeny();
    string val = Console.ReadLine();

    if (val == "1")
    {
        SparaMeddelande();
    }
    else if (val == "2")
    {
        LäsaMeddelanden();
    }
    else if (val == "3")
    {
        Console.WriteLine("Tack för idag!");
        break;
    }
    else
    {
        Console.WriteLine("Du valde ett ogiltigt alternativ");
    }
}

/* ************************************************
 * **************** Metoder ***********************
 ************************************************* */
```

### Testa prototypen - del 1

Innan vi implementerar metoderna kan vi testa prototypen för att se att programmet fungerar som det ska. Koden ovan är en bra startpunkt för att testa programmet och se att det fungerar som det ska.

> Testa programmet genom att köra det och se att det fungerar som det ska.

### Testa prototypen - del 2

Skapa metoderna som tomma skal och anropa dem från huvudprogrammet. Detta gör det enklare att testa programmet och se att strukturen fungerar som det ska.

I varje metod kan du skriva ut en enkel text för att se att metoden anropas korrekt. Exempel:

```csharp
static void VisaPresentation()
{
    Console.WriteLine("Visa presentation");
}

static void VisaMeny()
{
    Console.WriteLine("Visa meny");
}

...
```

> Testa programmet genom att köra det och se att det fungerar som det ska.

### Implementering av metoderna

Nu ska vi implementera (koda) de metoder som används i programmet. Här är en grundstruktur för varje metod:

```csharp
/// <summary>
/// Visar en presentation av programmet
/// </summary>
static void VisaPresentation()
{
    Console.Clear();
    Console.WriteLine("""
    Välkommen till meddelandeprogrammet
    ===================================
    """);
}

/// <summary>
/// Skriver ut en meny med tre alternativ och läser in användarens val
/// </summary>
/// <returns>Användarens val</returns>
static void VisaMeny()
{
    Console.WriteLine("""
    1. Spara meddelande
    2. Läsa meddelande
    3. Avsluta
    """);
    Console.Write("Välj ett alternativ: ");
}

/// <summary>
/// Sparar text till en fil
/// </summary>
/// <param name="filnamn">Namnet på filen</param>
static void SparaMeddelande(string filnamn)
{
    Console.Write("Skriv in meddelande du vill spara: ");
    string texten = Console.ReadLine();
    File.WriteAllText(filnamn, texten);
    Console.WriteLine("Meddelandet sparades.");
}

/// <summary>
/// Läser text från en fil
/// </summary>
/// <param name="filnamn">Namnet på filen</param>
static void LäsaMeddelande(string filnamn)
{
    string texten = File.ReadAllText(filnamn);
    Console.WriteLine("Meddelandet:");
    Console.WriteLine(texten);
}
```

## Uppgifter

1. Skapa en metod som låter användaren spara flera meddelanden genom att fylla på en befintlig fil med text. \
   Tips: Använd `File.AppendAllText()` för att lägga till text till en befintlig fil.

1. Skapa felhantering i metoden `LäsaMeddelande()` så att programmet inte kraschar om filen inte finns.\
   Tips: Använd en `File.Exists()` för att kontrollera om filen finns innan du läser innehållet.

1. Skapa en metod som raderar en befintlig fil.\
   Tips: Använd `File.Delete()` för att radera en fil.

1. Skapa en bekräftelsedialog innan en fil raderas.

1. Skapa en metod som listar alla filer i en mapp.\
   Tips: Använd `Directory.GetFiles()` för att hämta en lista med filer i en mapp.