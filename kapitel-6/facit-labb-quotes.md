# Facit till labb citatgenerator

## Introduktion  
I denna laboration skapar vi ett program som slumpar fram citat från en textfil. Programmet ska:  

- Läsa in citat från en textfil  
- Slumpa fram och visa ett citat  
- Visa alla citat **10 st i taget** genom bläddring  
- Söka efter citat som innehåller en viss sökterm  

Vi använder en `List<string>` för att lagra citaten och `File`-metoder för att läsa in filen.  

## Steg 1: Skapa huvudstrukturen  

Vi definierar **filnamn, lista och huvudloopen**.  

```csharp
string filnamn = "quotes.txt";
List<string> citatLista = LäsInCitat();

while (true)
{
    Console.WriteLine();
    Console.WriteLine("Meny:");
    Console.WriteLine("1. Slumpa fram ett citat");
    Console.WriteLine("2. Visa alla citat (10 i taget)");
    Console.WriteLine("3. Sök efter citat");
    Console.WriteLine("4. Avsluta");
    Console.Write("Välj ett alternativ: ");
    string val = Console.ReadLine()!;
    
    if (val == "1")
    {
        SlumpaCitat();
    }
    else if (val == "2")
    {
        VisaCitat();
    }
    else if (val == "3")
    {
        SökCitat();
    }
    else if (val == "4")
    {
        break;
    }
    else
    {
        Console.WriteLine("Ogiltigt val. Försök igen.");
    }
}
```

## Steg 2: Läs in citaten från textfil  
Metoden `LäsInCitat()` läser in citaten och returnerar en lista.  

```csharp
static List<string> LäsInCitat()
{
    List<string> citatLista = [];

    if (File.Exists(filnamn))
    {
        // Läs in alla rader från textfilen
        string[] rader = File.ReadAllLines(filnamn);
        string citat = "";

        // Loopa igenom alla rader som inte är tomma
        foreach (string rad in rader)
        {
            if (rad.Trim().Length > 0)
            {
                citat += rad + " ";
            }
            else if (citat.Length > 0)
            {
                citatLista.Add(citat.Trim());
                citat = "";
            }
        }

        // Lägg till sista citatet
        if (citat.Length > 0)
        {
            citatLista.Add(citat.Trim());
        }
    }
    else
    {
        Console.WriteLine("Filen med citat hittades inte.");
    }

    return citatLista;
}
```

## Steg 3: Slumpa fram ett citat  
Metoden `SlumpaCitat()` väljer slumpmässigt ett citat.  

```csharp
static void SlumpaCitat()
{
    if (citatLista.Count == 0)
    {
        Console.WriteLine("Det finns inga citat att visa.");
        return;
    }

    // Slumpa fram ett citat
    int slumptal = Random.Shared.Next(citatLista.Count);
    string citat = citatLista[slumptal];

    Console.WriteLine();
    Console.WriteLine("Slumpat citat:");
    Console.WriteLine(citat);
}
```

## Steg 4: Visa alla citat 10 i taget  
Metoden `VisaCitat()` visar citaten i omgångar om 10 st.  

```csharp
static void VisaCitat()
{
    if (citatLista.Count == 0)
    {
        Console.WriteLine("Det finns inga citat att visa.");
        return;
    }

    int index = 0;

    // Loopa tills alla citat har visats
    while (index < citatLista.Count)
    {
        Console.WriteLine();
        Console.WriteLine("Citat:");
        
        for (int i = 0; i < 10 && index < citatLista.Count; i++)
        {
            Console.WriteLine((index + 1) + ". " + citatLista[index]);
            index++;
        }

        Console.WriteLine();
        Console.Write("Tryck på Enter för att visa fler citat eller skriv 'sluta' för att avsluta: ");
        string val = Console.ReadLine().ToLower();

        if (val == "sluta")
        {
            break;
        }
    }
}
```

## Steg 5: Sök efter citat
Metoden `SökCitat()` hittar alla citat som innehåller söktermen.  

```csharp
static void SökCitat()
{
    Console.Write("Ange en sökterm: ");
    string sökterm = Console.ReadLine()!;
    
    // Skapa en lista för sökresultat
    List<string> resultat = [];

    // Loopa igenom alla citat och lägg till matchande citat i resultatlistan
    foreach (string citat in citatLista)
    {
        // Vi har hittat en matchning
        if (citat.Contains(sökterm))
        {
            resultat.Add(citat);
        }
    }

    // Skriv ut sökresultatet
    if (resultat.Count > 0)
    {
        Console.WriteLine();
        Console.WriteLine("Sökresultat:");

        foreach (string citat in resultat)
        {
            Console.WriteLine("- " + citat);
        }
    }
    else
    {
        Console.WriteLine("Inga citat matchade sökningen.");
    }
}
```
