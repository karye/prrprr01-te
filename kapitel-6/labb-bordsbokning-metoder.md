---
description: Bordsbokning med metoder.
---

# Labb - Bordsbokning 2 med metoder

![](../.gitbook/assets/image-110.png)

I denna laboration kommer du att utveckla ett program för hantering av bordsbokningar för Centralrestaurangen. Målet är att ge dig praktisk erfarenhet av att använda arrayer för att lagra information och fillagring för att bevara denna information mellan programkörningar. Genom att skapa ett användarvänligt gränssnitt där personalen enkelt kan lägga till och uppdatera bordsinformation, kommer du att lära dig vikten av strukturerad och modulär kod.

## Pseudokod

Programmet börjar med att initialisera systemet, kontrollera och läsa in befintlig bordsinformation från en fil, och sedan visa en meny med alternativ till användaren. Här är en översiktlig pseudokod för detta:

```text
Visa huvudmeny:
    1. Visa alla bord
    2. Ändra bordsinformation
    3. Markera bord som tomt
    4. Ange nota
    5. Avsluta programmet

Läs in användarens val

Hantera användarens val:
    OM användaren väljer 'Visa alla bord'
        Anropa metod för att visa alla bord
    ANNARS OM användaren väljer 'Ändra bordsinformation'
        Läs in bordsnummer, namn, och antal gäster
        Anropa metod för att ändra bordsinformation
    ANNARS OM användaren väljer 'Markera bord som tomt'
        Läs in bordsnummer
        Anropa metod för att markera bord som tomt
    ANNARS OM användaren väljer 'Ange nota'
        Läs in bordsnummer och nota
        Anropa metod för att ange nota
    ANNARS OM användaren väljer 'Avsluta programmet'
        Avsluta programmet
    ANNARS
        Visa felmeddelande och visa menyn igen
```

## Metoderna

För att implementera programmet med metoder, börjar vi med att identifiera de olika delarna av programmet och dela upp dem i separata metoder. Detta gör koden mer lättläst och underlättar förståelsen av hur programmet fungerar. Här är en översiktlig lista över metoder som kan användas för att implementera programmet:

```text
Metod InitieraSystemet()
    Skapa en lista för bordsinformation
    Sätt standardvärdet för varje bord till "0,Inga gäster,0"

Metod KontrolleraOchLäsInBordsinformation()
    Om filen finns
        Läs in filens innehåll till bordsinformation-listan
    Annars
        Skapa filen och fyll med standardvärden

Metod VisaMeny()
    Visa alternativ: Visa alla bord, Ändra bordsinformation, Markera bord som tomt, Ange nota, Avsluta
    Läs in användarens val och anropa motsvarande metod

Metod VisaAllaBord()
    Loopa igenom alla bord och visa information

Metod ÄndraBordsinformation(bordsnummer, namn, antalGäster)
    Uppdatera bordsinformation-listan med nya värden
    Uppdatera lagringsfilen

Metod MarkeraBordSomTomt(bordsnummer)
    Sätt bordsinformation till standardvärdet för tomt bord
    Uppdatera lagringsfilen

Metod AngeNota(bordsnummer, nota)
    Uppdatera bordsinformation med ny nota
    Uppdatera lagringsfilen
```

## Utmaningar

Utmaningarna nedan är utformade för att utöka programmets funktionalitet och testa din förmåga att implementera nya metoder baserat på befintliga koncept.

1. **Nota för varje bord**: Utöka programmet så att varje bord också har en nota associerad med sig. Modifiera lämpliga metoder för att hantera notan.

2. **Dynamisk menyhantering**: Lägg till funktionalitet i menysystemet så att användaren kan ange notan för ett specifikt bord. Detta kräver en ny menyval samt en ny funktion för att hantera detta.

3. **Felförhandling**: Implementera robust felförhandling för användarinmatningar, såsom att säkerställa att bordsnummer och nota är giltiga och inom tillåtna gränser.

## Starta projektet

Starta ett konsolprojekt och kalla det `BordsbokningApp`. Dela upp koden i två delar: `Main` och `Metoder`:

```csharp
/* *************************************************
 * Namn: BordsbokningApp
 * Beskrivning: Ett program för hantering av bordsbokningar
 * Av: [Ditt namn]
 * Datum: 2021-09-01
 *********************************************** */
// Presentation

// Programloop
    // Meny

    // Hantera användarens val

/* ************************************************
 * **************** Metoder ***********************
 ************************************************* */
```
