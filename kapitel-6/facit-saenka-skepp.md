# Facit: Spelet Sänka Skepp  

## Introduktion  

Programmet innehåller:  

- En **spelplan** med storlek **10x10**  
- **Slumpmässigt placerade skepp**  
- Möjlighet för **spelaren att skjuta** genom att ange koordinater  
- **Feedback** på träffar och missar  
- **Spelavslut** när alla skepp har sänkts  

Vi använder en **2D-lista** (`List<List<char>>`) för att representera spelplanen och `Random` för att placera skepp slumpmässigt.  

Vi använder en `tupel` för att definiera skeppen och deras storlekar.\
Och vi använder en `char` för att representera varje skepp på spelplanen. Fördelen med att använda en `char` är att vi kan använda bokstäver för att representera olika skepp.

## Steg 1: Skapa huvudstrukturen  

Programmet börjar med att **skapa spelplanen, placera skepp och starta spelet**.  

```csharp
// Storlek på spelplanen
int storlek = 10;

// Skapa spelplanen
List<List<char>> spelplan = InitialiseraSpelplan(storlek);

// Definiera skeppen och deras storlekar
List<(int storlek, char symbol)> skepp = []
{
    (5, 'B'), // B = Slagskepp
    (4, 'K'), // K = Kryssare
    (3, 'J'), // J = Jagare
    (3, 'J'), 
    (2, 'U')  // U = Ubåt
};

// Placera ut skeppen slumpmässigt
PlaceraSkeppSlump(skepp);

// Starta spelet
Spela();
```

## Steg 2: Skapa spelplanen  

Metoden `InitialiseraSpelplan()` **skapar en tom spelplan** fylld med `'-'`.  

```csharp
static List<List<char>> InitialiseraSpelplan(int storlek)
{
    // Skapa en lista för spelplanen
    List<List<char>> plan = [];

    // Loopa genom raderna
    for (int i = 0; i < storlek; i++)
    {
        // Skapa en rad
        List<char> rad = [];

        // Fyll raden med '-'
        for (int j = 0; j < storlek; j++)
        {
            rad.Add('-');
        }

        // Lägg till raden i spelplanen
        plan.Add(rad);
    }

    return plan;
}
```

## Steg 3: Placera skepp slumpmässigt  

Metoden `PlaceraSkeppSlump()` **försöker slumpa ut skepp tills de får plats**.  

```csharp
static void PlaceraSkeppSlump(List<(int storlek, char symbol)> skepp)
{
    // Slumpgenerator
    Random slump = [];

    // Loopa igenom varje skepp
    foreach (var (skeppStorlek, symbol) in skepp)
    {
        bool placerat = false;

        // Försök att placera skeppet tills det lyckas
        while (!placerat)
        {
            // Slumpa startposition
            int x = slump.Next(10);
            int y = slump.Next(10);

            // Slumpa riktning (horisontell eller vertikal)
            bool horisontell = slump.Next(2) == 0;

            // Kontrollera om skeppet får plats
            if (KanPlaceraUtSkepp(x, y, skeppStorlek, horisontell))
            {
                // Placera skeppet
                PlaceraSkepp(x, y, skeppStorlek, symbol, horisontell);
                placerat = true;
            }
        }
    }
}
```

## Steg 4: Kontrollera om skepp kan placeras  

Metoden `KanPlaceraUtSkepp()` **kontrollerar om ett skepp får plats på spelplanen**.  

```csharp
static bool KanPlaceraUtSkepp(int x, int y, int storlek, bool horisontell)
{
    // Kontrollera om skeppet går utanför spelplanen
    if (horisontell && x + storlek > 10)
    {
        return false;
    }
    
    if (!horisontell && y + storlek > 10)
    {
        return false;
    }

    // Kontrollera att skeppet inte krockar med andra skepp
    for (int i = 0; i < storlek; i++)
    {
        int dx = horisontell ? x + i : x;
        int dy = horisontell ? y : y + i;

        if (spelplan[dy][dx] != '-')
        {
            return false;
        }
    }

    return true;
}
```

## Steg 5: Placera skeppet  

Metoden `PlaceraSkepp()` **lägger in skeppet på spelplanen**.  

```csharp
static void PlaceraSkepp(int x, int y, int storlek, char symbol, bool horisontell)
{
    for (int i = 0; i < storlek; i++)
    {
        int dx = horisontell ? x + i : x;
        int dy = horisontell ? y : y + i;

        // Placera skeppet på spelplanen
        spelplan[dy][dx] = symbol;
    }
}
```

## Steg 6: Spela spelet  

Metoden `Spela()` **låter spelaren skjuta tills alla skepp är sänkta**.  

```csharp
static void Spela()
{
    int träffar = 0;
    int totalSkeppsrutor = 17;

    while (träffar < totalSkeppsrutor)
    {
        RitaSpelplan();

        // Läs in X-koordinat
        Console.Write("Ange X-koordinat (0-9): ");
        int x = int.Parse(Console.ReadLine()!);

        // Läs in Y-koordinat
        Console.Write("Ange Y-koordinat (0-9): ");
        int y = int.Parse(Console.ReadLine()!);

        // Kontrollera om det är en träff
        if (spelplan[y][x] != '-' && spelplan[y][x] != 'X')
        {
            Console.WriteLine("Träff!");
            spelplan[y][x] = 'X';
            träffar++;
        }
        else
        {
            Console.WriteLine("Miss!");
        }
    }

    SpeletSlut();
}
```

## Steg 7: Kontrollera om spelet är slut  

Metoden `SpeletSlut()` **avslutar spelet när alla skepp är sänkta**.  

```csharp
static void SpeletSlut()
{
    Console.WriteLine("Alla skepp är sänkta! Du vann!");
}
```

## Steg 8: Rita ut spelplanen  

Metoden `RitaSpelplan()` **visar spelplanen i konsolen**.  

```csharp
static void RitaSpelplan()
{
    Console.WriteLine();
    Console.WriteLine("  0 1 2 3 4 5 6 7 8 9");

    for (int y = 0; y < 10; y++)
    {
        Console.Write(y + " ");

        for (int x = 0; x < 10; x++)
        {
            char tecken = spelplan[y][x];

            // Visa X för träffade skepp, annars "-"
            if (tecken == 'X')
            {
                Console.Write("X ");
            }
            else
            {
                Console.Write("- ");
            }
        }

        Console.WriteLine();
    }
}
```