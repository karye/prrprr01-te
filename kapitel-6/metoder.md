---
description: Metoder hjälper till att organisera och strukturera koden. 
---
# Metoder

## Youtube video

Här är en video som går igenom hur metoder fungerar i C#.

{% embed url="https://youtu.be/bPQx0paXrbw" %}
How to Program in C# - Methods
{% endembed %}

## Enkla metoder

En metod är ett sätt att **återanvända** kod. Det är en samling av instruktioner som kan anropas från andra delar av koden. Metoder har som regel bara en uppgift.

```csharp
/// <summary>
/// Skapa en metod som skriver ut "Hej världen!"
/// </summary>
static void Hej()
{
    Console.WriteLine("Hej världen!");
}

/* Huvudprogrammet */
// Här anropar vi metoden
Hej();
```

En mer användbar metod som skriver ut en färdig meny:

```csharp
/// <summary>
/// Skapa en metod som skriver ut en meny
/// </summary>
static void SkrivUtMeny()
{
    Console.WriteLine("1. Lägg till todo");
    Console.WriteLine("2. Lista todos");
    Console.WriteLine("3. Avsluta");
}

/* Huvudprogrammet */
// Här anropar vi metoden
SkrivUtMeny();
```

## Metoder med parametrar

Metoder kan ta emot parametrar. Parametrar är värden som skickas till metoden när den anropas. Parametrar kan användas för att skicka data till metoden, som metoden sedan kan använda för att utföra sin uppgift. Parametrar kan vara av vilken typ som helst - allt från enkla datatyper som strängar och tal, till mer komplexa datatyper som objekt och samlingar.

```csharp
/// <summary>
/// Skapa en metod som skriver ut en hälsning
/// </summary>
/// <param name="namn">Namnet på personen</param>
static void SkrivHälsning(string namn)
{
    Console.WriteLine($"Hej {namn}, hur mår du?");
}

/* Huvudprogrammet */
// Här anropar vi metoden
SkrivHälsning("Kalle");
SkrivHälsning("Lisa");
```

I konsolen:
```text
Hej Kalle, hur mår du?
Hej Lisa, hur mår du?
```

### Parametrar med flera värden

En metod kan ta emot flera parametrar. Parametrarna separeras med kommatecken.

```csharp
/// <summary>
/// Skapa en metod som skriver ut en person
/// </summary>
/// <param name="namn">Namnet på personen</param>
/// <param name="ålder">Åldern på personen</param>
static void SkrivPerson(string namn, int ålder)
{
    Console.WriteLine($"Personen heter {namn} och är {ålder} år gammal");
}

/* Huvudprogrammet */
// Här anropar vi metoden
SkrivPerson("Kalle", 30);
SkrivPerson("Lisa", 25);
```

## Uppgifter

* Skapa ett nytt konsolprojekt **EnMassaMetoder**.

### Uppgift 1
* Skapa en metod som tar emot ett *heltal* och skriver ut om talet är jämnt eller udda.\
Tips: Använd operatorn `%` som ger resten vid heltalsdivision. Dvs om `tal % 2` är 0 är talet jämnt, annars är det udda.

Exempelvis (om talet är 5 och 6):
```text
5 är udda
6 är jämnt
```

### Uppgift 2
* Skapa en metod som omvandlar temperatur från Celsius till Fahrenheit. Formeln är `F = C * 9/5 + 32`.\
Dvs metoden tar emot ett *heltal* och skriver ut motsvarande temperatur i Fahrenheit.

Exempelvis (om talet är 20):
```text
20 grader Celsius motsvarar 68 grader Fahrenheit
```

### Uppgift 3
* Skapa en metod som tar emot ett *heltal* och skriver ut så många *#*-tecken på samma rad.\
Tips: Använd en `for`-loop.

Exempelvis (om talet är 6):
```text
######
```

### Uppgift 4
* Skapa en metod som tar emot ett *heltal* och skriver ut alla tal från 1 till det givna talet, en på varje rad.\
Tips: Använd en `for`-loop.

Exempelvis (om talet är 4):
```text
1
2
3
4
```

### Uppgift 5
* Skapa en metod som tar emot ett *heltal* och en *text* och skriver ut texten så många gånger som talet anger.\
Tips: Använd en `for`-loop.

Exempelvis (om talet är 3 och texten är "Hej"):
```text
Hej
Hej
Hej
```

### Uppgift 6

* Skapa en metod som tar emot ett *heltal* och skriver ut ett slumptal (1-6) så många gånger som talet anger.\
Tips: Använd `Random` se [dokumentationen](https://csharp.progdocs.se/grundlaeggande/slump)

Exempelvis (om talet är 5):
```text
Tärningen visar 3
Tärningen visar 6
Tärningen visar 1
Tärningen visar 4
Tärningen visar 2
```

### Uppgift 7

* Skapa en metod som tar emot två *heltal* och skriver det största av dem.\
Tips: Använd en `if`-sats.

Exempelvis (om talen är 5 och 10):
```text
10 är störst
```

### Uppgift 8

* Skapa en metod som tar emot två *heltal* och skriver ut alla tal mellan dem.\
Tips: Använd en `for`-loop.

Exempelvis (om talen är 5 och 10):
```text
6
7
8
9
```

### Uppgift 9

* Skapa en metod som tar emot ett *heltal* och skriver ut Fibonacci-talen upp till det givna talet.\
Fibonacci-talen är en serie där varje tal är summan av de två föregående talen. Serien börjar med 0 och 1.\
Exempel: 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, ...

Exempelvis (om talet är 10):
```text
0 1 1 2 3 5 8
```

### Uppgift 10
* Skapa en metod som tar emot ett heltal och skriver ut om talet är positivt, negativt eller noll.\
Tips: Använd `if`-satser för att kontrollera talets värde.

Exempelvis (om talet är -5, 0 och 10):
```text
-5 är negativt
0 är noll
10 är positivt
```

### Uppgift 11
* Skapa en metod som tar emot två strängar och skriver ut den längre av dem.\
Tips: Använd `.Length` för att få reda på strängarnas längd.

Exempelvis (om strängarna är "Hej" och "Hallå där"):
```text
"Hallå där" är längre
```

### Uppgift 12
* Skapa en metod som tar emot ett heltal och skriver ut om talet är ett primtal eller inte.\
Tips: Ett primtal är ett heltal större än 1 som inte har några andra delare än 1 och sig självt.

Exempelvis (om talet är 5 och 4):
```text
5 är ett primtal
4 är inte ett primtal
```

### Uppgift 13
* Skapa en metod som tar emot en `array` av heltal och skriver ut medelvärdet av dessa tal.\
Tips: Använd en loop för att summera talen och dela sedan summan med antalet tal.

Exempelvis (för arrayen {1, 2, 3, 4, 5}):
```text
Medelvärdet är 3
```

### Uppgift 14
* Skapa en metod som tar emot ett heltal n och skriver ut de första n kvadrattalen.\
Kvadrattal är tal som är kvadrater av heltal (1, 4, 9, 16, ...).

Exempelvis (om n är 4):
```text
1 4 9 16
```

### Uppgift 15
* Skapa en metod som tar emot ett heltal och skriver ut en triangel med så många rader som talet anger.\
Använd `*` för att rita triangeln.

Exempelvis (om talet är 3):
```text
*
**
***
```

## Metoder med returvärden

Utöver att ta emot parametrar kan metoder också returnera värden. När en metod returnerar ett värde, betyder det att den producerar ett resultat som kan användas någon annanstans i koden.

```csharp
/// <summary>
/// Skapa en metod som summerar två tal
/// </summary>
/// <param name="tal1">Första talet</param>
/// <param name="tal2">Andra talet</param>
static int Summa(int tal1, int tal2)
{
    return tal1 + tal2;
}

/* Huvudprogrammet */
int resultat = Summa(5, 10);
Console.WriteLine(resultat);

resultat = Summa(10, 15);
Console.WriteLine(resultat);
```

I konsolen:
```text
15
25
```

Tack vare parametrar och returvärden är metoder extremt kraftfulla verktyg i programmering. De kan göra ditt program mer organiserat, mer lättläst och mer återanvändbart. Dessutom kan de göra din kod mer robust och mindre benägen för fel, eftersom du kan isolera och testa individuella metoder separat från resten av ditt program.

## Exempel på praktiska metoder

### Läsa in ett tal 

En vanlig operation i konsolprogram är att läsa in en användares inmatning och konvertera den till ett numeriskt värde. Detta kan vara lite knepigt eftersom användaren kan skriva in något som inte är ett giltigt tal. Vi kan skapa en metod som hanterar detta och fortsätter att fråga användaren tills ett giltigt tal har matats in.

```csharp
/// <summary>
/// Läser in ett heltal från användaren
/// </summary>
/// <returns>Ett heltal</returns>
static int LäsInHeltal()
{
    int tal;
    while (!int.TryParse(Console.ReadLine(), out tal))
    {
        Console.WriteLine("Felaktig inmatning. Försök igen.");
    }
    return tal;
}

/* Huvudprogrammet */
Console.WriteLine("Skriv in ett heltal:");
int tal = LäsInHeltal();
Console.WriteLine($"Du skrev in {tal}");
``` 

I konsolen:

```text
Skriv in ett heltal:
5
Du skrev in 5
```

### Läsa in ett heltal med en parameter

Vi kan förbättra vår `LäsInHeltal`-metod genom att lägga till en parameter som låter oss anpassa instruktionsmeddelandet varje gång vi anropar metoden:

```csharp
/// <summary>
/// Läser in ett heltal från användaren
/// </summary>
/// <param name="fråga">Instruktionsmeddelande</param>
/// <returns>Ett heltal</returns>
static int LäsInHeltal(string fråga)
{
    Console.WriteLine(fråga);
    int tal;
    while (!int.TryParse(Console.ReadLine(), out tal))
    {
        Console.WriteLine(fråga);
    }
    return tal;
}

/* Huvudprogrammet */
int tal = LäsInHeltal("Skriv in ett heltal:");
Console.WriteLine($"Du skrev in {tal}");
```

I konsolen:

```text
Skriv in ett heltal:
5
Du skrev in 5
```

## Uppgifter

Skapa ett nytt konsolprojekt **MetoderFörText**.

### Uppgift 16

* Skapa en metod som tar emot en *sträng* och returnerar antalet bokstäver i strängen.\
Tips: Använd `Length`-egenskapen för strängar.

### Uppgift 17

* Skapa en metod som tar emot en *sträng* och returnerar antalet vokaler i strängen.\
Tips: Använd en `foreach`-loop för att gå igenom varje tecken i strängen och räkna antalet vokaler.

### Uppgift 18

* Skapa en metod som tar emot en *sträng* och returnerar antalet konsonanter i strängen.\
Tips: Använd en `foreach`-loop för att gå igenom varje tecken i strängen och räkna antalet konsonanter.

### Uppgift 19

* Skapa en metod som tar emot en *sträng* och returnerar antalet siffror i strängen.\
Tips: Använd en `foreach`-loop för att gå igenom varje tecken i strängen och räkna antalet siffror.

### Uppgift 20

* Skapa en metod som tar emot en *sträng* och returnerar antalet ord i strängen.\
Tips: Använd `Split`-metoden för att dela upp strängen i ord.


## Metodöverlagring

Metodöverlagring (eng. method overloading) innebär att flera metoder har samma namn men olika parameterlistor. Man kan säga att de har olika "signaturer". Detta kan vara användbart när metoder utför liknande uppgifter men behöver olika typer eller antal parametrar.

Låt oss titta på ett exempel:

```csharp
/// <summary>
/// Skapa en metod som summerar två tal
/// </summary>
/// <param name="tal1">Första talet</param>
/// <param name="tal2">Andra talet</param>
/// <returns>Summan av talen</returns>
static int Summa(int tal1, int tal2)
{
    return tal1 + tal2;
}

/// <summary>
/// Skapa en metod som summerar tre tal
/// </summary>
/// <param name="tal1">Första talet</param>
/// <param name="tal2">Andra talet</param>
/// <param name="tal3">Tredje talet</param>
/// <returns>Summan av talen</returns>
static int Summa(int tal1, int tal2, int tal3)
{
    return tal1 + tal2 + tal3;
}

/* Huvudprogrammet */
int summa1 = Summa(5, 10);
Console.WriteLine(summa1);  // Skriver ut 15

int summa2 = Summa(5, 10, 15);
Console.WriteLine(summa2);  // Skriver ut 30
```

I detta exempel har vi två versioner av metoden `Summa`. Den första versionen tar två heltalsparametrar och returnerar deras summa. Den andra versionen tar tre heltalsparametrar och returnerar deras summa. Beroende på hur många argument vi skickar till `Summa`-metoden, kommer den korrekta versionen att anropas.