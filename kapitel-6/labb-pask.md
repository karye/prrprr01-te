---
description: Påskäggsjakt i konsolen
---

# Labb: Påskäggsjakten

## Laborationsuppgift

I denna laboration ska du skapa ett konsolbaserat program som simulerar en påskäggsjakt. Användaren ska kunna "hitta" påskägg, "äta" påskägg och hålla koll på hur många påskägg som finns kvar. Programmet ska erbjuda en enkel meny där användaren kan välja mellan olika alternativ.

## Pseudokod

Här är en översiktlig beskrivning av hur programmet ska fungera:

```text
Initiera antal påskägg till 0

Loopa så länge användaren inte väljer att avsluta programmet

Visa en meny för användaren
    1. Hitta påskägg
    2. Äta påskägg
    3. Visa antal påskägg
    4. Avsluta programmet
    Läs användarens val

Hantera användarens val
    Om val 1
        Öka antal påskägg med ett slumpmässigt antal
    Om val 2
        Minska antal påskägg med ett slumpmässigt antal, visa varning om användaren äter för många
    Om val 3
        Visa det nuvarande antalet påskägg
    Om val 4
        Avsluta programmet
```

## Instruktioner

1. **Presentation:** Starta med att skriva en introduktion till programmet som förklarar vad användaren kan göra.
2. **Meny:** Implementera en enkel meny där användaren kan välja mellan att "hitta påskägg", "äta påskägg", "visa antal påskägg", och "avsluta programmet".
3. **Hantera Val:** Använd `switch`-sats eller `if`-satser för att hantera användarens val baserat på menyvalet.
4. **Slumpmässiga Händelser:** Använd `Random.Shared.Next()` för att slumpmässigt bestämma hur många påskägg användaren hittar eller äter.

## Metoder

1. `Presentation()` - Visar en introduktion till programmet
2. `Meny()` - Visar menyalternativ och returnerar användarens val
3. `HittaPåskägg(int antalPåskägg)` - Ökar antalet påskägg med ett slumpmässigt antal och returnerar det nya antalet
4. `ÄtaPåskägg(int antalPåskägg)` - Minskar antalet påskägg med ett slumpmässigt antal, varnar om användaren äter för många, och returnerar det nya antalet
5. `VisaAntalPåskägg(int antalPåskägg)` - Visar det nuvarande antalet påskägg

### Pseudokod för metoderna

```text
Metod för presentation:
    Skriv ut välkomstmeddelande och instruktioner
```

```text
Metod för meny:
    Visa menyalternativ
    Läs och returnera användarens val
```

```text
Metod för att hitta påskägg:
    Slumpa fram ett tal mellan 1 och 5
    Lägg till det slumpade talet till antalet påskägg
    Meddela användaren hur många påskägg som hittats
    Returnera det nya antalet påskägg
```

```text
Metod för att äta påskägg:
    Om antalet påskägg är 0, visa meddelande om att det inte finns några påskägg att äta
    Annars, slumpa fram ett tal baserat på det nuvarande antalet påskägg
    Minska antalet påskägg med det slumpade talet
    Om antalet ätna påskägg är över 3, visa varning om ont i magen
    Returnera det nya antalet påskägg
```

## Starta projektet

Starta ett konsolprojekt och kalla det `Påskäggsjakten`.

Dela upp programmet i flera metoder för att göra det mer lättläst och strukturerat:

```csharp
/* *************************************************
 * Namn: Påskäggsjakten
 * Beskrivning: Ett program som simulerar en påskäggsjakt
 * Av: [Ditt namn]
 * Datum: 2021-09-01
 *********************************************** */
// Presentation

// Programloop
    // Meny

    // Hantera användarens val

/* ************************************************
 * **************** Metoder ***********************
 ************************************************* */
```

## ExtraUppgifter

* Slumpa olika stora påskägg.
* Använd en `List<>` för att spara påskäggen istället för en int.