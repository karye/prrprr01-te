---
description: Topdown-strategin är en metod för att bryta ner ett problem i mindre delar. I denna labb ska du skapa en miniräknare med topdown-strategin
---

# Labb: Bygga en miniräknare med topdown-strategi

## Mål
I denna labb ska du träna på att **bryta ner ett problem i mindre delar** enligt **topdown-metoden**. Du ska skapa en **miniräknare** där användaren kan välja olika matematiska operationer.  
**Efteråt ska du jämföra det med att skriva all kod i en enda stor metod.**

## Steg 1: Planering
Innan du börjar koda ska du **skriva pseudokod** för programmet. Miniräknaren ska kunna:

1. **Visa en meny**
2. **Utföra en addition**
3. **Utföra en subtraktion**
4. **Utföra en multiplikation**
5. **Utföra en division**
6. **Avsluta programmet**

### Pseudokod

Här är en översiktlig pseudokod för programmet:

```
Visa en välkomsttext

Starta en loop som körs tills användaren väljer att avsluta programmet

    Visa meny och läs in användarens val
        1. Addition
        2. Subtraktion
        3. Multiplikation
        4. Division
        5. Avsluta programmet

    OM användaren väljer 1
        Läs in två tal och räkna ut summan
    ANNARS OM användaren väljer 2
        Läs in två tal och räkna ut differensen
    ANNARS OM användaren väljer 3
        Läs in två tal och räkna ut produkten
    ANNARS OM användaren väljer 4
        Läs in två tal och räkna ut kvoten
    ANNARS OM användaren väljer 5
        Avsluta programmet
    ANNARS
        Visa felmeddelande
```

## Steg 2: Implementera med topdown-strategin
Nu ska du **skriva programmet enligt topdown-strategin**. Det innebär att du skapar en **huvudmetod** som anropar flera mindre metoder.

**Skriv följande program i en ny fil i VS Code:**

```csharp
/* *************************************************
 * Namn: Miniräknare i konsolen
 * Beskrivning: Ett program för att utföra matematiska operationer
 * Datum: 2025-01-01
*********************************************** */

VisaVälkomsttext();

while (true)
{
    VisaMeny();
    string val = Console.ReadLine();

    if (val == "1")
    {
        Addition();
    }
    else if (val == "2")
    {
        Subtraktion();
    }
    else if (val == "3")
    {
        Multiplikation();
    }
    else if (val == "4")
    {
        Division();
    }
    else if (val == "5")
    {
        Console.WriteLine("👋 Tack för att du använde miniräknaren!");
        break;
    }
    else
    {
        Console.WriteLine("⚠️ Ogiltigt val. Försök igen.");
    }
}

/* ************************************************
 * **************** Metoder ***********************
 ************************************************* */
```

## Steg 3: Implementera metoderna

Här ser ut körningsexempel på hur programmet kan fungera:

```bash
🧮 Miniräknare

1. Addition
2. Subtraktion
3. Multiplikation
4. Division
5. Avsluta
Ange ditt val: 1
====================
Ange tal 1: 5
Ange tal 2: 3
Svar: 5 + 3 = 8

1. Addition
2. Subtraktion
3. Multiplikation
4. Division
5. Avsluta
Ange ditt val: 2
====================
Ange tal 1: 5
Ange tal 2: 3
Svar: 5 - 3 = 2

1. Addition
2. Subtraktion
3. Multiplikation
4. Division
5. Avsluta
Ange ditt val: 3
====================
Ange tal 1: 5
Ange tal 2: 3
Svar: 5 * 3 = 15

1. Addition
2. Subtraktion
3. Multiplikation
4. Division
5. Avsluta
Ange ditt val: 4
====================
Ange tal 1: 5
Ange tal 2: 3
Svar: 5 / 3 = 1,66666666666667

1. Addition
2. Subtraktion
3. Multiplikation
4. Division
5. Avsluta
Ange ditt val: 5
👋 Tack för att du använde miniräknaren!
```

## Utmaning för dig som vill ha extrauppgifter

* Spara alla beräkningar i en lista och skriv ut dem när användaren väljer att avsluta programmet.

