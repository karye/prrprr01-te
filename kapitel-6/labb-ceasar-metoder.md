---
description: Cesar-kryptot program med metoder.
---

# Labb - Ceasar-kryptot med metoder

![](../.gitbook/assets/image-109.png)

## Introduktion

I denna laboration kommer vi att utforska Ceasar-kryptot, en av de enklaste och mest välkända krypteringsmetoderna. Ceasar-kryptot är en typ av substitutionssiffra där varje bokstav i klartexten byts ut mot en bokstav med ett fast antal positioner längre ned i alfabetet. Till exempel, med en förskjutning på 3, skulle bokstaven 'A' krypteras till 'D', 'B' till 'E', och så vidare. Målet med denna labb är att träna på användningen av arrayer, strängar, loopar, och metoder i C# genom att bygga ett enkelt program för Ceasar-kryptering.

## Pseudokod

Här följer en översiktlig pseudokod för programmet. Den är avsiktligt ofullständig för att ge utrymme för eleverna att utforska och komma fram till egna lösningar.

```text
Visa huvudmeny:
    1. Kryptera meddelande
    2. Avkryptera meddelande
    3. Avsluta programmet
Läs in användarens val

OM användaren väljer 'Kryptera meddelande'
    Anropa metod för att kryptera meddelandet
ANNARS OM användaren väljer 'Avkryptera meddelande'
    Anropa metod för att avkryptera meddelandet
ANNARS OM användaren väljer 'Avsluta programmet'
    Avsluta programmet
ANNARS
    Visa felmeddelande och visa menyn igen
```

## Metoderna

```text
Metod för att kryptera meddelandet:
    Läs in meddelande från användaren
    Läs in nyckel
    För varje bokstav i meddelandet
        Beräkna den krypterade bokstaven 
        Lägg till den krypterade bokstaven i det krypterade meddelandet
    Skriv ut det krypterade meddelandet

Metod för att avkryptera meddelandet:
    Läs in det krypterade meddelandet från användaren
    Läs in nyckel
    För varje bokstav i det krypterade meddelandet:
        Beräkna den avkrypterade bokstaven
        Lägg till den avkrypterade bokstaven i det avkrypterade meddelandet
    Skriv ut det avkrypterade meddelandet

Metod för att läsa in ett heltal från användaren:
    Läs in en sträng från användaren
    Om strängen går att konvertera till ett heltal
        Returnera det konverterade heltalet
    Annars
        Visa felmeddelande och be användaren mata in ett nytt heltal
```

## Tips

* Loopa igenom varje bokstav i en sträng med en `foreach`-loop.
* Omvandla en bokstav till dess ASCII-värde med `(int)`-operatorn,\
  Tex: `int ascii = (int) bokstav;`
* Omvandla ett ASCII-värde till en bokstav med `(char)`-operatorn,\
  Tex: `char bokstav = (char) värde;`
* Använd `int.TryParse()` för att konvertera en sträng till ett heltal.

## Starta projektet

Starta ett konsolprojekt och kalla det `CeasarKryptoApp`.\
Dela upp koden i två delar: Main och Metoder:

```csharp
/* *************************************************
 * Namn: CeasarKryptoApp
 * Beskrivning: Ett program för Ceasar-kryptering
 * Datum: 2021-09-01
 *********************************************** */
Console.Clear();
Console.WriteLine("Välkommen till Ceasar-kryptot!");

// Programloop som körs tills användaren väljer att avsluta
    // Visa huvudmeny
    // mm..

/* **********************************************
 *  Metoder 
 ********************************************** */
```

