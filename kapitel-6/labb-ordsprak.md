---
description: Skapa ett program som kan hantera ett register med ordspråk.
---
# Labb: ordspråksregister

![](../.gitbook/assets/image-103.png)

## Laborationsuppgift

Skapa en ny konsolapplikation och kalla den **OrdsprakRegister**. I denna laboration ska du skapa ett program som kan hantera ett register med ordspråk.

## Flödesschema

Det här flödesschemat visar hur programmet är tänkt att fungera:

```mermaid
graph TD
    A[Visa meny] -->|Spara ordspråk| B(Fråga användaren om ordspråk)
    B --> C(Lägg till ordspråk i listan)
    C --> D(Spara ordspråk till filen ordsprak.txt)
    D --> E(Gå tillbaka till menyn)
    A -->|Läs alla ordspråk| F(Skriv ut alla ordspråk i listan)
    F --> G(Gå tillbaka till menyn)
    A -->|Slumpa fram ett ordspråk| H(Välj ett ordspråk slumpmässigt från listan)
    H --> I(Skriv ut ordspråk)
    I --> J(Gå tillbaka till menyn)
    A -->|Sök efter ordspråk| K(Fråga användaren om sökterm)
    K --> L(Sök efter ordspråk som innehåller sökterm)
    L --> M(Skriv ut resultat)
    M --> N(Gå tillbaka till menyn)
```

## Instruktioner

* Skapa en lista/array med namnet `ordspråkLista` som ska innehålla ordspråk.  
  Tips: Du kan använda dig av `List<>` se [dokumentationen](https://csharp.progdocs.se/grundlaeggande/listor-och-arrayer#list).
* Skapa en `while`-loop som kör tills användaren väljer att avsluta programmet.\
  Användaren ska mata in valet som en sträng och det ska jämföras med en `switch`-sats.
* Skapa metoden `SkrivOrdspråk()` som ska fråga användaren om ett ordspråk och lägga till det i listan `ordspråkLista`
* Skapa metoden `ListaOrdspråk()` som skriver ut alla ordspråk i listan numererade.
* Skapa metoden `SlumpaOrdspråk()` som slumpar fram ett ordspråk från listan och skriver ut det.\
  Tips: Tex slumpa såhär: `int slumptal = Random.Shared.Next(0, 100);`
* Skapa metoden `SökOrdspråk()` som frågar användaren efter en sökterm och skriver ut alla ordspråk som innehåller söktermen.
* Skapa en metod `SparaAllaOrdspråk()` som ska skriva alla ordspråk i listan till textfilen **ordsprak.txt**

## Starta projektet

Starta ett konsolprojekt och kalla det `OrdsprakRegister`.\
Dela upp programmet i flera metoder för att göra det mer lättläst och strukturerat:

```csharp
/* *************************************************
 * Namn: Ordspråksregister
 * Beskrivning: Ett program för att hantera ordspråk
 * Av: [Ditt namn]
 * Datum: 2021-09-01
 *********************************************** */
// Presentation

string filnamn = "ordsprak.txt";

// Programloop
    // Meny

    // Hantera användarens val

/* ************************************************
 * **************** Metoder ***********************
 ************************************************* */
```

## Extrauppgifter

* Skapa en metod `LäsInOrdspråk()` som läser in ordspråk från filen **ordsprak.txt** och lägger till dem i listan `ordspråkLista`.
* Skapa en metod `RaderaOrdspråk()` som tar bort ett ordspråk från listan.\
  Tips: Använd `RemoveAt()` eller `Remove()`.
* Skapa en metod `RedigeraOrdspråk()` som låter användaren redigera ett ordspråk i listan.\
  Tips: Först måste användaren söka efter ordspråket och sedan redigera det.

## Facit

Facit till denna laboration hittar du [här](facit-labb-ordsprak.md).