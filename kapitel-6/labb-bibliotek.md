---
description: Hantera ett bokbibliotek
---
# Labb: hantera ett bokbibliotek

![](../.gitbook/assets/image-104.png)

## Mål

Målet med denna laboration är att skapa en konsolapplikation som tillåter användaren att hantera ett bokbibliotek. Applikationen ska erbjuda funktioner för att lägga till böcker, visa alla böcker, söka efter böcker och ta bort böcker från biblioteket.

## Pseudokod

Här är en översiktlig beskrivning av hur programmet ska fungera:

```text
Börja med att definiera en lista som ska hålla alla böcker.

Loopa så länge användaren inte väljer att avsluta programmet
    Visa huvudmenyn med alternativen:
    1. Lägg till en bok
    2. Visa alla böcker
    3. Sök efter en bok
    4. Ta bort en bok
    5. Avsluta

    Läs användarens val

    Hantera användarens val
        Anropa metoden för att lägga till en bok
        Anropa metoden för att visa alla böcker
        Anropa metoden för att söka efter en bok
        Anropa metoden för att ta bort en bok
        Avsluta programmet
```

## Metoderna

```text
Metod för att lägga till en bok:
    Fråga användaren efter bokens titel
    Lägg till titeln i boklistan

Metod för att visa alla böcker:
    LOOP genom alla böcker i listan och skriv ut dem

Metod för att söka efter en bok:
    Fråga användaren efter en sökterm
    Sök igenom listan efter böcker som matchar söktermen och skriv ut dem

Metod för att ta bort en bok:
    Fråga användaren efter titeln på boken som ska tas bort
    Ta bort boken från listan om den finns
```

## Uppgifter

### Steg 1: Skapa projektet
1. Skapa ett nytt konsolprojekt med namnet `BokBibliotek`.
2. Öppna projektet i din utvecklingsmiljö.

### Steg 2: Implementera huvudmenyn
1. I `Program.cs`, skapa en huvudloop som visar en meny med alternativen:
   - Lägg till en bok
   - Visa alla böcker
   - Sök efter en bok
   - Ta bort en bok
   - Avsluta
2. Använd en `switch`-sats för att hantera användarens val.

### Steg 3: Implementera metoderna
Implementera följande metoder med lämpliga argument och returvärden:

1. **LäggTillBok(List<string> böcker):** Frågar användaren efter en boktitel och lägger till den i listan.
2. **VisaAllaBöcker(List<string> böcker):** Skriver ut alla böcker i listan.
3. **SökBok(List<string> böcker):** Frågar användaren efter en sökterm och skriver ut alla böcker som matchar termen.
4. **TaBortBok(List<string> böcker):** Frågar användaren efter titeln på boken som ska tas bort och tar bort den från listan.

### Steg 4: Testa applikationen
Kör din applikation och testa alla funktioner för att säkerställa att de fungerar som förväntat.

## Startkoden

Organisera din kod enligt följande struktur:

```csharp
/* *************************************************
 * Namn: BokBibliotek
 * Beskrivning: Ett program för att hantera ett bokbibliotek
 * Av: [Ditt namn]
 * Datum: 2021-09-01
 *********************************************** */
// Presentation

// Programloop
    // Meny

    // Hantera användarens val

/* ************************************************
 * **************** Metoder ***********************
 ************************************************* */
```

## Uppgifter för vidareutveckling
1. Lägg till felhantering för att hantera situationer där användaren matar in ogiltiga val eller textsträngar.
2. Utöka applikationen så att varje bok även har en författare och ett utgivningsår. Anpassa metoderna för att hantera denna extra information.
3. Implementera funktionen att spara och läsa böckernas information från en fil, så att biblioteket kvarstår mellan körningar av applikationen.



