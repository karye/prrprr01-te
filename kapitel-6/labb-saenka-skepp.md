---
description: Spelet sänka skepp.
---
# Spelet sänka skepp

![](../.gitbook/assets/image-105.png)

## Beskrivning

**Sänka skepp** är ett konsolprogram som simulerar spelet med samma namn, ett taktiskt spel där målet är att sänka motståndarens skepp genom att gissa deras position på ett rutnät. Spelet är textbaserat och spelaren ger kommandon via konsol för att göra sina gissningar och förflytta sina skepp.

Spelet börjar med att båda spelarna placera sina skepp på ett rutnät. Därefter tar spelarna turvis gissar var motståndarens skepp kan vara genom att ange koordinater på rutnätet. Om spelarens gissning träffar ett skepp, får spelaren veta att träff skedde och om det är sänkt eller inte. Spelaren fortsätter tills alla skeppen är sänkta.

Spelet kan innehålla olika svårighetsgrader, som storleken på rutnätet, antalet skepp eller om spelaren ska få hjälp av olika hjälpmedel.
Spelet ska också ha en funktion för att hålla reda på hur många träffar och missar spelaren gör.

## Pseudokod

Här följer en översiktlig pseudokod för programmet. Den är avsiktligt ofullständig för att ge utrymme för eleverna att utforska och komma fram till egna lösningar.

```text
Initialisera spelplan
Placera ut skepp slumpmässigt
Loopa tills spelet är klart
    Rita spelplan
    Läs in koordinater från användaren
    Kontrollera om träff eller miss
    Skriv ut resultat
SpeletSlut
```

## Krav

Konsolprogram kan se ut ungefär så här:

* Skapa en 2D array `spelplan` med storleken 10x10 för spelplanen
* Skapa en array `skepp` med bokstäverna för skeppstyperna och en array `skeppStorlek` med storleken för varje skeppstyp
* Skapa en metod `InitialiseraSpelplan()` som fyller spelplanen med "-" tecken
* Skapa en metod `PlaceraSkeppSlump()` som slumpmässigt placerar ut skeppen på spelplanen
* Skapa en metod `KanPlaceraUtSkepp()` som kontrollerar om ett skepp kan placeras på en viss position på spelplanen
* Skapa en metod `PlaceraSkepp()` som placerar ut ett skepp på spelplanen
* Skapa en metod `Spela()` som loopar tills spelet är klart och låter användaren skjuta på koordinater på spelplanen, och skriver ut om det är träff eller miss
* Skapa en metod `SpeletSlut()` som kontrollerar om spelet är slut och skriver ut resultatet
* Skapa en metod `RitaSpelplan()` som ritar ut spelplanen till konsolen
* I huvudprogrammet anropa metoderna `InitialiseraSpelplan()`, `PlaceraSkeppSlump()` och `Spela()`

Alla metoder är statiska.

## Facit

Facit hittar du [här](facit-saenka-skepp.md)