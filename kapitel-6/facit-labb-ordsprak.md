# Facit: Ordspråksregister

## Introduktion

I denna laboration skapar vi ett program som hanterar ett register med ordspråk. Programmet ska:  

- Lägga till ordspråk  
- Lista alla ordspråk  
- Slumpa fram ett ordspråk  
- Söka efter ordspråk  
- Spara och läsa från en fil  

Vi använder en `List<string>` för att lagra ordspråken och `File`-metoder för att spara och läsa från en fil.  

## Steg 1: Skapa huvudstrukturen

Vi börjar med att definiera **huvudloopen**, filnamn och lista.  

```csharp
string filnamn = "ordsprak.txt";
List<string> ordspråkLista = LäsInOrdspråk();

while (true)
{
    Console.WriteLine();
    Console.WriteLine("Meny:");
    Console.WriteLine("1. Lägg till ordspråk");
    Console.WriteLine("2. Lista alla ordspråk");
    Console.WriteLine("3. Slumpa ordspråk");
    Console.WriteLine("4. Sök efter ordspråk");
    Console.WriteLine("5. Spara ordspråk till fil");
    Console.WriteLine("6. Avsluta");
    Console.Write("Välj ett alternativ: ");
    
    string val = Console.ReadLine()!;
    
    if (val == "1")
    {
        SkrivOrdspråk();
    }
    else if (val == "2")
    {
        ListaOrdspråk();
    }
    else if (val == "3")
    {
        SlumpaOrdspråk();
    }
    else if (val == "4")
    {
        SökOrdspråk();
    }
    else if (val == "5")
    {
        SparaAllaOrdspråk();
    }
    else if (val == "6")
    {
        break;
    }
    else
    {
        Console.WriteLine("Ogiltigt val. Försök igen.");
    }
}
```

## Steg 2: Lägga till ordspråk

Metoden `SkrivOrdspråk()` lägger till ett nytt ordspråk i listan.  

```csharp
static void SkrivOrdspråk()
{
    Console.Write("Ange ett ordspråk: ");
    string ordspråk = Console.ReadLine()!;

    // Lägg till ordspråket i listan om det inte är tomt
    if (ordspråk.Length > 0)
    {
        ordspråkLista.Add(ordspråk);
        Console.WriteLine("Ordspråk tillagt!");
    }
    else
    {
        Console.WriteLine("Ordspråket får inte vara tomt.");
    }
}
```

## Steg 3: Lista alla ordspråk

Metoden `ListaOrdspråk()` skriver ut alla ordspråk **numererade**.  

```csharp
static void ListaOrdspråk()
{
    if (ordspråkLista.Count == 0)
    {
        Console.WriteLine("Inga ordspråk finns i registret.");
        return;
    }

    Console.WriteLine();
    Console.WriteLine("Ordspråk i registret:");
    
    // Skriv ut alla ordspråk i listan
    for (int i = 0; i < ordspråkLista.Count; i++)
    {
        Console.WriteLine(i + 1 + ". " + ordspråkLista[i]);
    }
}
```

## Steg 4: Slumpa fram ett ordspråk

Metoden `SlumpaOrdspråk()` **väljer slumpmässigt ett ordspråk**.  

```csharp
static void SlumpaOrdspråk()
{
    if (ordspråkLista.Count == 0)
    {
        Console.WriteLine("Det finns inga ordspråk att slumpa fram.");
        return;
    }

    // Slumpa fram ett index, dvs ett tal som pekar på en position i listan
    int slumptal = Random.Shared.Next(ordspråkLista.Count);
    string ordspråk = ordspråkLista[slumptal];

    Console.WriteLine("Slumpat ordspråk: " + ordspråk);
}
```

## Steg 5: Sök efter ordspråk

Metoden `SökOrdspråk()` söker efter **alla ordspråk som innehåller en sökterm**.  

```csharp
static void SökOrdspråk()
{
    Console.Write("Ange en sökterm: ");
    string sökterm = Console.ReadLine()!;
    
    List<string> resultat = [];

    // Loopa igenom alla ordspråk och lägg till de som matchar söktermen
    foreach (string ordspråk i ordspråkLista)
    {
        if (ordspråk.Contains(sökterm))
        {
            resultat.Add(ordspråk);
        }
    }

    // Skriv ut alla matchande ordspråk
    if (resultat.Count > 0)
    {
        Console.WriteLine();
        Console.WriteLine("Sökresultat:");
        
        foreach (string ordspråk i resultat)
        {
            Console.WriteLine("- " + ordspråk);
        }
    }
    else
    {
        Console.WriteLine("Inga ordspråk matchade sökningen.");
    }
}
```

## Steg 6: Spara ordspråk till fil**  
Metoden `SparaAllaOrdspråk()` **sparar listan till en fil**.  

```csharp
static void SparaAllaOrdspråk()
{
    File.WriteAllLines(filnamn, ordspråkLista);
    Console.WriteLine("Ordspråk sparade till fil.");
}
```

## Steg 7: Läs in ordspråk från fil

Metoden `LäsInOrdspråk()` **laddar ordspråk från filen vid start**.  

```csharp
static List<string> LäsInOrdspråk()
{
    // Läs in ordspråk från filen om den finns
    if (File.Exists(filnamn))
    {
        return File.ReadAllLines(filnamn).ToList();
    }

    return new List<string>();
}
```

## Extrauppgift: Ta bort ett ordspråk

Metoden `RaderaOrdspråk()` **låter användaren ta bort ett ordspråk**.  

```csharp
static void RaderaOrdspråk()
{
    ListaOrdspråk();

    Console.Write("Ange numret på ordspråket du vill ta bort: ");
    int index = int.Parse(Console.ReadLine()!) - 1;

    if (index >= 0 && index < ordspråkLista.Count)
    {
        ordspråkLista.RemoveAt(index);
        Console.WriteLine("Ordspråk borttaget.");
    }
    else
    {
        Console.WriteLine("Ogiltigt val.");
    }
}
```

## Extrauppgift: Redigera ett ordspråk

Metoden `RedigeraOrdspråk()` **låter användaren ändra ett befintligt ordspråk**.  

```csharp
static void RedigeraOrdspråk()
{
    ListaOrdspråk();

    Console.Write("Ange numret på ordspråket du vill redigera: ");
    int index = int.Parse(Console.ReadLine()!) - 1;

    if (index >= 0 && index < ordspråkLista.Count)
    {
        Console.Write("Ange det nya ordspråket: ");
        string nyttOrdspråk = Console.ReadLine()!;

        if (nyttOrdspråk.Length > 0)
        {
            ordspråkLista[index] = nyttOrdspråk;
            Console.WriteLine("Ordspråk uppdaterat.");
        }
        else
        {
            Console.WriteLine("Ordspråket får inte vara tomt.");
        }
    }
    else
    {
        Console.WriteLine("Ogiltigt val.");
    }
}
```