---
description: Dela data mellan metoder.
---

# Hur dela data med metoder?

Det finns flera sätt att göra variabler beständiga mellan metoder:

* **Returnera** en variabel: Du kan returnera en variabel från en metod och sedan använda den i en annan metod.

* **Returnera flera variabler med tuples**: C# erbjuder ett enkelt sätt att returnera flera värden från en metod genom att använda tuples. Detta gör det möjligt att paketera flera värden i en enda enhet som sedan kan returneras från metoden.

### Returnera en variabel

Du kan returnera en variabel från en metod och sedan använda den i en annan metod. Här är ett exempel på hur du kan returnera en variabel från en metod i C#:

```csharp
/// <summary>
/// Läser in lösenord från fil
/// </summary>
/// <returns>Lösenordet</returns>
static string LäsInLösenordFrånFil()
{
    // Kontrollera om filen finns
    if (!File.Exists("lösenord.txt"))
    {
        // Skapa filen
        File.Create("lösenord.txt").Close();
    }

    // Läs in lösenord från fil
    string lösenord = File.ReadAllText("lösenord.txt");

    // Returnera lösenordet
    return lösenord;
}

/// <summary>
/// Kontrollerar lösenord
/// </summary>
/// <param name="lösenord">Lösenordet</param>
static void KontrolleraLösenord(string lösenord)
{
    Console.WriteLine("Skriv in ditt lösenord: ");
    string användarLösenord = Console.ReadLine();

    if (användarLösenord == lösenord)
    {
        Console.WriteLine("Rätt lösenord!");
    }
    else
    {
        Console.WriteLine("Fel lösenord!");
    }
}

/* Huvudprogrammet */
// Exempel på hur du kan returnera en variabel från en metod
string lösenord = LäsInLösenordFrånFil();

// Anropa metod för att kontrollera lösenordet
KontrolleraLösenord(lösenord);
```

### Returnera flera variabler med tuples

C# tillåter att du returnerar flera värden från en metod med hjälp av tuples. Detta gör det möjligt att skicka tillbaka en grupp av relaterade data utan att behöva skapa en specifik klass eller struktur för ändamålet. Här är ett exempel på hur du kan använda tuples för att returnera flera värden från en metod:

```csharp
/// <summary>
/// Hämtar användarinformation
/// </summary>
/// <returns>Namn och ålder</returns>
static (string, int) HämtaAnvändarInformation()
{
    string namn = "Anna Svensson";
    int ålder = 30;

    // Returnera både namn och ålder som en tuple
    return (namn, ålder);
}

/* Huvudprogrammet */
// Använda tuple för att ta emot flera returvärden från en metod
string namn;
int ålder;
(namn, ålder) = HämtaAnvändarInformation();

Console.WriteLine($"Namn: {namn}, Ålder: {ålder}");
```

{% hint style="info" %}
Att returnera flera värden från en metod med hjälp av tuples är ett kraftfullt sätt att hantera relaterade data i C#. Det gör koden mer läsbar och effektiv genom att minska behovet av ytterligare klasser eller strukturer för dataöverföring mellan metoder.
{% endhint %}

## Konstanter

Konstanter är variabler som inte kan ändras efter att de har tilldelats ett värde. Detta är bra om du vill ha ett värde som inte ska ändras under programkörningen. Konstanter kan vara både lokala och globala.

Här är ett exempel på hur du skapar en lokal konstant i C#:

```csharp
// Skapa en lokal konstant
const string filnamn = "lösenord.txt";

// Läs in text från fil
string text = File.ReadAllText(Filnamn);
```

{% hint style="info" %}
**Varning:** Konstanter måste tilldelas ett värde när de skapas. Du kan inte tilldela ett värde efter att konstanten har skapats.
{% endhint %}

## Enumerationsvariabler

Enumerationsvariabler är en särskild typ av variabler som kan användas för att spara en lista med värden. Detta är bra om du vill ha en lista med värden som inte ska ändras under programkörningen. Enumerationsvariabler kan vara både lokala och globala.

Här är ett exempel på hur du skapar en lokal enumerationsvariabel i C#:

```csharp
// Enumerationsvariabel för filtyp
enum Filtyp
{
    Text,
    Bild,
    Video
}

/* Huvudprogrammet */
// Skapa en lokal enumerationsvariabel
Filtyp filtyp = Filtyp.Text;
```

{% hint style="info" %}
**Varning:** Enumerationsvariabler måste tilldelas ett värde när de skapas. Du kan inte tilldela ett värde efter att enumerationsvariabeln har skapats.
{% endhint %}