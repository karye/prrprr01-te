---
description: Slumpa fram kända citat från textfil
---

# Labb: citatgenerator

![](../.gitbook/assets/image-112.png)

## Laborationsuppgift

I denna laboration ska du skapa ett program som slumpar fram kända citat från en textfil. Programmet ska kunna visa ett slumpat citat och användaren ska kunna välja att visa ett nytt citat eller avsluta programmet.

## Pseudokod

Här är en översiktlig beskrivning av hur programmet ska fungera:

```text
Läs in citaten från textfilen

Loopa så länge användaren inte väljer att avsluta programmet

Visa en meny för användaren
    1. Slumpa fram ett citat och visa det
    1. Visa alla citat 10 st i taget genom bläddring
    1. Hitta citat som matchar en sökterm
    1. Avsluta programmet
    Läs användarens val

    Hantera användarens val
    ...
```

## Instruktioner

Hämta citaten från [denna textfil](https://gist.github.com/erickedji/68802) och spara i en textfilen i ditt projekt.\
Titta igenom textfilen och se hur citaten är strukturerade. Varje citat är skillt med en tom rad.

## Metoder

1. `LäsInCitat()` - Läser in citaten från textfilen och returnerar en lista med citat
  Tips: Använd `File.ReadAllLines()` för att läsa in alla rader från textfilen och hoppa över tomma rader.
1. `SlumpaCitat()` - Slumpar fram ett citat och skriver ut det\
  Tips: Använd `Random.Shared.Next()` för att slumpa fram ett tal mellan 0 och antalet citat
1. `VisaCitat()` - Skriver ut alla citat 10 st i taget genom bläddring
1. `SökCitat()` - Frågar användaren efter en sökterm och skriver ut alla citat som innehåller söktermen

### Psuedokod för metoderna

Så här kan metoderna implementeras:

```text
Metod för att läsa in citat:
    Läs in alla rader från textfilen
    Skapa en lista för citat
    Loopa igenom alla rader
        Om raden inte är tom
            Lägg till raden i en variabel
        Annars
            Lägg till variabeln i listan
    Returnera listan med citat
```

```text
Metod för att slumpa fram ett citat:
    Slumpa fram ett tal mellan 0 och antalet citat
    Skriv ut citatet på det slumpade indexet
```

## Starta projektet

Starta ett konsolprojekt och kalla det `CitatGenerator`.

Dela upp programmet i flera metoder för att göra det mer lättläst och strukturerat:

```csharp
/* *************************************************
 * Namn: CitatGenerator
 * Beskrivning: Ett program för att slumpa fram kända citat
 * Av: [Ditt namn]
 * Datum: 2021-09-01
 *********************************************** */

// Presentation

string filnamn = "quotes.txt";

...

// Programloop
    // Meny

    // Hantera användarens val

/* ************************************************
 * **************** Metoder ***********************
 ************************************************* */
```

## Facit

Facit till denna labb hittar du [här](facit-labb-quotes.md).