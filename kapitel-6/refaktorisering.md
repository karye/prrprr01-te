---
description: Refaktorisering är en viktig del av att skriva och underhålla kod.
---

# Refaktorisering till metoder

![](../.gitbook/assets/image-108.png)

## Introduktion

Refaktorisering är processen att omstrukturera koden utan att ändra dess beteende.

1. Vi kan förbättra koden genom att extrahera **upprepade delar** av koden till metoder. Detta gör koden mer läsbar och underhållbar.

1. Vi kan också extrahera delar av koden till metoder för att göra koden mer **modulär** och återanvändbar.

## Enkelt exempel

Här är ett enkelt exempel på ett konsolprogram med en meny där användaren uppmanas att välja mellan olika alternativ. För varje val måste användaren mata in ett heltal, och programmet använder `int.TryParse()` för att säkerställa att inmatningen är giltig.

```csharp
/* *************************************************
 * Namn: Konsolgrafik
 * Beskrivning: Ett program för att rita konsolgrafik
 * Datum: 2024-09-01
 *********************************************** */

Console.Clear();
Console.WriteLine("""
Program för att rita konsolgrafik
===================================
""");

while (true)
{
    // Skriva ut menyn
    Console.WriteLine("""
    1. Skriv ut fyrkant
    2. Skriv ut triangel
    3. Avsluta
    """);
    Console.Write("Välj ett alternativ: ");
    string val = Console.ReadLine();

    // Hantera användarens val
    if (val == "1")
    {
        Console.Write("Ange ett heltal: ");
        string talString = Console.ReadLine();
        int tal;
        while (!int.TryParse(talString, out tal))
        {
            Console.Write("Ange ett giltigt heltal: ");
            talString = Console.ReadLine();
        }
        
        // Skriv ut fyrkanten
        for (int i = 0; i < tal; i++)
        {
            for (int j = 0; j < tal; j++)
            {
                Console.Write("*");
            }
            Console.WriteLine();
        }
    }
    else if (val == "2")
    {
        Console.Write("Ange ett heltal: ");
        string talString = Console.ReadLine();
        int tal;
        while (!int.TryParse(talString, out tal))
        {
            Console.Write("Ange ett giltigt heltal: ");
            talString = Console.ReadLine();
        }
        
        // Skriv ut triangeln
        for (int i = 0; i < tal; i++)
        {
            for (int j = 0; j <= i; j++)
            {
                Console.Write("*");
            }
            Console.WriteLine();
        }
    }
    else if (val == "3")
    {
        break;
    }
    else
    {
        Console.WriteLine("Ogiltigt alternativ, vg försök igen.");
    }
}

/* **********************************************
 *  Metoder 
 ********************************************** */

```

### Refaktorisering

Vi kan börja med koden som **upprepas** för inmatning av heltal. Vi kan extrahera denna kod till en separat metod `LäsInHeltal()`.

#### Refaktorisera inmatning av heltal

Vi ersätter inmatning av heltal metoden `LäsInHeltal()`, som läser in och validerar inmatningen.

```csharp
static int LäsInHeltal()
{
    string talString = Console.ReadLine();
    int heltal;
    while (!int.TryParse(talString, out heltal))
    {
        Console.Write("Ange ett giltigt heltal: ");
        talString = Console.ReadLine();
    }
    return heltal;
}
```

Vi använder metoden `LäsInHeltal()` för att läsa in heltal i stället för att upprepa koden för inmatning av heltal i varje val.

```csharp
case "1":
    Console.Write("Ange ett heltal: ");
    int tal = LäsInHeltal();

    // Skriv ut fyrkanten
    for (int i = 0; i < tal; i++)
    {
        for (int j = 0; j < tal; j++)
        {
            Console.Write("*");
        }
        Console.WriteLine();
    }
    break;
...
```

#### Refaktorisering av presentationen

Vi extraherar koden som visar programrubriken till en ny metod `Intro()` och anropar denna metod i början av Main.

```csharp
static void Intro()
{
    Console.Clear();
    Console.WriteLine("""
    Program för att rita konsolgrafik
    ===================================
    """);
}
```

Main ser nu ut så här:

```csharp
Intro();

while (true)
{
    ...
}
```

#### Refaktorisera menyvisningen

Vi extraherar koden som visar menyn till en ny metod `Meny()` och anropar denna metod inuti huvudloopen.

```csharp
static void Meny()
{
    Console.WriteLine("""
    1. Skriv ut fyrkant
    2. Skriv ut triangel
    3. Avsluta
    """);
}
```

Main ser nu ut så här:

```csharp
Intro();

while (true)
{
    Meny();
    string val = Console.ReadLine();

    if (val == "1")
    {
        ...
    }
    else if (val == "2")
    {
        ...
    }
    else if (val == "3")
    {
        break;
    }
    else
    {
        Console.WriteLine("Ogiltigt alternativ, vg försök igen.");
    }
}
```

Helt klart ser koden mycket renare och mer läsbar ut efter refaktoriseringen. Genom att extrahera upprepade delar av koden till metoder kan vi göra koden mer modulär och lättare att underhålla.

### Uppgifter

1. Skapa separata metoder för varje val, till exempel `SkrivUtFyrkant()` och `SkrivUtTriangel()`. Anropa dessa metoder från huvudloopen.

1. Lägg till "Skriv ut gran" som ett alternativ i menyn.\
   Skriv en metod för att skriva ut en gran.\
   Tips: Koden för att skriva ut en gran kan se ut så här:

    ```csharp
    // Skriv ut granen
    for (int i = 0; i < tal; i++)
    {
        for (int j = 0; j < tal - i - 1; j++)
        {
            Console.Write(" ");
        }
        for (int j = 0; j < 2 * i + 1; j++)
        {
            Console.Write("*");
        }
        Console.WriteLine();
    }
    for (int i = 0; i < 2; i++)
    {
        for (int j = 0; j < tal - 1; j++)
        {
            Console.Write(" ");
        }
        Console.WriteLine("*");
    }
    ```

1. Kan metoden `Meny()` även hantera inmatning av användarens val?

## Utökat exempel

Här är ett utökat exempel på ett konsolprogram som låter användaren rita olika figurer och spara dem i en fil. Programmet har också en utmaning där användaren ska gissa storleken på en slumpmässigt genererad figur.

```csharp
/* *************************************************
 * Namn: Konsolgrafik
 * Beskrivning: Ett program för att rita konsolgrafik
 * Datum: 2024-09-01
 *********************************************** */

Console.Clear();
Console.WriteLine("""
Program för att rita konsolgrafik
===================================
""");

int poäng = 0; // Poängräkning för utmaningen

while (true)
{
    Console.WriteLine("""
    1. Skriv ut fyrkant
    2. Skriv ut triangel
    3. Skriv ut rektangel
    4. Skriv ut pyramid
    5. Spara figur
    6. Läs in sparad figur
    7. Utmaning: Gissa storleken
    8. Avsluta
    """);

    Console.Write("Välj ett alternativ: ");
    string val = Console.ReadLine();

    if (val == "1")
    {
        Console.Write("Ange storlek: ");
        string input = Console.ReadLine();
        int storlek;
        while (!int.TryParse(input, out storlek))
        {
            Console.Write("Ange ett giltigt heltal: ");
            input = Console.ReadLine();
        }

        Console.Write("Ange ett tecken att rita med: ");
        char tecken = Console.ReadKey().KeyChar;
        Console.WriteLine();

        for (int i = 0; i < storlek; i++)
        {
            for (int j = 0; j < storlek; j++)
            {
                Console.Write(tecken);
            }
            Console.WriteLine();
        }
    }
    else if (val == "2")
    {
        Console.Write("Ange höjd: ");
        string input = Console.ReadLine();
        int höjd;
        while (!int.TryParse(input, out höjd))
        {
            Console.Write("Ange ett giltigt heltal: ");
            input = Console.ReadLine();
        }

        Console.Write("Ange ett tecken att rita med: ");
        char tecken = Console.ReadKey().KeyChar;
        Console.WriteLine();

        for (int i = 0; i < höjd; i++)
        {
            for (int j = 0; j <= i; j++)
            {
                Console.Write(tecken);
            }
            Console.WriteLine();
        }
    }
    else if (val == "3")
    {
        Console.Write("Ange bredd: ");
        string inputBredd = Console.ReadLine();
        int bredd;
        while (!int.TryParse(inputBredd, out bredd))
        {
            Console.Write("Ange ett giltigt heltal: ");
            inputBredd = Console.ReadLine();
        }

        Console.Write("Ange höjd: ");
        string inputHöjd = Console.ReadLine();
        int höjd;
        while (!int.TryParse(inputHöjd, out höjd))
        {
            Console.Write("Ange ett giltigt heltal: ");
            inputHöjd = Console.ReadLine();
        }

        Console.Write("Ange ett tecken att rita med: ");
        char tecken = Console.ReadKey().KeyChar;
        Console.WriteLine();

        for (int i = 0; i < höjd; i++)
        {
            for (int j = 0; j < bredd; j++)
            {
                Console.Write(tecken);
            }
            Console.WriteLine();
        }
    }
    else if (val == "4")
    {
        Console.Write("Ange höjd: ");
        string input = Console.ReadLine();
        int höjd;
        while (!int.TryParse(input, out höjd))
        {
            Console.Write("Ange ett giltigt heltal: ");
            input = Console.ReadLine();
        }

        Console.Write("Ange ett tecken att rita med: ");
        char tecken = Console.ReadKey().KeyChar;
        Console.WriteLine();

        for (int i = 0; i < höjd; i++)
        {
            for (int j = 0; j < höjd - i - 1; j++)
            {
                Console.Write(" ");
            }
            for (int j = 0; j < 2 * i + 1; j++)
            {
                Console.Write(tecken);
            }
            Console.WriteLine();
        }
    }
    else if (val == "5")
    {
        Console.WriteLine("Ange figur att spara:");
        string figur = Console.ReadLine();
        File.WriteAllText("figur.txt", figur);
        Console.WriteLine("Figuren har sparats!");
    }
    else if (val == "6")
    {
        if (File.Exists("figur.txt"))
        {
            Console.WriteLine("Senast sparade figur:");
            Console.WriteLine(File.ReadAllText("figur.txt"));
        }
        else
        {
            Console.WriteLine("Ingen sparad figur hittades.");
        }
    }
    else if (val == "7")
    {
        Random slump = new Random();
        int korrektSvar = slump.Next(3, 10);

        Console.Write("Gissa storleken på figuren: ");
        string input = Console.ReadLine();
        int gissning;
        while (!int.TryParse(input, out gissning))
        {
            Console.Write("Ange ett giltigt heltal: ");
            input = Console.ReadLine();
        }

        if (gissning == korrektSvar)
        {
            Console.WriteLine("Rätt gissat! Du får 1 poäng.");
            poäng++;
        }
        else
        {
            Console.WriteLine($"Fel! Rätt svar var {korrektSvar}.");
        }

        Console.WriteLine($"Nuvarande poäng: {poäng}");

        for (int i = 0; i < korrektSvar; i++)
        {
            for (int j = 0; j < korrektSvar; j++)
            {
                Console.Write("*");
            }
            Console.WriteLine();
        }
    }
    else if (val == "8")
    {
        kör = false;
    }
    else
    {
        Console.WriteLine("Ogiltigt alternativ, försök igen.");
    }

    Console.WriteLine("\nTryck på valfri tangent för att fortsätta...");
    Console.ReadKey();
    Console.Clear();
}


/* **********************************************
 *  Metoder 
 ********************************************** */
```

### Refaktorisering

Vi kan refaktorisera koden genom att extrahera upprepade delar till metoder på samma sätt som i det föregående exemplet.

Men punkt 7 skiljer sig från de andra valen eftersom vi skall hantera poängräkning och slumpmässig generering av figuren. Vi kan extrahera denna funktionalitet till en separat metod `Utmaning()`.

```csharp
static int Utmaning(int poäng)
{
    Random slump = new Random();
    int korrektSvar = slump.Next(3, 10);

    Console.Write("Gissa storleken på figuren: ");
    ...

    return poäng;
}
```

Såhär kan metoden `Utmaning()` användas i huvudloopen:

```csharp
else if (val == "7")
{
    poäng = Utmaning(poäng);
}
```

Påsåvis kan vi uppdatera poängräkningen i huvudloopen!

### Uppgift

* Refaktorisera övriga koden genom att extrahera upprepade delar till metoder.

## Mer komplext exempel

Här är ett mer komplext exempel på ett konsolprogram som låter användaren mata in en lista av heltal och sedan beräknar summan av talen.

### Introduktion

I många program behöver vi hantera **listor** av värden, till exempel siffror, namn eller andra data. I detta avsnitt lär vi oss hur vi kan:

1. **Lägga till och visa värden i en lista.**
2. **Beräkna statistik**, t.ex. medelvärde och högsta/lägsta värde.
3. **Refaktorisera** koden för att göra den mer strukturerad.

Vi bygger ett program där användaren kan mata in tal i en lista, och sedan få en sammanställning av statistiken.

### Ursprungligt konsolprogram

Här är ett program där användaren kan lägga till tal i en lista och sedan få ut statistik:

```csharp
/* *************************************************
 * Namn: Lista och statistik
 * Beskrivning: Ett program där användaren matar in tal 
 * och får ut medelvärde, högsta och lägsta värde.
 *********************************************** */

Console.Clear();
Console.WriteLine("Program för att hantera en lista av tal");
Console.WriteLine("========================================");

List<int> talLista = [];
bool kör = true;

while (kör)
{
    Console.WriteLine("""
    1. Lägg till ett tal
    2. Visa alla tal
    3. Visa statistik
    4. Avsluta
    """);
    Console.Write("Välj ett alternativ: ");
    string val = Console.ReadLine();

    switch (val)
    {
        case "1":
            Console.Write("Ange ett heltal: ");
            string talString = Console.ReadLine();
            int tal;
            while (!int.TryParse(talString, out tal))
            {
                Console.Write("Ange ett giltigt heltal: ");
                talString = Console.ReadLine();
            }
            talLista.Add(tal);
            break;

        case "2":
            Console.WriteLine("Lista över inmatade tal:");
            foreach (int nummer in talLista)
            {
                Console.Write(nummer + " ");
            }
            Console.WriteLine();
            break;

        case "3":
            if (talLista.Count == 0)
            {
                Console.WriteLine("Listan är tom.");
                break;
            }
            int summa = 0, högsta = talLista[0], lägsta = talLista[0];

            foreach (int nummer in talLista)
            {
                summa += nummer;
                if (nummer > högsta) högsta = nummer;
                if (nummer < lägsta) lägsta = nummer;
            }

            double medelvärde = (double)summa / talLista.Count;

            Console.WriteLine($"Antal tal: {talLista.Count}");
            Console.WriteLine($"Högsta värde: {högsta}");
            Console.WriteLine($"Lägsta värde: {lägsta}");
            Console.WriteLine($"Medelvärde: {medelvärde:F2}");
            break;

        case "4":
            kör = false;
            break;

        default:
            Console.WriteLine("Ogiltigt alternativ, försök igen.");
            break;
    }
}


/* **********************************************
 *  Metoder 
 ********************************************** */
```

### Uppgifter

Vi kan förbättra programmet genom att bryta ut kod i **separata metoder**.

1. Skapaen metod för att läsa in ett heltal\
   Vi ersätter kodupprepningen för inmatning av heltal med en metod.

1. Skapaen metod för att visa listan\
   Vi extraherar kod för att visa listan till en metod:

1. Skapaen metod för att beräkna och visa statistik\
   Vi extraherar statistiken till en metod:

### Utmaningar 

1. Lägg till en funktion för att ta bort ett tal från listan.\
   - Skapa en ny menyval **"5. Ta bort ett tal"**.
   - Fråga användaren vilket tal som ska tas bort.
   - Om talet finns i listan, ta bort det. Annars, visa ett meddelande.

1. Lägg till sortering.\
   - Skapa en ny menyval **"6. Visa sorterad lista"**.
   - Visa listan i stigande ordning med `lista.Sort();`.

1. Lägg till en funktion för att rensa listan.\
   - Skapa en ny menyval **"7. Rensa listan"**.
   - Radera alla tal från listan.

1. Utveckla en metod för att hitta det vanligaste talet i listan.\
   - Skapa en metod som räknar hur ofta varje tal förekommer och visar det mest förekommande.
