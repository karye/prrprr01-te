---
description: Träna på att skapa och använda metoder i C# för att strukturera din kod.
---

# Mikroövning: enkla metoder

I denna övning ska du träna på att skapa och använda metoder för att strukturera din kod. Du får lära dig att använda metoder för att hantera inmatning, göra beräkningar och visa resultat.

Till din hjälp: [C#-lathund](https://csharp.progdocs.se/lathund)

## 1. Skapa en metod som skriver ut en hälsning
- Skapa en metod som heter `Hälsa` och skriver ut texten:  
  `"Välkommen till metoder i C#!"`
- Anropa metoden från `Main`.

Exempel:
```
Välkommen till metoder i C#!
```

## 2. Skapa en metod med en parameter
- Utöka metoden `Hälsa` så att den tar emot en parameter som innehåller ett namn och inkluderar namnet i hälsningen.
  
Exempel:
```
Ange ditt namn: Alex
Välkommen till metoder i C#, Alex!
```

## 3. Skapa en metod som returnerar ett värde
- Skapa en metod som heter `Addering` och tar emot två heltal som parametrar.
- Metoden ska returnera summan av de två talen.
- Anropa metoden från `Main` och skriv ut resultatet.

Exempel:
```
Ange första talet: 5
Ange andra talet: 10
Summan är: 15
```

## 4. Skapa en metod för att kontrollera jämna och udda tal
- Skapa en metod som heter `ÄrJämnt` och tar emot ett heltal som parameter.
- Metoden ska returnera `true` om talet är jämnt och `false` om det är udda.
- Anropa metoden och visa resultatet.

Exempel:
```
Ange ett tal: 7
Talet 7 är udda.
```

## 5. Skapa en metod för att skriva ut en multiplikationstabell
- Skapa en metod som heter `Multiplikationstabell` som tar emot ett heltal som parameter.
- Metoden ska skriva ut multiplikationstabellen för det talet (1 till 10).

Exempel:
```
Ange ett tal: 4
4 x 1 = 4
4 x 2 = 8
...
4 x 10 = 40
```

## Valfria förbättringar
1. **Overloads:** Skapa flera versioner av en metod med olika antal parametrar.
2. **Flera parametrar:** Skapa en metod som tar emot tre parametrar och gör en operation med dem (t.ex. addition eller multiplikation).
3. **Rekursiv metod:** Skapa en rekursiv metod som skriver ut en nedräkning från ett visst tal.
4. **Validering:** Lägg till logik i metoderna för att validera inmatningen innan de används.
