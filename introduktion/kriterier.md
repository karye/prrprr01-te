---
description: Betygskriterier för kursen Programmering 1
---

# Betygskriterier för kursen Programmering 1

Förutom att lära sig programmeringsspråket C# och att skapa fungerande program, bedöms även förmågan att följa goda programmeringsvanor.

En annan viktig del av kursen är förmågan att skapa enkla och komplexa program. Detta visas genom koda enkla och komplexa algoritmer.

## Vad betyder “Goda vanor”?

Goda vanor är principer och tekniker som används för att skriva bra och lättläst kod. Det kan till exempel handla om att använda sig av tydliga variabelnamn, att skriva kommentarer och att strukturera koden på ett bra sätt:

* Använd tydliga variabelnamn som beskriver vad variabeln innehåller, t.ex. `antalElever` istället för `a`.
* Följ C#-konventioner för kodning, **camelCase** för variabler och metoder, **PascalCase** för klasser.
* Använd kommentarer för att förklara vad koden gör.
* Använde metodkommentarer för att beskriva vad metoden gör.
* Använd kodindentering för att visa kodens struktur.
* Undvik onödiga kodrader och tomma rader.
* Spara ofta och använd versionshantering för att undvika att förlora kod.
* Dela upp koden i mindre metoder för att göra koden mer lättläst och underhållbar.

Exempel på kod med goda vanor:
```csharp
// Exempel på dåliga och goda variabelnamn
int a = 10; // Dåligt variabelnamn
int antalElever = 10; // Bra variabelnamn
string n1 = "Kalle"; // Dåligt variabelnamn
string namn = "Kalle"; // Bra variabelnamn

// Exempel på camelCase och PascalCase
int AntalElever = 10; // Dåligt, följer inte C#-konventionen
int antalElever = 10; // Bra, camelCase
string[] FinaNamn = [ "Kalle", "Pelle" ]; // Dåligt, följer inte C#-konventionen
string[] finaNamn = [ "Kalle", "Pelle" ]; // Bra, camelCase

/// <summary>
/// Metoden räknar ut medelvärdet av två tal
/// </summary>
/// <param name="tal1">Det första talet</param>
/// <param name="tal2">Det andra talet</param>
/// <returns>Medelvärdet av tal1 och tal2</returns>
public static float Medelvärde(float tal1, float tal2)
{
    return (tal1 + tal2) / 2;
}
```

## Algoritmer

I kursen bedöms även förmågan att skapa enkla och komplexa algoritmer. En algoritm är en uppsättning instruktioner som löser ett problem. Algoritmer kan vara enkla eller komplexa beroende på antalet steg och kontrollstrukturer som används.

### Vad är en enkel algoritm?

En enkel algoritm består av ett steg: det kan vara en enkel if-sats eller en enkel loop

#### Exempel 1

Exempel på en enkel algoritm med en if-sats och en loop:
```csharp
for (int i = 1; i <= 10; i++)
{
    if (i % 2 == 0)
    {
        Console.WriteLine(i);
    }
}
```

#### Exempel 2

Ett till exempel på en enkel algoritm:

```csharp
//Skapar slump index
slumpIndex = Random.Shared.Next(0, frågorLista.Count);

//Om det är slut på frågor
if (frågorLista.Count < 1)
{
    Console.WriteLine("Slut på frågor");
}
else
{
    //Deklarerar den valda frågan och dett rätta svaret
    valdFråga = frågorLista[slumpIndex].Split(';')[0];
    rättSvar = frågorLista[slumpIndex].Split(';')[1];
}
```

#### Exempel 3

Ännu ett exempel på en enkel algoritm:

```csharp
//Loopar igenom alla rader i csv filen
foreach (var profil in allaProfiler)
{
    //Splittar på namnet och rekordet och lägger in det i en array
    namnOchRekord = profil.Split(';');

    //Kollar om användarnamnet finns
    if (användarnamn == namnOchRekord[0])
    {
        //Om det finns bryts man ut ur loopen
        hittadeAnvändarnamnet = true;
        break;
    }
    else
    {
        //Om det inte finns så får användaren skriva namn igen
        hittadeAnvändarnamnet = false;
    }
}
```

Förklaring: I koden ovan finns en enkel algoritm som söker igenom en lista av profiler för att se om ett givet användarnamn finns i listan. Algoritmen består av en enkel loop som går igenom varje profil i listan och jämför användarnamnet med namnet i profilen. Om användarnamnet hittas bryts loopen och variabeln `hittadeAnvändarnamnet` sätts till `true`, annars sätts den till `false`.

### Vad är en komplex algoritm?

En komplex algoritm består av flera steg: det kan vara flera if-satser, loopar och andra konstruktioner som tillsammans löser ett problem:

* En eller flera loopar
* En eller flera if-satser
* I flera nivåer (t.ex. en loop i en loop, med if-sats inuti)

#### Exempel 1

Exempel på en komplex algoritm:
```csharp
bool hittadeAnvändarnamnet = false;
string harProfil;
string[] namnOchRekord = new string[2];
float coins = 100;

//Kollar om csv-filen finns, annars skapar den filen
SökEfterFil();

while (true)
{
    //Frågar om användaren har en profil sen tidigare
    Console.Write("Har du en profil sen tidigare? (j/n) ");
    harProfil = Console.ReadLine().ToLower();

    if (harProfil == "j")
    {
        while (true)
        {
            //Frågar efter användarnamnet
            Console.Write("Vad är ditt användarnamn: ");
            användarnamn = Console.ReadLine();

            //Söker igenom csv-filen och kollar om det givna användarnamnet finns i filen
            namnOchRekord = JämförNamn(namnOchRekord, ref hittadeAnvändarnamnet);

            //Kollar om profilen med användarnamnet finns
            if (hittadeAnvändarnamnet)
            {
                //Om profilen finns så skrivs den ut
                Console.WriteLine("\nLetar efter din profil...");
                Console.WriteLine($"Användarnamn: \"{användarnamn}\" du har {namnOchRekord[1]} coins");

                //Lägger in pengarna som finns sparade på textfilen i variabeln coins
                coins = int.Parse(namnOchRekord[1]);
                Console.ReadKey();
                break;
            }
            else
            {
                //Annars får användaren testa ett annat användarnamn
                Console.WriteLine("\nLetar efter din profil...");
                Console.WriteLine($"Hittade ingen profil med namnet \"{användarnamn}\", testa igen");
            }

        }
        break;
    }
}
```

I koden ovan finns flera if-satser och loopar som tillsammans löser problemet att logga in en användare. Koden innehåller flera nivåer av kontrollstrukturer som tillsammans bildar en komplex algoritm.

#### Exempel 2

Ett till exempel på en komplex algoritm:
```csharp
...
    //Om användaren inte har en profil
    else if (harProfil == "n")
    {
        while (true)
        {
            //Frågar efter användarnamn
            Console.Write("\nVälj användarnamn: ");
            användarnamn = Console.ReadLine();

            //Söker igenom csv-filen och kollar om det givna användarnamnet finns i filen
            JämförNamn(namnOchRekord, ref hittadeAnvändarnamnet);

            if (användarnamn.Length >= 3)
            {
                //Om namnet är upptaget får användaren testa ett annat namn
                if (hittadeAnvändarnamnet)
                {
                    Console.WriteLine("Namnet är upptaget, testa ett annat namn");
                }
                else
                {
                    //Annars skapas en ny profil till användaren

                    //Lägger till den nya profilen i listan av rekord
                    allaProfiler.Add($"{användarnamn];{100}");
                    namnOchRekord[0] = användarnamn;
                    namnOchRekord[1] = "100";

                    //Skriver ut den nya listan av rekord
                    File.WriteAllLines(filVäg, allaProfiler);

                    //Skriver ut att profilen är skapad
                    Console.WriteLine($"Profil skapad, ditt användarnamn är: {användarnamn}");
                    Console.ReadKey();
                    break;
                }
            }
            else
            {
                //Om användarnamnet man anger är för kort
                Program.FelMeddelande("Namnet är för kort");
            }
        }
        break;
    }
    else
    {
        //Om användaren skriver in ogiltigt svarsalternativ
        Program.FelMeddelande("Du måste svara \"j\" eller \"n\"");
    }
}
```
I koden finns flera if-satser, en loop och en metod i flera nivåer som tillsammans löser problemet att skapa en ny profil för en användare. Koden innehåller flera kontrollstrukturer som tillsammans bildar en komplex algoritm.

#### Exempel 3

Ännu ett exempel på en komplex algoritm:
```csharp
if (timeCoolDown <= 0)
{
    //For every cell add all cells in a 3 by 3 area to potential new cells
    foreach (var cell in cells)
    {
        for (var X = -1; X < 2; X++)
        {
            for (var Y = -1; Y < 2; Y++)
            {
                potentialCells.Add(new Vector2(cell.X + X, cell.Y + Y));
            }
        }
    }

    //Count the amount of occurences of a cell in the potential cells list
    foreach (var cell in potentialCells)
    {
        if (!checkedCells.Contains(cell))
        {
            checkedCells.Add(cell);
            int newCellCount = 0;
            foreach (var cell2 in potentialCells)
            {
                if (cell == cell2)
                {
                    newCellCount++;
                }
            }

            //The amount of occurences and wether the cell is live will determine if it will be alive in the next generation
            if ((newCellCount == 3 && !cells.Contains(cell)) || (newCellCount > 2 && newCellCount < 5 && cells.Contains(cell)))
            {
                //newCells is the list of cells that will become live in the next generation
                newCells.Add(cell);
            }
        }
    }

    //Make cells list all the cells that were going to become live and then clear all lists used for calculation
    cells = new List<Vector2>(newCells);
    newCells.Clear();
    potentialCells.Clear();
    checkedCells.Clear();

    //Reset the cooldown according to the target generations per second
    timeCoolDown += 1 / targetGPS;
}
```

Förklaring: I koden ovan finns en komplex algoritm som implementerar spelet Game of Life. Algoritmen består av flera loopar och if-satser som tillsammans simulerar cellernas liv och död i varje generation. Algoritmen innehåller flera nivåer av kontrollstrukturer som tillsammans bildar en komplex algoritm. 