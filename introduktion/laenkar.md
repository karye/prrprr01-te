---
description: Matnyttiga länkar
---

# Länkar

Här är några länkar som kan vara bra att ha när du lär dig C# på egen hand.

## Kurser

* Bra kurs på svenska med uppgifter och uppgifter: https://www.progsharp.se
![alt text](../.gitbook/assets/image-123.png)

* Microsofts online-kurs i C#: https://learn.microsoft.com/sv-se/users/dotnet/collections/yz26f8y64n7k07
![alt text](../.gitbook/assets/image-125.png)

* Youtube-tutorial på engelska C#:
{% embed url="https://youtu.be/GhQdlIFylQ8?si=WI578JzllIkVcCb7" %}

## Dokumentation

* Mickes referens på svenska över det viktigaste delarna i C#: https://csharp.progdocs.se  
![alt text](../.gitbook/assets/image-124.png)

* Dokumentation på hela C#: https://docs.microsoft.com/en-us/dotnet/csharp/
![alt text](../.gitbook/assets/image-126.png)

* Cheat Sheet med bra överblick: https://www.codecademy.com/learn/learn-c-sharp/modules/csharp-hello-world/cheatsheet
![alt text](../.gitbook/assets/image-127.png)