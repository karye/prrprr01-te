# Vad är en god kodstrategi?

Att skriva bra kod handlar inte bara om att få programmet att fungera – det handlar också om att göra koden **lättläst, underhållbar och effektiv**. 

En god kodstrategi handlar om **struktur, läsbarhet och återanvändning**. Genom att följa dessa principer blir din kod **enklare att underhålla, snabbare att förstå och roligare att arbeta med**! 🎯💡🚀

---

## 1. Läsbar kod

Kod skrivs för människor lika mycket som för datorer. Om du (eller någon annan) tittar på din kod om några månader, ska den vara lätt att förstå.

### Så här gör du:
- Använd **beskrivande variabelnamn** som `antalStudenter` istället för `a`.
- Skriv **kommentarer** vid komplex kod.
- Håll kodblock korta och tydliga.

### ❌ Dålig kod:
```csharp
int a = 10;
int b = 20;
int c = a + b;
Console.WriteLine(c);
```

### ✅ Bättre kod:
```csharp
int antalStudenter = 10;
int antalLärare = 20;
int totalPersoner = antalStudenter + antalLärare;
Console.WriteLine($"Totalt antal personer: {totalPersoner}");
```

---

## 2. Dokumentera din kod

Bra kod har korta, men tydliga **kommentarer** där det behövs.

### ✅ Exempel på bra dokumentation:
```csharp
// Denna metod beräknar summan av en lista med heltal
static int BeräknaSumma(List<int> talLista)
{
    return talLista.Sum();
}
```

### ❌ Exempel på dålig dokumentation:
```csharp
// Beräkna summan
static int Summa(List<int> t)
{
    return t.Sum();
}
```

### 💡 Tips:
✔ Kommentera bara om det behövs – tydlig kod behöver ofta färre kommentarer.\
✔ Använd **sammanhängande namn** på metoder och variabler så att koden talar för sig själv.

---

## 3. Använd listor och loopar istället för upprepning

Om du upprepar kod med små variationer, är det ofta bättre att använda **loopar eller listor**.

### ❌ Dålig kod:
```csharp
Console.WriteLine("Ange tal 1:");
int tal1 = int.Parse(Console.ReadLine());

Console.WriteLine("Ange tal 2:");
int tal2 = int.Parse(Console.ReadLine());

Console.WriteLine("Ange tal 3:");
int tal3 = int.Parse(Console.ReadLine());

int summa = tal1 + tal2 + tal3;
Console.WriteLine("Summan är: " + summa);
```

### ✅ Bättre kod med loop:
```csharp
List<int> talLista = new();
for (int i = 1; i <= 3; i++)
{
    Console.Write($"Ange tal {i}: ");
    talLista.Add(int.Parse(Console.ReadLine()));
}
Console.WriteLine($"Summan är: {talLista.Sum()}");
```

### 💡 Fördelar:
✔ Enklare att utöka till fler tal.\
✔ Koden blir kortare och tydligare.

---

## 4. Använd felhantering för att undvika krascher

Använd `try-catch` för att hantera fel och ge användaren tydliga felmeddelanden.

### ❌ Kod utan felhantering kan krascha:
```csharp
Console.Write("Ange ett heltal: ");
int tal = int.Parse(Console.ReadLine());  // Om användaren skriver "hej" kraschar programmet
Console.WriteLine($"Du angav: {tal}");
```

### ✅ Bättre kod med felhantering:
```csharp
static int LäsInSäkertHeltal()
{
    int heltal;
    while (true)
    {
        Console.Write("Ange ett heltal: ");
        if (int.TryParse(Console.ReadLine(), out heltal))
        {
            return heltal;
        }
        Console.WriteLine("Fel! Ange ett giltigt heltal.");
    }
}

int tal = LäsInSäkertHeltal();
Console.WriteLine($"Du angav: {tal}");
```

### 💡 Fördelar:
✔ Användaren får en ny chans att ange rätt inmatning.\
✔ Programmet kraschar inte vid felaktig inmatning.

---

## 5. Testa och debugga regelbundet

- Testa din kod ofta för att upptäcka fel tidigt.
- Använd `Console.WriteLine()` för att se vad som händer i koden.
- Lär dig använda en **debugger** i din kodredigerare (t.ex. VS Code).

---

## 6. Följ DRY-principen ("Don't Repeat Yourself")

Om du märker att du skriver samma kod flera gånger, bör du skapa en metod istället.

### ❌ Dålig kod med upprepning:
```csharp
Console.Write("Ange ett tal: ");
int tal1 = int.Parse(Console.ReadLine());

Console.Write("Ange ett tal: ");
int tal2 = int.Parse(Console.ReadLine());
```

### ✅ Bättre kod med metod:
```csharp
static int LäsInHeltal()
{
    Console.Write("Ange ett tal: ");
    return int.Parse(Console.ReadLine());
}

int tal1 = LäsInHeltal();
int tal2 = LäsInHeltal();
```

### 💡 Fördelar:
✔ Mindre kod att underhålla.\
✔ Om vi behöver ändra hur inmatning fungerar, gör vi det bara på ett ställe.

---

## 7. Dela upp koden i metoder och håll huvudprogrammet kort

Långa kodstycken i `Main` gör programmet svårare att läsa och underhålla. Genom att dela upp koden i **metoder** gör vi den mer organiserad.

### Så här gör du:
- **Håll `Main` så kort som möjligt** – den ska bara anropa metoder.
- **Flytta all logik till separata metoder**.

### ❌ Dålig kod i `Main`:
```csharp
int summa = 0;
for (int i = 0; i < 10; i++)
{
    Console.WriteLine("Ange ett tal:");
    int tal = int.Parse(Console.ReadLine());
    summa += tal;
}
Console.WriteLine("Summan är: " + summa);
```

### ✅ Bättre kod med metoder:
```csharp
static int LäsInTal()
{
    Console.Write("Ange ett tal: ");
    return int.Parse(Console.ReadLine());
}

static int BeräknaSumma(int antal)
{
    int summa = 0;
    for (int i = 0; i < antal; i++)
    {
        summa += LäsInTal();
    }
    return summa;
}

Console.WriteLine($"Summan är: {BeräknaSumma(10)}");
```

### 💡 Fördelar:
✔ `Main` blir kort och tydlig.\
✔ Koden blir mer modulär och lättare att testa.\
✔ `LäsInTal()` kan återanvändas på andra ställen i programmet.

---

## Sammanfattning

| God kodstrategi | Varför? |
|----------------|---------|
| ✅ Läsbar kod | Enklare att förstå och ändra |
| ✅ Dokumentera kod | Gör det lättare att förstå i framtiden |
| ✅ Listor och loopar | Gör koden kortare och mer flexibel |
| ✅ Felhantering | Förhindrar krascher och förbättrar användarupplevelsen |
| ✅ Testa regelbundet | Upptäcker problem tidigt |
| ✅ DRY-principen | Minskar dubbelarbete och buggar |
| ✅ Håll `Main` kort och använd metoder | Strukturerad och lättläst kod |

---