---
description: Vilket programmeringsspråk är populärast?
---

# Bakgrund

## Varför lära sig C#?

* C# är ett **populärt** programmeringsspråk som används tex i många moderna spel
* C# är ett modernt **högnivåspråk** likt C++ och Java
* Genom att lära dig C# kommer du väl förbered till **högskolan** som också använder liknande språk
* Det finns en stor **community** och många fria **resurser** som [nuget.org](https://www.nuget.org/)

## Historien om programmeringsspråk

I programmeringsspråkens barndom fanns bara maskinkod. Maskinkod är binära instruktioner som processorn kan förstå. Det är väldigt svårt att skriva maskinkod och det är svårt att läsa maskinkod. Därför skapades assembler. 
Assembler är ett språk som är lättare att läsa och skriva än maskinkod. Assembler är fortfarande väldigt svårt att läsa och skriva. Efter assembler kom högnivåspråken.
Högnivåspråk är språk som är lättare för människor att läsa och skriva än assembler. Högnivåspråk översätts till assembler av en kompilator. Det finns många olika högnivåspråk. De flesta högnivåspråk är specialiserade på olika saker. C# är ett högnivåspråk som är specialiserat på att skapa program för Windows. 

Listan över programmeringsspråk är lång. Här är en lista över de mest populära programmeringsspråken 1960-2020:
* Basic
* C
* C++
* C#
* Cobol
* Fortran
* Java
* Javascript
* Pascal
* Python
* Visual Basic

{% embed url="https://youtu.be/Og847HVwRSI" %}
Programmeringsspråk popularitet 1960-2020
{% endembed %}
