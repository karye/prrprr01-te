---
description: Exempel på projekt
---
# Konsolprogram

Här är några exempel på konsolprogram:

* [Kontaktuppgiftsprogram](kontakter.md): Skapa ett program som låter användaren mata in och spara kontaktuppgifter (namn, telefonnummer, e-postadress) i en textfil.
* [Budgetsprogram](budget.md): Skapa ett program som håller reda på en budget, där användaren kan lägga till utgifter och inkomster och se sin budgethistorik som sparas i en textfil.
* Skapa ett program som låter användaren lägga till och ta bort böcker från en läslista som sparas i en textfil.
* Skapa ett program som simulerar ett bankkonto, där användaren kan sätta in och ta ut pengar samt visa saldo och transaktionshistorik som sparas i en textfil.
* Skapa ett program som låter användaren lägga till och ta bort maträtter från en veckomeny som sparas i en textfil.
* Skapa ett program som simulerar ett lån, där användaren kan ansöka om ett lån, se sin lånehistorik och göra avbetalningar som sparas i en textfil.
* Skapa ett program som låter användaren lägga till och ta bort produkter från en inköpslista som sparas i en textfil.
* Skapa ett program som håller reda på en träningsdagbok, där användaren kan lägga till och ta bort träningspass och se sin träningshistorik som sparas i en textfil.
* Skapa ett program som låter användaren lägga till och ta bort filmer från en watchlist som sparas i en textfil.
* Skapa ett program som simulerar en lottdragning, där användaren kan köpa lotter och se tidigare dragningar och vinnare som sparas i en textfil.

