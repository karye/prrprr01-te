---
description: Kontaktuppgiftsprogram
---

# Kontaktuppgiftsprogram

![](../../.gitbook/assets/image-100.png)

## Beskrivning

**Kontaktuppgiftsprogram** är ett konsolprogram som låter användaren mata in och spara kontaktuppgifter såsom namn, telefonnummer och e-postadress i en textfil. Programmet är textbaserat och användaren ger kommandon via konsolen för att mata in sina kontaktuppgifter. Programmet har en meny med flera val, såsom att lägga till en ny kontakt, visa alla kontakter eller avsluta programmet.

Användaren kommer att kunna mata in ett namn, telefonnummer och e-postadress för varje kontakt och programmet sparar dessa uppgifter i en textfil. Om filen inte finns kommer programmet att skapa en ny fil, annars kommer det att lägga till de nya kontaktuppgifterna i befintlig fil.

## Pseudokod

Här är ett exempel på pseudokod för ett konsolprogram **Kontaktuppgiftsprogram** med en meny:

* Presentera programmet och instruktionerna
  * Skriv ut "Välkommen till Kontaktuppgiftsprogrammet!" till konsolen
* Starta en loop för att visa menyn
  * Skriv ut menyn med följande val till konsolen:
    1. Lägg till ny kontakt
    2. Visa alla kontakter
    3. Avsluta programmet
  * Be användaren att välja ett alternativ från menyn
    * Skriv ut "Välj ett alternativ (1-3):" till konsolen
    * Spara användarens inmatning i en variabel
  * Utför åtgärden baserat på användarens val
    * Om användaren väljer 1 (Lägg till ny kontakt):
      * Utför stegen för att lägga till en ny kontakt (se tidigare pseudokod)
    * Om användaren väljer 2 (Visa alla kontakter):
      * Läs in alla kontakter från textfilen
      * Skriv ut alla kontakter till konsolen
    * Om användaren väljer 3 (Avsluta programmet):
      * Avsluta loopen för menyn
* Avsluta programmet och skriv ut ett avslutningsmeddelande
  * Skriv ut "Tack för att du använde Kontaktuppgiftsprogrammet. Hej då!" till konsolen

Observera att detta är en enkel pseudokod och kan kräva ytterligare detaljer och kod för att fungera korrekt. Det kan också kräva att man lägger till felhantering och validering av användarens inmatningar för att förbättra programmets robusthet.

## Förslag på utökningar

Här är några förslag på hur du kan utöka **Kontaktuppgiftsprogrammet** för att göra det mer omfattande och användbart:

1. **Sökfunktion**: Lägg till en sökfunktion som låter användaren söka efter specifika kontakter baserat på namn, telefonnummer eller e-postadress.

2. **Sortera kontakter**: Lägg till funktioner för att sortera kontakter efter namn, telefonnummer eller e-postadress, både i stigande och fallande ordning.

3. **Redigera kontakter**: Ge användaren möjlighet att redigera befintliga kontakter, exempelvis ändra namn, telefonnummer eller e-postadress.

4. **Ta bort kontakter**: Låt användaren ta bort kontakter från listan.

5. **Importera och exportera kontakter**: Lägg till funktioner för att importera och exportera kontakter från och till olika filformat, som CSV eller vCard, för att underlätta datahantering och delning.

6. **Gruppera kontakter**: Ge användaren möjlighet att gruppera kontakter i kategorier, exempelvis familj, vänner eller arbetskamrater. Detta gör det enklare att hitta och hantera kontakter.

7. **Spara i molnet**: Implementera molnlagring för att spara kontaktuppgifter, så att användaren kan komma åt sina kontakter från olika enheter och säkerhetskopiera informationen.

8. **Skydda kontakter med lösenord**: Lägg till lösenordsskydd för att skydda användarens kontaktuppgifter och säkerställa att endast behöriga personer har åtkomst till dem.

Observera att varje förslag kan kräva ytterligare forskning och kodning för att implementeras korrekt. Det är viktigt att ta hänsyn till användbarhet, prestanda och säkerhet vid utformningen av dessa utökningar.
