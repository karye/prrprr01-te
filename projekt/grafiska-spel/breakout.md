---
description: Spelet Breakout
---
# Breakout

## Beskrivning

Spelet Breakout är ett spel där spelaren styr en platta som ska försöka träffa en vägg av brickor med en boll. När spelaren träffar en brick så försvinner den och spelaren får poäng. Spelet är över när spelaren inte längre kan träffa en brick med bollen.

Spelet är inspirerat av [Atari Breakout](https://en.wikipedia.org/wiki/Breakout_(video_game)).

## Pseudokod

Pseudokoden för spelet ser ut ungefär så här: