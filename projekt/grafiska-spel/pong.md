---
description: Spelet Pong
---

# Spelet Pong

![](../../.gitbook/assets/pong.png)

## Beskrivning

Pong är ett arkadspel som är skapat av Atari 1972. Spelet är grafiskt och spelas på en dator eller konsol. Spelet går ut på att två spelare kontrollerar paddlar som ska försöka få en boll att träffa motståndarens sida. Bollen studsar från paddlarna och väggarna. Spelet är över när bollen träffar en spelares sida.

Spelet kan ha olika svårighetsgrader, som att paddlarna kan röra sig snabbare eller att bollen studsar snabbare. Spelet kan också ha olika variationer, som att paddlarna kan röra sig vertikalt istället för horisontellt.

## Pseudokod

Pseudokoden för ett Pong spel i C# som konsolprogram ser ut ungefär så här:

* Skriv ut en välkomstmeddelande och instruktioner för hur pong spelet fungerar.
* Skapa en spelplan med två paddlar och en boll.
* Skapa variabler för bollens x och y position, samt hastighet och riktning.
* Skapa variabler för paddlarnas x och y position och deras höjd och bredd.
* Skapa en loop för att hålla spelet igång.
* Inuti loopen:
  * Läs in spelarnas input för att flytta paddlarna.
  * Uppdatera bollens position baserat på hastighet och riktning.
  * Kontrollera om bollen träffar en paddel eller en kant av spelplanen, och ändra riktning om det är fallet.
  * Rita ut bollen, paddlarna och spelplanen på skärmen.
  * Kontrollera om någon av spelarna har vunnit och avsluta spelet om det är fallet.
  * Öka poängen för den spelaren som träffar bollen.
  * Pausa spelet för en kort stund för att ge spelarna tid att reagera.

* Kontrollera om bollens x och y positioner är inom paddelns x och y positioner och höjd och bredd.
* Ändra riktningen på bollen vid träff:
  *  Om bollen träffar toppen eller botten av paddeln, ändra bollens vertikala riktning
  * Om bollen träffar sidorna av paddeln, ändra bollens horisontella riktning
* Öka bollens hastighet vid träff med paddel.

Raylib har en inbyggd funktion som heter `CheckCollisionRecs()` som kan användas för att kontrollera kollisioner mellan rektanglar. Den tar två rektangler som argument och returnerar true om de kolliderar och false om de inte gör det. Detta skulle kunna användas i pseudokod för att kontrollera om bollen träffar paddeln i spelet Pong:

```csharp
if (CheckCollisionRecs(bollRectangle, paddelRectangle)) {
    // Ändra bollens riktning och hastighet
}
```

Detta skulle göra det enklare att kontrollera om bollen träffar paddeln och kan minska koden för detta syfte.

Detta är en grov beskrivning av pseudokoden, det faktiska implementeringen skulle kräva mer detaljerade instruktioner och kod för att få spelet att fungera som det ska.