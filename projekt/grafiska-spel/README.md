---
description: Exempel på projekt
---
# Raylib-spel

Här är några exempel på andra grafiska spel som kan skapas i C# med Raylib:

* [Pong](pong.md): Ett enkelt spel där två spelare tar tur att skjuta en boll till varandra och försöka träffa varandras mål.
* Breakout: Ett spel där en boll studsar runt på skärmen och spelaren ska försöka träffa en vägg av brickor med bollen. 
* Space Invaders: Ett spel där en armada av fienden försöker ta över jorden och spelaren ska försöka skjuta ner dem. 
* Asteroids: Ett spel där spelaren styr ett rymdskepp och ska försöka undvika asteroider och skjuta ner dem. 
* Pacman: Ett spel där spelaren styr en figur som ska äta alla matbitar eller annat på skärmen. 
* Tetris: Ett spel där spelaren ska försöka få ihop rader med block. 
* Flappy Bird: Ett spel där spelaren styrs en fågel och ska försöka undvika hindren. 
* Snake: Ett spel där en orm rör sig runt på skärmen och äter mat, ju mer mat ormen äter desto längre blir den. Spelet är över när ormen kolliderar med kanterna eller sin egen svans.
* Tic Tac Toe: Ett enkelt brädspel där två spelare tar tur att markera en ruta på en 3x3 rutnät. Programmet avgör vem som vinner eller om det är oavgjort.
