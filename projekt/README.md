---
description: Exempel på projekt
---
# Viktigt att tänka på

Ett bra konsolprogram kan ha följande delar:

* En meny där användaren kan välja vilken funktion som ska köras
* En funktion för att läsa in data från användaren
* En funktion för att spara data till en textfil
* En funktion för att läsa in data från en textfil
* Felhantering
  * Om användaren matar in felaktig data, som tex ett felaktigt telefonnummer
  * Om användaren matar in data som inte kan sparas, som tex en tom sträng

## Exempel på meny

```csharp
// Skriver ut en meny
static void SkrivUtMeny()
{
    Console.WriteLine("1. Lägg till kontakt");
    Console.WriteLine("2. Visa kontakter");
    Console.WriteLine("3. Avsluta");

    // Hantera val med en switch-sats
    switch (Console.ReadLine())
    {
        case "1":
            LaggTillKontakt();
            break;
        case "2":
            VisaKontakter();
            break;
        case "3":
            Avsluta();
            break;
        default:
            Console.WriteLine("Felaktigt val!");
            break;
    }
}
```

## Exempel på inläsning av data från användaren

Användaren ska ibland mata in text, ibland siffror. Det är viktigt att kontrollera att användaren matar in rätt data, annars kan programmet krascha.

```csharp
// Läser in en sträng från användaren
static string LaggTillKontakt()
{
    Console.Write("Namn: ");
    string namn = Console.ReadLine();

    // Kontrollera att användaren matar in något
    if (namn == "")
    {
        Console.WriteLine("Du måste mata in ett namn!");
        return "";
    }

    Console.Write("Telefonnummer: ");
    string telefonnummer = Console.ReadLine();

    // Kontrollera att användaren matar in ett telefonnummer
    if (telefonnummer.Length != 10)
    {
        Console.WriteLine("Du måste mata in ett telefonnummer!");
        return "";
    }

    Console.Write("E-postadress: ");
    string epost = Console.ReadLine();

    // Kontrollera att användaren matar in en e-postadress
    if (!epost.Contains("@"))
    {
        Console.WriteLine("Du måste mata in en e-postadress!");
        return "";
    }

    // Returnera en sträng med kontaktuppgifterna
    return $"{namn};{telefonnummer};{epost}";
}
```

## Exempel inläsning av siffror från användaren

Mha `TryParse()` kan vi kontrollera att användaren matar in en siffra.  
Om det blir fel använder vi en loop för att låta användaren mata in data tills det blir rätt.

```csharp
// Läser in en siffra från användaren
static int LaggTillKontakt()
{
    Console.Write("Ålder: ");
    int alder;

    // Kontrollera att användaren matar in en siffra
    while (!int.TryParse(Console.ReadLine(), out alder))
    {
        Console.WriteLine("Du måste mata in en siffra!");
        Console.Write("Ålder: ");
    }

    return alder;
}
```

## Exempel på att spara data till en textfil

Vi använder `System.IO` för att skriva till en textfil.

```csharp
// Sparar en sträng till en textfil
static void SparaKontakt(string kontakt)
{
    // Namnet på textfilen
    string filnamn = "kontakter.txt";

    // Skapa en sträng med kontaktuppgifterna
    string text = $"{kontakt}\n";

    // Spara texten till en textfil
    File.WriteAllText(filnamn, text);

    Console.WriteLine("Kontakt sparad!");
}
```

## Exempel på att läsa in data från en textfil

Vi använder `System.IO` för att läsa in data från en textfil.

```csharp
// Läser in data från en textfil
static void VisaKontakter()
{
    // Namnet på textfilen
    string filnamn = "kontakter.txt";

    // Läs in texten från textfilen
    string text = File.ReadAllText(filnamn);

    // Skriv ut texten
    Console.WriteLine(text);
}
```
