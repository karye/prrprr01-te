---
description: Exempel på projekt
---
# Konsolspel

Här är några exempel på spel som kan skapas i C# som konsolprogram:

* [Hangman](hangman.md): Ett ordgissningsspel där användaren gissar bokstäver i ett ord och programmet ritar ut en gubbe för varje felaktig gissning.
* [Blackjack](blackjack.md): Ett kortspel där användaren spelar mot datorn, användaren får välja om man vill dra ett kort eller stanna och datorn gör samma val. Programmet räknar ut vem som vinner.
* [Snake](snake.md): Ett spel där en orm rör sig runt på skärmen och äter mat, ju mer mat ormen äter desto längre blir den. Spelet är över när ormen kolliderar med kanterna eller sin egen svans.
* [Tic Tac Toe](tictactoe.md): Ett enkelt brädspel där två spelare tar tur att markera en ruta på en 3x3 rutnät. Programmet avgör vem som vinner eller om det är oavgjort.
* [Mine Sweeper](mine-sweeper.md): Ett spel där användaren ska avslöja rutor på ett bräde medan undviker att avslöja rutor med miner. Programmet hjälper användaren att hålla koll på hur många miner som finns kvar att hitta.
* [Sänka skepp](../../kapitel-6/labb-saenka-skepp.md): Ett brädspel där två spelare tar tur att skjuta på varandra och försöka träffa varandras skepp. Programmet avgör vem som vinner.
* [Mastermind](mastermind.md): Ett brädspel där en spelare försöker gissa en hemlig kod som en annan spelare har valt. Programmet ger feedback om gissningen är rätt eller fel.
* [Månlandare](manlandare.md): Ett spel där spelaren ska landa en rymdskepp på månen. Programmet avgör om spelaren lyckas landa eller om skeppet kraschar.
* [Zork](zork.md): Ett textbaserat äventyrsspel där användaren navigerar genom en historia och gör val som påverkar utgången av spelet.

