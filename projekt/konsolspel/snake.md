---
description: Spelet Snake
---
# Spelet Snake

![](../../.gitbook/assets/snake.png)

## Beskrivning

**Snake** är ett konsolprogram som simulerar spelet med samma namn, ett arkadspel där målet är att styra en orm för att samla föda och växa så lång som möjligt utan att krocka med väggarna eller sin egen kropp. Spelet är textbaserat och spelaren ger kommandon via konsol för att styra ormen.

Spelet börjar med en orm som rör sig framåt på ett rutnät. Föda dyker slumpmässigt upp på rutnätet och när ormen äter födan växer den. Ormen måste undvika att krocka med väggarna eller sin egen kropp. Varje gång ormen krockar så förlorar spelet.

Spelet kan ha olika svårighetsgrader, som hastigheten på ormen eller antalet föda som dyker upp. Spelet kan också ha olika liv, där spelaren får flera chanser att fortsätta spela efter kollisioner.

## Pseudokod

Här är ett exempel på pseudokod för ett konsolprogram Snake:

* Presentera spelet och initiera spelplanen
  * Skriv ut "Välkommen till Snake!" till konsolen
  * Skapa en matris för spelplanen
  * Placera ut ormen i mitten av spelplanen
* Starta en loop för att hålla spelet igång
  * Så länge ormen inte har krockat med en vägg eller sig själv
* Be användaren att välja riktning
  * Skriv ut "Välj riktning (w, a, s, d):" till konsolen
* Uppdatera ormens position baserat på vald riktning
  * Flytta ormens huvud en ruta i den valda riktningen
  * Lägg till en ny ruta till ormens svans
* Kontrollera om ormen har ätit mat
  * Om ormens huvud råkar på mat, öka ormens längd och placera en ny matruta
* Rita upp spelplanen och ormen på skärmen
* Upprepa från steg 3 tills spelet är avslutat.
* Visa ett resultatmeddelande när ormen krockat med en vägg eller sig själv.

