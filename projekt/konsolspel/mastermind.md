---
description: Spelet Mastermind
---

# Pseudokod för spelet Mastermind

![](../../.gitbook/assets/brain.png)

## Beskrivning

**Mastermind** är ett konsolprogram som simulerar spelet med samma namn, ett logiskt tänkande spel där målet är att gissa en hemlig kod bestående av färger genom att göra gissningar och få feedback. Spelet är textbaserat och spelaren ger kommandon via konsol för att göra sina gissningar.

Spelet börjar med att en dator slumpar en hemlig kod bestående av färger. Spelaren gör sedan gissningar genom att välja färger och placera dem i en viss ordning. Datorn ger feedback till spelaren i form av svarta och vita pinnar. En **svart pinne** representerar att en färg är rätt och på rätt plats, medan en **vit pinne** representerar att en färg är rätt men på fel plats. Spelaren får en viss antal gissningar för att gissa koden.

Spelet kan ha olika svårighetsgrader, som antalet färger som finns tillgängliga eller antalet färger som ska gissas. Spelet kan också ha olika antal gissningar för att gissa koden.

## Pseudokod

Pseudokoden för ett **Mastermind**-spel som konsolprogram kan se ut ungefär så här:

* Presentera spelet och initiera spelet
* Generera en slumpmässig kod bestående av färger (exempelvis Röd, Grön, Blå, Gul, Orange, Vit)
* Sätt antalet försök till 0
* Starta en loop för att hålla spelet igång
* Be användaren att gissa koden
  * Skriv ut "Gissa koden (exempel: R G B Y):" till konsolen
* Kontrollera användarens gissning
  * Jämför varje färg i gissningen med motsvarande färg i koden
  * Räkna antalet rätt färg och rätt position
* Visa resultatet till användaren
  * Skriv ut antalet rätt färg och rätt position
  * Öka antalet försök med 1
* Upprepa från steg 3 tills spelet är avgjort
* Visa ett resultatmeddelande när spelet är avgjort.