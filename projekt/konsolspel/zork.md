---
description: Spelet Zork
---

# Spelet Zork

![](../../.gitbook/assets/map.png)

## Vad är Zork?

**Zork** är ett textbaserat äventyrsspel som ursprungligen utvecklades på 1970-talet av gruppen "Infocom". Spelet utspelar sig i en fiktiv värld kallad "Zork" där spelaren tar rollen som en äventyrare och får utforska olika platser, samla föremål och lösa gåtor för att nå ett mål. Spelet är interaktivt och spelaren ger kommandon via konsol för att navigera i världen, interagera med föremål och karaktärer och utföra olika handlingar. Spelet är känt för sin komplexa värld och djupa berättelse och var en av de första textbaserade äventyrsspelen som blev populära.

Spelet kan ha olika svårighetsgrader och mål, som att samla alla föremål i världen eller att döda en viss karaktär. Spelet kan också ha olika liv, där spelaren får flera chanser att fortsätta spela efter att ha dött.

## Pseudokod för Zork

Här är en exempel på pseudokod för ett konsolprogram **Zork**:

* Presentera spelet och initiera spelet
  * Skapa en databas för spelets värld med olika rum, föremål och karaktärer
  * Sätt spelarens startposition
  * Skriv ut en beskrivning av nuvarande rum
* Starta en loop för att hålla spelet igång
* Be användaren att mata in en kommando
  * Skriv ut "Vad vill du göra?" till konsolen
  * Spara användarens kommando i en variabel
* Verifiera och utför kommandot
  * Kontrollera om kommandot är giltigt och utför det
  * Om kommandot inte är giltigt, skriv ut ett felmeddelande
* Uppdatera spelets tillstånd
  * Uppdatera spelarens position och inventarier
  * Uppdatera föremål och karaktärer i världen
  * Kontrollera om spelets mål är uppnått
* Skriv ut en beskrivning av nuvarande rum och spelets tillstånd
* Upprepa från steg 3 tills spelet är klart
* Visa ett resultatmeddelande när spelet är klart.

Observera att detta är en enkel pseudokod och kan kräva ytterligare detaljer och kod för att göra spelet mer avancerat och realistiskt. Det kan också kräva att man lägger till mer interaktion med användaren, fler kommandon, olika scenarion för spelets avslutning, och olika objekt och karaktärer för att göra spelet mer utmanande och intressant.

Rum och kommandon kan läsas från en textfil. Detta gör det möjligt att lägga till nya rum och kommandon utan att behöva ändra källkoden. Detta är en bra idé för att göra spelet mer utmanande och intressant.