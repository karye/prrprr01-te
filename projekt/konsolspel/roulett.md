---
description: Spelet Roulett
---

# Spelet Roulett

![](../../.gitbook/assets/roulett.png)

## Beskrivning

**Roulett** är ett kasinospel där spelare satsar på var en liten boll kommer att landa när den snurras runt på ett hjul. Spelet är textbaserat och spelaren ger kommandon via konsol för att göra sina insatser och snurra hjulet.

Spelet börjar med att spelaren får välja hur mycket pengar han vill satsa. Sedan kan spelaren välja att satsa på olika nummer eller grupper av nummer, som enskilda nummer, jämna/udda nummer, färger osv. När alla insatser är gjorda, snurrar hjulet och slumpar ett nummer. Om spelarens insats matchar det nummer som bollen landade på, vinner spelaren en viss summa pengar.

Spelet kan också innehålla olika variationer av roulett, som Amerikansk roulett och Europeisk roulett, som har lite olika regler och utbetalningar. Spelet ska också ha en funktion för att hålla reda på spelarens totala vinst eller förlust.

Spelet kan ha olika svårighetsgrader, som antalet nummer på hjulet eller om datorn ska fungera som motståndare.

## Pseudokod

Pseudokoden för ett **roulettspel** som konsolprogram kan se ut ungefär så här:

* Skriv ut ett välkomstmeddelande och instruktioner för hur roulett spelet fungerar.
* Fråga användaren hur mycket pengar de vill satsa.
* Fråga användaren om de vill satsa på ett specifikt nummer, röd/svart, jämna/udda eller flera nummer.
* Spela roulett genom att slumpa ett nummer och visa det för användaren.
* Kontrollera om användarens val vann eller förlorade och uppdatera deras saldo.
* Fråga användaren om de vill spela igen och om svaret är ja, gå tillbaka till steg 2. Annars, avsluta spelet och visa * avslutande meddelande.

För att implementera valet av röd/svart och udda/jämna kan man lägga till ett steg innan användaren gör sin insats, där användaren väljer om de vill satsa på röd/svart eller udda/jämna.

För att kolla om ett nummer är rött eller svart kan man skapa en lista med de röda numren och en annan lista med de svarta numren. Sedan kollar man om numret som slumpats fram finns i den ena eller den andra listan.
För att kolla om ett nummer är udda eller jämnt kan man använda **modulus-operatorn %**. Om resultatet av numret mod 2 är 0 är numret jämnt, annars är det udda.