---
description: Spelet Hangman
---

# Spelet Hangman

![](../../.gitbook/assets/death.png)

## Beskrivning

**Hangman** är ett konsolprogram som simulerar spelet med samma namn, ett logiskt tänkande spel där målet är att gissa ett hemligt ord genom att göra gissningar och få feedback. Spelet är textbaserat och spelaren ger kommandon via konsol för att göra sina gissningar.

Spelet börjar med att en dator slumpar ett hemligt ord. Spelaren gör sedan gissningar genom att välja bokstäver. Datorn ger feedback till spelaren i form av gissade bokstäver. En gissad bokstav representerar att bokstaven finns i ordet, medan en tom plats representerar att bokstaven inte finns i ordet. Spelaren får en viss antal gissningar för att gissa ordet.

Spelet kan ha olika svårighetsgrader, som antalet bokstäver som ska gissas. Spelet kan också ha olika antal gissningar för att gissa ordet.

## Pseudokod

Här är ett exempel på pseudokod för ett konsolprogram **Hangman**:

* Presentera spelet och initiera spelet
  * Välj ett ord från en ordlista eller generera ett slumpmässigt ord
  * Skapa en tom sträng för att visa gissade bokstäver
  * Sätt antalet felgissningar till 0
* Starta en loop för att hålla spelet igång
* Be användaren att gissa en bokstav
  * Skriv ut "Gissa en bokstav:" till konsolen
  * Spara användarens gissning i en variabel
* Kontrollera om bokstaven finns i ordet
  * Om bokstaven finns i ordet, lägg till den i den tomma strängen för gissade bokstäver
* Annars, öka antalet felgissningar med 1
* Rita upp en gubbe baserat på antalet felgissningar
* Kontrollera om ordet är gissat
  * Om ordet är gissat, avsluta spelet och visa ett vinstmeddelande
* Upprepa från steg 3 tills spelet är avgjort
* Visa ett resultatmeddelande när spelet är avgjort
  * Om ordet är gissat, skriv ut "Grattis! Du gissade rätt ord"
  * Om antalet felgissningar är högre än tillåten gräns, skriv ut "Tyvärr, du har gissat fel för många gånger. Ordet var: [ordet]."

Observera att detta är en enkel pseudokod och kan kräva ytterligare detaljer och kod för att göra spelet mer realistiskt. Det kan också kräva att man lägger till mer interaktion med användaren för att hålla spelet mer intressant.

## Förslag på utökningar

Här är några förslag på hur du kan utöka **Hangman**-programmet för att göra det mer omfattande och underhållande:

1. **Kategori och svårighetsgrad**: Låt användaren välja mellan olika kategorier, såsom djur, länder eller kända personer, och svårighetsgrader för att anpassa spelet efter sina intressen och färdigheter.

2. **Multiplayer**: Lägg till stöd för flera spelare, antingen genom nätverkskommunikation eller genom att låta användare dela samma enhet. Detta kan göra spelet mer socialt och konkurrenskraftigt.

3. **Grafiskt gränssnitt**: Ersätt konsolgränssnittet med ett grafiskt gränssnitt som visar gubben, gissade bokstäver och annan information på ett mer visuellt sätt.

4. **Animeringar och ljud**: Lägg till animeringar och ljud för att förbättra spelupplevelsen och göra den mer engagerande.

5. **Statistik och historik**: Spara spelarens resultat, såsom antal vinster, förluster och oavgjorda, och visa dem efter varje omgång eller på begäran. Detta ger spelaren en översikt över deras prestationer.

6. **Anpassade ordlistor**: Ge användaren möjlighet att skapa och använda egna ordlistor, så att de kan anpassa spelet efter sina intressen och önskemål.

7. **Tidsbegränsning**: Inför en tidsbegränsning för varje gissning eller omgång, så att spelaren måste fatta snabba beslut. Detta kan öka spänningen och svårighetsgraden.

8. **Ledtrådar**: Ge spelaren möjlighet att be om ledtrådar eller hjälp, antingen genom att visa en definition av ordet, en synonym eller en bild som är relaterad till ordet.

9. **Highscores och prestationer**: Inför en highscore-lista som sparar de bästa resultaten och låter spelare tävla mot varandra. Lägg även till prestationer som spelaren kan låsa upp under spelets gång.

10. **Lärande läge**: Skapa ett lärande läge där spelaren får tips och råd om hur man spelar Hangman och förbättrar sina gissningsfärdigheter. Detta kan hjälpa nybörjare att lära sig spelet snabbare och förbättra sina färdigheter.

Observera att varje förslag kan kräva ytterligare forskning och kodning för att implementeras korrekt. Det är viktigt att ta hänsyn till användbarhet, prestanda och säkerhet vid utformningen av dessa utökningar.
