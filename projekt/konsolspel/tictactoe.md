---
description: Spelet Tic Tac Toe
---

# Spelet Tic Tac Toe

![](../../.gitbook/assets/tic-tac-toe.png)

## Beskrivning

**Tic Tac Toe** är ett enkel strategispel där två spelare tar turvis och försöker få en rad med sin symbol (vanligtvis X och O) genom att fylla i rutnätet med sina symboler. Spelet är textbaserat och spelaren ger kommandon via konsol för att göra sina drag.

Spelet börjar med ett tomt rutnät med tre rader och tre kolumner. Spelarna tar turvis och väljer en ledig plats att placera sin symbol. Varje spelare försöker få en rad med sin symbol genom att placera sina symboler på rad, kolumn eller diagonal. Om ingen av spelarna lyckas med detta, blir spelet oavgjort.

Spelet kan ha olika svårighetsgrader, som antalet rader och kolumner i rutnätet eller om datorn ska fungera som motståndare.

## Pseudokod

Pseudokoden för ett **Tic Tac Toe** spel som konsolprogram kan se ut ungefär så här:

* Presentera spelet och initiera spelplanen
* Skapa en matris för spelplanen, sätt alla rutorna till tomma
* Sätt aktuell spelare till X
* Starta en loop för att hålla spelet igång
* Be den aktuella spelaren att välja en ruta
  * Skriv ut "Spelare [X/O], välj en ruta (1-9):" till konsolen
  * Spara spelarens val i en variabel
* Kontrollera om rutan är ledig
  * Om rutan är ledig, sätt rutan till den aktuella spelarens symbol
* Kontrollera om någon har vunnit
* Kontrollera om någon har vunnit genom att ha fått tre i rad på något sätt
* Byt spelare
  * Om den aktuella spelaren är X, byt till O
  * Annars, byt till X
* Rita upp spelplanen på skärmen
* Upprepa från steg 3 tills spelet är avgjort
* Visa ett resultatmeddelande när spelet är avgjort.

