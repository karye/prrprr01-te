---
description: Spelet Månlandare
---

# Spelet Månlandare

![](../../.gitbook/assets/lander.png)

## Beskrivning

**Månlandare** är ett konsolprogram som simulerar en månlandning. Spelet går ut på att man ska styra en månlandare ner på månen på ett säkert sätt. Spelet är textbaserat och man ger kommandon via konsol för att styra raketen och landa den på månen. Spelet består av flera olika faser, inklusive raketens start, raketens nedstigning mot månen, och den faktiska landningen. Under varje fas, måste spelaren göra viktiga beslut och navigera genom olika Utmaningar som kan inkludera problem med utrustning, räkning, och ogynnsamma väderförhållanden för att säkerställa en framgångsrik landning. Spelet kräver också kontroll av hastighet och höjd för att säkerställa en säker landning.

## Pseudokod

Pseudokoden för ett **Månlandarspel** som konsolprogram kan se ut ungefär så här:

* Presentera spelet och initiera spelet
* Sätt startpositionen för månlandaren på ytan av månen
* Sätt bränslemängd och hastighet till initiala värden
* Starta en loop för att hålla spelet igång
* Be användaren att göra ett val
  * Skriv ut "Välj hastighet (1-10) och bränsleförbrukning (1-5):" till konsolen
* Uppdatera månlandarens position och bränslemängd
* Flytta månlandaren baserat på vald hastighet
* Minska bränslemängden baserat på vald bränsleförbrukning
* Kontrollera om månlandaren har landat eller bränslet är slut
* Rita ut månlandarens position och bränslemängd på skärmen
* Upprepa från steg 3 tills spelet är avslutat

Observera att denna pseudokod är enkel och kan kräva ytterligare detaljer och kod för att göra spelet mer realistiskt. Det kan också kräva att man lägger till mer interaktion med användaren för att hålla spelet mer intressant.