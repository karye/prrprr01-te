---
description: Bra resurser för att träna C#-programmering på egen hand
---

# Kontrollstrukturer

## Videogenomgångar

{% embed url="https://youtu.be/k-1JWMQLHrg?si=T__wlyRzIePRjWGi" %}

## Allmänna bra länkar

* [C# referens: if-satser](https://csharp.progdocs.se/grundlaeggande/if-satser)
* [C# referens: loopar](https://csharp.progdocs.se/grundlaeggande/loopar)

## TLDR;

```csharp
// Så här skapar du en if-sats
if (namn == "Pelle")
{
   Console.WriteLine("Hej Pelle!");
}

// Så här skapar du en if-else-sats
if (namn == "Gary")
{
   Console.WriteLine("Hej Gary!");
}
else
{
   Console.WriteLine("Hej någon annan!");
}

// Så här skapar du en oändlig while-loop
while (true)
{
   Console.WriteLine("Hej världen!");
}

// Så här skapar du en for-loop som skriver ut alla tal mellan 1 och 10
for (int i = 1; i <= 10; i++)
{
	Console.WriteLine(i);
}

// Så här kollar du om en sträng kan konverteras till ett tal
string text = "123";
int tal;
if (int.TryParse(text, out tal))
{
   Console.WriteLine("Texten kan konverteras till ett tal!");
}
else
{
   Console.WriteLine("Texten kan inte konverteras till ett tal!");
}
```

## Uppgifter

 > ### Tips: Infoga uppgiftens steg som kommentarer i din kod för att hålla reda på vad du ska göra.

### if-satser

#### 1. Projekt HejVärlden

1. Skapa ett nytt konsolprojekt **HejVärlden**.
1. Börja med att fråga efter användarens namn via `Console.ReadLine()`.
1. Skriv en hälsning till användaren via `Console.WriteLine()`.\
   Tips: Använd `Console.WriteLine($"Hej {namn}!");` för att skriva ut namnet.
1. Skriv ut "Du är bäst!" ifall användarens namn är "Pelle".\
   Tips: Använd en `if`-sats.
1. Annars skriv ut "Du duger ändå!".

Exempel på körning:

```
Ange ditt namn: Pelle
Pelle, du är bäst!
Ange ditt namn: Kalle
Kalle, du duger ändå!
```

Fortsätt med att lägga till följande funktionalitet:

1. Fråga användaren efter deras favoritfärg.
1. Om användarens favoritfärg är "blå", skriv ut "Blå är en riktigt snygg färg!".
1. Annars skriv ut "Hmm, jag föredrar blå, men {färg} är också fint!".
1. Fråga användaren om de vill ha ett roligt fakta om dagen.
1. Om användaren svarar "ja", skriv ut ett roligt fakta, till exempel "Visste du att människor och giraffer har samma antal halskotor?".
1. Om användaren svarar "nej", skriv ut "Inga problem, ha en trevlig dag!".

#### 2. Projekt Användare

1. Skapa ett nytt konsolprojekt **Användare**.
1. Skapa en kod som först hämtar in ett användarnamn från användaren.\
   Gör sedan en `if`-sats som skriver ut "Välkommen!" ifall användarnamnet är lika med "noname".
1. Hämta nu också in lösenord från användaren.
1. "Välkommen" ska skrivas ut enbart om användarnamnet är lika med "noname" och lösenordet är "nopass".\
   Tips: Använd `&&` för att kombinera två villkor.
1. Om någon av dem inte stämmer så ska koden skriva ut "Fel användarnamn eller lösenord!".

Exempel på körning:

```
Ange användarnamn: noname
Anga lösenord: pass
Fel användarnamn eller lösenord!
```

Fortsätt med att lägga till följande funktionalitet:

1. Efter autentiseringsprocessen, fråga om användaren vill se dagens datum.
1. Om användaren svarar "ja", använd `DateTime.Today` för att skriva ut dagens datum.\
   Tips: Använd `Console.WriteLine($"Dagens datum är {DateTime.Today:yyyy-MM-dd}.");`
1. Om användaren svarar "nej", skriv ut "Okej, ingen information om datum visas.".
1. Slutligen, ge användaren möjligheten att avsluta programmet genom att trycka på valfri tangent.\
   Tips: Använd `Console.ReadKey();`

### loopar

#### 1. Projekt Hej32

1. Skapa ett nytt konsolprojekt **Hej32**.
1. Skapa en loop som skriver ut "Hej världen!" 32 gånger.\
   Tips: Använd en `for`-loop.

Exempel på körning:

```
Hej världen!
Hej världen!
...
Hej världen!
```

Fortsätt med att lägga till följande funktionalitet:

1. Efter loopen, fråga användaren om de vill köra programmet igen.
   1. Om användaren svarar "ja", använd en ytterligare loop för att upprepa hela processen.
   1. Om användaren svarar "nej", skriv ut "Programmet avslutas." och avsluta programmet.
1. Implementera en räknare som visar vilken iteration av loopen som körs.\
   Tips: Skapa en variabel `int i = 1;` före loopen och öka den med 1 varje gång loopen körs.

#### 2. Projekt Inloggning

1. Skapa ett nytt konsolprojekt **Inloggning**.
1. Skapa en loop som fortsätter så länge användaren inte har skrivit in rätt lösenord.\
   Du måste alltså lägga in din `Console.ReadLine()` inuti en `while`-loop.
1. Om användaren skriver in rätt lösenord, skriv ut "Välkommen!".\
   Och avbryt loopen med `break`.
1. Om användaren skriver in fel lösenord, skriv ut "Fel lösenord!".

Exempel på körning:

```
Anga lösenord: 123
Fel lösenord!
Ange lösenord: 456
Fel lösenord!
Ange lösenord: nopass
Välkommen!
```

Fortsätt med att lägga till följande funktionalitet:

1. Lägg till en räknare som håller koll på antalet försök användaren har gjort.
1. Begränsa antalet inloggningsförsök till 3. Om gränsen nås, skriv ut "För många försök!" och avsluta programmet.
1. Lägg till en funktion där användaren kan begära att få se lösenordshjälp efter tre misslyckade försök.
1. Om användaren skriver "hjälp", visa en förinställd ledtråd som "Tänk på ditt favoritår.".

### Konvertering till tal

#### 1. Projekt MyndigKoll

1. Skapa ett nytt konsolprojekt **MyndigKoll**.
1. Fråga användaren efter deras namn och ålder.
1. Konvertera användarens ålder från en string till en `int`.\
   Tips: Använd `int tal = int.Parse(text)` för att konvertera en string till en `int`.
1. Om användaren är under 21 år, skriv ut "Du är för ung för att handla på Systemet!".
1. Om användaren är äldre än 18 år, skriv ut "Du är gammal nog att ta körkort!".

Exempel på körning:

```
Ange ditt namn: Pelle
Ange din ålder: 20
Pelle, du är för ung för att handla på Systemet!
Pelle, du är gammal nog att ta körkort!
```

Fortsätt med att lägga till följande funktionalitet:

1. Om användaren är under 18 år, fråga om de har något tillstånd för att köra moped.
   1. Om svaret är "ja", skriv ut "Du får köra moped."
   1. Om svaret är "nej", skriv ut "Du får inte köra moped."
1. Fråga alla användare om de vill ha information om valåret när de fyller 18.\
   Tips: `Console.WriteLine("Vill du veta i vilket år du får rösta? (ja/nej)");`
   1. Om svaret är "ja", beräkna och skriv ut året då användaren blir 18.\
      Tips: Använd `Console.WriteLine($"Du får rösta år {DateTime.Now.Year - ålder + 18}.");`
   1. Lägg till en kontroll för att hantera icke-numeriska svar när åldern matas in, och visa ett felmeddelande om det inträffar.\
      Tips: Använd `int.TryParse(text, out int ålder)` för att validera input.

#### 2. Projekt TestaTal

1. Skapa ett nytt konsolprojekt **TestaTal**.
1. Skapa en loop som körs så länge användaren skriver in en string som inte kan konverteras till en `int`.\
   Be användaren skriva in ett tal och konvertera det till en `int`.\
   Tips: Använd `int.TryParse()` för att konvertera en string till en `int`.
1. Om användaren skriver in en string som kan konverteras till en `int`, skriv ut "Du skrev in en siffra!".
1. Om användaren skriver in en string som inte kan konverteras till en `int`, skriv ut "Du skrev in text!".

Exempel på körning:

```
Ange ett tal: 123
Du skrev in en siffra!
Ange ett tal: abc
Du skrev inte in en siffra!
```

#### 3. Projekt GissaSiffran

1. Skapa ett nytt konsolprojekt **GissaSiffran**.
1. Slumpa eller välj ett tal som användaren ska gissa.\
   Tips: Använd `int slumptal = Random.Shared.Next(1, 11);` för att slumpa ett tal mellan 1 och 10.
1. Skapa en `while`-loop. Varje gång loopen körs ska användaren skriva in ett tal.\
   Observera att du måste konvertera talet från en string till en `int` för att kunna göra jämförelsen.
1. Varje gång användaren gissar fel, skriv ut om gissningen var för hög eller för låg.
1. Skriv ut hur många gissningar det tog för användaren att gissa rätt.\
   Tips: Skapa en variabel `antal` före loopen som räknar antalet gissningar.\
   Öka variabeln med 1 varje gång användaren gissar.

Exempel på körning:

```
Gissa ett tal mellan 1 och 10: 5
Fel! Gissa högre!
Gissa ett tal mellan 1 och 10: 7
Fel! Gissa lägre!
Gissa ett tal mellan 1 och 10: 6
Rätt! Du gissade rätt på 3 försök!
```

#### 4. Projekt GissaSiffran2

1. Skapa ett nytt konsolprojekt **GissaSiffran2**.
1. Slumpa eller välj ett tal som användaren ska gissa.
1. Skapa en loop som körs tills användaren gissat rätt.
1. Varje gång användaren gissar fel, skriv ut om gissningen var för hög eller för låg.
1. Om användaren gissar fel 10 gånger, skriv ut att användaren förlorat och avsluta programmet.
1. Skriv ut hur många gissningar det tog för användaren att gissa rätt.
   1. Om användaren gissar rätt på första försöket, skriv ut att användaren vunnit med en perfekt poäng.
   1. Om användaren gissar rätt på andra försöket, skriv ut att användaren vunnit med en bra poäng.
   1. Om användaren gissar rätt på tredje försöket, skriv ut att användaren vunnit med en medel poäng.
   1. Om användaren gissar rätt på fjärde försöket, skriv ut att användaren vunnit med en dålig poäng.
   1. Om användaren gissar rätt på femte försöket, skriv ut att användaren vunnit med en dålig poäng.
   1. Om användaren gissar rätt på sjätte försöket eller senare, skriv ut att användaren förlorat.

Exempel på körning:

```
Gissa ett tal mellan 1 och 10: 5
Fel! Gissa högre!
Gissa ett tal mellan 1 och 10: 7
Fel! Gissa lägre!
Gissa ett tal mellan 1 och 10: 6
Rätt! Du gissade rätt på 3 försök! Du får poäng medel!
...
```

### Programloop och meny

#### 1. Projekt LoopMeny

1. Skapa ett nytt konsolprojekt **LoopMeny**.
2. Skapa ett program med en meny med följande val:

> 1. Slumpa ett tal mellan 1 och 10
> 2. Slumpa ett tal mellan 1 och 100
> 3. Slumpa 5 tal mellan 1 och 10
> 4. Avsluta

1. Skapa en `if`-sats som:\
   1. Som slumpar **ett** tal mellan 1 och 10 ifall användaren skriver in "1" i menyn.\
   1. Som slumpar **ett** tal mellan 1 och 100 ifall användaren skriver in "2" i menyn.\
   1. Som slumpar **fem** tal mellan 1 och 10 ifall användaren skriver in "3" i menyn.
4. Lägg all kod inuti en `while`-loop
5. Lägg till ett villkor som avbryter loopen ifall användaren skriver in "4" i menyn.
6. Lägg till ett villkor som skriver ut "Felaktigt val" ifall användaren skriver in något annat än "1", "2", "3" eller "4" i menyn.

Exempel på körning:

```
1. Slumpa ett tal mellan 1 och 10
2. Slumpa ett tal mellan 1 och 100
3. Slumpa 5 tal mellan 1 och 10
4. Avsluta
Välj ett alternativ: 1
Slumpat tal: 5
1. Slumpa ett tal mellan 1 och 10
2. Slumpa ett tal mellan 1 och 100
3. Slumpa 5 tal mellan 1 och 10
4. Avsluta
Välj ett alternativ: 2
Slumpat tal: 42
...
```

7. Provat att byta ut `if`-satserna mot en `switch`-sats.

#### 2. Projekt VariablersLivslangd

1. Skapa ett nytt konsolprojekt **VariablersLivslangd**.
2. Skapa ett program med en meny med följande val:

> 1. Skriv in ett tal
> 2. Skriv ut summan av alla tal
> 3. Avsluta

1. Skapa en `while`-loop som fortsätter så länge användaren inte har skrivit in "3" i menyn.
2. Skapa en variabel som lagrar summan av alla tal som användaren skrivit in. OBS: Variabeln måste skapas ovanför `while`-loopen.
3. Skapa en `if`-sats som skriver ut "Skriv in ett tal" ifall användaren skriver in "1" i menyn.
4. Skapa en `if`-sats som skriver ut summan av alla tal som användaren skrivit in ifall användaren skriver in "2" i menyn.
5. Skapa en `if`-sats som avbryter loopen ifall användaren skriver in "3" i menyn.
6. Skapa en `if`-sats som skriver ut "Felaktigt val" ifall användaren skriver in något annat än "1", "2" eller "3" i menyn.
7. Skapa en `if`-sats som läser in ett tal från användaren och lägger till det i summan av alla tal som användaren skrivit in.
8. Byt ut `if`-satserna mot en `switch`-sats.

Exempel på körning:

```
1. Skriv in ett tal
2. Skriv ut summan av alla tal
3. Avsluta
Välj ett alternativ: 1
Skriv in ett tal: 5
1. Skriv in ett tal
2. Skriv ut summan av alla tal
3. Avsluta
Välj ett alternativ: 1
Skriv in ett tal: 10
1. Skriv in ett tal
2. Skriv ut summan av alla tal
3. Avsluta
Välj ett alternativ: 2
Summan av alla tal: 15
1. Skriv in ett tal
2. Skriv ut summan av alla tal
3. Avsluta
Välj ett alternativ: 3
```
