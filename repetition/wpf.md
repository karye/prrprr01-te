---
description: Bra resurser för att träna C#-programmering på egen hand
---
# WPF

## Allmänna bra länkar

* [WPF Tutorial](https://www.wpf-tutorial.com/)

## Uppgifter

### Skapa en enkel valutaomvandlare

Appen kommer att konvertera mellan svenska kronor och amerikanska dollar.

#### Steg 1: Skapa en ny WPF-projektmapp

* Skapa en ny WPF-projektmapp med namnet **Valutaomvandlare**.
* I konsolen skriv:

```powershell
dotnet new wpf
```

#### Steg 2: Skapa layouten i MainWindow.xaml

* I `MainWindow.xaml` lägg till följande kod:

```xml
<Window x:Class="Valutaomvandlare.MainWindow"
        xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
        xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
        Title="Valutaomvandlare" Height="200" Width="400">
    <StackPanel Margin="5">
        <Label Margin="5">Svenska kronor:</Label>
        <TextBox Name="svenskaKronorTextBox"></TextBox>
        
        <Label Margin="5">Amerikanska dollar:</Label>
        <TextBox Name="amerikanskaDollarTextBox"></TextBox>
        
        <Button Margin="5" Click="KonverteraValuta">Konvertera</Button>
    </StackPanel>
</Window>

```

#### Steg 3: Skapa logiken i MainWindow.xaml.cs

Knappen `Konvertera` är kopplad till en metod i `MainWindow.xaml.cs` som heter `KonverteraValuta()`.  
Namnet på metoden får vi från följande rad i `MainWindow.xaml`:

```xml
<Button Margin="5" Content="Konvertera" Click="KonverteraValuta"/>
```

* I `MainWindow.xaml.cs` lägg till följande kod:

```csharp
using System.Windows;

namespace Valutaomvandlare
{
    public partial class MainWindow : Window
    {
        // Konstanter för valutakurser
        private const double KRONOR_PER_DOLLAR = 9.3;
        private const double DOLLAR_PER_KRONOR = 0.11;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void KonverteraValuta(object sender, RoutedEventArgs e)
        {

        }
    }
}
```

#### Steg 4: Implementera KonverteraValuta-metoden

* Först läser vi in värdet från `svenskaKronorTextBox` och konverterar det till en `double`.
* Sedan konverterar vi värdet till amerikanska dollar.
* Sedan skriver vi ut värdet i `amerikanskaDollarTextBox`.

```csharp
private void KonverteraValuta(object sender, RoutedEventArgs e)
{
    // Läs in värdet från svenskaKronorTextBox
    string svenskaKronorText = svenskaKronorTextBox.Text;

    // Konvertera texten till en double
    double svenskaKronor = double.Parse(svenskaKronorText);

    // Konvertera svenska kronor till amerikanska dollar
    double amerikanskaDollar = svenskaKronor * DOLLAR_PER_KRONOR;

    // Skriv ut värdet i amerikanskaDollarTextBox
    amerikanskaDollarTextBox.Text = amerikanskaDollar.ToString();
}
```

#### Steg 5: Felhantering

* Om användaren skriver in något annat än en siffra i `svenskaKronorTextBox` så kommer programmet att krascha.
* Vi måste därför hantera felaktiga inmatningar.

```csharp
private void KonverteraValuta(object sender, RoutedEventArgs e)
{
    // Läs in värdet från svenskaKronorTextBox
    string svenskaKronorText = svenskaKronorTextBox.Text;

    // Konvertera texten till en double
    double svenskaKronor = 0;
    if (double.TryParse(svenskaKronorText, out svenskaKronor))
    {
        // Konvertera svenska kronor till amerikanska dollar
        double amerikanskaDollar = svenskaKronor * DOLLAR_PER_KRONOR;

        // Skriv ut värdet i amerikanskaDollarTextBox
        amerikanskaDollarTextBox.Text = amerikanskaDollar.ToString();
    }
    else
    {
        // Skriv ut felmeddelande
        MessageBox.Show("Felaktig inmatning!");
    }
}
```

#### Några utmaningar

1. Lägg till en knapp för att rensa textrutorna: `svenskaKronorTextBox` och `amerikanskaDollarTextBox`.

2. Lägg till en knapp för att konvertera från amerikanska dollar till svenska kronor.
  Tips: Du behöver behöver ska skapa en ny metod för att konvertera från amerikanska dollar till svenska kronor.

### Skapa en todo-lista

Appen kommer att visa en lista med todo-objekt.

#### Steg 1: Skapa en ny WPF-projektmapp

* Skapa en ny WPF-projektmapp med namnet **TodoLista**.
* I konsolen skriv:

```powershell
dotnet new wpf
```

#### Steg 2: Skapa layouten i MainWindow.xaml

* I `MainWindow.xaml` lägg till följande kod:

```xml
<Window x:Class="TodoLista.MainWindow"
        xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
        xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
        Title="TodoLista" Height="200" Width="400">
    <StackPanel Margin="5">
        <Label Margin="5">Todo:</Label>
        <TextBox Name="todoTextBox"></TextBox>
        
        <Button Margin="5" Click="LaggTillTodo">Lägg till</Button>
        
        <Label Margin="5">Todo-lista:</Label>
        <ListBox Name="todoListBox"></ListBox>
    </StackPanel>
</Window>
```

#### Steg 3: Skapa logiken i MainWindow.xaml.cs

* Koppla knappen till en metod i MainWindow.xaml.cs.
* I `MainWindow.xaml.cs` lägg till följande kod:

```csharp
using System.Windows;

namespace TodoLista
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void LaggTillTodo(object sender, RoutedEventArgs e)
        {

        }
    }
}
```

#### Steg 4: Implementera LaggTillTodo-metoden

* Först läser vi in värdet från `todoTextBox` och konverterar det till en `string`.
* Sedan lägger vi till värdet i `todoListBox`.

```csharp
private void LaggTillTodo(object sender, RoutedEventArgs e)
{
    // Läs in värdet från todoTextBox
    string todo = todoTextBox.Text;

    // Lägg till värdet i todoListBox
    todoListBox.Items.Add(todo);
}
```

#### Steg 5: Felhantering

* Om användaren glömmer att skriva in något i `todoTextBox` så ska en felmeddelande visas.
* Vi måste därför hantera tomma inmatningar.

```csharp
private void LaggTillTodo(object sender, RoutedEventArgs e)
{
    // Läs in värdet från todoTextBox
    string todo = todoTextBox.Text;

    if (todo.Length > 0)
    {
        // Lägg till värdet i todoListBox
        todoListBox.Items.Add(todo);
    }
    else
    {
        // Skriv ut felmeddelande
        MessageBox.Show("Du måste skriva in något!");
    }
}
```

#### Några utmaningar

1. Lägg till en knapp för att rensa textrutan `todoTextBox`.  
  Tips: Du behöver bara sätta värdet på `todoTextBox` till en tom sträng.

2. Lägg till en knapp för att rensa listboxen `todoListBox`.  
  Tips: Du behöver bara rensa listboxen med `todoListBox.Items.Clear();`.

3. Lägg till en knapp för att ta bort det markerade objektet i listboxen `todoListBox`.  
  Tips: Du behöver bara ta bort det markerade objektet med `todoListBox.Items.RemoveAt(todoListBox.SelectedIndex);`.


### Skapa en BMI-kalkylator

Appen kommer att visa användarens BMI.

#### Steg 1: Skapa en ny WPF-projektmapp

* Skapa en ny WPF-projektmapp med namnet **BmiKalkylator**.
* I konsolen skriv:

```powershell
dotnet new wpf
```

#### Steg 2: Skapa layouten i MainWindow.xaml

* I `MainWindow.xaml` lägg till följande kod:

```xml
<Window x:Class="BmiKalkylator.MainWindow"
        xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
        xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
        Title="BmiKalkylator" Height="200" Width="400">
    <StackPanel Margin="5">
        <Label Margin="5">Vikt (kg):</Label>
        <TextBox Name="viktTextBox"></TextBox>
        
        <Label Margin="5">Längd (m):</Label>
        <TextBox Name="langdTextBox"></TextBox>
        
        <Button Margin="5" Click="BeraknaBmi">Beräkna</Button>
        
        <Label Margin="5">BMI:</Label>
        <TextBox Name="bmiTextBox"></TextBox>
    </StackPanel>
</Window>
```

#### Steg 3: Skapa logiken i MainWindow.xaml.cs

* Koppla knappen till en metod i MainWindow.xaml.cs.

#### Steg 4: Implementera BeraknaBmi-metoden

* Först läser vi in värdet från `viktTextBox` och `langdTextBox` och konverterar det till en `double`.
* Sedan beräknar vi BMI.
* Slutligen skriver vi ut BMI i `bmiTextBox`.

#### Steg 5: Felhantering

* Om användaren skriver något annat än siffror i `viktTextBox` och `langdTextBox` så ska en felmeddelande visas.
* Vi måste därför hantera felaktig inmatning.
  * Vi kan använda `double.TryParse` för att kontrollera om inmatningen är en siffra.
  * Vi kan använda `MessageBox.Show` för att skriva ut felmeddelande.

#### Några utmaningar

Det finns en tabell som visar BMI och dess betydelse. Tabellen ser ut såhär:

| BMI | Betydelse |
| --- | -------- |
| Under 18,5 | Undervikt |
| 18,5 - 24,9 | Normalvikt |
| 25,0 - 29,9 | Övervikt |
| 30,0 - 34,9 | Fetma grad 1 |
| 35,0 - 39,9 | Fetma grad 2 |
| 40,0 eller högre | Fetma grad 3 |

1. Skriv ut en text som visar användarens BMI och dess betydelse.  
  Tips: Du behöver bara skriva ut en sträng i `bmiTextBox`.

