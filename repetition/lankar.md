# Länkar

## Videogenomgångar  

* [How to program C# (en)](https://www.youtube.com/watch?v=N775KsWQVkw&list=PLPV2KyIb3jR4CtEelGPsmPzlvP7ISPYzR)

{% embed url="https://www.youtube.com/watch?v=N775KsWQVkw&list=PLPV2KyIb3jR4CtEelGPsmPzlvP7ISPYzR" %}

* [Dani Krossing C# Tutorials (en)](https://www.youtube.com/playlist?list=PL0eyrZgxdwhxD9HhtpuZV22KxEJAZ55X-)

{% embed url="https://www.youtube.com/playlist?list=PL0eyrZgxdwhxD9HhtpuZV22KxEJAZ55X-" %}

* [Mr1Buying C# (sv)](https://www.youtube.com/playlist?list=PLDH562Qb745NFrMwlweruEZH4rraEcRJ3)

{% embed url="https://www.youtube.com/playlist?list=PLDH562Qb745NFrMwlweruEZH4rraEcRJ3" %}

* [C#-skolan Programmering 1 (sv)](https://www.youtube.com/playlist?list=PLGIaaBeLgSj-gR2ce4llrGt9c_1J7JqoC)

{% embed url="https://www.youtube.com/playlist?list=PLGIaaBeLgSj-gR2ce4llrGt9c_1J7JqoC" %}

* [Distansakademin: Lär dig C# (sv)](https://www.youtube.com/playlist?list=PLI5JF23TK_8CB6bhsiRski3W3PCFqVltx)

{% embed url="https://www.youtube.com/playlist?list=PLI5JF23TK_8CB6bhsiRski3W3PCFqVltx" %}

## C# Fundamentals course - avancerad

{% embed url="https://app.pluralsight.com/course-player?clipId=83e472fe-1c5a-45ef-bce0-7120bea00dea" %}
