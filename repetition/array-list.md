---
description: Bra resurser för att träna C#-programmering på egen hand
---
# Samlingar - arrayer och listor

## Videogenomgångar

{% embed url="https://youtu.be/7R4YDqisbyU?si=-_ICs-XezWSSlmNj" %}

{% embed url="https://youtu.be/43FGQoSebHQ?si=sk5Yi7OZQyvpJIq_" %}

## Allmänna bra länkar

* [C# referens: array & listor](https://csharp.progdocs.se/grundlaeggande/listor-och-arrayer#array)
* [Listor](https://www.youtube.com/watch?v=GgZlgYq-eLs)
* [Arrayer & Random](https://www.youtube.com/watch?v=i_6FQ0ZlGek)

## TLDR;

```csharp
// Så här skapar du en array med 5 platser
int[] tal = new int[5];

// Så här skapar du en array med 5 platser och fyller den med värden
string[] namn = [ "Pelle", "Lisa", "Kalle", "Anna", "Olle" ];

// Så här läser du ett element från en array
string namn = namn[0];

// Så här skriver du ett element till en array
namn[0] = "Pelle";
```

## Uppgifter

 > ### Tips: Infoga uppgiftens steg som kommentarer i din kod för att hålla reda på vad du ska göra.

### Array och loopar

#### 1. Projekt ArrayOchLoopar

1. Skapa ett nytt konsolprojekt **ArrayOchLoopar**.
1. Skapa en `string`-array med namnen på fem olika spel.
1. Skapa en `for`-loop (eller `foreach`-loop) som skriver ut namnet på varje spel på en egen rad.
1. Skapa en `string`-array med namnen på fem av dina klasskamrater.
1. Skapa en `int`-array med fem valfria värden mellan 0 och 10.
1. Komplettera `for`-loopen så att den på varje rad skriver ut något liknande "Pelle ger CounterStrike 99 betyget 5".  
Använd dina klasskamraters namn, spelens namn och värdena från `int`-arrayen.

Exempel på körning:
```text
Pelle ger CounterStrike 99 betyget 5
Lars ger ...
```

#### 2. Projekt ArrayMedStäder

1. Skapa ett nytt konsolprojekt **ArrayMedStäder**.
1. Skapa en tom `string`-array som heter `städer`.
1. Skapa en loop där du varje gång loopen körs hämtar en `string` från användaren med `Console.ReadLine()`.  
  1. Lägg till den `string` du får från användaren till `städer`-arrayen. 
  1. Avbryt loopen om användaren bara trycker Return.

Exempel på körning:
```text
Ange en stad (eller Return för att avsluta): Göteborg
Ange en stad (eller Return för att avsluta): Malmö
...
Ange en stad (eller Return för att avsluta): 
```

#### 3. Projekt ArrayMedTal

1. Skapa ett nytt konsolprojekt **ArrayMedTal**.
1. Skapa en tom `int`-array som heter `talen`.
1. Skapa en loop där du varje gång loopen körs hämtar en `int` från användaren med `Console.ReadLine()`.  
  1. Lägg till den `int` du får från användaren till `talen`-arrayen. 
  1. Avbryt loopen om användaren bara trycker Return.
1. Skapa en loop som ligger efter den ovanstående. 
  1. Den här loopen ska skriva ut alla tal som finns sparade i `talen`-arrayen.

Exempel på körning:
```text
Ange ett tal (eller Return för att avsluta): 1
Ange ett tal (eller Return för att avsluta): 2
Alla tal: 1, 2
```

### Array, programloop och meny

#### 1. Projekt ArrayMedNamn

1. Skapa ett nytt konsolprojekt **ArrayMedNamn**.
1. Skapa en programloop mha en `while`-loop.
1. Skapa en tom `string`-array som heter `namnen`.
1. Skapa en meny som skriver ut en meny med följande val:

> 1. Lägg till namn
> 1. Lista namnen 
> 1. Avsluta  

1. Om användaren väljer "1" så ska användaren få skriva in ett namn. Namnet ska läggas till i `namnen`-listan.
1. Om användaren väljer "2" så ska alla namn som finns i `namnen`-arrayen skrivas ut.
1. Om användaren väljer "3" så ska loopen avbrytas.
1. Om användaren väljer något annat så ska menyn skrivas ut igen.

Exempel på körning:
```text
1. Lägg till namn
2. Lista namnen
3. Avsluta
Välj: 1
Ange namn: Pelle
1. Lägg till namn
2. Lista namnen
3. Avsluta
Välj: 2
Pelle
1. Lägg till namn
2. Lista namnen
3. Avsluta
Välj: 3
```
