---
description: Bra resurser för att träna C#-programmering på egen hand
---
# Metoder

## Videogenomgångar

{% embed url="https://youtu.be/yH0xxLe29eQ?si=aYODLYFid2hgGOlK" %}

## Allmänna bra länkar

* [Metoder](https://csharp.progdocs.se/grundlaeggande/egna-metoder)

## Uppgifter

 > ### Tips: Infoga uppgiftens steg som kommentarer i din kod för att hålla reda på vad du ska göra.

### Metoder som skriver ut text

#### 1. Projekt MetodHej

1. Skapa ett nytt konsolprogram **MetodHej**.
1. Skapa en metod som heter `SkrivHej()` som inte tar några parametrar. Metoden ska skriva ut "Hej!".

Tips: Du kan skapa en metod genom att skriva `void` följt av metodenamnet:

```csharp
void SkrivHej()
{
    // Metoden ska skriva ut "Hej!"
}

SkrivHej();

```

Exempel på körning:
```text
Hej!
```

### Metoder som tar parametrar

#### 1. Projekt MetodNamn

1. Skapa ett nytt konsolprogram **MetodNamn**.
1. Skapa en metod som heter `SkrivHejMedNamn()` som tar en `string`-parameter som heter `namn`. Metoden ska skriva ut "Hej {namn}!" 

Tips: Du kan skapa en metod som tar en parameter genom att skriva `void` följt av metodenamnet, parameternamnet och parametertypen:

```csharp
void SkrivHejMedNamn(string namn)
{
    // Metoden ska skriva ut "Hej {namn}!"
}

Console.Write("Skriv in namn: ");
string namn = Console.ReadLine();
SkrivHejMedNamn(namn);

```

Exempel på körning:
```text
Skriv in namn: Oskar
Hej Oskar!
```

#### 2. Projekt MetodNamnÅlder

1. Skapa ett konsolprogram **MetodNamnÅlder**.
1. Skapa en metod som heter `SkrivHejMedNamnOchAlder()` som tar en `string`-parameter som heter `namn` och en `int`-parameter som heter `ålder`. 
  1. Metoden ska skriva ut "Hej {namn}! Du är {ålder} år gammal."
  1. Lägg till en `if`-sats i metoden `SkrivHejMedNamnOchAlder()` som skriver ut "Du är myndig!" om `ålder` är större än 18.

Tips: Du kan skapa en metod som tar flera parametrar genom att skriva `void` följt av metodenamnet, parameternamnet och parametertypen:

```csharp
void SkrivHejMedNamnOchAlder(string namn, int ålder)
{
    // Metoden ska skriva ut "Hej {namn}! Du är {ålder} år gammal."
}

Console.Write("Skriv in namn: ");
string namn = Console.ReadLine();
Console.Write("Skriv in ålder: ");
int ålder = int.Parse(Console.ReadLine());
SkrivHejMedNamnOchAlder(namn, ålder);
```

Exempel på körning:
```text
Skriv in namn: Oskar
Skriv in ålder: 17
Hej Oskar! Du är 17 år gammal.
```

#### 3. Projekt MetodTempOmvandlare

1. Skapa ett konsolprogram **MetodTempOmvandlare**.
1. Skapa en metod `CelsiusTillFahrenheit()` som tar en `double`-parameter som heter `celsius`.
1. Metoden ska skriva ut `celsius` * 9 / 5 + 32.

Tips: Du kan skapa en metod som tar flera parametrar genom att skriva `void` följt av metodenamnet, parameternamnet och parametertypen:

```csharp
// Fråga användaren om grader i Celsius och skicka in värdet till metoden `CelsiusTillFahrenheit()`.
void CelsiusTillFahrenheit(double celsius)
{
    // Metoden ska skriva ut `celsius` * 9 / 5 + 32.
}

Console.Write("Skriv in grader i Celsius: ");
double celsius = double.Parse(Console.ReadLine());

// Kör metoden `CelsiusTillFahrenheit()` med parametern `celsius`.
CelsiusTillFahrenheit(celsius);
```

Exempel på körning:
```text
Skriv in grader i Celsius: 20
20 grader Celsius är 68 grader Fahrenheit
```

#### 4. Projekt MetodFahrenheit

1. Skapa en metod `FahrenheitTillCelsius()` som tar en `double`-parameter som heter `fahrenheit`.
1. Metoden ska skriva ut `(fahrenheit - 32) * 5 / 9`.

Tips: Du kan skapa en metod som tar flera parametrar genom att skriva `void` följt av metodenamnet, parameternamnet och parametertypen:

```csharp
// Fråga användaren om grader i Fahrenheit och skicka in värdet till metoden `FahrenheitTillCelsius()`.
void FahrenheitTillCelsius(double fahrenheit)
{
    // Metoden ska skriva ut `(fahrenheit - 32) * 5 / 9`.
}
    
FahrenheitTillCelsius(fahrenheit);
```

Exempel på körning:
```text
Skriv in grader i Fahrenheit: 68
68 grader Fahrenheit är 20 grader Celsius
```

#### 5. Projekt MetodMånad

1. Skapa ett konsolprogram **MetodMånad**.
1. Skapa en metod `SkivUtMånad()` som tar en `int`-parameter som heter månad.
  1. I metoden skapa en `array` med 12 strängar som innehåller månadernas namn.
  1. Använd `int`-parametern som index i arrayen och skriv ut månadens namn.

Tips: Du kan skapa en metod som tar flera parametrar genom att skriva `void` följt av metodenamnet, parameternamnet och parametertypen:

```csharp
// Fråga användaren om en månad och skicka in värdet till metoden `SkrivUtMånad()`.
void SkrivUtMånad(int månad)
{
    // Metoden ska skriva ut månadens namn.
}

Console.Write("Skriv in månad (1-12): ");
int månad = int.Parse(Console.ReadLine());
SkrivUtMånad(månad);
```

Exempel på körning:
```text
Skriv in månad: 1
Januari
```

### Metoder som returnerar värden

#### 1. Projekt MetoderSummera

1. Skapa ett konsolprogram **MetoderSummera**.
1. Skapa en metod som heter `Summera()` och som tar två int-parametrar som heter `tal1` och `tal2`.
  1. Inuti metoden, skapa en variabel som heter `summa` som är summan av `tal1` och `tal2`.
  1. Returnera sedan summan genom att använda `return`-satsen.

Tips: Du kan skapa en metod som tar flera parametrar genom att skriva `void` följt av metodenamnet, parameternamnet och parametertypen:

```csharp
int Summera(int tal1, int tal2)
{
    // Metoden ska returnera summan av tal1 och tal2.
}

// Fråga användaren om två tal.

int summa = Summera(tal1, tal2);
    
// Skriv ut summan.

```

Exempel på körning:
```text
Skriv in ett tal: 5
Skriv in ett till tal: 10
Summan av talen är: 15
```

#### 2. Projekt MetoderArea

1. Skapa ett konsolprogram **MetodArea**.
1. Skapa en metod som heter `BeräknaCircleArea()` som tar en `double`-parameter som heter `radius` och returnerar arean av en cirkel med den angivna radien.
  1. Returnera `Math.PI * radius * radius`.

Tips: Du kan skapa en metod som tar flera parametrar genom att skriva `void` följt av metodenamnet, parameternamnet och parametertypen:
  
```csharp
double BeräknaCircleArea(double radius)
{
    // Metoden ska returnera arean av en cirkel med den angivna radien.
}

// Fråga användaren om radien på cirkeln.

double area = BeräknaCircleArea(radius);

// Skriv ut arean av cirkeln.

```

Exempel på körning:
```text
Skriv in radien: 5
Arean av cirkeln är: 78.53981633974483
```

#### 3. Projekt MetodTriangel

1. Skapa ett konsolprogram **MetodTriangel**.
1. Skapa en metod som heter `BeräknaTriangelArea()` som tar två `double`-parameter som heter `bas` och `höjd` och returnerar arean av en triangel med den angivna basen och höjden.
  1. Returnera `bas` * `höjd` / 2.

Tips: Du kan skapa en metod som tar flera parametrar genom att skriva `void` följt av metodenamnet, parameternamnet och parametertypen:
  
```csharp
double BeräknaTriangelArea(double bas, double höjd)
{
    // Metoden ska returnera arean av en triangel med den angivna basen och höjden.
}

// Fråga användaren om basen och höjden på triangeln.

double area = BeräknaTriangelArea(bas, höjd);

// Skriv ut arean av triangeln.

```

Exempel på körning:
```text
Skriv in basen: 5
Skriv in höjden: 10
Arean av triangeln är: 25
```

#### 4. Projekt MetodKvot

1. Skapa ett konsolprogram **MetoderKvot**.
1. Skapa en metod som heter `HeltalsDivision()` som tar två `int`-parameter som heter `täljare` och `nämnare`.
  1. Returnera `täljare` / `nämnare`.
  1. Lägg till en `if`-sats som kontrollerar att `nämnare` inte är 0 och skriver ut ett felmeddelande om det är fallet.

Tips: Du kan skapa en metod som tar flera parametrar genom att skriva `void` följt av metodenamnet, parameternamnet och parametertypen:
    
```csharp
double HeltalsDivision(int täljare, int nämnare)
{
    // Metoden ska returnera kvoten av heltalsdivisionen av täljaren och nämnaren.
}

// Fråga användaren om täljaren och nämnaren.

double kvot = HeltalsDivision(täljare, nämnare);

// Skriv ut kvoten.

```

Exempel på körning:
```text
Skriv in täljaren: 5
Skriv in nämnaren: 0
Fel: Nämnaren kan inte vara 0
```

#### 5. Projekt MetodRäkna

1. Skapa ett konsolprogram **MetoderRäkna**.
1. Skapa en metod som heter `RäknaBokstäver()` som tar en `string`-parameter som heter `text` och returnerar antalet bokstäver i texten (exklusive mellanslag).
  1. Om parametern är tom ("") ska metoden returnera 0.
  1. Om parametern innehåller mer än 100 tecken ska metoden returnera -1.

Tips: Du kan skapa en metod som tar flera parametrar genom att skriva `void` följt av metodenamnet, parameternamnet och parametertypen:

```csharp
int RäknaBokstäver(string text)
{
    // Metoden ska returnera antalet bokstäver i texten (exklusive mellanslag).
}

// Fråga användaren om en text.

int antalBokstäver = RäknaBokstäver(text);

// Skriv ut antalet bokstäver i texten.

```

Exempel på körning:
```text
Ange en text: Hej på dig!
Antalet bokstäver i texten är: 8
```

#### 6. Projekt MetodPrimtal

1. Skapa ett konsolprogram **MetoderPrimtal**.
1. Skapa en metod som heter `ÄrTaletPrimtal()` som tar en `int`-parameter som heter `tal` och returnerar `true` om talet är ett primtal, annars returnerar den `false`.
  1. Skapa en loop som går igenom alla tal mellan 2 och parametern.
  1. Använd `if`-satsen och använd modulus-operatorn (%) för att kontrollera om talet är jämnt delbart med något tal mellan 2 och talet. Dvs om `talet %% i == 0`.

Tips: Du kan skapa en metod som tar flera parametrar genom att skriva `void` följt av metodenamnet, parameternamnet och parametertypen:
  
```csharp
bool ÄrTaletPrimtal(int tal)
{
    // Metoden ska returnera true om talet är ett primtal, annars returnerar den false.
}

// Fråga användaren om ett tal.

bool ärPrimtal = ÄrTaletPrimtal(tal);

// Skriv ut om talet är ett primtal eller inte.

```

Exempel på körning:
```text
Skriv in ett tal: 7
Talet är ett primtal
```

### Metoder som tar array och listor som parametrar

#### 1. Projekt MetoderSamlingar

1. Skapa ett nytt konsolprogram och kalla det **MetoderSamlingar**.
1. Skapa en metod som heter `SummeraAllaTal()` som tar en `array` av `int`-värden som parameter och returnerar summan av alla tal i arrayen.
  1. Skapa en variabel `summa` och tilldela den värdet 0.
  1. Använd en `foreach`-loop för att iterera över arrayen och lägga till varje element till summan.
  1. Returnera summan när loopen är klar.

Tips: Du kan skapa en metod som tar flera parametrar genom att skriva `void` följt av metodenamnet, parameternamnet och parametertypen:

```csharp
int SummeraAllaTal(int[] talen)
{
    // Skapa en variabel som heter summa och tilldela den värdet 0.

    // Skapa en foreach-loop som går igenom alla tal i arrayen.

    // Lägg till varje tal i arrayen till summan.

}

// Skapa en array med talen 5, 10, 15

int summa = SummeraAllaTal(talen);
Console.WriteLine($"Summan av talen är: {summa}");

```

Exempel på körning:
```text
Summan av talen är: 15
```

#### 2. Projekt MetoderListor

1. Skapa ett konsolprogram **MetoderListor**.
1. Skapa en metod som heter `SummeraLista()` som tar en `List<int>`-parameter som heter `lista`. Metoden ska returnera summan av alla tal i listan.
  1. Skapa en foreach-loop som går igenom alla tal i listan.
  1. Lägg till varje tal i listan till en variabel som heter `summa`.
  1. Returnera summan.

Tips: Du kan skapa en metod som tar flera parametrar genom att skriva `void` följt av metodenamnet, parameternamnet och parametertypen:

```csharp
int SummeraLista(List<int> talen)
{
    // Skapa en variabel som heter summa och tilldela den värdet 0.

    // Loopa igenom alla tal i listan och lägg till varje tal till summan.

    // Returnera summan.

}

List<int> talen = new List<int>();
  
// Skapa en loop som frågar användaren (5x) om ett tal och lägger till det i listan med talen.Add(tal).

int summa = SummeraLista(talen);
Console.WriteLine($"Summan av talen är: {summa}");

```

Exempel på körning:
```text
Summan av talen är: 15
```

#### 3. Projekt MetoderBilar

1. Skapa ett konsolprogram **MetoderBillista**.
1. Skapa en metod som heter `Bilar()` som en string parameter som heter `bokstav`. Metoden ska returnera en lista med alla bilar som börjar på bokstaven.
  1. Skapa en lista med bilar.
  1. Skapa en tom lista som heter `bilarMedBokstav`.
  1. Skapa en foreach-loop som går igenom alla bilar i listan.
  1. Om bokstaven i bilar är samma som bokstaven som skickas in som parameter ska bilar läggas till i listan `bilarMedBokstav`.
  1. Returnera listan `bilarMedBokstav`.

Tips: Du kan skapa en metod som tar flera parametrar genom att skriva `void` följt av metodenamnet, parameternamnet och parametertypen:

```csharp
List<string> Bilar(string bokstav)
{
    // En  lista på 20 bilmärken.
    List<string> bilar = ["Audi", "BMW", "Citroen", "Dacia", "Fiat", 
                                              "Ford", "Honda", "Hyundai", "Jaguar", "Kia", 
                                              "Lada", "Lamborghini", "Lancia", "Land Rover", 
                                              "Mazda", "Mercedes", "Mini", "Mitsubishi", "Nissan", "Opel"];

    // Skapa en tom lista som heter bilarMedBokstav.

    // Skapa en foreach-loop som går igenom alla bilar i listan.

    // Om bokstaven i bilar är samma som bokstaven som skickas in som parameter ska bilar läggas till i listan bilarMedBokstav.

    // Returnera listan bilarMedBokstav.

}

// Fråga användaren om en bokstav.

List<string> bilar = Bilar(bokstav);

// Skriv ut alla bilar som börjar på bokstaven.

```

#### 4. Projekt MetoderStörsta

1. Skapa ett konsolprogram **MetoderStörsta**.
1. Skapa en metod som heter `HittaStörstaTalet()` som tar en `List<int>`-parameter som heter talen. Metoden ska returnera det största talet i listan.
  1. Lägg till en `if`-sats i metoden `HittaStörstaTalet()` som kollar om listan är tom. Om så är fallet ska metoden returnera 0.

Tips: Du kan skapa en metod som tar flera parametrar genom att skriva `void` följt av metodenamnet, parameternamnet och parametertypen:

```csharp
int HittaStörstaTalet(List<int> talen)
{
    // Metoden ska returnera det största talet i listan.
}

// Skapa en lista med talen 5, 10, 15

int störstaTalet = HittaStörstaTalet(talen);
    
// Skriv ut det största talet i listan. Tex: "Största talet är: 15"

```

Exempel på körning:
```text
Största talet är: 10
```

#### 5. Projekt MetoderVokaler

1. Skapa ett konsolprogram **MetoderVokaler**.
1. Skapa en metod som heter `RäknaAntalVokaler()` som tar en `string`-parameter och returnerar antalet vokaler i strängen.
  1. Skapa en variabel `antal` och tilldela den värdet 0.
  1. Använd en `foreach`-loop för att iterera över strängen och öka antalet med 1 om varje element är en vokal.
  1. Returnera antalet när loopen är klar.

Tips: Du kan skapa en metod som tar flera parametrar genom att skriva `void` följt av metodenamnet, parameternamnet och parametertypen:

```csharp
int RäknaAntalVokaler(string mening)
{
    // Metoden ska returnera antalet vokaler i strängen.
}

// Fråga användaren om en mening.

int antalVokaler = RäknaAntalVokaler(mening);
    
// Skriv ut antalet vokaler: Tex "Antal vokaler i meningen är: 3"

```

Exempel på körning:
```text
Skriv en mening: Hej på dig!
Antal vokaler i meningen är: 3
```