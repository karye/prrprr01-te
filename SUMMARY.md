# Table of contents

## Introduktion

* [Kursinnehåll](README.md)
* [Kriterier](introduktion/kriterier.md)
* [Utvecklingsmiljön](introduktion/utvecklingsmiljoen.md)
* [Bakgrund](introduktion/historia.md)
* [Länkar](introduktion/laenkar.md)
* [God kod](introduktion/god-kod.md)

## Repetition

* [Länkar](repetition/lankar.md)
* [Kontrollstrukturer](repetition/if-loop.md)
* [Array & listor](repetition/array-list.md)
* [Metoder](repetition/metoder.md)
* [WPF](repetition/wpf.md)

## 1. Konsolen

* [Projekt i VS Code](kapitel-1/skapa-projekt.md)
* [Interagera i konsolen](kapitel-1/konsolen.md)
* [Programmeringssyntax](kapitel-1/programmerings-syntax.md)
* [Pseudokod & flödesschema](kapitel-1/floedesschema.md)

## 2. Variabler

* [Använda variabler & datatyper](kapitel-2/variabler-datatyper.md)
* [Stränghantering](kapitel-2/straenghantering.md)
* [Labb Mars position](kapitel-2/uppgifter.md)

## 3. Villkor

* [Skapa villkor med if-satser](kapitel-3/villkor.md)
* [Räknare och flaggor](kapitel-3/raknare-flaggor.md)
* [Uppgift: chatt](kapitel-3/uppgift-chatt.md)
* [Mikroövningar](kapitel-3/ovningar/README.md)
  * [if-satser 1](kapitel-3/ovningar/ovningar-if-1.md)
  * [if-satser 2](kapitel-3/ovningar/ovningar-if-2.md)

## 4. Loopar

* [Gissa tal med loopar](kapitel-4/gissa-loop.md)
* [Skapa meny med loopar](kapitel-4/meny-loop.md)
* [Uppgift: quiz](kapitel-4/uppgift-quiz.md)
* [Uppgift: sten, sax, påse](kapitel-4/uppgift-stensaxpase.md)
* [Upprepa kod med for-loopen](kapitel-4/for-loop.md)
* [Använda slumptal i spel](kapitel-4/slumptal.md)
* [Säker inmatning](kapitel-4/baettre-inmatning.md)
* [Mikroövningar](kapitel-4/ovningar/README.md)
  * [for & inmatning](kapitel-4/ovningar/ovningar-grunder-loopar.md)
  * [for & inmatning 2](kapitel-4/ovningar/ovningar-grunder-loopar-2.md)
  * [for-loop](kapitel-4/ovningar/ovningar-for-loop.md)
  * [for & tryParse](kapitel-4/ovningar/ovningar-for-tryparse.md)

## 5. Samlingar

* [Skapa samling med en lista](kapitel-5/listor-foreach.md)
* [Elementen i en lista](kapitel-5/element.md)
* [Hantera samlingar med for-loop](kapitel-5/lista-for-loop.md)
* [Labb: Ceasar-kryptot](kapitel-5/labb-ceasar-kryptot.md)
* [Labb: morsekod](kapitel-5/labb-morsekod.md)
* [Labb: slumpad lista](kapitel-5/labb-slumpad-lista.md)
* [Labb: register](kapitel-5/register.md)
* [Labb: Dungeon-spel](kapitel-5/labb-dungeon.md)
* [Labb: bordsbokning](kapitel-5/labb-bordsbokning.md)
* [Labb: 1D-bana](kapitel-5/labb-1d-bana.md)
* [Labb: konsolpositioner](kapitel-5/labb-position.md)
* [Labb: 2D-bana](kapitel-5/labb-2d-bana.md)
* [Labb: Sokoban](kapitel-5/labb-sokoban.md)
* [Uppgifter](kapitel-5/uppgift-spelet-labyrint.md)
* [Debugga i VS Code](kapitel-5/debug.md)
* [Mikroövningar](kapitel-5/ovningar/README.md)
  * [Listor 1](kapitel-5/ovningar/ovningar-list-1.md)
  * [Listor 2](kapitel-5/ovningar/ovningar-list-2.md)
  * [Listor 3](kapitel-5/ovningar/ovningar-list-3.md)
  * [Listor 4](kapitel-5/ovningar/ovningar-list-4.md)

## 6. Metoder

* [Organisera kod med metoder](kapitel-6/metoder.md)
* [Refaktorisering](kapitel-6/refaktorisering.md)
* [Topdown strategin](kapitel-6/topdown-1.md)
* [Topdown labb](kapitel-6/topdown-2.md)
* [Dela data](kapitel-6/dela-data.md)
* [Labb: bloggappen](kapitel-6/labb-bloggen.md)
* [Labb: påskäggjakten](kapitel-6/labb-pask.md)
* [Labb: bokbibliotek](kapitel-6/labb-bibliotek.md)
* [Labb: Ceasar-kryptot 2](kapitel-6/labb-ceasar-metoder.md)
* [Labb: ordspråksregister](kapitel-6/labb-ordsprak.md)
* [Labb: citatgenerator](kapitel-6/labb-quotes.md)
* [Labb: bordsbokning 2](kapitel-6/labb-bordsbokning-metoder.md)
* [Labb: sänka skepp](kapitel-6/labb-saenka-skepp.md)
* [Mikroövningar](kapitel-6/ovningar-metoder.md)

## 7. Fönsterappar

* [Intro till WPF](gui-appar/intro.md)
* [WPF-appar](gui-appar/intro-till-wpf/README.md)
  * [Eventhantering](gui-appar/intro-till-wpf/events.md)
  * [Labb: area app](gui-appar/intro-till-wpf/labb-area-app.md)
  * [Labb: slumptal app](gui-appar/intro-till-wpf/labb-slump-app.md)
  * [Fler labbar](gui-appar/intro-till-wpf/labbar.md)
* [Layout med StackPanel](gui-appar/layout-med-stackpanel/README.md)
  * [Brännbollsräknare](gui-appar/layout-med-stackpanel/brannboll.md)
  * [Labb: anteckningar](gui-appar/layout-med-stackpanel/labb-anteckningar.md)
  * [Labb: vänd text app](gui-appar/layout-med-stackpanel/labb-reverse-app.md)
  * [Uppgifter](gui-appar/layout-med-stackpanel/uppgifter.md)
* [Layout med Grid](gui-appar/layout-med-grid/README.md)
  * [Uppgifter](gui-appar/layout-med-grid/uppgifter.md)
  * [Labb: luffarschak](gui-appar/layout-med-grid/labb-luffarschak.md)
* [Provuppgifter](gui-appar/prov-uppgifter.md)

## Raylib

* [Intro](raylib/README.md)
  * [Skapa ett fönster](raylib/grunder/skapa-ett-foenster.md)
  * [Rita fyrkanter](raylib/grunder/rita-fyrkanter.md)
  * [Rita cirklar och text](raylib/grunder/rita-cirklar-och-text.md)
  * [Rita bilder](raylib/grunder/rita-bilder.md)
  * [Input](raylib/grunder/input.md)
  * [Kollisioner](raylib/grunder/kollisioner.md)
  * [Scener](raylib/grunder/scener.md)
  * [Fallande objekt](raylib/grunder/fallande-objekt.md)
* [Labb: jaga monster](raylib/labbar/README.md)
  * [Metoder som ritar](raylib/labbar/metoder.md)
  * [Metoder som ändrar 1:2](raylib/labbar/spelfigurer.md)
  * [Metoder som ändrar 2:2](raylib/labbar/monster.md)
* [Labb: Shmup](raylib/shmup/README.md)
  * [Skapa en spelare](raylib/shmup/shmup-1.md)
  * [Skapa ett skott](raylib/shmup/shmup-2.md)
  * [Rita ut gränssnitt](raylib/shmup/shmup-3.md)
  * [Rita ut stjärnor](raylib/shmup/shmup-4.md)
  * [Skapa en fiende](raylib/shmup/shmup-5.md)
* [Game of Life](raylib/life/life.md)

## Unity

* [Intro](unity/README.md)
* [Labb: Shmup](unity/shmup/README.md)
  * [Skapa en spelare](unity/shmup/shmup-1.md)
  * [Skapa en fiende](unity/shmup/shmup-2.md)
  * [Kollisioner](unity/shmup/shmup-3.md)
  * [Skapa flera fiender](unity/shmup/shmup-4.md)
  * [Skapa skott](unity/shmup/shmup-5.md)

## Projekt

* [Viktigt i projekt](projekt/README.md)
* [Exempel på konsolprogram](projekt/konsolprogram/README.md)
  * [Kontaktlista](projekt/konsolprogram/kontakter.md)
  * [Budget](projekt/konsolprogram/budget.md)
* [Exempel på Konsolspel](projekt/konsolspel/README.md)
  * [Hangman](projekt/konsolspel/hangman.md)
  * [Snake](projekt/konsolspel/snake.md)
  * [Mastermind](projekt/konsolspel/mastermind.md)
  * [Månlandare](projekt/konsolspel/manlandare.md)
  * [Roulette](projekt/konsolspel/roulett.md)
  * [Mine sweeper](projekt/konsolspel/mine-sweeper.md)
  * [Zork](projekt/konsolspel/zork.md)
  * [Blackjack](projekt/konsolspel/blackjack.md)
* [Exempel på Raylib-spel](projekt/grafiska-spel/README.md)
  * [Pong](projekt/grafiska-spel/pong.md)
* [Vintage-spel på Youtube](raylib/exempel-pa-spel.md)

