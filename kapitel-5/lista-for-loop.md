---
description: Loopa igenom en lista med `for`-loop
---

# Loopa igenom lista med for-loopen

För att effektivt hantera varje element i en lista är det vanligt att använda loopar. En av de mest användbara looparna för detta ändamål är `for`-loopen. Den låter oss iterera över varje element i en lista, med möjlighet att även fånga upp varje elements position eller index.

## Introduktion till `for`-loopen

En `for`-loop används för att upprepa en viss kod ett bestämt antal gånger. Detta är särskilt användbart när vi vill gå igenom alla element i en lista. Strukturen i en `for`-loop består av tre delar: 

* initialisering
* villkor
* inkrementering. 

### Ett enkelt exempel

För att illustrera hur en `for`-loop fungerar, tänk dig en situation där du vill skriva ut siffrorna från 1 till 5. Istället för att skriva ut varje siffra separat, kan vi använda en `for`-loop:

```csharp
for (int i = 1; i <= 5; i++)
{
    Console.WriteLine(i);
}
```

Resultatet av detta program blir:

```text
1
2
3
4
5
```

I detta exempel startar vi vår räknare `i` på 1 (`int i = 1`). Loopen fortsätter så länge som `i` är mindre än eller lika med 5 (`i <= 5`). Efter varje iteration ökar vi värdet på `i` med 1 (`i++`), vilket flyttar oss ett steg närmare slutet av vår loop.

### Skriva ut element från en `List<>`

Eftersom vi kan använda en variabel för att representera indexet för varje element i en `List<>`, kan vi använda en `for`-loop för att skriva ut varje element i `List<>`en:

```csharp
List<string> frukter = ["äpple", "banan", "päron", "apelsin", "kiwi"];

for (int i = 0; i < frukter.Length; i++)
{
    Console.WriteLine($"Frukt {i + 1}: {frukter[i]}");
}
```

Resultatet av detta program blir:

```text
Frukt 1: äpple
Frukt 2: banan
Frukt 3: päron
Frukt 4: apelsin
Frukt 5: kiwi
```

I detta exempel använder vi en variabel `i` för att representera indexet för varje element i listaen. Vi börjar med att sätta `i` till 0 (`int i = 0`). Sedan fortsätter vi så länge som `i` är mindre än längden på listaen (`i < frukter.Length`). Efter varje iteration ökar vi värdet på `i` med 1 (`i++`), vilket flyttar oss ett steg närmare slutet av vår loop.

 Skillnaden mellan `foreach`-loopen och `for`-loopen är att `foreach`-loopen inte ger oss tillgång till indexet för varje element i listaen. Om vi behöver indexet för varje element i listaen, måste vi använda en `for`-loop.

### Inmatning av värden i en lista

För att illustrera hur vi kan använda en `for`-loop för att mata in värden i en lista, tänk dig en situation där du vill be användaren mata in namn på 5 länder. Istället för att be användaren mata in varje land separat, kan vi använda en `for`-loop:

```csharp
// Skapa en tom lista
List<string> länder = [];

// Be användaren mata in 5 länder
for (int i = 0; i < länder.Length; i++)
{
    Console.Write("Ange land: ");
    länder.Add(Console.ReadLine());
}
```

### Mer avancerat exempel

Slår vi ihop `for`-loopen för inmatning och `foreach`-loopen för utskrift kan vi göra ett program som låter användaren mata in valfritt antal länder och sedan skriver ut alla länder:

```csharp
// Be användaren mata in antalet personer
Console.Write("Hur många länder vill du registrera? ");
int antal = int.Parse(Console.ReadLine());

// Skapa en tom lista
List<string> länder = [];

// Registrera alla länder
for (int i = 0; i < antal; i++)
{
    Console.Write("Ange land: ");
    länder.Add(Console.ReadLine());
}

// Skriv ut alla länder
Console.Write("Du har registrerat länderna: ");
foreach (var landet in länder)
{
    Console.Write($"{landet} ");
}
```

Resultatet av detta program blir:

```text
Hur många länder vill du registrera? 3
Ange land: Sverige
Ange land: Norge
Ange land: Danmark
Du har registrerat länderna: Sverige Norge Danmark
```

### Uppgift 1

* Skapa ett nytt konsolprojekt och döp det till **Kursregister**.
* Skapa en `List<>` med plats för 25 kursnamn.
* Be användaren mata in 5 kursnamn.
* Skriv ut alla kursnamn på en rad.

Konsolen kan det se ut så här:

```text
Kursregister
============
Ange kursnamn 1: prog 1
Ange kursnamn 2: prog 2
Ange kursnamn 3: Webbprog
Ange kursnamn 4: Databaser
Ange kursnamn 5: C#

Du har registrerat kurserna: prog 1, prog 2, Webbprog, Databaser, C#
```

## Ett sökbart register

En användbar tillämpning av en `List<>` är att använda den som ett sökbart register. Vi kan till exempel skapa en `List<>` med namn på länder och sedan låta användaren söka efter ett land. Om landet finns i registret så skriver vi ut ett meddelande om detta.

```csharp
// Skapa en lista med 10 länder
List<string> länder = ["Sverige", "Norge", "Danmark", "Finland", "Island", "Tyskland", "Frankrike", "Spanien", "Portugal", "Italien"];

// Presentera programmet
Console.WriteLine("Landregister");
Console.WriteLine("============");
Console.WriteLine();

// Programloop
while (true)
{
    // Be användaren mata in ett namn
    Console.Write("Ange ett land: ");
    string sökterm = Console.ReadLine();

    // Sök i listaen
    foreach (var landet in länder)
    {
        if (landet == sökterm)
        {
            Console.WriteLine("Landet finns i registret!");
            break;
        }
    }

    // Fråga om användaren vill söka igen
    Console.Write("Vill du söka igen? (j/n) ");
    string svar = Console.ReadLine();

    // Om användaren svarar 'n' så avslutar vi programmet
    if (svar == "n")
    {
        break;
    }
}
```

Vi kan även implementera fritextsökning med `.Contains()`:

```csharp
...
// Ange en sökterm
Console.Write("Ange en sökterm: ");
string sökterm = Console.ReadLine();

// Sök i listaen
foreach (var landet in länder)
{
    if (landet.Contains(sökterm))
    {
        Console.WriteLine($"{landet} finns i registret!");
    }
}   
...
```

## Uppgifter

### Uppgift 3

* Skapa ett nytt konsolprojekt och döp det till **Sokregister**.
* Skapa ett program som låter användaren registrera valbart antal namn.
* Om namnet redan finns i listaen så måste användaren mata in ett nytt namn.

Konsolen kan det se ut så här:

```text
Sökregister
===========
Hur många namn vill du registrera? 5
Ange namn 1: Adam
Ange namn 2: Bertil
Ange namn 3: Adam
Namnet finns redan, ange ett nytt namn: Cesar
Ange namn 4: David
Ange namn 5: Erik

Du har registrerat namnen: Adam, Bertil, Cesar, David, Erik
```

### Uppgift 4

* Utöka föregående program så att användaren kan spara alla namn i textfil.
* Och läsa in namnen från textfilen när programmet startar.
* Tips: använd `File.WriteAllLines()` för att spara namnen i textfilen, och `File.ReadAllLines()` för att läsa in namnen från textfilen in i listaen.

Konsolen kan det se ut så här:

```text
Sökregister
===========
Läser in namn från filen namn.txt...
Hittade 3 namn i filen: Adam, Bertil, Cesar

Hur många namn vill du registrera? 5
Ange namn 1: Adam
Ange namn 2: Bertil
Ange namn 3: Adam
Namnet finns redan, ange ett nytt namn: Cesar
Ange namn 4: David
Ange namn 5: Erik

Du har registrerat namnen: Adam, Bertil, Cesar, David, Erik
Programmet sparar alla namn i en textfil.
```