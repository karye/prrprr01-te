
# Labb: Ceasar-kryptot

![Ceasar kryptot](../.gitbook/assets/image-35.jpg)

Kryptering har använts i århundraden för att skydda information och säkerställa att endast rätt mottagare kan läsa den. Ett av de äldsta och mest kända krypteringssystemen är Ceasar-kryptot, som användes av Julius Caesar för att skydda sina militära meddelanden. Det är en enkel form av substitutionskryptering där varje bokstav i ett meddelande flyttas ett visst antal steg i alfabetet.

I denna laboration kommer du att implementera Ceasar-kryptot och stegvis utöka funktionaliteten från att bara kryptera ett enskilt ord till att hantera hela meddelanden. Slutligen kommer vi att lägga till felhantering och förhindra oönskade inmatningar. Syftet är att ge dig träning i att arbeta med strängar, loopar och indexering i C#.

Testa Ceasar-kryptot online: [Cryptii Caesar Cipher](https://cryptii.com/pipes/caesar-cipher)

---

## Del 1: Ceasar-kryptering av ett enskilt ord

Vi börjar med att implementera en enkel version av Ceasar-krypteringen där vi tar ett enskilt ord och krypterar det genom att förskjuta bokstäverna med en viss nyckel.

### Steg 1: Starta projektet

Skapa ett nytt konsolprojekt och kalla det `CeasarKrypto`. Be användaren mata in ett ord som vi vill kryptera och läs in det med `Console.ReadLine()`.

```csharp
Console.Clear();
Console.InputEncoding = System.Text.Encoding.Unicode;
Console.WriteLine("Ceasar-krypto: Kryptera ett ord.");

Console.Write("Ange ett ord: ");
string ord = Console.ReadLine().ToUpper();
```

### Steg 2: Loopa igenom varje bokstav i ordet

Vi använder en sträng för att representera alfabetet. Varje bokstav i strängen har ett index, vilket vi kommer att använda för att förskjuta bokstäverna.\
Ordet loopas igenom och varje bokstavs position i alfabetet skrivs ut.

```csharp
...

string alfabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZÅÄÖ";

// Loopa igenom varje bokstav i ordet
foreach (char bokstav in ord)
{
    // Skriv ut varje bokstav
    Console.WriteLine(bokstav);
}
```

Exempel på körning:

```text
Ceasar-krypto: Kryptera ett ord.
Ange ett ord: Hej!
H
E
J
!
```

### Steg 3: Hitta varje bokstavs position i alfabetet

Vi använder `IndexOf` för att hitta varje bokstavs position i alfabetet.

```csharp
...

// Loopa igenom varje bokstav i ordet
foreach (char bokstav i ord)
{
    // Hitta bokstavens position i alfabetet
    int index = alfabet.IndexOf(bokstav);
    if (index != -1)
    {
        Console.WriteLine($"{bokstav} finns på position {index}");
    }
    else
    {
        Console.WriteLine($"{bokstav} är inte en giltig bokstav i alfabetet.");
    }
}
```

Exempel på körning:

```text
Ceasar-krypto: Kryptera ett ord.
Ange ett ord: HEJ!
H finns på position 7
E finns på position 4
J finns på position 9
! är inte en giltig bokstav i alfabetet.
```

### Steg 4: Kryptera bokstäverna

Vi förskjuter varje bokstav med en viss nyckel och hanterar omslag när vi går över sista bokstaven i alfabetet (dvs Ö).

```csharp
Console.Write("Ange en nyckel (1-9): ");
int nyckel = int.Parse(Console.ReadLine());

// Loopa igenom varje bokstav i ordet
foreach (char bokstav in ord)
{
    int index = alfabet.IndexOf(bokstav);
    if (index != -1)
    {
        // Lägg till nyckeln och använd modulus för att hantera omslag
        int nyPosition = index + nyckel;
        
        // Plocka ut det nya tecknet
        char krypteradBokstav = alfabet[nyPosition];

        Console.WriteLine($"{bokstav} krypteras till {krypteradBokstav}");
    }
    else
    {
        // Lämna tecken som inte finns i alfabetet oförändrade
        Console.WriteLine($"{bokstav} lämnas oförändrad");
    }
}

Console.WriteLine($"Krypterad text: {krypteradText}");
```

Exempel på körning:

```text
Ceasar-krypto: Kryptera ett ord.
Ange ett ord: Hej!
Ange en nyckel (1-9): 3
H krypteras till K
E krypteras till H
J krypteras till M
! lämnas oförändrad
```

### Steg 5: Kryptera ett meddelande

Vi justerar programmet så att det kan kryptera hela meningar istället för bara ett ord.

```csharp
Console.WriteLine("Ceasar-krypto: Kryptera ett meddelande.");
Console.Write("Ange ett meddelande: ");
string meddelande = Console.ReadLine().ToUpper();
...
```

### Utmaning 1: Skriv ut allt på en rad

Uppdatera programmet så att alla krypterade bokstäver skrivs ut på samma rad, istället för på en ny rad varje gång.

Exempel på körning:

```text
Ceasar-krypto: Kryptera ett meddelande.
Ange ett meddelande: Hej på dig!
Ange en nyckel (1-9): 3
-----------------------
Chiffertext: KHM SD GLJ!
```

### Steg 6: Gör krypteringen cirkulär

Vi vill att krypteringen ska vara cirkulär, dvs att bokstäverna som går över Ö eller under A fortsätter från början av alfabetet.

```csharp
...
// Kolla om nyPosition är utanför alfabetet och börja isåfall om
if (nyPosition >= alfabet.Length)
{
    nyPosition -= alfabet.Length;
}
...
```

Exempel på körning:

```text
Ceasar-krypto: Kryptera ett meddelande.
Ange ett meddelande: Öster o eden
Ange en nyckel (1-9): 3
-----------------------
Chiffertext: RUVQO R HAGQ
```

## Del 2: En meny för att välja kryptering eller dekryptering

Vi kan utöka programmet genom att lägga till en meny där användaren kan välja att kryptera eller dekryptera ett meddelande.

### Steg 7: Lägg till en meny

Vi skapar en meny där användaren kan välja att kryptera eller dekryptera ett meddelande.

```csharp
Console.WriteLine("Ceasar-krypto: Kryptera eller dekryptera ett meddelande.");
Console.WriteLine("1. Kryptera meddelande");
Console.WriteLine("2. Dekryptera meddelande");
Console.Write("Ange ditt val: ");
string val = Console.ReadLine();

if (val == "1")
{
    // Kryptera meddelande
}
else if (val == "2")
{
    // Dekryptera meddelande
}
else
{
    Console.WriteLine("Ogiltigt val. Avslutar programmet.");
}
```

### Utmaning 2: Dekryptering

Implementera en funktion för att dekryptera ett meddelande. Du kan använda samma logik som för kryptering, men istället för att lägga till nyckeln, subtraherar du den.

Exempel på körning:

```text
Ceasar-krypto: Kryptera eller dekryptera ett meddelande.
1. Kryptera meddelande
2. Dekryptera meddelande
Ange ditt val: 2
Ange ett meddelande: KHM SD GLJ
Ange en nyckel (1-9): 3
Klartext: HEJ PÅ DIG!
```

## Del 3: Inmatningskontroll

### Utmaning 3: Tomma meddelanden

Vi vill förhindra att användaren skickar in tomma meddelanden. Lägg till en kontroll för att se om meddelandet är tomt.

### Steg 8: Ogiltiga nycklar

Vi vill också förhindra att användaren skickar in ogiltiga nycklar. Vi lägger till en kontroll för att se om nyckeln är ett heltal mellan 1 och 9.

```csharp
...
Console.Write("Ange en nyckel (1-9): ");

while (true)
{
    nyckel = int.Parse(Console.ReadLine());
    if (nyckel < 1 || nyckel > 9)
    {
        Console.WriteLine("Nyckeln måste vara ett heltal mellan 1 och 9.");
        Console.Write("Ange en nyckel (1-9): ");
    }
    else
    {
        break;
    }
}
...
```

## Fler Utmaningar

Gör programmet mer flexibelt genom att låta användaren välja vilket alfabet (svenska eller engelska) som ska användas.
