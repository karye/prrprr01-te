---
description: Slumpa tal och spara i en lista
---

# Laboration: Slumpa tal och spara i en lista

![](../.gitbook/assets/image-120.png)

I denna laboration kommer du att skapa ett program som slumpar tal inom ett givet intervall och sparar dessa i en lista. Programmet kommer att fråga användaren om intervallet för de slumpade talen, hur många tal som ska slumpas, och sedan skriva ut de slumpade talen i en lista.

## Steg 1: Slumpa ett tal mellan 1 och 10

I det här steget slumpar vi ett tal mellan 1 och 10 och skriver ut resultatet.

```csharp
Console.Clear();
Console.WriteLine("Välkommen till slumptalsprogrammet!");

// Slumpar ett tal mellan 1 och 10
int slumptal = Random.Shared.Next(1, 11);
Console.WriteLine("Slumpat tal: " + slumptal);
```

### Uppgift 1

* Ändra intervallet till att slumpa ett tal mellan 5 och 15. Kör koden flera gånger och observera hur intervallet påverkar de slumpade talen.
* Lägg till en `Console.WriteLine()` som informerar användaren om intervallet innan talet slumpas, t.ex., "Ett tal mellan 1 och 10 kommer att slumpas:".

## Steg 2: Slumpa 5 tal mellan 1 och 10

Nu ska vi slumpa 5 tal mellan 1 och 10 och skriva ut varje tal.

```csharp
// Loopar 5 gånger och slumpar ett tal
for (int i = 0; i < 5; i++)
{
    int slumptal = Random.Shared.Next(1, 11);
    Console.WriteLine("Slumpat tal: " + slumptal);
}
```

Om vi vill skriva ut ordningen på de slumpade talen kan vi använda variabeln `i` i loopen.

```csharp
for (int i = 0; i < 5; i++)
{
    int slumptal = Random.Shared.Next(1, 11);
    Console.WriteLine("Slumpat tal " + (i + 1) + ": " + slumptal);
}
```

### Uppgift 2

* Ändra programmet så att det slumpar och skriver ut 10 tal istället för 5. Kontrollera att 10 tal slumpas fram och skrivs ut.

### Uppgift 3

* Prova att skriva ut i omvänd ordning genom att ändra startvärdet för `i` till 4 och ändra villkoret i loopen till `i >= 0`. Testa att slumpa och skriva ut 5 tal för att se att de skrivs ut i omvänd ordning.

## Steg 3: Fråga användaren hur många tal som ska slumpas

Låt användaren ange hur många tal som ska slumpas och använd det värdet i loopen.

```csharp
Console.Write("Hur många tal vill du slumpa? ");
int antal = int.Parse(Console.ReadLine());

// Loopar antal gånger och slumpar ett tal
for (int i = 0; i < antal; i++)
{
    int slumptal = Random.Shared.Next(1, 11);
    Console.WriteLine("Slumpat tal: " + slumptal);
}
```

### Uppgift 4

* Lägg till kod som visar ett felmeddelande om användaren matar in ett negativt tal (t.ex. "Antalet tal måste vara större än 0")
* Testa genom att mata in olika positiva och negativa tal.

### Uppgift 5

* Lägg till en loop som fortsätter fråga efter antalet tills användaren anger ett giltigt (positivt) tal.

## Steg 4: Fråga användaren om min- och maxvärde

Nu ska vi låta användaren ange ett min- och maxvärde för intervallet. Använd dessa värden för att justera intervallet i `Random.Shared.Next()`.

```csharp
Console.Write("Ange ett minimivärde för intervallet: ");
int min = int.Parse(Console.ReadLine());

Console.Write("Ange ett maxvärde för intervallet: ");
int max = int.Parse(Console.ReadLine());

Console.Write("Hur många tal vill du slumpa? ");
int antal = int.Parse(Console.ReadLine());

// Loopar antal gånger och slumpar ett tal
for (int i = 0; i < antal; i++)
{
    int slumptal = Random.Shared.Next(min, max + 1);
    Console.WriteLine("Slumpat tal: " + slumptal);
}
```

### Uppgift 6

* Lägg till en kontroll som säkerställer att `max` är större än `min`. Om inte, visa ett felmeddelande och fråga användaren om att ange min och max igen.
* Lägg till en instruktion som förklarar intervallet för användaren (t.ex., "Ange ett minsta och största värde för intervallet.").

## Steg 5: Spara de slumpade talen i en lista

Nu ska vi spara de slumpade talen i en lista istället för att bara skriva ut dem direkt.

```csharp
Console.Write("Ange ett minimivärde för intervallet: ");
int min = int.Parse(Console.ReadLine());

Console.Write("Ange ett maxvärde för intervallet: ");
int max = int.Parse(Console.ReadLine());

Console.Write("Hur många tal vill du slumpa? ");
int antal = int.Parse(Console.ReadLine());

// Skapa en lista för att lagra talen
List<int> listaSlumptal = [];

// Loopar antal gånger och slumpar ett tal
for (int i = 0; i < antal; i++)
{
    int slumptal = Random.Shared.Next(min, max + 1);

    // Lägg till talet i listan
    listaSlumptal.Add(slumptal);
}
```

### Uppgift 7

* Efter loopen, skriv ut antalet tal som har sparats i listan (t.ex., "Antalet sparade tal är: X"). Testa att slumpa olika antal tal för att se om listan innehåller rätt antal element.
* Testa vad som händer om du anger ett tal för `antal` som är större än intervallet mellan min och max. Diskutera vad som skulle krävas för att förhindra dubbletter i listan.

## Steg 6: Skriv ut den fyllda listan

Sist skriver vi ut alla tal i listan `listaSlumptal` efter att de har slumpats och lagrats.

```csharp
Console.Write("Ange ett minimivärde för intervallet: ");
int min = int.Parse(Console.ReadLine());

Console.Write("Ange ett maxvärde för intervallet: ");
int max = int.Parse(Console.ReadLine());

Console.Write("Hur många tal vill du slumpa? ");
int antal = int.Parse(Console.ReadLine());

// Skapa en lista för att lagra talen
List<int> listaSlumptal = [];

for (int i = 0; i < antal; i++)
{
    int slumptal = Random.Shared.Next(min, max + 1);

    // Lägg till talet i listan
    listaSlumptal.Add(slumptal);
}

Console.WriteLine("De slumpade talen är: ");
foreach (int tal in listaSlumptal)
{
    Console.WriteLine(tal);
}
```

### Uppgift 8

* Lägg till en funktionalitet som summerar alla tal i listan och visar summan efter att listan har skrivits ut.
* Tips: Använd en variabel för att räkna summan och lägg till varje tal i listan i denna variabel.

### Uppgift 9

* Lägg till en funktionalitet som hittar det högsta och lägsta talet i listan och skriver ut dessa värden.
* Tips: Använd två variabler för att spara det högsta och lägsta talet och uppdatera dessa värden i loopen.

## Steg 7: Upprepa programmet

Låt användaren välja om de vill köra programmet igen. Vi använder en `while (true)`-loop för att upprepa programmet tills användaren väljer att avsluta.

```csharp
// Programmet körs tills användaren väljer att avsluta
while (true)
{
    Console.Write("Ange ett minimivärde för intervallet: ");
    int min = int.Parse(Console.ReadLine());

    Console.Write("Ange ett maxvärde för intervallet: ");
    int max = int.Parse(Console.ReadLine());

    Console.Write("Hur många tal vill du slumpa? ");
    int antal = int.Parse(Console.ReadLine());

    // Skapa en lista för att lagra talen
    List<int> listaSlumptal = [];

    // Loopar antal gånger och slumpar ett tal
    for (int i = 0; i < antal; i++)
    {
        int slumptal = Random.Shared.Next(min, max + 1);
        listaSlumptal.Add(slumptal);
    }

    // Skriv ut alla slumpade talen
    Console.WriteLine("De slumpade talen är: ");
    foreach (int tal in listaSlumptal)
    {
        Console.WriteLine(tal);
    }

    Console.Write("Vill du köra programmet igen? (j/n): ");
    string svar = Console.ReadLine().ToLower();

    if (svar != "j")
    {
        break; // Avslutar while-loopen och därmed programmet
    }
}
```

### Uppgift 10

* Lägg till ett felmeddelande om användaren matar in något annat än "j" eller "n" som svar.
* Lägg till en bekräftelse som visar hur många gånger programmet har körts om efter att användaren väljer att köra om det.

## Steg 8: Hantera felaktig inmatning

När användaren matar in ett felaktigt värde, ska programmet visa ett felmeddelande och sen starta om programloopen.

```csharp
while (true)
{
    Console.Write("Ange ett minimivärde för intervallet: ");
    int min = int.Parse(Console.ReadLine());
    if (min < 0)
    {
        Console.WriteLine("Minimivärdet måste vara större än 0.");

        // Startar om loopen
        continue;
    }

    Console.Write("Ange ett maxvärde för intervallet: ");
    int max = int.Parse(Console.ReadLine());
    if (max < min)
    {
        Console.WriteLine("Maxvärdet måste vara större än minimivärdet.");

        // Startar om loopen
        continue;
    }

    Console.Write("Hur många tal vill du slumpa? ");
    int antal = int.Parse(Console.ReadLine());
    if (antal < 1)
    {
        Console.WriteLine("Antalet tal måste vara större än 0.");

        // Startar om loopen
        continue;
    }

    ...

}
```

## Fortsättning och utmaningar

1. **Undvik dubbletter**: Justera koden så att inga dubbletter av tal tillåts i listan. Om intervallet mellan `min` och `max` är mindre än antalet tal som ska slumpas, visa ett meddelande om att det är omöjligt att generera unika tal.

2. **Spara till fil**: Lägg till en funktionalitet som sparar listan med slumpade tal till en textfil. Be användaren att namnge filen.

3. **Visa statistik**: Lägg till funktionalitet som beräknar och visar statistik över talen i listan, inklusive medelvärde, median, standardavvikelse, och antalet förekomster av varje tal.

4. **Grafisk representation**: Försök skapa en enkel grafisk representation av talen (t.ex., en horisontell stapel för varje tal i listan, där stapelns längd representerar talets värde).
