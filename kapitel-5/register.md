---
description: Använd listor för att lagra och hantera data i C#-program. Skapa en meny för att registrera och skriva ut data i listan.
---

# Laboration: hantera samlingar med listor och loopar

## Introduktion

I den här laborationen kommer du att lära dig hantera listor i C#. Du kommer skapa en meny, läsa in värden från användaren och registrera dessa i en lista. Vi kommer också repetera hur man loopar genom en lista för att skriva ut innehållet.


## Skapa och fyll en lista

### Steg 1: Skapa programstrukturen

Innan vi börjar lägga till funktionalitet är det viktigt att ha en grundläggande struktur på vårt program. Detta innebär att skapa en tom loop och en enkel meny. Denna meny kommer vi successivt att bygga ut.

1. Skapa en `while`-loop som kör programmet tills användaren väljer att avsluta.
2. Lägg till en enkel meny där användaren kan välja mellan olika alternativ.
3. Be användaren mata in sitt val.

### Exempel på kod:
```csharp
Console.Clear();
Console.WriteLine("Välkommen till landregistret!");

// Programmet körs tills användaren väljer att avsluta
while (true)
{
    // Visa meny
    Console.WriteLine("1. Registrera nytt land");
    Console.WriteLine("2. Skriv ut alla registrerade länder");
    Console.WriteLine("3. Avsluta");
    
    // Be användaren göra ett val
    Console.Write("Välj ett alternativ: ");
    string val = Console.ReadLine();
    
    // Lägg till ett sätt att avsluta loopen
    if (val == "3")
    {
        break;
    }
}
```

### Steg 2: Skapa listan för att lagra länder

Nu när vi har en grundläggande programstruktur behöver vi en plats att lagra de länder som användaren registrerar. Vi kommer att använda en `List<string>` för detta.

1. Skapa en tom lista i början av programmet där vi kommer att lagra de registrerade länderna.
2. Placera denna lista utanför loopen så att den är tillgänglig för hela programmet.

### Exempel på kod:
```csharp
// Skapa en tom lista för att lagra länder
List<string> listaLänder = [];

while (true)
{
    // Visa meny
    Console.WriteLine("1. Registrera nytt land");
    Console.WriteLine("2. Skriv ut alla registrerade länder");
    Console.WriteLine("3. Avsluta");

    Console.Write("Välj ett alternativ: ");
    string val = Console.ReadLine();
    
    if (val == "3")
    {
        break;
    }
}
```

### Steg 3: Hantera ogiltiga val

När användaren gör ett val i menyn, kan de ibland välja ett ogiltigt alternativ. Vi ska lägga till en enkel validering som informerar användaren om att de har gjort ett felaktigt val.

1. Kontrollera om användaren skriver in något som inte är ett giltigt menyval.
2. Visa ett felmeddelande om användarens val är ogiltigt.

### Exempel på kod:
```csharp
else
{
    Console.WriteLine("Ogiltigt val, försök igen.");
}
```

Nu ser programmet ut så här:
```csharp
List<string> listaLänder = [];

while (true)
{
    Console.WriteLine("1. Registrera nytt land");
    Console.WriteLine("2. Skriv ut alla registrerade länder");
    Console.WriteLine("3. Avsluta");

    Console.Write("Välj ett alternativ: ");
    string val = Console.ReadLine();
    
    if (val == "3")
    {
        break;
    }
    else
    {
        Console.WriteLine("Ogiltigt val, försök igen.");
    }
}
```

### Steg 4: Registrera ett nytt land

Nu ska vi bygga vidare på menyn och lägga till funktionaliteten för att registrera ett land. När användaren väljer att registrera ett nytt land, ska vi be dem skriva in landets namn och lägga till det i listan.

1. Skapa en meny som fångar upp när användaren väljer att registrera ett nytt land.
2. Be användaren skriva in landets namn.
3. Lägg till det nya landet i listan med hjälp av `Add()`-metoden.

### Exempel på kod:
```csharp
if (val == "1")
{
    // Registrera ett nytt land
    Console.Write("Ange ett land: ");
    string land = Console.ReadLine();
    länder.Add(land); // Lägger till landet i listan
    Console.WriteLine($"{land} har lagts till i listan.");
}
```

Nu ser programmet ut så här:
```csharp
List<string> listaLänder = [];

while (true)
{
    Console.WriteLine("1. Registrera nytt land");
    Console.WriteLine("2. Skriv ut alla registrerade länder");
    Console.WriteLine("3. Avsluta");

    Console.Write("Välj ett alternativ: ");
    string val = Console.ReadLine();
    
    if (val == "1")
    {
        Console.Write("Ange ett land: ");
        string land = Console.ReadLine();
        länder.Add(land);
        Console.WriteLine($"{land} har lagts till i listan.");
    }
    else if (val == "3")
    {
        break;
    }
    else
    {
        Console.WriteLine("Ogiltigt val, försök igen.");
    }
}
```

### Steg 5: Skriv ut alla registrerade länder

Nu ska vi skapa funktionen som låter användaren skriva ut alla länder som har registrerats i listan. Vi kommer att:
1. Kontrollera om det finns några länder i listan.
2. Om listan är tom, informera användaren om att inga länder har registrerats.
3. Om det finns länder i listan, använda en `foreach`-loop för att skriva ut varje land.

#### Skapa ett menyval för att skriva ut länderna

Vi börjar med att lägga till en ny del i vår meny där användaren kan välja att skriva ut alla registrerade länder.

1. När användaren väljer "2" i menyn ska programmet gå till det nya valet där vi skriver ut länderna.

```csharp
else if (val == "2")
{
    // Skriv ut alla registrerade länder
}
```

Här skapar vi ett nytt alternativ som exekveras om användaren väljer "2" i menyn. Det är här vi snart ska lägga till vår logik för att skriva ut listan med länder.

#### Informera användaren om listan är tom

Vi måste kontrollera om listan faktiskt innehåller några länder innan vi försöker skriva ut den. Om listan är tom, ska programmet berätta för användaren att inga länder har registrerats än.

1. **Kontrollera om listan är tom** med hjälp av `Count`-egenskapen på listan.
   - `Count` returnerar antalet objekt som finns i listan.
2. **Om listan är tom** (dvs. `Count == 0`), visa ett meddelande som säger att inga länder har registrerats.

```csharp
if (länder.Count == 0)
{
    Console.WriteLine("Inga länder har registrerats ännu.");
}
```

Den här koden säger att om antalet objekt i listan `länder` är 0 (vilket betyder att listan är tom), ska programmet skriva ut meddelandet **"Inga länder har registrerats ännu."**.

---

#### Använd en `foreach`-loop för att skriva ut länderna

Om listan inte är tom (dvs. om det finns länder i den) behöver vi skriva ut varje land ett i taget.

1. **`foreach`-loopen** går igenom varje objekt i listan, och för varje objekt i listan utförs den kod som vi placerar inuti loopen.
2. I detta fall kommer varje objekt i listan att representeras av variabeln `land`, och vi kommer att skriva ut värdet av `land` på en ny rad för varje objekt.

```csharp
else
{
    foreach (string land in länder)
    {
        Console.WriteLine(land);
    }
}
```

**Hur fungerar `foreach`-loopen?**
- `string land in länder` betyder att för varje objekt av typen `string` i listan `länder`, så tilldelas detta objekt till variabeln `land`.
- Sedan utför loopen kommandot `Console.WriteLine(land)` för varje objekt, vilket gör att varje land skrivs ut på en ny rad.

---

#### Fullständig kod för att skriva ut länder

Nu sätter vi samman hela delen som hanterar utskrift av listan. Här är hela blocket av kod:

```csharp
else if (val == "2")
{
    Console.WriteLine("Registrerade länder:");

    // Kontrollera om listan är tom
    if (länder.Count == 0)
    {
        // Meddelande om inga länder är registrerade
        Console.WriteLine("Inga länder har registrerats ännu.");
    }
    else
    {
        // Loopar igenom listan och skriver ut varje land
        foreach (string land in länder)
        {
            Console.WriteLine(land);
        }
    }
}
```

## Uppgifter

### Uppgift 1: skapa din egen lista

* Lägg till menyalternativ för att registrera någon valfri typ av `string`-objekt i en lista. Skriv sedan ut alla objekt i listan.
* Tips:
  * Skapa först en tom lista av rätt typ.
  * Skapa ett menyval för att registrera objekt i listan med `Add()`-metoden.
  * Skapa ett menyval för att skriva ut alla objekt i listan med en `foreach`-loop.

### Uppgift 2: skapa en lista med heltal

* Lägg till menyalternativ för att registrera ålder på personer i en lista. Skriv sedan ut alla åldrar i listan.

## Skapa och fyll en lista med tuples

Tuple är en datatyp som kan användas för att lagra flera värden i en variabel. I den här delen av laborationen ska du skapa en lista av tuples för att lagra information om personer.

### Steg 1: Skapa en tuple

En tuple skapas genom att använda parenteser och separera värdena med kommatecken. I exemplet nedan skapas en tuple med två värden: namn och ålder.

```csharp
(string namn, int ålder) person = ("Kalle", 85);
```

### Steg 2: Skapa en lista av tuples

En lista av tuples skapas på samma sätt som en lista av andra datatyper. I exemplet nedan skapas en lista av tuples som innehåller information om personer.

```csharp
// Lista för att lagra: (namn, ålder)
List<(string namn, int ålder)> personer = [];
```

### Steg 3: Registrera personer i listan

När användaren väljer att registrera en ny person, ska de få mata in namn och ålder. Denna information ska lagras i en tuple och läggas till i listan.

Vi bygger ut vårt menyval för att registrera personer i listan.

```csharp
// Registrera ny person
else if (val == "8")
{
    Console.Write("Ange namn: ");
    string förnamn = Console.ReadLine();
    Console.Write("Ange ålder: ");
    int ålder = int.Parse(Console.ReadLine());
    personer.Add((namn, ålder));
    Console.WriteLine($"{namn} har lagts till i listan.");
}
```
### Steg 4: Skriv ut listan med personer

När användaren väljer att skriva ut alla registrerade personer, ska programmet loopa genom listan och skriva ut varje person i ordning.

```csharp
// Skriv ut alla registrerade personer
else if (val == "9")
{
    Console.WriteLine("Registrerade personer:");
    foreach ((string, int) person in personer)
    {
        Console.WriteLine($"{person.namn} är {person.ålder} år");
    }
}
```