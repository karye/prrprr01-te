---
description: Debugga i VS Code
---

# Debugga kod

## Debugga i VS Code

Debuggning är en kritisk del av programmeringsprocessen, där du kan följa programmets exekvering steg för steg, undersöka variabler och flödet i koden. Det är ett ovärderligt verktyg när du behöver hitta och rätta till buggar.

## Hur man hittar buggar i koden

- Gå igenom koden noggrant och lös alla syntaxfel först. Alla röda **squiggly lines** ska försvinna.
- Läs felmeddelandena? Vad säger de dig?
- Kör koden. Vad händer?
    - Kraschar den, isåfall på vilken rad?
    - Lägg till en `breakpoint` på den raden och kör koden i debuggern.
    - Vad innehåller variablerna på den raden?
    - Fortsätt stega igenom koden tills koden kraschar igen.
- Efter att du har åtgärdat felen, verifiera att koden fungerar som den ska genom att köra den och testa olika scenarier.

### Skapa ett projekt att debugga

Skapa ett nytt konsolprojekt i VS Code `Debugging`:

```csharp
// Skapa en tom array med 5 platser
string[] namnen = new string[5];

// Lägg till några namnen
namnen[0] = "Kalle";
namnen[1] = "Lisa";
namnen[2] = "Pelle";
namnen[3] = "Anna";
namnen[4] = "Olle";

// Skriv ut alla namnen
foreach (var namnet in namnen)
{
    Console.WriteLine(namnet);
}
```

### Debugging i VS Code

När projektet är uppsatt, är det dags att börja debugga:

1. **Starta debuggningen:**
   - Klicka på ikonen för debuggning i VS Code (det ser ut som en insekt eller en bug med en förbjuden skylt över den) i aktivitetsfältet på vänster sida.
   - I panelen som öppnas, klicka på knappen "Starta debuggning" eller tryck på `F5` på tangentbordet.
   - Programmet kommer att starta och exekveras tills det når breakpointen du satte.

2. **Sätt breakpoints:**
   - En breakpoint är en markör som du sätter på en rad kod där du vill att exekveringen ska pausa.
   - Klicka på vänsterkanten bredvid radnumret i din kod där du vill ha en breakpoint, exempelvis på `namnen[0] = "Kalle";`.
   - En röd cirkel ska visas, vilket betyder att en breakpoint är satt.

3. **Steg genom din kod:**
   - När exekveringen pausar vid din breakpoint kan du använda knapparna i debug-verktygsfältet för att stega genom koden:
     - "Continue" (`F5`): Fortsätter exekveringen till nästa breakpoint eller tills programmet avslutas.
     - "Step Over" (`F10`): Går till nästa rad i samma funktion utan att gå in i funktioner.
     - "Step Into" (`F11`): Går in i funktioner som kallas på aktuell rad.
   - Medan du stegar igenom, observera "Variables"-fliken för att se hur värdet av `namn` och `i` förändras.

4. **Undersök variabler:**
   - Under debuggning kan du undersöka och modifiera värden på variabler.
   - Hover över en variabel i koden för att se dess nuvarande värde.
   - Du kan också använda "Watch"-fliken för att lägga till uttryck eller variabler du vill övervaka noggrant.

## Uppgifter

### Uppgift 1: Hitta och åtgärda felaktigheterna

**Uppgift:** Följande C#-kod innehåller ett antal fel som kan vara syntaktiska, logiska eller semantiska. Din uppgift är att identifiera och korrigera dessa fel så att programmet fungerar enligt följande krav:

1. Programmet ska ta emot exakt 5 unika namn från användaren och lagra dem i en array.
2. Sedan ska programmet skriva ut det längsta nanmet.
3. Om användaren matar in "Avbryt" ska programmet avsluta input-loopen omedelbart.

```csharp
// Skapa en tom array på 5 platser
string[] namnen = new string[5];

// Räknare för att hålla koll på antalet sparade namn
int antal = 5;

// Inmatningsloopen
for (int i = 1; i < antal; i++)
{
    // Fråga efter ett namn
    Console.WriteLine("Ange ett namn eller 'avbryt' för att avsluta:");
    string input = Console.ReadLine().tolower();

    // Om användaren matar in "avbryt", avsluta loopen
    if (input = "Avbryt")
    {
        break;
    }
    // Om användaren matar in ett namn som redan finns, meddela användaren
    else if (namnen.Contains(input))
    {
        Console.WriteLine("Namnet finns redan, ange ett unikt namn.");
    }
    // Annars, lägg till namnet i arrayen
    else
    {
        namnen[i] = input;
    }
}

// Hitta det längsta och kortaste namnet
string longestNamn = "";
foreach (var name in names)
{
    // Om namnet är längre än det längsta namnet, spara det
    if (name.Length > longestnamn)
    {
        longestNamn = name;
    }
}

// Skriv ut det längsta och kortaste namnet
Console.WriteLine($"Det längsta är: " + longestnamn);
```

### Uppgift 2: Identifiera och rätta till fel i spelet "Gissa talet"

**Uppgift:** Följande kod ska representera ett enkelt "Gissa talet"-spel. Det finns 10 fel i koden, både syntaktiska och logiska. Din uppgift är att hitta dessa fel och korrigera dem så att spelet fungerar enligt beskrivningen:

1. Spelet väljer ett slumpmässigt tal mellan 1 och 100 som spelaren ska gissa.
2. Spelaren får fortsätta gissa tills rätt tal gissas.
3. Efter varje gissning ska programmet ge en ledtråd om att gissa högre eller lägre.
4. Spelet ska hålla reda på antalet gissningar och meddela spelaren när talet har gissats rätt.
5. Användaren kan skriva "avsluta" för att avsluta spelet.

```csharp
// Slumpa ett tal mellan 1 och 100
int hemligtNummer = Random.Shared.Next(1, 101);

// Variabel för att hålla reda på antalet gissningar
int antalGissningar = 0;

// Variabel för att hålla reda på om gissningen är korrekt
bool isCorrect = false;

Console.WriteLine("Välkommen till 'Gissa talet'-spelet!");
Console.WriteLine("Jag tänker på ett tal mellan 1 och 100. Kan du gissa vilket? Skriv 'avsluta' för att avsluta spelet.");

// Programloopen
while (isCorrect =! true)
{
    // Fråga efter en gissning
    Console.Write("Din gissning: ");
    var input = Console.ReadLine();

    // Om användaren skriver "avsluta", avsluta loopen
    if (input.Tolower() = "avsluta")
    {
        break;
    }

    // Försök konvertera input till ett nummer
    int Guess;
    bool isNumber = int.tryParse(input, out Guess);

    // Om input inte är ett nummer, meddela användaren och fortsätt loopen  
    if (!isNumber || Guess < 0 || Guess > 100)
    {
        Console.WriteLine("Var god och ange ett nummer mellan 1 och 100.");
        continue;
    }

    // Öka antalet gissningar med 1
    numberOfGuesses++;

    // Om gissningen är för låg, meddela användaren och fortsätt loopen
    if ( Guess < hemligtNummer)
    {
        Console.WriteLine("För lågt! Försök igen.");
    }
    // Annars om gissningen är för hög, meddela användaren och fortsätt loopen
    elseif (Guess > hemligtNummer)
    {
        Console.WriteLine("För högt! Försök igen.");
    }
    // Annars, meddela användaren att gissningen är korrekt och avsluta loopen
    else
    {
        Console.WriteLine("Grattis! Du gissade rätt nummer efter " + numberofGuesses + " försök.");
        isCorrect = false;
    }
}

Console.WriteLine("Spelet är över. Tack för att du spelade!");
```

#### Instruktioner:

- Gå igenom koden noggrant och lös alla syntaxfel först. Alla röda **squiggly lines** ska försvinna.
- Läs felmeddelandena? Vad säger de dig?
- Kör koden. Vad händer?
    - Kraschar den, isåfall på vilken rad?
    - Lägg till en `breakpoint` på den raden och kör koden i debuggern.
    - Vad innehåller variablerna på den raden?
    - Fortsätt stega igenom koden tills koden kraschar igen.
- Efter att du har åtgärdat felen, verifiera att koden fungerar som den ska genom att köra den och testa olika scenarier.
