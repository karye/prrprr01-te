---
description: Ofta behöver man hantera en samling av värden
---

# Listor och loopar

## Skapa en samling av värden

När du programmerar kan det ofta vara nödvändigt att hantera en samling av relaterade värden. Dessa kan vara allt från en lista med namn, årtal eller städer. Att samla och organisera sådan information är en grundläggande del av många program.

Tänk dig att du vill registrera en lista med länder. Ett enkelt, men inte särskilt effektivt sätt, är att skapa en separat variabel för varje land:

```csharp
Console.WriteLine("Program för att registrera städer");

// Registrera första landet
Console.Write("Ange land 1: ");
string land1 = Console.ReadLine();

// Registrera andra landet
Console.Write("Ange land 2: ");
string land2 = Console.ReadLine();

// Registrera tredje landet
Console.Write("Ange land 3: ");
string land3 = Console.ReadLine();

// Skriv ut alla städer
Console.WriteLine($"Du har registrerat städerna {land1}, {land2} och {land3}");
```

Denna metod är dock opraktisk när antalet värden ökar. Istället för separata variabler, kan vi använda en `List<>` för att effektivt hantera flera värden.

Annars är `array` en äldre klassisk datatyp som används för att lagra en samling av värden. En `array` är en samling av värden som lagras i en enda variabel. Du kan tänka på en `array` som en låda med fack, där varje fack innehåller ett värde.
Nackdelen med en `array` är att den har en fast storlek, vilket innebär att du måste veta antalet värden i förväg. `List<>` är en modernare datatyp som kan växa och krympa dynamiskt.

## Skapa en lista

En `List<>` är en av de mest användbara datatyperna inom programmering för att lagra och hantera samlingar av relaterade värden. Med en `List<>` kan du organisera och manipulera data på ett smidigt sätt.

![](../.gitbook/assets/image-48.png)

### Alternativ 1: Skapa en tom `List<>`

Om du behöver en `List<>` med en bestämd storlek men inte har värdena än, kan du starta med en tom `List<>`. Här skapar vi en tom `List<>`:

```csharp
// Här skapas en tom lista
List<string> länder = [];
```

```csharp
// Så här fyller vi listan med värden
länder.Add("Sverige");
länder.Add("Norge");
länder.Add("Danmark");
```

```csharp

// Skriv ut alla städer
Console.WriteLine($"Du har registrerat städerna {länder[0]}, {länder[1]} och {länder[2]}");
```

Nu ser listan ut såhär:

| index | string |
|:-----:|:-------:|
| 0 | Sverige |
| 1 | Norge   |
| 2 | Danmark |

### Alternativ 2: Skapa och fylla en lista direkt

Ett av de mest direkta sätten att skapa en `List<>` är att definiera dess värden direkt vid skapandet:

```csharp
// Här skapar vi en lista på 3 strings, vilket är ett exempel på en lista som innehåller stadsnamn
List<string> länder = ["Sverige", "Norge", "Danmark"];
```

Så här ser listan ut när den skapas:

| index | string  |
|:-----:|:-------:|
| 0 | Sverige |
| 1 | Norge   |
| 2 | Danmark |

```csharp
// Här skriver vi ut innehållet i vår lista
Console.WriteLine($"Tre länder i Europa: {länder[0]}, {länder[1]}, {länder[2]}");

// Här skapar vi en lista på 3 heltal, vilket är ett exempel på en lista som innehåller årtal
List<int> årtal = [1990, 2000, 2010];

// Här skriver vi ut innehållet i vår lista
Console.WriteLine($"Tre årtal: {årtal[0]}, {årtal[1]}, {årtal[2]}");
```

## Loopa igenom lista med foreach-loopen

Om du vill loopa igenom en lista men inte bryr dig om positionen för varje element, kan du använda en `foreach`-loop:

```csharp
// Här loopar vi igenom vår 'länder' lista
foreach (var landet in länder)
{
    Console.WriteLine(landet); // Skriver ut namnet på varje land
}
```
I detta exempel representerar `landet` varje element i `länder` listan. För varje iteration av loopen, kommer `landet` att innehålla nästa värde i listan.

## Övning - slumpa kort ur en kortlek

Skapa ett nytt konsolprojekt och döp det till **SlumpaKort**. \
Vi kan använda en lista för att skapa en kortlek och slumpa ut kort. I detta exempel skapar vi en kortlek med  kort och slumpar ett index för att dra ett kort. Vi använder en `while`-loop för att dra 5 kort.

### Enkel lösning

Vi använder en `foreach`-loop för att skriva ut varje kort.

```csharp
// Skapa en lista med 13 kort
List<string> kortlek = ["Ess", "2", "3", "4", "5", "6", "7", "8", "9", "10", "Knekt", "Dam", "Kung"];

// Slumpa ut 5 kort
int antalKort = 5;
while (antalKort > 0)
{
    // Slumpa ett index för ett kort i kortleken
    int index = Random.Shared.Next(0, kortlek.Length);
    string kort = kortlek[index];
    Console.WriteLine($"Du drog kortet: {kort}");

    // Räkna ner antalet dragna kort
    antalKort--;

    // Vill du dra fler kort?
    Console.Write("Vill du dra ett till kort? (j/N): ");
    if (Console.ReadLine().ToLower() != "j")
    {
        break;
    }
}
```

### Mer avancerad lösning

Problemet med den enkla lösningen är att vi kan dra samma kort flera gånger.\
För att undvika detta, kan vi ta bort kortet från kortleken när vi har dragit det:

```csharp
// Skapa en lista med 52 kort
List<string> kortlek = ["Ess", "2", "3", "4", "5", "6", "7", "8", "9", "10", "Knekt", "Dam", "Kung"];

// Slumpa ut 5 kort
int antalKort = 5;
while (antalKort > 0)
{
    // Slumpa ett index för ett kort i kortleken
    int index = Random.Shared.Next(0, kortlek.Length);
    string kort = kortlek[index];
    Console.WriteLine($"Du drog kortet: {kort}");

    // Räkna ner antalet dragna kort
    antalKort--;

    // Ta bort kortet från kortleken
    kortlek.RemoveAt(index);

    // Avsluta om det inte finns fler kort i kortleken
    if (kortlek.Count == 0)
    {
        Console.WriteLine("Det finns inga fler kort i kortleken.");
        break;
    }

    // Skriv ut alla kort kvar i kortleken
    Console.Write("Kvar i kortleken: ");
    foreach (var kort in kortlek)
    {
        Console.Write(kort);
    }
    Console.WriteLine();

    // Vill du dra fler kort?
    Console.Write("Vill du dra ett till kort? (j/N): ");
    if (Console.ReadLine().ToLower() != "j")
    {
        break;
    }
}
```

### Övning

* Skapa ett nytt konsolprojekt och döp det till **Kortlek**.
* Skapa en lista med 52 kort.
* Slumpa ut 5 kort.
* Skriv ut alla kort som finns kvar i kortleken efter att du har dragit kort.

### Lista med bilder på spelkort

Du kan också använda Unicode för att visa spelkort i konsolen.

[Spelkort i Unicode](https://en.wikipedia.org/wiki/Playing_cards_in_Unicode?authuser=0) \
Här är ett exempel på en kortlek med 

```csharp
Console.Clear();

// Aktivera Unicode för att visa Unicode-tecken
Console.OutputEncoding = System.Text.Unicode;

// Skapa en lista med kort för en kortlek
List<string> kortlek =  ["🂡", "🂢", "🂣", "🂤", "🂥", "🂦", "🂧", "🂨", "🂩", "🂪", "🂫", "🂭", "🂮"];

// Slumpa fram 5 kort
int antalKort = 5;
while (antalKort > 0)
{
    // Slumpa fram ett tal mellan 0 och antalet kort
    int index = Random.Shared.Next(0, kort.Count);
    string kort = kortlek[index];
    Console.WriteLine($"Du drog kortet: {kort}");
    antalKort--;

    // Ta bort kortet från kortleken
    kortlek.RemoveAt(index);

    // Avsluta om det inte finns fler kort i kortleken
    if (kortlek.Count == 0)
    {
        Console.WriteLine("Det finns inga fler kort i kortleken.");
        break;
    }

    // Vill du dra fler kort?
    Console.Write("Vill du dra ett till kort? (j/N): ");
    if (Console.ReadLine().ToLower() != "j")
    {
        break;
    }
}
```

### Lista med emojis

Du kan också använda emojis för att visa kort i konsolen.\
Emojis kan du hitta på [emojipedia.org](https://emojipedia.org/).

```csharp
Console.Clear();

// Aktivera Unicode för att visa Emoji
Console.OutputEncoding = System.Text.Unicode;

// Skapa en lista med emojis
List<string> emojis =  ["🃏", "🍥", "😻", "🐭"];

// Slumpa fram 5 emojis
int antalEmojis = 2;
while (antalEmojis > 0)
{
    // Slumpa fram ett tal mellan 0 och antalet emojis
    int index = Random.Shared.Next(0, emojis.Count);
    string emoji = emojis[index];
    Console.WriteLine($"Du valde: {emoji}");
    antalEmojis--;

    // Ta bort emoji från listan
    emojis.RemoveAt(index);

    // Avsluta om det inte finns fler emojis
    if (emojis.Count == 0)
    {
        Console.WriteLine("Det finns inga fler emojis.");
        break;
    }

    // Vill du välja fler emojis?
    Console.Write("Vill du välja en till emoji? (j/N): ");
    if (Console.ReadLine().ToLower() != "j")
    {
        break;
    }
}
```

## Uppgifter

### Uppgift 1

* Skapa ett nytt konsolprojekt och döp det till **Favoritgodis**.
* Skapa en `List<>` med plats för 4 favoritgodisar.
* Fyll `List<>`en med namn på godisar.
* Skriv ut alla godisar i listan på samma rad.

I konsolen kan det se ut så här:

```text
En lista med 4 godisar
======================
Du har registrerat godisarna: Marabou, Daim, Snickers, Twix
```

### Uppgift 2

* Fortsätt på föregående konsolprojekt.
* Skapa en lista med 4 maträtter som du sen fyller med namn på maträtter.
* Skriv ut alla maträtter i listan, en per rad.

Resultat:

```text
En lista med 4 maträtter
========================
En av mina favoritmaträtter är Köttbullar.
En av mina favoritmaträtter är Spaghetti.
En av mina favoritmaträtter är Pizza.
En av mina favoritmaträtter är Sushi.
```

### Uppgift 3

* Fortsätt på föregående konsolprojekt.
* Skapa en lista `åldersyskon` som du fyller med ålder på dina syskon.
* Skriv ut alla åldrar i listan, alla på samma rad.

I konsolen kan det se ut så här:

```text
En lista med 3 åldrar
=====================
Ett av mina syskon är 10 år gammalt.
Ett av mina syskon är 12 år gammalt.
Ett av mina syskon är 14 år gammalt.
```

### Uppgift 4

* Fortsätt på föregående konsolprojekt.
* Skriv ut alla maträtter i listan med en `foreach`-loop, alla på samma rad.
* Skriv ut alla åldrar i listan med en `foreach`-loop, en per rad.

I konsolen kan det se ut så här:

```text
En lista med 4 maträtter
========================
Du har registrerat maträtterna: Köttbullar, Spaghetti, Pizza, Sushi
```

### Uppgift 5

* Skapa ett nytt konsolprojekt och döp det till **Namnregister**.
* Skapa en lista med plats för 10 namn.
* Fyll listan med 5 namn.
* Låt användaren fylla resten av listan med namn.
* Skriv ut alla namn i listan.

I konsolen kan det se ut så här:

```text
Namnregister
============
Ange namn 6: Filip
Ange namn 7: Lova
Ange namn 8: Gustav
Ange namn 9: Märta
Ange namn 10: Isak

Arrayen innehåller följande namn:
=================================
Adam
Bella
Cesar
David
Erik
Filip
Lova
Gustav
Märta
Isak
```
