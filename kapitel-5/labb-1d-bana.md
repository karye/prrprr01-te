# Labb: raden aka 1D-bana

![Snake](../.gitbook/assets/preview_937.png)

## Mål

Målet med denna labb är att förstå hur man använder enkla konsolutskrifter och arrays för att skapa en grundläggande animation och interaktion i konsolen.

## Starta projektet

Starta ett konsolprojekt **1D-bana** med följande rader:

```csharp
Console.Clear();

Console.WriteLine("Labb: spelet masken");

Console.Write("Vill du starta spelet? (j/n): ");
string svar = Console.ReadLine().ToLower();
if (svar != "n")
{
    return;
}
```

## Använda emojis

Du kan hitta emojis som funkar i konsolen https://raw.githubusercontent.com/anton-bot/Full-Emoji-List/master/Emoji.cs.

### Rita ut en vit ruta

Här är ett exempel på hur du kan göra det:

```csharp
// Aktivera UTF8 för att visa Emoji
Console.OutputEncoding = System.Text.Encoding.UTF8;

// Rita ut en vit ruta
Console.Write("⬜️");
```

När du kör programmet kommer det att rita ut en vit ruta i konsolfönstret. Du kan enkelt rita ut flera vita rutor genom att anropa `Console.Write()` flera gånger, eller genom att använda en loop och en array för att rita ut en rad med flera vita rutor.

När du kör programmet kommer det att se ut ungefär så här i konsolfönstret:

```text
⬜️
```

### Rita ut en rad med vita rutor

För att rita ut en rad med vita rutor kan vi göra så här:

```csharp
// Aktivera UTF8 för att visa Emoji
Console.OutputEncoding = System.Text.Encoding.UTF8;

// Rita ut en rad med vita rutor
Console.Write("⬜️⬜️⬜️⬜️⬜️⬜️⬜️⬜️⬜️⬜️");
```

När du kör programmet kommer det att se ut ungefär så här i konsolfönstret:

```text
⬜️⬜️⬜️⬜️⬜️⬜️⬜️⬜️⬜️⬜️
```

### Rita ut en rad med vita rutor med en array

Om vi att raden skall animeras måste vi göra den till en variabel. Vi kan göra det med en `array`.

Här är ett exempel på hur du kan göra det i VS Code:

```csharp
// Definera och initialisera arrayen
int[] raden = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

// Rita ut raden med emojis
foreach (int ruta in raden)
{
    if (ruta == 0)
    {
        Console.Write("⬜️");
    }
}
```

Som du kan se i kodexemplet ovan definierar vi och initialiserar `array` en med 10 vita rutor (talet 0) direkt i deklarationen. Därefter använder vi en vanlig `foreach`-loop för att rita ut raden med hjälp av metoden `Console.Write()` och emojin "⬜️" för att rita ut en vit ruta.

När du kör programmet som ritar ut en rad med vita rutor kommer det att se ut ungefär så här i konsolfönstret:

```text
⬜️⬜️⬜️⬜️⬜️⬜️⬜️⬜️⬜️⬜️
```

### Rita ut en rad med en smiley

För att rita ut en vit rad med en smiley kan vi göra så här:

```csharp
// Skapa en virtuell rad med 10 positioner
int[] raden = [ 0, 0, 0, 0, 0, 1, 0, 0, 0, 0 ];

// Loopa igenom raden och skriv ut den
//Console.Clear();
foreach (var ruta in raden)
{
    // Skriv ut en ruta
    if (ruta == 0)
    {
        Console.Write("⬜");
    }
    else
    {
        Console.Write("😁");
    }
}
```

Vår array **raden** innehåller en 1 på position 5:

| index | int |
|:-----:|:-----:|
| 0 | 0 |
| 1 | 0 |
| 2 | 0 |
| 3 | 0 |
| 4 | 0 |
| 5 | 1 |
| 6 | 0 |
| 7 | 0 |
| 8 | 0 |
| 9 | 0 |

När du kör programmet kommer det att se ut ungefär så här i konsolfönstret:

```text
⬜️⬜️⬜️⬜️⬜️😁⬜️⬜️⬜️⬜️
```

### Utmaning

* Gör raden längre.
* Lägg till fler Emojis i arrayen.

## Skapa en animation

För att det ska bli en animation måste vi rita ut raden med emojis i en oändlig loop. Vi kan göra det med en `while`-loop:

```csharp
// Animera smileyn
while (true)
{
    // Rita ut raden inkl emojis
    foreach (int ruta in raden)
    {
        if (ruta == 0)
        {
            Console.Write("⬜️");
        }
        else
        {
            Console.Write("😁");
        }
    }
    // Raden blir snyggare med en tom rad efter
    Console.WriteLine();
}
```

När du kör programmet kommer det att se ut ungefär så här i konsolfönstret:

```text
⬜️⬜️⬜️⬜️⬜️😁⬜️⬜️⬜️⬜️
⬜️⬜️⬜️⬜️⬜️😁⬜️⬜️⬜️⬜️
⬜️⬜️⬜️⬜️⬜️😁⬜️⬜️⬜️⬜️
...
```

### Fördröjning

För att animationen ska bli snyggare kan vi lägga till en fördröjning med `Thread.Sleep(500)` så att vi kan se att smileyn flyttar sig. 

```csharp
// Animera smileyn
while (true)
{
    // Rita ut raden inkl emojis
    foreach (int ruta in raden)
    {
        if (ruta == 0)
        {
            Console.Write("⬜️");
        }
        else
        {
            Console.Write("😁");
        }
    }
    // Raden blir snyggare med en tom rad efter
    Console.WriteLine();

    // Fördröjning
    Thread.Sleep(500);
}
```

## Flytta på smileyn

Vi kan animera smileyn genom att flytta den åt höger. Vi kan göra det genom att ändra på arrayen **raden** som innehåller smileyns position.

Vår array **raden** innehåller en 1 på position 5:

| index | int |
|:-----:|:----:|
| 0 | 0 |
| 1 | 0 |
| 2 | 0 |
| 3 | 0 |
| 4 | 0 |
| 5 | 1 |
| 6 | 0 |
| 7 | 0 |
| 8 | 0 |
| 9 | 0 |

Vi kan flytta smileyn genom att ändra på index 5 till 0 och index 6 till 1:

| index | int |
|:-----:|:----:|
| 0 | 0 |
| 1 | 0 |
| 2 | 0 |
| 3 | 0 |
| 4 | 0 |
| 5 | 0 |
| 6 | 1 |
| 7 | 0 |
| 8 | 0 |
| 9 | 0 |

Vi använder en variabel `smileyPos` för att hålla reda på smileyns position på raden. Vi kan flytta smileyn genom att öka `smileyPos` med 1:

```csharp
// Smileyns 😁 position på raden
int smileyPos = 0;

// Animera smileyn
while (true)
{
    // Rita ut raden med emojis
    foreach (int ruta in raden)
    {
        if (ruta == 0)
        {
            Console.Write("⬜️");
        }
        else
        {
            Console.Write("😁");
        }
    }
    // Raden blir snyggare med en tom rad efter
    Console.WriteLine($" {smileyPos}");

    // Flytta smileyn åt vänster eller höger
    smileyPos++;

    // Fyll den nya positionen med 1
    raden[smileyPos] = 1;

    // Fördröjning
    Thread.Sleep(1000);
}
```

Såhär ser det ut i terminalen:

```text
Labb: spelet masken
Vill du starta spelet? (j/n): j
😁⬜⬜⬜⬜⬜⬜⬜⬜⬜ 0
😁😁⬜⬜⬜⬜⬜⬜⬜⬜ 1
😁😁😁⬜⬜⬜⬜⬜⬜⬜ 2
😁😁😁😁⬜⬜⬜⬜⬜⬜ 3
😁😁😁😁😁⬜⬜⬜⬜⬜ 4
😁😁😁😁😁😁⬜⬜⬜⬜ 5
😁😁😁😁😁😁😁⬜⬜⬜ 6
😁😁😁😁😁😁😁😁⬜⬜ 7
😁😁😁😁😁😁😁😁😁⬜ 8
😁😁😁😁😁😁😁😁😁😁 9
```

### Uppgift

* Varför kraschar programmet?
* Hur kan vi fixa det?

### "Flytta" smileyn

Vi kan flytta smileyn genom att ändra på arrayen **raden** som innehåller smileyns position.
När vi flyttar smileyn åt höger ökar vi smileyPos med 1. Den nya positionen i arrayen fylls med 1. Men den gamla positionen är fortfarande 1. Vi måste därför tömma den gamla positionen genom att sätta den till 0.

```csharp
// Smileyns 😁 position på raden
int smileyPos = 0;

// Animera smileyn
while (true)
{
    // Rita ut raden med emojis
    foreach (int ruta in raden)
    {
        if (ruta == 0)
        {
            Console.Write("⬜️");
        }
        else
        {
            Console.Write("😁");
        }
    }
    // Raden blir snyggare med en tom rad efter
    Console.WriteLine($" {smileyPos}");

    // Flytta smileyn åt vänster eller höger
    smileyPos++;

    // Rensa smileyns gamla position
    raden[smileyPos - 1] = 0;

    // Fyll den nya positionen med 1
    raden[smileyPos] = 1;

    // Fördröjning
    Thread.Sleep(1000);
}
```

Såhär ser det ut i terminalen:

```text
Labb: spelet masken
Vill du starta spelet? (j/n): j
😁⬜⬜⬜⬜⬜⬜⬜⬜⬜ 0
⬜😁⬜⬜⬜⬜⬜⬜⬜⬜ 1
⬜⬜😁⬜⬜⬜⬜⬜⬜⬜ 2
...
```

## interaktion med användaren

### Läsa in en tangent

Istället för att smileyn rör sig automatiskt skall vi nu låta användaren styra smileyn med piltangenterna. För detta behöver vi läsa in en tangenttryckning från användaren. Vi kan göra det med metoden `Console.ReadKey()`.

Vi använder metoden `Console.ReadKey(true)` för att inte visa tangenten i konsolen. Vi kan sedan använda `ConsoleKeyInfo` för att få reda på vilken tangent som trycktes.

```csharp
// Läs in en tangent
ConsoleKeyInfo tangent = Console.ReadKey(true); // true = göm tangenten

Console.WriteLine(tangent.Key);
```

### Smileyns position

Vi skapa en variabel `smileyPos` som anger på vilken index den finns i arrayen `rad`.\
När man trycker på "Vänsterpil" minskar vi `smileyPos` med 1.\
När man trycker på "Högerpil" ökar vi `smileyPos` med 1.

```csharp
// Läs in en tangent
ConsoleKeyInfo tangent = Console.ReadKey(true);
if (tangent.Key == ConsoleKey.LeftArrow)
{
    smileyPos--;
}
else if (tangent.Key == ConsoleKey.RightArrow)
{
    smileyPos++;
}
```

## Animera smileyn

Vi lägger nu in interaktionen med användaren efter att vi har ritat ut raden med emojis. Vi lägger också in en `while`-loop så att vi kan flytta på smileyn:

```csharp
// Aktivera UTF8 för att visa Emoji
Console.OutputEncoding = System.Text.Encoding.UTF8;

// Definera och initialisera arrayen
int[] rad = [1, 0, 0, 0, 0, 0, 0, 0, 0, 0];

// Definiera index för smileyn
int smileyPos = 0;

// Animera smileyn
while (true)
{
    // Rensa skärmen
    Console.Clear();

    // Rita ut raden med emojis
    foreach (int ruta in raden)
    {
        if (ruta == 0)
        {
            Console.Write("⬜️");
        }
        else
        {
            Console.Write("😁");
        }
    }
    // Raden blir snyggare med en tom rad efter
    Console.WriteLine();

    // Läs in tangenttryckning
    var tangent = Console.ReadKey();

    // Flytta smileyn åt vänster eller höger
    if (tangent.Key == ConsoleKey.LeftArrow)
    {
        smileyPos--;
    }
    else if (tangent.Key == ConsoleKey.RightArrow)
    {
        smileyPos++;
    }
}
```

Resultatet i konsolen efter att man tryckt på "Vänsterpil" och "Högerpil" ser ut så här:

```text
⬜️⬜️⬜️⬜️⬜️😁😁😁😁⬜️
```

Vi kan flytta på smileyn men den försvinner inte från skärmen. Vi måste rita ut en vit ruta på den gamla positionen innan vi ritar ut smileyn på den nya positionen. Vi kan göra det med koden:

```csharp
// Rensa smileyns gamla position
rad[smileyPos] = 0;

// Läs in tangenttryckning
...

// Rita ut smileyn på den nya positionen
rad[smileyPos] = 1;
```

Nu blir det rätt - smileyn flyttar sig åt vänster och höger.

## Utmaningar

### Utmaning 1

* Programmet kraschar när man "går över kanten".
* Gör så att man börjar om på "andra" sidan istället.

### Utmaning 2

* Gör så att smileyn kan äta en annan emoji.
* Utöka med poäng. Poängen visas i konsolen en rad under.

I konsolen kan se ut så här:

```text
⬜️⬜️⬜️⬜️⬜️😁⬜️⬜️⬜️⬜️
Poäng: 0
```

### Utmaning 3

* Utöka till ett helt rutnät 10x10.
* Gör så att smileyn kan flyttas upp-och-ned med pil upp och pil ned.
* Lägg till monster som kan döda smileyn och avsluta spelet.
* Gör så att monster rör sig slumpmässigt.

### Rita med Console.SetCursorPosition()

Med metoden `Console.SetCursorPosition(kolumn, rad)` kan man direkt rita på en specifik position i konsolfönstret utan att behöva använda `Console.Write()`. Metoden tar två heltal som argument för att ange kolumn och rad.

Här är ett exempel på hur du kan implementera metoden i ditt program:

```csharp
// Aktivera UTF8 för att visa Emoji
Console.OutputEncoding = System.Text.Encoding.UTF8;

// Göm markören
Console.CursorVisible = false;

// Definiera index för smileyns position
int smileyKol = 0;
int smileyRad = 0;

// Animera smileyn
while (true)
{
    // Flytta markören till smileyns nya position
    Console.SetCursorPosition(smileyKol, smileyRad);

    // Rita ut en vit ruta på smileyns nya position
    Console.Write("⬜️");

    // Läs in tangenttryck
    ConsoleKeyInfo tangent = Console.ReadKey(true);

    // Kontrollera om piltangenterna är nedtryckta
    if (tangent.Key == ConsoleKey.LeftArrow)
    {
        smileyKol--;
    }
    else if (tangent.Key == ConsoleKey.RightArrow)
    {
        smileyKol++;
    }
    else if (tangent.Key == ConsoleKey.UpArrow)
    {
        smileyRad--;
    }
    else if (tangent.Key == ConsoleKey.DownArrow)
    {
        smileyRad++;
    }
}
```
