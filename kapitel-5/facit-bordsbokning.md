# Facit till labben "Bordsbokning"

Här är facit uppdelat enligt de olika utmaningarna.

## 1. Sätt ihop all kod

Här skapas en konsolapplikation med en meny som loopar tills användaren väljer att avsluta.

### Kod:
```csharp
Console.Clear();
Console.InputEncoding = System.Text.Encoding.Unicode;
Console.OutputEncoding = System.Text.Encoding.Unicode;

Console.WriteLine("Centralrestaurangens bordsbokning");

string filnamn = "centralbord.csv";
string loggfil = "historik.log";
string tomtBordBeskrivning = "0,Inga gäster,0";
int antalBord = 8;
List<string> bordInformation = LaddaEllerSkapaFil(filnamn, antalBord, tomtBordBeskrivning);

while (true)
{
    Console.WriteLine("\n1. Visa alla bord");
    Console.WriteLine("2. Ändra bordsinformation");
    Console.WriteLine("3. Markera bord som ledigt");
    Console.WriteLine("4. Lägg till nota");
    Console.WriteLine("5. Visa total nota");
    Console.WriteLine("6. Avsluta");
    Console.Write("Välj ett alternativ: ");
    string val = Console.ReadLine();
    Console.Clear();

    switch (val)
    {
        case "1":
            VisaBord(bordInformation);
            break;
        case "2":
            BokaBord(bordInformation, filnamn);
            break;
        case "3":
            MarkeraBordSomTomt(bordInformation, filnamn);
            break;
        case "4":
            LäggTillNota(bordInformation, filnamn);
            break;
        case "5":
            VisaTotalNota(bordInformation);
            break;
        case "6":
            return;
        default:
            Console.WriteLine("Felaktigt val, försök igen.");
            break;
    }
}
```

## 2. Spara och läsa bordsinformation från fil

Programmet sparar bordsinformationen i en CSV-fil (`centralbord.csv`) och läser in den vid start.

### Kod:
```csharp
static List<string> LaddaEllerSkapaFil(string filnamn, int antalBord, string tomtBordBeskrivning)
{
    // Kolla om filen finns, annars skapa en ny
    if (File.Exists(filnamn))
    {
        return File.ReadAllLines(filnamn).ToList();
    }

    // Annars skapa en tom lista som fylls med tomma bord
    List<string> nyLista = [];
    for (int i = 0; i < antalBord; i++)
    {
        nyLista.Add(tomtBordBeskrivning);
    }

    File.WriteAllLines(filnamn, nyLista);
    return nyLista;
}
```

## 3. Lägg till nota för varje bord

Programmet hanterar nu noter för varje bord.

### Kod:
```csharp
static void LäggTillNota(List<string> bordInformation, string filnamn)
{
    Console.Write("Ange bordsnummer (1-8): ");
    if (!int.TryParse(Console.ReadLine(), out int bordsnr) || bordsnr < 1 || bordsnr > bordInformation.Count)
    {
        Console.WriteLine("Felaktigt bordsnummer!");
        return;
    }

    Console.Write("Ange belopp att lägga till på notan: ");
    if (!decimal.TryParse(Console.ReadLine(), out decimal belopp) || belopp < 0)
    {
        Console.WriteLine("Felaktigt belopp!");
        return;
    }

    string[] delar = bordInformation[bordsnr - 1].Split(',');
    decimal nuvarandeNota = decimal.Parse(delar[2]);
    nuvarandeNota += belopp;

    bordInformation[bordsnr - 1] = $"{delar[0]},{delar[1]},{nuvarandeNota}";
    File.WriteAllLines(filnamn, bordInformation);
    LoggaHistorik($"Nota uppdaterad på bord {bordsnr} med {belopp:C}.");

    Console.WriteLine($"Nota uppdaterad! Nuvarande belopp: {nuvarandeNota:C}");
}
```

## 4. Visa totalt belopp för alla notor

Summerar och visar totalbeloppet.

### Kod:
```csharp
static void VisaTotalNota(List<string> bordInformation)
{
    int totalNota = 0;
    // Loopa igenom alla bord och summera notorna
    foreach (string bord in bordInformation)
    {
        string[] delar = bord.Split(',');
        // Plocka ut beloppet för varje bord och lägg till i totalen
        totalNota += int.Parse(delar[2]);
    }

    Console.WriteLine($"Total nota för alla bord: {totalNota:C}");
}
```

## 5. Fler användarvänliga kontroller

- Kontroll att användaren anger giltiga bordnummer (1–8).
- Kontroll att antal gäster är inom det tillåtna intervallet (1–8).

### Kod:
```csharp
static void BokaBord(List<string> bordInformation, string filnamn)
{
    Console.Write("Ange bordsnummer (1-8): ");
    if (!int.TryParse(Console.ReadLine(), out int bordsnr) || bordsnr < 1 || bordsnr > bordInformation.Count)
    {
        Console.WriteLine("Felaktigt bordsnummer!");
        return;
    }

    Console.Write("Ange bokningsnamn: ");
    string namn = Console.ReadLine();

    Console.Write("Ange antal gäster (1-8): ");
    if (!int.TryParse(Console.ReadLine(), out int antalGäster) || antalGäster < 1 || antalGäster > 8)
    {
        Console.WriteLine("Felaktigt antal gäster!");
        return;
    }

    // Uppdatera bordsinformationen
    bordInformation[bordsnr - 1] = $"{antalGäster},{namn},0";
    File.WriteAllLines(filnamn, bordInformation);

    Console.WriteLine("Bordet är bokat!");
}
```

## 6. Skapa en historiklogg

Lägger till en loggfil `historik.log`.

### Kod:
```csharp
static void LoggaHistorik(string text)
{
    File.AppendAllText("historik.log", $"{DateTime.Now}: {text}\n");
}
```

## 7. Lägg till fler bordsalternativ

Gör antalet bord konfigurerbart.

### Kod:
```csharp
static void StällInAntalBord()
{
    Console.Write("Ange antal bord: ");
    if (!int.TryParse(Console.ReadLine(), out int antalBord) || antalBord < 1)
    {
        Console.WriteLine("Felaktigt antal, standard 8 används.");
        antalBord = 8;
    }

    List<string> bordInformation = [];
    for (int i = 0; i < antalBord; i++)
    {
        bordInformation.Add("0,Inga gäster,0");
    }

    File.WriteAllLines("centralbord.csv", bordInformation);
}
```

## 8. Avancerad filhantering med säkerhet

Säkerställer att filen inte skrivs över felaktigt.

### Kod:
```csharp
static void SparaTillFilSäkert(List<string> bordInformation, string filnamn)
{
    if (File.Exists(filnamn))
    {
        File.WriteAllLines(filnamn, bordInformation);
    }
    else
    {
        Console.WriteLine("Filen saknas. Skapar ny fil.");
        File.WriteAllLines(filnamn, bordInformation);
    }
}
```
