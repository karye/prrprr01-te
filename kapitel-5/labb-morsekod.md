---
description: Översätt från svenska till morsekod
---

# Labb: morsekod

![Morsekod](../.gitbook/assets/International_Morse_Code.png)

## Uppgift

I denna övning ska du skapa en enkel applikation som omvandlar en text till Morse-kod. Applikationen ska använda två listor: en för bokstäver och en för deras motsvarande Morse-koder. Du ska skriva ut Morse-koden direkt när den hittas.

### Pseudokod

1. **Skapa listorna**

Börja med att skapa två listor:
- En lista för bokstäver och siffror (t.ex. 'A', 'B', 'C', '1', '2', osv.).
- En lista för motsvarande Morse-koder (t.ex. ".-", "-...", "-.-.", osv.).

2. **Läs in text**

Läs in text från användaren. Omvandla texten till versaler så att den matchar tecknen i din lista.

3. **Omvandla till Morse**

Loopa igenom varje tecken i den inmatade texten, hitta motsvarande tecken i listan och skriv ut motsvarande Morse-kod direkt. Om tecknet inte finns i listan, skriv ut ett frågetecken (`?`).

4. **Skriv ut resultatet**

Skriv ut den resulterande Morse-koden i samma loop som du omvandlar tecknen.

### Flödesschema

```mermaid
graph TD
    A[Start] --> B[Skapa två parallella arrayer]
    B --> C[Läs in en bokstav]
    C --> D[Hitta bokstaven i alfabetet och dess index]
    D --> E[Hitta morsekoden mha indexet]
    E --> F[Skriv ut morsekoden]
```

## Morsekod

Meningen "Programmering är kul" översatt till morsekod blir:

```text
.--. .-. --- --. .-. .- -- -- . .-. .. -. --. / .-.- .-. / -.- ..- .-..
```

Man använder [Morsealfabetet](https://sv.wikipedia.org/wiki/Morsealfabetet) för att översätta bokstäverna till morsekod.

På webbsidan [morsedecoder.com](https://morsedecoder.com) kan du översätta text till morsekod.

### Skapa projektet

Skapa ett konsolprojekt **MorseCode** och skriv koden i `Program.cs`.

```csharp
Console.Clear();
Console.WriteLine("Detta program översätter din text till Morsekod");

// För att kunna skriva svenska tecken
Console.InputEncoding = System.Text.Encoding.Unicode; 
```

### Samlingar

Vi skapar två samlingar för:

* Svenska alfabetet
* Morsekoden

Båda samlingarna har 30 platser och börjar på index 0. 

```csharp
// Två parallella samlingar
List<string> alfabet = [ "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", 
            "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", 
            "U", "V", "W", "X", "Y", "Z", "Å", "Ä", "Ö", " "];
List<string> morsekod = [ ".-", "-...", "-.-.", "-..", ".", "..-.", "--.", 
            "....", "..", ".---", "-.-", ".-..", "--", "-.", "---", ".--.", 
            "--.-", ".-.", "...", "-", "..-", "...-", ".--", "-..-", 
            "-.--", "--..", ".--.-", ".-.-", "---.", "/" ];
```

### Som en tabell

Som vi ser i tabellen nedan är bokstaven "A" på plats 0 i alfabetet och har morsekoden ".-". Om vi kan hitta bokstaven "A" i alfabetet kan vi använda samma index för att hitta morsekoden i morsekodssamlingen.

| alfabetet | index | morsekod |
|:-:|:-:|:-:|
| A | 0 | .- |
| B | 1 | -... |
| C | 2 | -.-. |
| D | 3 | -.. |
| .. | .. |  |
| Ö | 28 | ---. |
|   | 29 | / |

## Implementering

Vi börjar med att skapa ett program som översätter en bokstav till morsekod.

### Steg 1 - läs in en bokstav

Vi läser in en bokstav från användaren och översätter den till morsekod.

```csharp
Console.Write("Ange en bokstav att översätta: ");
string bokstav = Console.ReadLine();
```

### Steg 2 - hitta bokstaven i alfabetet

Vi kan använda en `.IndexOf()`-metod för att hitta bokstaven och dess position i alfabetet.

```csharp
// Hitta bokstaven i alfabetet
int index = alfabet.IndexOf(bokstav);
```

### Steg 3 - översätt till morsekod

När vi hittat bokstavens position i alfabetet kan vi använda samma index för att hitta morsekoden.

```csharp
...
// Hitta bokstaven i alfabetet
int index = alfabet.IndexOf(bokstav);

// Hitta morsekoden
string kod = morsekod[index];

// Skriv ut morsekoden
Console.WriteLine($"Morsekoden för {bokstav} är {kod}");
```

## Utmaningar

1. **Översätt en hel mening**

* Utöka programmet så att den översätter en hel mening till morsekod.
* Tips: Man kan loopa igenom en sträng med en `foreach()`-loop.

1. **Översätt siffror**

* Utöka programmet så att det ocskå översätter siffror till morsekod.

1. **Översätt från morsekod till svenska**

* Utöka programmet med en meny som låter användaren välja om hen vill översätta från svenska till morsekod eller från morsekod till svenska.

1. **Lägg till stöd för små bokstäver**

Just nu omvandlar vi texten till versaler för att matcha tecknen i listan. Uppdatera programmet så att det hanterar både små och stora bokstäver utan att omvandla input.

1. **Stöd för specialtecken**

Lägg till stöd för några vanliga specialtecken som punkt (`.`), komma (`,`), frågetecken (`?`) och andra tecken som kan vara viktiga i Morse-kod.

1. **Omvandla Morse tillbaka till text**

Lägg till en funktion som tar Morse-kod som input och omvandlar den tillbaka till vanlig text.

1. **Lägg till ljudutgång**

Utvidga programmet så att varje Morse-tecken (punkt eller streck) spelas upp som ett kort pip (t.ex. med `Console.Beep()`).

1. **Felhantering för tom inmatning**

Hantera situationer där användaren inte matar in någon text. Skriv ut ett meddelande som säger att de måste skriva något.

1. **Översätt till emojis**

* Utöka programmet så att det översätter från svenska till valfria emojis.
* För att emojis skall synas korrekt i terminalen måste du lägga till följande kod i början av programmet:

```csharp
// Aktivera UTF8 för att visa Emoji
Console.OutputEncoding = System.Text.Encoding.UTF8;
```


