# Facit: Labyrint i C#

Programmet:

- Ritar upp en labyrint i konsolfönstret med emojis.
- Låter spelaren navigera genom labyrinten med piltangenter.
- Implementerar förflyttningsregler så att spelaren inte kan gå genom väggar.
- Läser in labyrinten från en textfil.
- Implementerar vinstvillkor och loggar resultatet.

## 1. Sätt ihop all kod

Programmet startar genom att rensa konsolen, ställa in UTF-8 och ladda labyrinten.

### Kod:
```csharp
Console.Clear();
Console.OutputEncoding = System.Text.Encoding.UTF8;

Console.WriteLine("Välkommen till Labyrintspelet!");

string filnamn = "labyrint.txt";
int[,] labyrint = LaddaLabyrint(filnamn);

int spelareRad = 1;
int spelareKol = 1;

Console.CursorVisible = false;
RitaLabyrint(labyrint);
(spelareRad, spelareKol) = SpelLoop(labyrint, spelareRad, spelareKol);
```

✅ **Initiering av spelet och labyrinten**  
✅ **Labyrinten ritas upp och spelet startar**  

## 2. Spara och läsa labyrinten från en textfil

Labyrinten lagras i en textfil där `1` representerar väggar, `0` är gångar och `2` är utgången.

### Exempel på `labyrint.txt`:
```
1111111111
1001000001
1001010101
1000010001
1011100001
1000100001
1010001011
1011101011
1100000011
1111111121
```

### Kod:
```csharp
static int[,] LaddaLabyrint(string filnamn)
{
    // Läs in alla rader från filen
    string[] rader = File.ReadAllLines(filnamn);

    // Hur många rader och kolumner finns det?
    int raderAntal = rader.Length;
    int kolumnerAntal = rader[0].Length;

    // Skapa en labyrint-array med dom rätta dimensionerna
    int[,] labyrint = new int[raderAntal, kolumnerAntal];

    // Fyll labyrinten med siffror från textfilen
    for (int rad = 0; rad < raderAntal; rad++)
    {
        for (int kol = 0; kol < kolumnerAntal; kol++)
        {
            labyrint[rad, kol] = int.Parse(rader[rad][kol].ToString());
        }
    }

    return labyrint;
}
```

✅ **Labyrinten laddas in från en textfil**  

## 3. Rita ut labyrinten i konsolen

Labyrinten ritas ut med emojis:  
🟦 = Väggar  
⬜ = Gångar  
🚪 = Utgång  

### Kod:
```csharp
static void RitaLabyrint(int[,] labyrint)
{
    Console.Clear();

    // Loopa igenom alla rader och kolumner och rita ut labyrinten
    for (int rad = 0; rad < labyrint.GetLength(0); rad++)
    {
        for (int kol = 0; kol < labyrint.GetLength(1); kol++)
        {
            Console.SetCursorPosition(kol * 2, rad);

            if (labyrint[rad, kol] == 1)
            {
                Console.Write("🟦");
            }
            else if (labyrint[rad, kol] == 2)
            {
                Console.Write("🚪");
            }
            else
            {
                Console.Write("⬜");
            }
        }
    }
}
```

✅ **Labyrinten ritas ut i konsolen**  

## 4. Spel-loop och tangentavläsning

Spelaren kan röra sig med piltangenterna, men inte gå genom väggar.\
Här använder vi en tuple för att returnera spelarens nya position.

### Kod:
```csharp
static (int, int) SpelLoop(int[,] labyrint, int spelareRad, int spelareKol)
{
    while (true)
    {
        // Rita ut nuvarande position för spelaren
        Console.SetCursorPosition(spelareKol * 2, spelareRad);
        Console.Write("😃");

        // Läs av tangenttryckningar
        ConsoleKeyInfo tangent = Console.ReadKey(true);

        int nyRad = spelareRad;
        int nyKol = spelareKol;

        // Uppdatera spelarens position beroende på tangenttryckning
        switch (tangent.Key)
        {
            case ConsoleKey.UpArrow:
                nyRad--;
                break;
            case ConsoleKey.DownArrow:
                nyRad++;
                break;
            case ConsoleKey.LeftArrow:
                nyKol--;
                break;
            case ConsoleKey.RightArrow:
                nyKol++;
                break;
        }

        // Om spelaren försöker gå utanför labyrinten återgå till början av loopen
        if (labyrint[nyRad, nyKol] == 1)
        {
            continue;
        }

        // Spelaren förflyttar sig i en gång
        Console.SetCursorPosition(spelareKol * 2, spelareRad);
        Console.Write("⬜");

        // Sparar den nya positionen
        spelareRad = nyRad;
        spelareKol = nyKol;

        // Om spelaren når utgången, avsluta spelet
        if (labyrint[spelareRad, spelareKol] == 2)
        {
            Console.SetCursorPosition(0, labyrint.GetLength(0) + 1);
            Console.WriteLine("🎉 Grattis! Du hittade utgången! 🎉");
            LoggaResultat();
            break;
        }
    }

    return (spelareRad, spelareKol);
}
```

✅ **Spelaren kan röra sig genom labyrinten**  
✅ **Väggar blockeras**  
✅ **Spelet avslutas vid utgången**  

## 5. Fler användarvänliga kontroller

- Spelaren kan bara röra sig i gångarna.
- Försök att gå genom en vägg ignoreras.

**Denna funktionalitet finns redan i `SpelLoop()` ovan.** ✅  

## 6. Skapa en historiklogg

När spelaren når utgången, loggas resultatet i `historik.log`.

### Kod:
```csharp
static void LoggaResultat()
{
    string loggText = $"{DateTime.Now}: Spelaren klarade labyrinten!";
    File.AppendAllText("historik.log", loggText + Environment.NewLine);
}
```

✅ **Resultatet loggas till en fil**  

## 7. Lägg till fler labyrintalternativ

Vi kan enkelt skapa en större labyrint genom att ändra `labyrint.txt`.

### Exempel på en större labyrint:
```
111111111111
100000100001
101110101101
100010101001
101010101101
101010101001
101011101101
101000000001
111111111121
```

✅ **Spelet kan enkelt ha olika labyrinter**  

## 8. Avancerad filhantering med säkerhet

Säkerställer att filen finns innan vi laddar den.

### Kod:
```csharp
static int[,] LaddaLabyrintSäkert(string filnamn)
{
    if (!File.Exists(filnamn))
    {
        Console.WriteLine("Labyrintfil saknas! Skapar en standardlabyrint.");
        File.WriteAllLines(filnamn, new string[]
        {
            "111111",
            "100001",
            "101101",
            "100101",
            "101101",
            "100121"
        });
    }

    return LaddaLabyrint(filnamn);
}
```

✅ **Om filen saknas, skapas en standardlabyrint**  
