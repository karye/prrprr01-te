# Labb: bordsbokning

![](../.gitbook/assets/image-38.png)

## Mål

Målet med labben är att träna på att skapa ett helt fungerande program som använder sig av Lista för att lagra information, och fillagring för att spara informationen mellan körningarna.

## Inledning

Ett program som hanterar bordsinformationen åt en påhittad restaurang som vi kallar för **Centralrestaurangen**.

**Centralrestaurangen** har 8 bord och dess personal vill ha ett program där de enkelt kan skriva in ett namn för varje bord samt hur många som sitter på bordet.

Bordshanteraren ska lagra all bordsinformation i en textfil.

### Exempel på bokning

I detta exempel har bord 3 bokats av familjen Svensson med 4 gäster. Bord 7 har bokats av familjen Andersson med 3 gäster.

![](../.gitbook/assets/image-39.png)

Bordens status sparas i en textfil som heter **centralbord.csv**.\
Filen innehåller en rad för varje bord.\
Varje rad innehåller antalet gäster, namnet på bokningen och bordets status **separerade med kommatecken**.

```text
0,Inga gäster
0,Inga gäster
4,Svensson
0,Inga gäster
0,Inga gäster
0,Inga gäster
3,Andersson
0,Inga gäster
```

## Användarinteraktion

Här följer ett exempel på hur programmet kan se ut när det körs.\
Det börjar med att användaren väljer att lägga till en ny bokning på bord 4.\
Användaren skriver in namnet på gästen och hur många som sitter på bordet. Användaren bokar sedan bord 7.

```text
Detta är Centralrestaurangens bordshanterare
Fil med bordsinformation hittades ej, ny information skapades.

1. Visa alla bord
2. Ändra bordsinformation
3. Markera att ett bord är ledigt
4. Avsluta programmet
Välj ett alternativ: 2
Vilket bordsnummer vill du ändra informationen för? 3
Skriv in bordets namn: Svensson
Hur många gäster finns vid bordet? 4

1. Visa alla bord
2. Ändra bordsinformation
3. Markera att ett bord är ledigt
4. Avsluta programmet
Välj ett alternativ: 2
Vilket bordsnummer vill du ändra informationen för? 7
Skriv in bordets namn: Andersson
Hur många gäster finns vid bordet? 3
```

## Steg 1: Programmets initiering

Programmet ska starta med att skapa en lista med bordsinformation. Om filen `centralbord.csv` finns ska programmet läsa in bordsinformationen från filen. Annars ska programmet skapa en ny lista med standardvärden och spara den till filen.

### Exempelkod

```csharp
Console.Clear();
Console.WriteLine("Centralrestaurangens bordsbokning");

// Skapa en lista för bordsinformation
List<string> bordsInformation = [];

// Standardvärde för tomt bord
string tomtBordBeskrivning = "0,Inga gäster";

// Antal bord
int antalBord = 8;

// Namn på lagringsfil
string filnamn = "centralbord.csv";

// Kontrollera om filen finns
if (File.Exists(filnamn))
{
    // Läs in bordsinformationen från filen
    bordsInformation = File.ReadAllLines(filnamn).ToList();
}
else
{
    // Skapa listan med standardvärden
    for (int i = 0; i < antalBord; i++)
    {
        bordsInformation.Add(tomtBordBeskrivning);
    }
    // Spara till fil
    File.WriteAllLines(filnamn, bordsInformation);
}
```

## Steg 2: Bokning av bord

Programmet ska låta användaren boka ett bord genom att ange bordets nummer, ett namn och antal gäster.

### Exempelkod

```csharp
Console.Write("Ange bordsnummer (1-8): ");
int bordsnr = int.Parse(Console.ReadLine());
Console.Write("Ange bokningsnamn: ");
string namn = Console.ReadLine();
Console.Write("Ange antal gäster (1-8): ");
int antalGäster = int.Parse(Console.ReadLine());

// Uppdatera bordsinformationen
bordInformation[bordsnr - 1] = $"{antalGäster},{namn}";

// Spara uppdateringen till fil
File.WriteAllLines(filnamn, bordInformation);

Console.WriteLine("Bordet är bokat!");
```

## Steg 3: Visa alla bord

Programmet ska kunna visa alla bord med deras aktuella bokningar och det totala antalet gäster.

### Exempelkod

```csharp
int totaltAntalGäster = 0;

for (int i = 0; i < bordInformation.Count; i++)
{
    string[] delar = bordInformation[i].Split(',');
    int antalGäster = int.Parse(delar[0]);
    string namn = delar[1];

    Console.WriteLine($"Bord {i + 1}: {namn}, antal gäster: {antalGäster}");
    totaltAntalGäster += antalGäster;
}

Console.WriteLine($"Totalt antal gäster: {totaltAntalGäster}");
```

## Steg 4: Markera bord som tomt

Programmet ska låta användaren markera ett bord som ledigt igen.

### Exempelkod

```csharp
Console.Write("Ange bordsnummer att markera som tomt (1-8): ");
int bordsnr = int.Parse(Console.ReadLine());
bordInformation[bordsnr - 1] = tomtBordBeskrivning;

// Spara uppdateringen till fil
File.WriteAllLines(filnamn, bordInformation);

Console.WriteLine("Bordet är nu tomt.");
```

## Utmaningar

### 1. Sätt ihop all kod

Sätt ihop all kod i en fungerande konsolapplikation. Använd gärna exempelkoden ovan som en grund. Lägg till menyn och loopa programmet tills användaren väljer att avsluta.

### 2. Spara och läsa bordsinformation från fil

Utöka programmet så att det sparar bordsinformationen i en textfil (`centralbord.csv`) och läser in den vid programstart. Om filen inte finns ska den skapas med initiala värden.

### 3. Lägg till nota för varje bord

Utöka programmet så att varje bord kan ha en nota kopplad till sig. Strängformatet blir då: `"antalGäster,namn,nota"`. Lägg till funktionalitet för att ange och visa notan.

### 4. Visa totalt belopp för alla notor

Lägg till funktionalitet som summerar alla notor och visar totalbeloppet i slutet av "Visa alla bord".

### 5. Fler användarvänliga kontroller

- Kontrollera att användaren anger giltiga bordnummer (1–8).
- Kontrollera att antal gäster är inom det tillåtna intervallet (1–8).

### 6. Skapa en historiklogg

Lägg till en funktion som loggar alla ändringar (t.ex. bokningar och notor) i en separat loggfil (`historik.log`).

### 7. Lägg till fler bordsalternativ

Lägg till fler bord i programmet genom att utöka antalet bord. Gör så att detta antal kan ändras genom en variabel och att listan automatiskt anpassar sig.

### 8. Avancerad filhantering med säkerhet

Se till att filen alltid är låst under läsning/skrivning för att förhindra korruption vid fel.

## Facit

Facit till denna labb: [här](facit-bordsbokning.md)