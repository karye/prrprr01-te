# Uppgift: spelet labyrint

## Resultat

![](../.gitbook/assets/image-46.png)

## Flödesschema

![](../.gitbook/assets/image-47.png)

## Instruktioner

### Labyrinten

* Skapa ett konsolprojekt **Labyrint**.
* Skapa en 2d-array som representarar labyrinten.
* Skriv ut labyrinten med hjälp av dubbla loopar.

```csharp
// Labyrinten
int[,] labyrint = [
  {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},  // 0
  ...
];

// Skriv ut labyrinten med hjälp av dubbla loopar
for (int y = 0; y < labyrinten.GetLength(0); y++)
{
    
}
```

### Slumpa ut en spelfigur

* Skapa en slumpgenerator.
* Slumpa ut en position för spelfiguren.
* Skriv ut spelfiguren på den slumpade positionen.
* Talet 2 representerar spelfiguren.

### Slumpa ut tomtar

* På samma sätt kan skapa tomtar mha av en array.
* Slumpa ut en position för tomtarna.
* Skriv ut tomtarna på den slumpade positionen.
* Talet 3 representerar tomtarna.

### Flytta spelfiguren

* Läs in en tangent från tangentbordet.
* Flytta spelfiguren beroende på vilken tangent som trycktes.
* Skriv ut labyrinten igen.

Tips: Använd `Console.ReadKey()` för att läsa in en tangent från tangentbordet:

```csharp
// Flytta spelfiguren
ConsoleKeyInfo tangent = Console.ReadKey();
```

### Väggkollision

* Kontrollera om spelfiguren kolliderar med en vägg.
* Om spelfiguren kolliderar med en vägg så flyttas den inte.
