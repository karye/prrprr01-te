# Labb: positioner i konsolfönstret

## Mål

I denna labb kommer vi att utforska hur man använder `Console.SetCursorPosition(kolumn, rad)` för att skriva ut text på olika positioner i konsolfönstret. Detta är användbart för att skapa allt från enkla menyer till komplexa textbaserade gränssnitt.

## Console.SetCursorPosition(kolumn, rad)

Här är ett exempel på hur du kan använda `Console.SetCursorPosition(kolumn, rad)` för att skriva ut text på olika positioner i konsolfönstret:

```csharp
Console.Clear();
// Placerar texten "Hej världen!" på rad 5, kolumn 10.
Console.SetCursorPosition(10, 5);
Console.WriteLine("Hej världen!");

// Flyttar kursor till rad 10, kolumn 20 och skriver ut "Här är jag!".
Console.SetCursorPosition(20, 10);
Console.WriteLine("Här är jag!");
```

Resultatet ser ut så här i konsolfönstret:

```text
            Hej världen!
                        Här är jag!
```

## Uppgifter 

### Uppgift 1

Skapa ett konsolprojekt **FyraHorn** som ut text i konsolfönstret fyra hörn.
För att göra detta, måste du först ta reda på storleken på konsolfönstret med `Console.WindowWidth` och `Console.WindowHeight`.

### Uppgift 2

Skapa ett nytt konsolprojekt **MinSmiley** som skriver ut en smiley på specifika positioner.
Användaren anger ett koordinatpar (kolumn, rad) och programmet skriver ut en smiley på den positionen.

## Flytta konsolmarkören

Låt oss lyssna på användarens tangenttryckningar och flytta konsolmarkören med hjälp av piltangenterna.

```csharp
Console.Clear();

// Läs tangenttryckningar
Consoletangent tangent = Console.ReadKey(true);

Console.WriteLine($"Du tryckte på tangenten {tangent.Key}");
```

Så här ser det ut i konsolfönstret:

```text
Du tryckte på tangenten UpArrow
```

Vi läser in tangnettryckningen en gång. För att läsa in flera tangenttryckningar behöver vi en loop.

```csharp
Console.Clear();

// Läs tangenttryckningar
while (true)
{
    Consoletangent tangent = Console.ReadKey(true);
    Console.WriteLine($"Du tryckte på tangenten {tangent.Key}");
}
```

Vi har början på ett program som lyssnar på tangenttryckningar. Nu ska vi flytta konsolmarkören med hjälp av piltangenterna.

```csharp
Console.Clear();

// Markörens position
int kol = 0;
int rad = 0;

// Läs tangenttryckningar
while (true)
{
    Consoletangent tangent = Console.ReadKey(true);

    // Flytta konsolmarkören
    if (tangent.Key == ConsoleKey.DownArrow)
    {
        rad++;
    }

    // Rita ut en vit ruta på konsolmarkörens nya position
    // kol * 2 för att rutan är 2 tecken bred
    Console.SetCursorPosition(kol * 2, rad);
    Console.Write("⬜️");
}
```

För varje tangenttryckning flyttar vi konsolmarkören ett steg nedåt. Vi ritar ut en vit ruta på konsolmarkörens nya position. Vi lägger till pil uppåt:

```csharp
Console.Clear();

// Markörens position
int kol = 0;
int rad = 0;

// Läs tangenttryckningar
while (true)
{
    Consoletangent tangent = Console.ReadKey(true);

    // Flytta konsolmarkören
    if (tangent.Key == ConsoleKey.DownArrow)
    {
        rad++;
    }
    else if (tangent.Key == ConsoleKey.UpArrow)
    {
        rad--;
    }

    // Rita ut en vit ruta på konsolmarkörens nya position
    // kol * 2 för att rutan är 2 tecken bred
    Console.SetCursorPosition(kol * 2, rad);
    Console.Write("⬜️");
}
```

### Rensa konsolfönstret

Nu ser det ut som en orm som ritas fram. Vi vill att det ska se ut som att konsolmarkören rör sig. Vi kan göra det genom att rensa konsolfönstret innan vi ritar ut den vita rutan.

```csharp
Console.Clear();

// Markörens position
int kol = 0;
int rad = 0;

// Läs tangenttryckningar
while (true)
{
    Consoletangent tangent = Console.ReadKey(true);

    // Flytta konsolmarkören
    if (tangent.Key == ConsoleKey.DownArrow)
    {
        rad++;
    }
    else if (tangent.Key == ConsoleKey.UpArrow)
    {
        rad--;
    }

    // Rensa konsolfönstret
    Console.Clear();

    // Rita ut en vit ruta på konsolmarkörens nya position
    // kol * 2 för att rutan är 2 tecken bred
    Console.SetCursorPosition(kol * 2, rad);
    Console.Write("⬜️");
}
```

Bra! Nu ser det ut som att konsolmarkören rör sig. Vi kan göra det ännu bättre genom att använda emojis.

### Uppgift 3

* Gör så att man kan flytta markören åt höger och vänster med piltangenterna.

### Uppgift 4

* Om markören flyttas för långt åt höger eller vänster så krashar programmet. Fixa så att markören inte kan flyttas utanför konsolfönstret.

## Skapa mönster med loopar

Nu ska vi skapa ett mönster med hjälp av loopar. Vi börjar med att skapa ett mönster av rutor.

```csharp
Console.Clear();

// Aktivera UTF8 för att visa Emoji
Console.OutputEncoding = System.Text.Encoding.UTF8;

// Dölj konsolmarkören
Console.CursorVisible = false;

// Skapa en array med rutor
int[] mönster = [ 0, 1, 1, 1, 2, 3, 1, 1, 1, 0 ];

// Talen i arrayen betyder UTF8-tecknen
// 0 = "⬜️"
// 1 = "🔵"
// 2 = "🔴"
// 3 = "🟡"

// Rita ut mönstret
rad = 0;
for (int kol = 0; kol < mönster.Length; kol++)
{
    // Rita ut en ruta på konsolmarkörens nya position
    // kol * 2 för att rutan är 2 tecken bred
    Console.SetCursorPosition(kol * 2, rad);

    // Rita ut rutan
    if (mönster[kol] == 0)
    {
        Console.Write("⬜️");
    }
    else if (mönster[kol] == 1)
    {
        Console.Write("🔵");
    }
    else if (mönster[kol] == 2)
    {
        Console.Write("🔴");
    }
    else if (mönster[kol] == 3)
    {
        Console.Write("🟡");
    }
}
```

### Uppgift 5

Lägg till ett mål som användaren ska nå med hjälp av konsolmarkören. När användaren når målet ska programmet avslutas.
Tips: Målets position anges som två variabler `målKol` och `målRad`.
Tips 2: Lägg till en slumpmässig flyttning av målet efter varje framgångsrik träff.

```csharp
// Anta att målKol och målRad är definierade någonstans i koden
while (true)
{
    // Läs tangenttryckningar och flytta markören ...

    // Om användaren når målet
    if (...)
    {
        Console.WriteLine("Målet nått!");
        // Flytta målet till en ny slumpmässig position
        målKol = Random.Shared.Next(0, Console.WindowWidth);
        målRad = Random.Shared.Next(0, Console.WindowHeight);
    }
}
```