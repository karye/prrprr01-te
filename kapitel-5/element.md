﻿# Element i en lista

![](../.gitbook/assets/image-98.png)

## Förstå och använda element i en lista

En lista kan vi tänka oss som en äggkartong där varje ägg är ett värde eller ett element. Varje ägg har sin egen lilla plats som kallas för en indexposition. Indexpositionerna börjar alltid med 0, inte 1. Så det första ägget ligger på plats 0, det andra på plats 1, och så vidare.

### Hur man kommer åt ett element

För att kolla på ett specifikt ägg i kartongen, behöver vi bara veta platsens nummer. Så här kan vi göra det i programmering:

```csharp
// Vi skapar en lista som en äggkartong med 3 platser för våra ägg
List<string> frukter = ["äpple", "banan", "kiwi"];

// För att komma åt första frukten (äpplet) som ligger på plats 0
Console.WriteLine(frukter[0]); // Detta kommer skriva ut "äpple"

// För att komma åt andra frukten (bananen) som ligger på plats 1
Console.WriteLine(frukter[1]); // Detta kommer skriva ut "banan"
```

Såhär ser listan ut i minnet:

| index | string |
| :---: | :----: |
| 0 | äpple |
| 1 | banan |
| 2 | kiwi |

### Ändra ett element

Om vi vill byta ut ett ägg mot ett annat, behöver vi bara bestämma vilken plats vi vill ändra på och sedan sätta in det nya ägget där.

```csharp
// Nu byter vi ut "banan" mot "apelsin" på plats 1 i kartongen
frukter[1] = "apelsin";

// Låt oss kolla vad som finns på plats 1 nu
Console.WriteLine(frukter[1]); // Detta kommer skriva ut "apelsin"
```

Såhär ser nu listan ut i minnet:

| index | string  |
| :---: | :-----: |
| 0 | äpple |
| 1 | apelsin |
| 2 | kiwi |

### Uppgift 1: Byt plats på frukter

* Skapa ett nytt konsolprojekt **Fruktsamling**
* Skapa en lista med 5 frukter.
* Skriv ut alla frukterna i listan.
* Fråga användaren vilka två frukter hen vill byta plats på.
* Efter bytet, skriv ut den nya ordningen av frukterna i listan.

**Tips!** Använd `Console.ReadLine()` för att få användarens input och konvertera det till en siffra med `int.Parse()` för att använda som index.

Konsolen kan det se ut så här:

```text
En lista med 5 frukter
======================
Du har registrerat frukterna: äpple, banan, päron, apelsin, kiwi
Vilken frukt vill du byta plats på? 1
Vilken frukt vill du byta plats med? 3
Du har nu bytt plats på banan och apelsin
Den nya ordningen är: äpple, apelsin, päron, banan, kiwi
```

### Uppgift 2: Hitta och ersätt

* Skapa ett nytt konsolprojekt **HittaOrd**
* Skapa en lista med 5 slumpmässiga ord.
* Fråga användaren efter ett ord att söka efter och ett ord att ersätta det med.
* Om ordet finns i listan, byt ut det mot det nya ordet.
* Skriv ut den uppdaterade listan.

**Tips!** Använd en loop för att gå igenom varje element i listan och en if-sats för att jämföra strängar med `==`.

Konsolen kan det se ut så här:

```text
En lista med 5 ord
======================
Du har registrerat orden: hus, bil, båt, cykel, tåg
Vilket ord vill du byta ut? bil
Vilket ord vill du byta ut det mot? flygplan
Du har nu bytt ut bil mot flygplan
Den nya ordningen är: hus, flygplan, båt, cykel, tåg
```

### Uppgift 3: Gör din egen fruktsallad

* Skapa ett nytt konsolprojekt **Fruktsallad**
* Skapa en lista med namnet `fruktsallad` som kan innehålla 10 frukter.
* Låt användaren lägga till frukter en i taget tills de säger "klar".
* När de är klara, skriv ut alla frukter som finns i `fruktsallad`.

**Tips!** Du kan använda en while-loop som fortsätter fråga användaren tills de skriver "klar". Använd en räknare för att hålla koll på hur många frukter som har lagts till.

Konsolen kan det se ut så här:

```text
En lista med 10 frukter
======================
Du har registrerat frukterna: äpple, banan, päron, apelsin, kiwi, mango, ananas, jordgubbe, hallon, blåbär
```