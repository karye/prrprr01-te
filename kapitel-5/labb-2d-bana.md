# Labb: Labyrint

![Labyrint](../.gitbook/assets/images.jpg)

## Mål

Målet med denna labb är att skapa en Labyrint i konsolfönstret och låta användaren navigera genom Labyrinten med piltangenterna och hitta en utgång.

## Starta projektet

Starta ett konsolprojekt **Labyrint** med följande rader:

```csharp
Console.Clear();

// Aktivera UTF8 för att visa Emoji
Console.OutputEncoding = System.Text.Encoding.UTF8;
```

## Skapa en Labyrint

Vi kan skapa en Labyrint genom att använda en array. Vi kan använda en array med två dimensioner för att lagra Labyrinten. '1' kan representera en vägg och '0' kan representera en gång i Labyrinten.

```csharp
// Skapa en string-array med Labyrint data
int[,] spelplan = [
            { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
            { 1, 0, 0, 1, 0, 0, 0, 1, 0, 1 },
            { 1, 0, 0, 1, 0, 1, 0, 1, 0, 1 },
            { 1, 0, 0, 0, 0, 1, 0, 0, 0, 1 },
            { 1, 0, 1, 1, 1, 0, 0, 0, 0, 1 },
            { 1, 0, 0, 0, 1, 0, 0, 0, 0, 1 },
            { 1, 0, 1, 0, 0, 0, 1, 0, 0, 1 },
            { 1, 0, 1, 1, 1, 0, 1, 1, 0, 1 },
            { 1, 1, 0, 0, 0, 0, 0, 1, 0, 1 },
            { 1, 1, 1, 1, 1, 1, 1, 1, 2, 1 }
        ];
```

## Skriv ut Labyrinten

## Använda Emojis som symboler

Vi kan använda emojikoder som "⬛" och "⬜️" för att rita ut Labyrinten.

Ladda ned **emojis.cs** till projektmappen:

{% embed url="https://raw.githubusercontent.com/anton-bot/Full-Emoji-List/master/Emoji.cs" %}
{% endembed %}

## Rita med Console.SetCursorPosition()

Med `Console.SetCursorPosition()` kan vi rita ut i konsolfönstret. `Console.SetCursorPosition()` tar två argument, en rad och en kolumn, och flyttar markören till den positionen i konsolfönstret. vi kan sedan använda `Console.Write()` för att skriva ut en symbol på den positionen.

Här är ett exempel på hur vi använder `Console.SetCursorPosition()` för att rita ut en ruta och en smiley i konsolfönstret:

```csharp
// Rita ut en ruta på position 0, 0
Console.SetCursorPosition(0, 0);
Console.Write("⬛");

// Rita ut en smmiley på position 5, 5
Console.SetCursorPosition(5, 5);
Console.Write("😃");
```

### Rita ut Labyrinten

Vi använder en `for`-loop för att gå igenom alla element i arrayen och rita ut en emoji för varje element.

Här är ett exempel på hur vi kan använda en `for`-loop för att gå igenom alla element i arrayen och rita ut en emoji för varje element:

```csharp
// Rita ut Labyrinten
// Gå igenom alla element i arrayen och skriv ut en emoji för varje element
for (int rad = 0; rad < spelplan.GetLength(0); rad++)
{
    for (int kol = 0; kol < spelplan.GetLength(1); kol++)
    {
        // Flytta markören till rätt position
        // Vi multiplicerar kolumnen med 2 för att emojin är 2 tecken bred
        Console.SetCursorPosition(kol * 2, rad);

        // Rita ut en emoji för varje vägg (⬛) eller gång (⬜️) i Labyrinten
        if (spelplan[rad, kol] == "0")
        {
            Console.Write("⬛");
        }
        else
        {
            Console.Write("⬜️");
        }
    }
}
```

När vi kör programmet kommer vi då att få en Labyrint ritad i konsolfönstret, som ser ut ungefär som följer:

```text
⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛
⬛⬜️⬜️⬛⬜️⬜️⬜️⬛⬜️⬛
⬛⬜️⬜️⬛⬜️⬛⬜️⬛⬜️⬛
⬛⬜️⬜️⬜️⬜️⬛⬜️⬜️⬜️⬛
⬛⬜️⬛⬛⬛⬜️⬜️⬜️⬜️⬛
⬛⬜️⬜️⬜️⬛⬜️⬜️⬜️⬜️⬛
⬛⬜️⬛⬜️⬜️⬜️⬛⬜️⬜️⬛
⬛⬜️⬛⬛⬛⬜️⬛⬛⬜️⬛
⬛⬛⬜️⬜️⬜️⬜️⬜️⬛⬜️⬛
⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛
```

### Utmaning

* Gör spelplanen större
* Lägg till fler emojis som vi ritar ut i Labyrinten

## Interaktion med användaren

Vi låter sedan användaren navigera genom Labyrinten med piltangenterna och hitta en utgång.

### Animationsloopen

Vi använder en `while`-loop för att skapa en animationsloop som körs tills användaren stänger programmet. Vi kan sedan använda `Console.ReadKey()` för att läsa in tangenttryckningar från användaren.

```csharp
// Dölj konsolmarkören
Console.CursorVisible = false;

// Animationsloopen
while (true)
{
    // Läs in tangenttryckningar från användaren
    ConsoleKeyInfo tangenttryckning = Console.ReadKey(true);

    // Om användaren trycker på piltangenterna
    switch (tangenttryckning.Key)
    {
        case ConsoleKey.UpArrow:
            // Flytta upp spelaren
            break;
        case ConsoleKey.DownArrow:
            // Flytta ner spelaren
            break;
        case ConsoleKey.LeftArrow:
            // Flytta vänster spelaren
            break;
        case ConsoleKey.RightArrow:
            // Flytta höger spelaren
            break;
    }
}
```

### Spelfiguren

Vi kan använda en variabel för att lagra positionen för spelfiguren i Labyrinten. Vi kan sedan uppdatera positionen för spelfiguren när användaren trycker på piltangenterna.

Vi använder följande kod för att lagra positionen för spelfiguren i Labyrinten:

```csharp
// Lagra positionen för spelaren
int spelareRad = 1;
int spelareKol = 1;
```

Vi uppdaterar sedan positionen för spelfiguren när användaren trycker på piltangenterna:

```csharp
// Om användaren trycker på piltangenterna
switch (tangenttryckning.Key)
{
    case ConsoleKey.UpArrow:
        // Flytta upp spelaren
        spelareRad--;
        break;
    case ConsoleKey.DownArrow:
        // Flytta ner spelaren
        spelareRad++;
        break;
    case ConsoleKey.LeftArrow:
        // Flytta vänster spelaren
        spelareKol--;
        break;
    case ConsoleKey.RightArrow:
        // Flytta höger spelaren
        spelareKol++;
        break;
}
```

### Rita ut spelfiguren

Vi använder `Console.SetCursorPosition()` för att rita ut spelfiguren på den nya positionen.

```csharp
// Rita ut figuren
Console.SetCursorPosition(spelareKol, spelareRad);
Console.Write("😃");
```

### Radera spelfigurens spår

När vi flyttar spelfiguren så kommer den att lämna ett spår efter sig. Vi kan använda `Console.SetCursorPosition()` för att rita ut en tom ruta på den gamla positionen för spelfiguren.

Vi behöver dock först lagra den gamla positionen för spelfiguren i en variabel. Vi kan sedan använda `Console.SetCursorPosition()` för att rita ut en tom ruta på den gamla positionen för spelfiguren.

```csharp
// Lagra den gamla positionen för spelaren
int gammalSpelareRad = spelareRad;
int gammalSpelareKol = spelareKol;

// Om användaren trycker på piltangenterna
switch (tangenttryckning.Key)
...

// Radera spåret efter spelaren
Console.SetCursorPosition(gammalSpelareKol, gammalSpelareRad);
Console.Write("⬜️");

// Rita ut figuren på den nya positionen
Console.SetCursorPosition(spelareKol, spelareRad);
Console.Write("😃");
```

### Förflyttningsregler

Figuren ska inte kunna gå igenom väggar i Labyrinten. När spelaren trycker på en piltangenter kollar vi om det är en vägg på den nya positionen. Om det är en vägg så ska inte spelaren kunna flytta sig till den nya positionen.

Vi använder följande kod för att kolla om det är en vägg på den nya positionen:

```csharp
// Om användaren trycker på piltangenterna
switch (tangenttryckning.Key)
{
    case ConsoleKey.UpArrow:
        // Flytta upp spelaren
        if (spelplan[spelareRad - 1, spelareKol] != 1)
        {
            spelareRad--;
        }
        break;
    case ConsoleKey.DownArrow:
        // Flytta ner spelaren
        if (spelplan[spelareRad + 1, spelareKol] != 1)
        {
            spelareRad++;
        }
        break;
    case ConsoleKey.LeftArrow:
        // Flytta vänster spelaren
        if (spelplan[spelareRad, spelareKol - 1] != 1)
        {
            spelareKol--;
        }
        break;
    case ConsoleKey.RightArrow:
        // Flytta höger spelaren
        if (spelplan[spelareRad, spelareKol + 1] != 1)
        {
            spelareKol++;
        }
        break;
}
```

## Läsa in Labyrinten från en textfil

För att läsa in en Labyrint från en textfil kan vi använda metoden `File.ReadAllLines()` från namespace `System.IO`. Med hjälp av `File.ReadAllLines()` kan vi läsa in alla rader i textfilen på en gång och sedan lagra dem i en array.

Här är ett exempel på hur vi kan använda `File.ReadAllLines()` för att läsa in en Labyrint från en textfil och lagra innehållet i en array:

```csharp
// Definera en array för Labyrinten
int[,] grid = new int[10, 10];

// Läs in alla rader från textfilen och lagra dem i en strängarray
string[] rader = File.ReadAllLines("labyrint.txt");

// Gå igenom alla rader i strängarrayen
for (int rad = 0; rad < grid.GetLength(0); rad++)
{
    // Gå igenom alla tecken i raden
    for (int kol = 0; kol < grid.GetLength(1); kol++)
    {
        // Lagra tecknet i arrayen
        grid[rad, kol] = int.Parse(rader[rad][kol].ToString());
    }
}
```

Som vi kan se i kodexemplet ovan använder vi `File.ReadAllLines()` för att läsa in alla rader från textfilen och lagra dem i en strängarray. Därefter går vi igenom alla rader i strängarrayen och lagrar tecknen i Labyrintarrayen på samma sätt som tidigare.

När vi har läst in Labyrinten från textfilen kan vi sedan använda samma metod som tidigare för att rita ut Labyrinten i konsolfönstret.

Till exempel kan vi låta användaren navigera genom Labyrinten med piltangenterna och hitta en utgång. Vi kan också lägga till en poängräkning och hantera olika scenarier, såsom att användaren går in i en fälla eller hittar en skatt.

## Facit

Facit hittar du [här](facit-2d-bana.md).