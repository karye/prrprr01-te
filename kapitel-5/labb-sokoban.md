﻿# Labb: spelet Sokoban i konsolen

![](../.gitbook/assets/image-99.png)

## Mål

I denna laboration ska vi bygga ett Sokoban-spel som körs i konsolen. Målet är att du som programmerare ska skapa ett interaktivt spel där spelaren styr en figur för att flytta lådor till specifika mål.

## Starta projektet

Starta ett nytt konsolprojekt **Sokoban** och lägg till följande kod:

```csharp
Console.Clear();

// Aktivera UTF8 för att visa Emoji
Console.OutputEncoding = System.Text.Encoding.UTF8;
Console.CursorVisible = false; // Gömmer markören

// Presentera programmet
Console.WriteLine("Spelet Sokoban");

// Fråga om spelet ska starta
Console.Write("Vill du starta spelet? (j/n): ");
if (Console.ReadLine().ToLower() != "j")
{
    // Avsluta programmet
    return;
}
```

## Flödeschema

I följande flödeschema kan du se hur programmet ska fungera:

```mermaid
graph TD
    A[Start] --> B[Skapa virtuell spelplan]
    B --> C[Rita ut spelplan i konsolen]
    C --> D[Loopen börjar]
    D --> E[Läs in tangenttryckning]
    E --> F[Kolla om spelare kan flyttas]
    F --> G[Kolla om låda kan flyttas]
    G --> H[Kolla om låda flyttas till mål, räkna upp poäng]
    H --> I[Är spelet klart?] --> D
    I --> J[Avsluta programmet]
```

## Spelplanen

### Skapa spelplanen

Definiera en tvådimensionell `array` som representerar spelplanen. Varje tal i arrayen representerar en typ av ruta:

* 0: Tom ruta
* 1: Vägg
* 2: Mål
* 3: Låda
* 4: Spelare

Exempel:

```csharp
int[,] spelplan = [
    {1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
    {1, 0, 0, 0, 1, 0, 2, 0, 0, 1},
    // ... Fyll i resten av spelplanen
    {1, 1, 1, 1, 1, 1, 1, 1, 1, 1}
];
```

### Rita ut spelplanen

Använd dubbla `for`-loopar för att rita ut spelplanen:

```csharp
// Loopa igenom raderna
for (int rad = 0; rad < spelplan.GetLength(0); rad++)
{
    // Loopa igenom kolumnerna
    for (int kol = 0; kol < spelplan.GetLength(1); kol++)
    {
        // Flytta markören till rätt position
        Console.SetCursorPosition(kol * 2, rad);

        // Rita ut rätt symbol beroende på vilken ruta det är
        switch (spelplan[rad, kol])
        {
            // ... Fyll i resten av switch-satsen
        }
    }
    Console.WriteLine();
}
```

### Rita ut lådor och figurer

Låt oss anta att du har lagt till lådor och en spelare i arrayen. Rita om spelplanen med lådorna och spelaren på rätt plats genom att anpassa `switch`-satsen ovan.

### Rita ut spelaren

För att rita ut spelaren, använd `Console.Write` för att skriva ut en emoji på rätt position:

```csharp
// Spelarens position
int spelareRad = 1;
int spelareKol = 1;

// Rita ut spelaren
Console.SetCursorPosition(spelareKol * 2, spelareRad);
Console.Write("😃");
```

## Animationsloopen

Använd en `while`-loop för att uppdatera spelplanen och rita ut den på nytt i varje iteration:

```csharp
while (true)
{
    // Läs in tangenttryckning
    // Ändra spelplanen beroende på tangenttryckning
    // Räkna upp poäng om låda flyttas till mål
}
```

### Interaktion med användaren

För att läsa in en tangenttryckning från användaren och agera beroende på input:

```csharp
ConsoleKeyInfo key = Console.ReadKey(true);
switch (key.Key)
{
    case ConsoleKey.UpArrow:
        // Flytta spelaren uppåt
        break;
    case ConsoleKey.DownArrow:
        // Flytta spelaren nedåt
        break;
    case ConsoleKey.LeftArrow:
        // Flytta spelaren åt vänster
        break;
    case ConsoleKey.RightArrow:
        // Flytta spelaren åt höger
        break;
}
```

### Flytta lådor

När spelaren rör sig och nästa ruta är en låda, kontrollera om rutan bakom lådan är tom eller ett mål innan du flyttar lådan:

```csharp
// Exempel: Flytta lådan åt höger
if (spelplan[spelareRad, spelareKol + 1] == 3)
{
    // Kolla om rutan bakom lådan är tom eller ett mål
    ...
}
```

### Poäng

För att hålla reda på poäng, öka en poängräknare varje gång en låda flyttas till ett mål:

```csharp
int poäng = 0;

// När en låda flyttas till ett mål
poäng++;
Console.SetCursorPosition(0, 11);
Console.Write($"Poäng: {poäng}");
```

## Utmaningar

* Lägg till logik för att hantera när spelet är klart, t.ex. när alla lådor är på målen.
* Använd en loop för att fortsätta ta emot användarinput tills spelet är över.
* Lägg till en meny för att starta om spelet eller avsluta.
* Lägg till fler banor.
