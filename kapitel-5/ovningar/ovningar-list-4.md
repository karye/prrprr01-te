---
description: Övningar på listor i C#.
---

# Mikroövning: Listor 4:4

I denna övning ska du träna på att använda listor för att lagra, lägga till, ta bort och visa data. Du kommer också att hantera användarens inmatning för att arbeta med listan.

Till din hjälp: [C#-lathund](https://csharp.progdocs.se/lathund)

## 1. Skapa en lista med action-spel

- Skapa en tom lista för att lagra spel av typen **action**.
- Fyll sedan listorna med **två** spel i varje kategori.
- Tips 1: Använd tex `List<string> listaNamn = [];` för att skapa en tom lista.
- Tips 2: Använd `Add()` för att lägga till ett element i listan.

## 2. Skapa en lista med äventyrsspel

- På samma sätt som i föregående övning, skapa en lista av typen `List<string>` för att lagra spel av typen **äventyr**.

## 3. Användaren fyller på lista med action-spel

- Skapa en fråga som ber användaren att fylla i ytterligare **två** spel.
- Tips: Använd `Console.ReadLine()` för att läsa in användarens inmatning.

Exempel på körning:
```
Nu ska du fylla i två action-spel i listan:
Ange ett action-spel: GTA V
Ange ett action-spel: Battlefield
```

## 4. Användaren fyller på lista med äventyrsspel med for-loop

- Använd en `for-loop` som snurrar **tre** gånger.
- Tips: Använd `.Add()` för att lägga till användarens inmatning i listan.

Exempel på körning:
```
Nu ska du fylla i tre äventyrsspel:
Ange ett äventyrsspel: Uncharted
Ange ett äventyrsspel: The Last of Us
Ange ett äventyrsspel: Tomb Raider
```

## 5. Användaren fyller på lista med action-spel med for-loop

- Använd en `for-loop` som snurrar **två** gånger.

Exempel på körning:
```
Nu ska du fylla i två action-spel:
Ange ett action-spel: Call of Duty
Ange ett action-spel: Battlefield
```

## 6. Skriv ut de två första action-spelen mha index

- Skriv ut de **två** första spelen i listan.
- Tips: Använd index för att komma åt element i listan: tex `listaNamn[0]`. 

Exempel på körning:
```
Här är de två första action-spelen i listan:
- Spel 1: Call of Duty
- Spel 2: Battlefield
```	

## 7. Skriv ut äventyrsspel med index

- Skriv ut de **två** sista spelen i listan.

Exempel på körning:
```
Här är de två sista äventyrsspelen i listan:
- Spel 3: The Last of Us
- Spel 4: Tomb Raider
```

## 8. Skriv ut alla action-spel

- Skriv ut alla spel i varje kategori.
- Tips: Använd `foreach-loop` för att skriva ut varje spel i listan.

Exempel på körning:
```
Här är alla action-spel i listan:
- Call of Duty
- Battlefield
- GTA V

Här är alla äventyrsspel i listan:
- Tomb Raider
- The Last of Us
- Uncharted
```

## 9. Skriv ut de tre första spelen i listan mha for-loop

- Skriv ut de **tre** första spelen i listan med hjälp av en `for-loop`.

Exempel på körning:
```
Här är de tre första action-spelen i listan:
- Spel 1: GTA V
- Spel 2: Battlefield
- Spel 3: Call of Duty
```

## 10. Använd en for-loop för att registrera fler spel

- Låt användaren fylla på listorna med **två** spel i varje kategori.
- Tips: Använd en `for-loop` för att fråga användaren om att fylla i fler spel.

Exempel på körning:
```
Registrera fler action-spel:
Ange ett action-spel: Call of Duty 2
Ange ett action-spel: Call of Duty 3

Registrera fler äventyrsspel:
Ange ett äventyrsspel: Tomb Raider 2
Ange ett äventyrsspel: The Last of Us
```

## 11. Skriv ut listorna igen

- Skriv ut alla spel i varje kategori med hjälp av en `foreach`-loop.

Exempel på körning:
```
Lista på alla action-spel:
- Call of Duty
- Battlefield
- GTA V
- Call of Duty 2
- Call of Duty 3

Lista på alla äventyrsspel:
- Tomb Raider
- The Last of Us
- Uncharted
- Tomb Raider 2
- The Last of Us 2
```

## 12. Skapa en meny

- Skapa en meny som låter användaren välja att:
  - Visa alla action-spel.
  - Registrera ett action-spel.
  - Avsluta programmet.
- Tips 1: Använd en `while`-loop för att hålla menyn igång tills användaren väljer att avsluta.
- Tips 2: Använd en `if-else`-sats för att hantera varje val.

Exempel på körning:
```
a. Visa alla action-spel
b. Registrera ett action-spel
c. Avsluta
Välj ett alternativ:
...
```

## 13. Skapa logiken för menyalternativen

- Implementera logiken för varje menyalternativ:
  - Visa alla action-spel.
  - Registrera ett action-spel.
  - Avsluta programmet.

## 14. Fler menyalternativ

- Lägg till fler menyalternativ för att:
  - Visa alla äventyrsspel.
  - Registrera tre äventyrsspel.

Exempel på körning:
```
a. Visa alla action-spel
b. Registrera ett action-spel
c. Visa alla äventyrsspel
d. Registrera tre äventyrsspel
e. Avsluta
Välj ett alternativ:
...
```