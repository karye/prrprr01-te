# Övningar 

På egen hand får du nu lösa några uppgifter som tränar på for-loopar och tryParse.

Till din hjälp finns en [C#-lathund](https://csharp.progdocs.se/lathund).

## Grundläggande om listor

```csharp
// Skapa en tom lista av strängar
List<string> listaNamn = [];

// Skapa en lista av strängar
List<string> listaNamn = ["Anna", "Björn", "Cecilia", "Daniel", "Eva"];

// Skapa en tom lista av heltal
List<int> listaÅrtal = [];

// Skapa en lista av heltal
List<int> listaÅrtal = [1492, 1776, 1914, 1945, 1969];

// Skriv ut en lista av strängar
Console.WriteLine(string.Join(", ", listaNamn));

// Skriv ut ett specifikt element i listan
Console.WriteLine(listaNamn[2]);

// Lägg till ett element i listan
listaNamn.Add("Fredrik");

// Ta bort ett element från listan
listaNamn.Remove("Björn");

// Ta bort ett element från listan på en specifik position
listaNamn.RemoveAt(2);

// Räkna antalet element i listan
int antalNamn = listaNamn.Count;

// Sortera listan
listaNamn.Sort();
```