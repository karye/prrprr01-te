---
description: Övningar på listor i C#.
---

# Mikroövning: Listor 3:4

I denna övning ska du träna på att använda listor för att lagra, lägga till, ta bort och visa data. Du kommer också att hantera användarens inmatning för att arbeta med listan.

Till din hjälp: [C#-lathund](https://csharp.progdocs.se/lathund)

## 1. Skriv ut lista med foreach
- Skapa en lista av maträtter med minst tre rätter.
- Använd en `foreach`-loop för att skriva ut alla maträtter.

Exempel på körning:
```
Alla maträtter:
- Pizza
- Pasta
- Sallad
```

Tips:
```csharp
// Exempel med namn
List<string> listaNamn = ["Anna", "Björn", "Cecilia"];
foreach (string namn in listaNamn)
{
    Console.WriteLine($"- {namn}");
}
```

## 2. Skriv ut alla årtalen i en lista
- Skapa en lista av typen `List<int>` med fem olika årtal.
- Använd en `foreach`-loop för att skriva ut alla årtal.

Exempel på körning:
```
Alla årtal i listan:
* 1492
* 1776
* 1914
* 1945
* 1969
```

## 3. Skriv ut på en rad
- Ta föregående övning och skriv ut alla årtal på en rad med kommatecken mellan varje år.
- Tips: Använd `Console.Write` istället för `Console.WriteLine`.

Exempel på körning:
```
Alla årtal: 1492, 1776, 1914, 1945, 1969,
```

## 4. Användaren fyller på en lista
- Skapa en tom lista av typen `List<string>`.
- Låt användaren fylla på listan med namn tills hen skriver `exit`.
- Visa listan efter att användaren har avslutat inmatningen.

Exempel på körning:
```
Ange ett namn att lägga till (eller 'exit' för att avsluta): Anna
Ange ett namn att lägga till (eller 'exit' för att avsluta): Björn
Ange ett namn att lägga till (eller 'exit' för att avsluta): Cecilia
Ange ett namn att lägga till (eller 'exit' för att avsluta): exit

Alla namn: Anna, Björn, Cecilia
```

Tips:
```csharp
// Exempel med djur
List<string> listaDjur = [];
while (true)
{
    Console.Write("Ange ett djur att lägga till (eller 'exit' för att avsluta): ");
    string svar = Console.ReadLine();
    if (svar == "exit")
    {
        break;
    }
    listaDjur.Add(svar);
}
```

## 5. Användaren fyller på en lista med årtal
- Skapa en tom lista av typen `List<int>`.
- Låt användaren fylla på listan med årtal tills hen skriver `0`.

Exempel på körning:
```
Ange ett årtal att lägga till (eller '0' för att avsluta): 1492
Ange ett årtal att lägga till (eller '0' för att avsluta): 1776
Ange ett årtal att lägga till (eller '0' för att avsluta): 1914
Ange ett årtal att lägga till (eller '0' för att avsluta): 0

Alla årtal: 1492, 1776, 1914
```
## 6. Numrerad lista
- Skapa en lista av typen `List<string>` med tre namn.
- Använd en `for`-loop för att skriva ut alla namn med ett nummer framför.

Exempel på körning:
```
1. Anna
2. Björn
3. Cecilia
```

Tips:
```csharp
// Exempel med färger
List<string> listaFärger = ["Röd", "Blå", "Grön"];
for (int i = 0; i < listaFärger.Count; i++)
{
    Console.WriteLine($"{i + 1}. {listaFärger[i]}");
}
```

## 7. Loopa o fråga
- Skapa en lista av typen `List<string>` med fem städer.
- Använde en `while`-loop för att fråga användaren vilken stad hen vill besöka.
- Be användaren ange indexet för staden hen vill besöka.

Exempel på körning:
```
Lista med städer:
0. Göteborg
1. Malmö
2. Stockholm
3. Uppsala
4. Örebro

Vilken stad vill du besöka (0-4)? 2
Du valde att besöka Stockholm.

Vilken stad vill du besöka (0-4)? 5
Fel: Ange ett index mellan 0 och 4.
```

## 8. Hitta namn i en lista
- Skapa en lista av typen `List<string>` med tre namn.
- Låt användaren lägga till **två** namn.
- Låt användaren skriva in ett namn att söka efter.

Exempel på körning:
```
Ange ett namn att söka efter: Björn
Björn finns i listan.
```

Tips:
```csharp
// Exempel med djur
List<string> listaDjur = ["Hund", "Katt", "Häst", "Kanin", "Fisk"];
string djurAttSöka = "Katt";
if (listaDjur.Contains(djurAttSöka))
{
    Console.WriteLine($"{djurAttSöka} finns i listan.");
}
else
{
    Console.WriteLine($"{djurAttSöka} finns inte i listan.");
}
```

## 9. Hitta namn som börjar på en viss bokstav
- Skapa en lista av typen `List<string>` med fem namn.
- Låt användaren skriva in en bokstav.
- Visa alla namn i listan som börjar på den bokstaven.

Exempel på körning:
```
Ange en bokstav: A
Namn som börjar på A: Anna
```

Tips:
```csharp
// Exempel med färger
List<string> listaFärger = ["Röd", "Blå", "Grön", "Gul", "Rosa"];
char bokstav = 'R';
foreach (string färg in listaFärger)
{
    if (färg.StartsWith(bokstav))
    {
        Console.WriteLine(färg);
    }
}
```

## 10. Lägg till namn tills listan har 5 namn
- Skapa en tom lista av typen `List<string>`.
- Använd en `for`-loop för att låta användaren lägga ett namn i taget tills listan har fem namn.

Exempel på körning:
```
Nuvarande lista:
Ange ett namn: Anna
Nuvarande lista: Anna
Ange ett namn: Björn
Nuvarande lista: Anna, Björn
...
```

Tips:
```csharp
// Exempel med maträtter
List<string> listaMaträtter = [];
for (int i = 0; i < 5; i++)
{
    Console.WriteLine("Ange en maträtt:");
    string maträtt = Console.ReadLine();
    listaMaträtter.Add(maträtt);
    Console.WriteLine($"Nuvarande lista: {string.Join(", ", listaMaträtter)}");
}
```

## 11. Ta bort ett namn om det finns
- Skapa en lista av typen `List<string>` med fem namn.
- Låt användaren skriva in ett namn som ska tas bort.
- Kontrollera med en `if`-sats om namnet finns i listan:
  - Om namnet finns, ta bort det.
  - Om det inte finns, visa ett felmeddelande.

Exempel på körning:
```
Ange ett namn att ta bort: Björn
Nuvarande lista: Anna, Cecilia, Daniel, Eva

Ange ett namn att ta bort: Alex
Fel: Alex finns inte i listan.
```

Tips:
```csharp
// Exempel med djur
List<string> listaDjur = ["Hund", "Katt", "Häst", "Kanin", "Fisk"];
string djurAttTaBort = "Katt";
if (listaDjur.Contains(djurAttTaBort))
{
    listaDjur.Remove(djurAttTaBort);
}
```

## 12. Upprepa en meny med alternativ
- Skapa en meny där användaren kan välja:
  1. Lägg till ett namn.
  2. Ta bort ett namn.
  3. Visa listan.
  4. Avsluta programmet.
- Använd en `while`-loop för att hålla menyn igång tills användaren väljer att avsluta.

Exempel på körning:
```
1. Lägg till namn
2. Ta bort namn
3. Visa lista
4. Avsluta
Välj ett alternativ: 1
Nuvarande lista: Anna
```

Tips:
```csharp
// Exempelmeny
List<string> lista = [];
while (true)
{
    Console.WriteLine("""
    1. Lägg till namn
    2. Ta bort namn
    3. Visa lista
    4. Avsluta
    """);
    string val = Console.ReadLine();
    if (val == "1")
    {
        lista.Add("Anna"); // Simulerad inmatning
    }
    else if (val == "2")
    {
        // Läs in namn att ta bort
        ...
        lista.Remove("Anna"); // Simulerad inmatning
    }
    else if (val == "3")
    {
        Console.WriteLine(string.Join(", ", lista));
    } 
    else if (val == "4")
    {
        break;
    }
}
```

## 13. Summera alla tal i en lista
- Skapa en lista av typen `List<int>` med fem tal.
- Använd en `foreach`-loop för att summera alla tal i listan.
- Visa summan.

Exempel på körning:
```
Summan av talen är: 30
```

Tips:
```csharp
// Exempel med jämna tal
List<int> listaTal = [2, 4, 6, 8, 10];
int summa = 0;
foreach (int tal in listaTal)
{
    summa += tal;
}
Console.WriteLine($"Summan av talen är: {summa}");
```
