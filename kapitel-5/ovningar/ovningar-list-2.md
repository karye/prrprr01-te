---
description: Övningar på listor i C#.
---

# Mikroövning: Listor 2:4

I denna övning ska du träna på att använda listor för att lagra, lägga till, ta bort och visa data. Du kommer också att hantera användarens inmatning för att arbeta med listan.

Till din hjälp: [C#-lathund](https://csharp.progdocs.se/lathund)

## 1. Skapa en lista och lägg till några initiala värden
- Skapa en lista av typen `List<string>` som innehåller några namn, t.ex.:
  ```
  ["Anna", "Björn", "Cecilia"]
  ```
- Visa listan för användaren.

Exempel:
```
Nuvarande lista: Anna, Björn, Cecilia
```

## 2. Lägg till ett namn
- Låt användaren skriva in ett namn som ska läggas till i listan.
- Visa hela listan efter att namnet har lagts till.

Exempel:
```
Ange ett namn att lägga till: Daniel
Listan efter tillägg: Anna, Björn, Cecilia, Daniel
```

## 3. Ta bort ett namn
- Låt användaren skriva in ett namn som ska tas bort från listan.
- Visa hela listan efter att namnet har tagits bort.
- Tips: Kontrollera med `if (namnLista.Contains(namnAttTaBort))` innan du försöker ta bort namnet.

Exempel:
```
Ange ett namn att ta bort: Alex
Fel: Alex finns inte i listan.

Ange ett namn att ta bort: Björn
Listan efter borttagning: Anna, Cecilia, Daniel
```

## 4. Sök efter ett namn
- Låt användaren skriva in ett namn att söka efter i listan.
- Kontrollera om namnet finns i listan och visa ett meddelande om det hittades eller inte.

Exempel:
```
Ange ett namn att söka efter: Cecilia
Cecilia finns i listan.

Ange ett namn att söka efter: Erik
Erik finns inte i listan.
```

## 5. Sortera och visa listan
- Sortera listan i alfabetisk ordning och skriv ut den.
- Tips: Använd `Sort()`-metoden för att sortera listan.

Exempel:
```
Vill du sortera listan? (j/n): j
Listan sorterad i alfabetisk ordning: Anna, Cecilia, Daniel
```

## Valfria förbättringar
1. Skapa en meny som låter användaren välja mellan att lägga till, ta bort, söka, sortera eller visa listan.
1. Lägg till ett alternativ som tömmer listan med `Clear()`.
1. Visa hur många namn som finns i listan med `Count`.
1. Kontrollera att inga dubbletter läggs till i listan.
