---
description: Övningar på listor i C#.
---

# Mikroövning: Listor 1:4

I denna övning ska du träna på att använda listor för att lagra, lägga till, ta bort och visa data. Du kommer också att hantera användarens inmatning för att arbeta med listan.

Till din hjälp: [C#-lathund](https://csharp.progdocs.se/lathund)

## 1. Skapa en lista med 5 namn
- Skapa en lista av typen `List<string>` som innehåller exakt 5 namn.\

Exempel på körning:
```
Min kompisar är: Anna, Björn, Cecilia, Daniel, Eva
```

Tips:
```csharp
// Exempel med frukter
List<string> listaFrukter = ["Äpple", "Banan", "Apelsin", "Päron", "Kiwi"];
Console.WriteLine($"Min kompisar är: {string.Join(", ", listaFrukter)}");
```

## 2. Skapa en lista med 5 orter
- Skapa en lista av typen `List<string>` som innehåller 5 orter.
- Visa listans innehåll.

Exempel på körning:
```
Fem orter (Stockholm, Göteborg, Malmö, Uppsala, Lund) ligger i Sverige.
```

## 3. Skapa en lista med 5 årtal
- Skapa en lista av typen `List<int>` som innehåller 5 olika årtal.

Exempel på körning:
```
Fem viktiga årtal är: '1492', '1776', '1914', '1945', '1969'.
```

## 4. Visa första och sista objektet i en lista
- Skapa en lista av typen `List<string>` som innehåller 5 namn.
- Visa det första och det sista namnet i listan.

Exempel på körning:
```
Jag har en namnlista med 5 namn:
Första namnet: Anna
Sista namnet: Eva
```

Tips:
```csharp
// Exempel med språk
List<string> listaSpråk = ["C#", "Java", "Python", "JavaScript", "Swift"];
Console.WriteLine("Första språket: " + listaSpråk[0]);
Console.WriteLine("Sista språket: " + listaSpråk[4]);
```

## 5. Skapa en lista med 5 favoritdrycker
- Skapa en lista av typen `List<string>` som innehåller dina 5 favoritdrycker.
- Visa listans innehåll, en färg per rad.

Exempel på körning:
```
Mina favoritdrycker:
1. Kaffe
2. Te
3. Cola
4. Juice
5. Vatten
```

## 6. Skapa en lista med födelseår
- Skapa en lista av typen `List<int>` som innehåller födelseåren för 5 personer.
- Visa hela listans innehåll i fallande ordning.

Tips:
```csharp
// Exempel med åldrar
List<int> listaÅldrar = [18, 22, 25, 30, 35];
Console.WriteLine(string.Join(", ", listaÅldrar));
```

Exempel på körning:
```
Födelseår:
1. 2000
2. 1995
3. 1990
4. 1985
5. 1980
```

## 7. Lägg till ett namn baserat på en variabel
- Skapa en lista av typen `List<string>` med två namn från början.
- Läs in en namn från användaren.
- Lägg till namnet och visa listans innehåll.

Exempel på körning:
```
Nuvarande lista: Anna, Björn
Ange ett namn att lägga till: Cecilia
Uppdaterad lista: Anna, Björn, Cecilia
```

Tips:
```csharp
List<string> listaFrukter = ["Äpple", "Banan"];
Console.Write("Ny frukt att lägga till: ");
string nyFrukt = Console.ReadLine();
listaFrukter.Add(nyFrukt);
```

## 8. Lägg till ett årtal baserat på en variabel
- Skapa en lista av typen `List<int>` med två årtal från början.
- Läs in ett årtal från användaren.
- Lägg till året och visa listans innehåll.

Exempel på körning:
```
Nuvarande lista: 1492, 1776
Ange ett årtal att lägga till: 1914
Uppdaterad lista: 1492, 1776, 1914
```

## 9. Kontrollera om ett namn redan finns
- Skapa en lista av typen `List<string>` med tre namn.
- Låt användaren skriva in ett namn.
- Kontrollera om namnet redan finns i listan med en `if`-sats.
  - Om namnet finns, visa ett meddelande.
  - Om namnet inte finns, lägg till det och visa listan.

Exempel på körning:
```
Namn i listan: Anna, Björn, Cecilia
Ange ett namn: Anna
Anna finns redan i listan.
```

Tips:
```csharp
// Exempel med länder
List<string> listaLänder = ["Sverige", "Norge", "Danmark"];
string nyttLand = "Island";
if (listaLänder.Contains(nyttLand))
{
    Console.WriteLine($"{nyttLand} finns redan i listan.");
}
else
{
    listaLänder.Add(nyttLand);
    Console.WriteLine(string.Join(", ", listaLänder));
}
```

## 10. Kontrollera om ett årtal redan finns
- Skapa en lista av typen `List<int>` med tre årtal.
- Låt användaren skriva in ett årtal.
- Kontrollera om året redan finns i listan med en `if`-sats.
  - Om året finns, visa ett meddelande.
  - Om året inte finns, lägg till det och visa listan.  

Exempel på körning:
```
Årtal i listan: 1492, 1776, 1914
Ange ett årtal: 1776
1776 finns redan i listan.

Årtal i listan: 1492, 1776, 1914
Ange ett årtal: 1945
Uppdaterad lista: 1492, 1776, 1914, 1945
```