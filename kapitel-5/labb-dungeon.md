---
description: Skapa ett textbaserat äventyrsspel i konsolen. Lär dig använda listor, slumptal och spel-loopar för att skapa en dungeon som spelaren kan utforska.
---

# Laboration: Dungeon-äventyr

![](../.gitbook/assets/image-121.png)

## Introduktion

Målet med spelet är att gå från rum till rum, hitta föremål, besegra fiender och försöka hitta utgången ur dungeonen. Vi bygger spelet stegvis och lägger till funktionalitet för varje del.

---

## Steg 1: Skapa en hall

Vi börjar enkelt! I detta steg skapar vi ett rum och låter spelaren välja om de vill utforska eller gå vidare.

```csharp
Console.Clear();
Console.WriteLine("Välkommen till Dungeon-äventyret! Du är i ett mörkt rum.");

// Programvariabler
string rum = "hallen";  // Starta i "hallen"

// Meny
Console.WriteLine("1. Titta runt");
Console.WriteLine("2. Gå vidare");
Console.Write("Vad vill du göra? ");
string val = Console.ReadLine();

if (val == "1")
{
    Console.WriteLine("Du tittar runt i rummet och ser mörka hörn.");
}
else if (val == "2")
{
    rum = "rum 1"; // Flytta till nästa rum
    Console.WriteLine("Du går vidare till nästa rum...");
}
```

### Uppgift 1

* Lägg till fler alternativ som:
  * "3. Vila"
  * "4. Leta efter gömda föremål"
* Skriv ett meddelande som hälsar spelaren välkommen och uppmuntrar dem att utforska dungeonen.

## Steg 2: Skapa flera rum och navigera mellan dem

Låt spelaren röra sig mellan olika rum och skapa ett enkelt system för att hålla reda på vilket rum de är i. Vi kan använda en `rum`-variabel för att hålla koll på var spelaren befinner sig och ändra dess värde när spelaren flyttar sig.

```csharp
// Programvariabler
string rum = "hallen";
List<string> inventarie = []; // Lista för att lagra föremål

// Spelloopen
while (true)
{
    Console.WriteLine($"\nDu är i {rum}.");

    // Visa alternativ baserat på vilket rum spelaren är i
    if (rum == "hallen")
    {
        Console.WriteLine("1. Titta runt");
        Console.WriteLine("2. Gå till rum 1");
        Console.Write("Vad vill du göra? ");
        string val = Console.ReadLine();

        if (val == "1")
        {
            Console.WriteLine("Du tittar runt i hallen och ser några gamla tavlor.");
        }
        else if (val == "2")
        {
            rum = "rum 1";
            Console.WriteLine("Du går vidare till rum 1...");
        }
    }
    else if (rum == "rum 1")
    {
        Console.WriteLine("1. Titta runt");
        Console.WriteLine("2. Gå till hallen");
        Console.WriteLine("3. Gå till rum 2");
        Console.Write("Vad vill du göra? ");
        string val = Console.ReadLine();

        if (val == "1")
        {
            Console.WriteLine("Du ser en dammig bokhylla i rummet.");
        }
        else if (val == "2")
        {
            rum = "hallen";
            Console.WriteLine("Du går tillbaka till hallen...");
        }
        else if (val == "3")
        {
            rum = "rum 2";
            Console.WriteLine("Du går vidare till rum 2...");
        }
    }
    else if (rum == "rum 2")
    {
        Console.WriteLine("1. Titta runt");
        Console.WriteLine("2. Gå till rum 1");
        Console.Write("Vad vill du göra? ");
        string val = Console.ReadLine();

        if (val == "1")
        {
            Console.WriteLine("Du hittar en gammal kista i hörnet.");
        }
        else if (val == "2")
        {
            rum = "rum 1";
            Console.WriteLine("Du går tillbaka till rum 1...");
        }
    }
}
```

### Uppgift 2

* Lägg till fler rum och låt spelaren navigera mellan dem.
* Ge varje rum unika egenskaper, t.ex. att vissa rum innehåller ledtrådar eller faror.

## Steg 3: Använd listor för att lagra föremål

Låt spelaren hitta föremål i rummen och lagra dem i en lista. Lägg till föremål i vissa rum, och ge spelaren möjlighet att plocka upp dem.

```csharp
List<string> inventarie = [];

if (rum == "rum 2")
{
    Console.WriteLine("Du hittar en magisk dryck! Vill du plocka upp den? (j/n)");
    string svar = Console.ReadLine().ToLower();

    if (svar == "j")
    {
        inventarie.Add("Magisk dryck");
        Console.WriteLine("Du har plockat upp en magisk dryck.");
    }

    Console.WriteLine($"Din inventarie: {string.Join(", ", inventarie)}");
}
```

### Uppgift 3

* Lägg till fler olika föremål i olika rum som spelaren kan hitta och plocka upp.
* Visa inventarielistan varje gång spelaren hittar ett nytt föremål.

## Steg 4: Slumpa händelser i rummen

Nu gör vi spelet mer spännande genom att slumpa händelser när spelaren utforskar rummen. Varje gång spelaren går in i ett nytt rum, kan en slumpad händelse ske.

```csharp
// Slumpa en händelse när spelaren går till ett nytt rum
int händelse = Random.Shared.Next(1, 5);

if (händelse == 1)
{
    Console.WriteLine("Du hittar ett svärd på marken!");
    if (!inventarie.Contains("Svärd"))
    {
        inventarie.Add("Svärd");
        Console.WriteLine("Du plockar upp svärdet.");
    }
}
else if (händelse == 2)
{
    Console.WriteLine("En fiende dyker upp!");
    // Låt spelaren slåss eller fly
}
else if (händelse == 3)
{
    Console.WriteLine("En fälla aktiveras och du förlorar ett liv.");
    // Minska spelarens liv
}
else
{
    Console.WriteLine("Rummet är tomt.");
}
```

### Uppgift 4

* Lägg till fler händelser i olika rum, som att hitta skatter eller stöta på fiender.
* Lägg till variation i meddelandena så att varje händelse känns unik.

## Steg 5: Ge spelaren livspoäng

Nu lägger vi till en utmaning genom att ge spelaren livspoäng. Spelaren förlorar liv när de stöter på faror som fällor eller fiender.

```csharp
int liv = 3;  // Spelaren börjar med 3 liv

if (händelse == 3) // Exempel på fälla
{
    liv--;
    Console.WriteLine($"Du förlorade ett liv! Liv kvar: {liv}");
}
```

### Uppgift 5

* Lägg till ett slumpat element där vissa rum innehåller fällor.
* Avsluta spelet om spelaren förlorar alla sina liv.

## Steg 6: Slåss mot fiender

Låt spelaren möta fiender i rummen och välja om de vill slåss eller fly. Använd listan `inventarie` för att kontrollera om spelaren har ett vapen.

```csharp
// Programvariabler
List<string> inventarie = [];
int liv = 3;

...

Console.WriteLine("En fiende dyker upp!");
Console.Write("Vill du slåss? (j/n) ");
string strid = Console.ReadLine().ToLower();

if (strid == "j" && inventarie.Contains("Svärd"))
{
    Console.WriteLine("Du besegrar fienden med ditt svärd!");
}
else if (strid == "j" && !inventarie.Contains("Svärd"))
{
    Console.WriteLine("Du försöker slåss utan vapen och förlorar ett liv.");
    liv--;
}
else
{
    Console.WriteLine("Du flyr från fienden.");
}

Console.WriteLine($"Liv kvar: {liv}");
```

### Uppgift 6

* Låt spelaren möta olika typer av fiender (t.ex. skelett, troll, eller drakar) och anpassa svårigheten efter fiendetyp.
* Lägg till fler vapen och gör vissa fiender svårare att besegra utan rätt vapen.

## Steg 7: Skapa en utgång och vinnarscenario

Låt spelaren hitta en utgång i slumpmässiga rum. Om de hittar utgången, vinner de spelet.

```csharp
bool spelIGång = true;

if (händelse == 5) // Om händelsen är att hitta utgången
{
    Console.WriteLine("Du hittar en utgång! Grattis, du har klarat spelet!");
    spelIGång = false; // Avsluta spelet
}
```

### Uppgift 7

* Gör utgången svår att hitta genom att endast lägga den i ett visst rum eller med ett särskilt krav.
* Skapa fler möjliga vinstscenarion, som att besegra en boss eller hitta en skatt.

## Steg 8: Lägg in en spelloop

Gör spelet spelbart i flera rum med en spelloop som låter spelaren utforska tills de vinner eller förlorar.

```csharp
// Spelloopen
while (spelIGång && liv > 0)
{
    Console.WriteLine($"\nDu är i {rum}.");
    Console.WriteLine("1. Titta runt");
    Console.WriteLine("2. Gå vidare");

    string val = Console.ReadLine();

    if (val == "2")
    {
        // Uppdatera rum, slumpa händelser etc.
    }

    Console.WriteLine($"Liv kvar: {liv}");
    Console.WriteLine($"Inventarie: {string.Join(", ", inventarie)}");
}
```

### Uppgift 8

* Lägg till ett poängsystem och visa poängen i slutet.
* Låt spelaren spela om spelet när de vinner eller förlorar.

## Skriva ut en karta

Här är en enkel representation av kartan som visar hur du kan använda tecken för att markera spelarens position, utforskade rum, oupptäckta rum och utgången.

| S | . | ? | ? | ? |
|---|---|---|---|---|
| . | . | ? | ? | ? |
| ? | ? | ? | ? | ? |
| ? | ? | ? | ? | ? |
| ? | ? | ? | ? | U |

### Symbolförklaringar

- `S`: Spelarens nuvarande position
- `.`: Utforskat rum (tidigare rummet spelaren har varit i)
- `?`: Oupptäckt rum
- `U`: Utgång

## Dela upp i metoder

För att göra koden mer lättläst och underhållbar kan du dela upp den i mindre metoder som hanterar specifika delar av spelet, t.ex. `UtforskaRum()`..

```csharp
Console.Clear();
Console.WriteLine("Ett Dungeon-spel");

// Programvariabler
string rum = "hallen";
List<string> inventarie = new List<string>();
int liv = 3;

// Spelloopen
while (liv > 0)
{
    if (rum == "hallen")
    {
        UtforskaHallen();
    }
    else if (rum == "rum 1")
    {
        UtforskaRum1();
    }
    else if (rum == "rum 2")
    {
        UtforskaRum2();
    }
}

void UtforskaHallen()
{
    Console.WriteLine("Du är i hallen.");
    Console.WriteLine("1. Titta runt");
    Console.WriteLine("2. Gå till rum 1");
    string val = Console.ReadLine();

    if (val == "1")
    {
        Console.WriteLine("Du tittar runt i hallen och ser några gamla tavlor.");
    }
    else if (val == "2")
    {
        rum = "rum 1";
        Console.WriteLine("Du går vidare till rum 1...");
    }
}
...
```

## Utmaningar och fortsättningar

1. **Skapa en karta**: Låt spelaren välja riktningar och skapa en karta över rum de har besökt.
2. **Highscore**: Implementera poäng och låt spelaren spara sitt bästa resultat.
3. **Flerapelarläge**: Låt två spelare turas om att utforska olika rum och tävla om högsta poäng.
