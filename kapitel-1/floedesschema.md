# Planera före kodning

## Pseudokod

Innan man börjar koda kan man skriva sk pseudokod. Pseudokod skrivs med vanliga meningar gärna i punktform. 

Fördelarna med pseudokod är:

1. **Språköverskridande**: Pseudokod är inte bundet till något specifikt programmeringsspråk, vilket gör det universellt användbart för att skissa på algoritmer.
  
2. **Mänskligt läsbart**: Pseudokod är utformat för att vara enkelt att förstå för människor, snarare än maskiner, vilket gör det till ett utmärkt verktyg för att planera och diskutera kod.

3. **Ingen strikt syntax**: Till skillnad från programmeringskoder behöver pseudokod inte följa några stränga syntaktiska regler, vilket gör det mer flexibelt och anpassningsbart.

4. **Representerar logik och flöde**: Med pseudokod kan man lätt illustrera de grundläggande logiska strukturerna i en algoritm, som if-satser, loopar och funktioner.

5. **Varierande detaljnivå**: Pseudokod kan skrivas med olika nivåer av detaljrikedom, allt från mycket övergripande skisser till nästan fullständig kod.

6. **Planering och dokumentation**: Pseudokod används ofta i planeringsfasen av ett projekt för att utforma algoritmer, och kan även fungera som en del av dokumentationen för att förklara hur en viss koddel fungerar.

7. **Pedagogiskt verktyg**: Pseudokod är utmärkt för utbildning och inlärning, eftersom det hjälper studenter att fokusera på algoritmiskt tänkande snarare än att fastna i syntaktiska detaljer.

### Exempel

```
* Slumpa ett ord
* Be användaren gissa ett ord
* Om gissningen är rätt
    * Skriv ut "Rätt! Du vann!"
* Annars
    * Skriv ut "Fel! Försök igen!"
```

## Flödesschema

Ett annat sätt att planera sin kod är att använda flödesschema.

Ett flödesschema är en grafisk representation av ett program. Det används för att planera och förstå hur ett program fungerar. Flödesschemat består av olika symboler som representerar olika delar av programmet.

Exempel på ett flödesschema för ett program som läser in ett ord och kontrollerar om det är "Hej".

![](../.gitbook/assets/image-62.png)

```csharp
Console.Write("Skriv ett ord: ");
string ord = Console.ReadLine();

if (ord == "Hej")
{
    Console.WriteLine("Rätt! Du vann!");
}
else
{
    Console.WriteLine("Fel! Försök igen!");
}
```

### Ändpunkter

Början och slutet på ett flödesschema kallas för ändpunkter. I exemplet ovan är det två ändpunkter: `Start` och `Slut`.

![](../.gitbook/assets/image-63.png)

### Sekvens

En sekvens är en serie av steg som utförs i ordning. I exemplet ovan är det en sekvens som består av stegen `Läs in ord`, `Kontrollera om ordet är Hej` och `Skriv ut resultat`.

![](../.gitbook/assets/image-64.png)

### Input & output

Input och output är de olika sätten som ett program kan kommunicera med omvärlden. I exemplet är `Mata in ett ord` input och `Din gissning är fel` output.

![](../.gitbook/assets/image-65.png)

### Villkor

I exemplet nedan är kontrollen `Helg?` ett villkor. Villkoret är sant om det är helg och falskt om det inte är helg. Är svaret `Ja` så går flödet till `Sov vidare` och är svaret `Nej` så går flödet till `Kliv upp`.

![](../.gitbook/assets/image-66.png)

### Loop

Det finns inga symboler för loopar i flödesschemat. Istället används en sekvens med en pil som pekar tillbaka till början av sekvensen. I exemplet nedan är det en loop som består av vilkoret  `Klockan < 06:30` som pekar tillbaka om svaret är `Ja`.

![](../.gitbook/assets/image-67.png)

## Uppgifter

### Uppgift 1: Summera en Lista
Målet är att skriva pseudokod för att summera alla tal i en given lista.

### Uppgift 2: Hitta Maximalt Värde
Skriv pseudokod för att hitta det största talet i en lista av heltal.

### Uppgift 3: Palindromkontroll
Målet är att skriva pseudokod för att kontrollera om ett ord är ett palindrom eller inte.

### Uppgift 4: Användarautentisering
Skriv pseudokod som simulerar en enkel inloggningsprocess. Kontrollera om användarnamn och lösenord matchar fördefinierade värden.

### Uppgift 5: FizzBuzz
Skriv pseudokod för det klassiska FizzBuzz-problemet. Om ett nummer är delbart med 3, skriv ut "Fizz". Om det är delbart med 5, skriv ut "Buzz". Om det är delbart med båda, skriv ut "FizzBuzz".

### Uppgift 6: "Sten, Sax, Påse"
Målet är att skriva pseudokod för ett "Sten, Sax, Påse"-spel där spelaren spelar mot datorn.

### Uppgift 7: "Fyra i rad"
Skriv pseudokod för ett enkelt "Fyra i rad"-spel. Fokusera på hur man kontrollerar om någon har vunnit och hur man hanterar turordningen mellan två spelare.

### Uppgift 8: "Skattjakt"
Skapa pseudokod för ett enkelt spel där spelaren måste hitta en skatt på en tvådimensionell karta. Användaren kan röra sig upp, ner, till vänster och till höger.

