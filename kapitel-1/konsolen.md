---
description: Introduktion till konsolinteraktioner i C#
---

# Grundläggande konsolinteraktioner

I ett C#-program använder vi `Console.WriteLine()` och `Console.ReadLine()` för att skriva ut och läsa in data från konsolen.

## Använda Console.WriteLine()

`Console.WriteLine()` används för att skriva ut ett meddelande till konsolen.

### Exempel:

```csharp
Console.WriteLine("Hej världen!");
```

När ovanstående kod körs, skrivs "Hej värld!" ut i konsolen:

```text
Hej världen!
```

## Använda Console.ReadLine()

`Console.ReadLine()` läser en linje av text från konsolen, oftast användarinmatning. Den returnerar det inlästa som en sträng.

### Exempel:

```csharp
Console.Write("Vad heter du? ");
string namn = Console.ReadLine();
Console.WriteLine("Hej " + namn + "!");  // Strängkonkatenering
```

Resultat:

```text
Vad heter du? Adam
Hej Adam!
```

När ovanstående kod körs, väntar programmet på användarinmatning. Efter inmatning, hälsar programmet användaren med det inskrivna namnet.

## Snygga till konsolen

För att göra konsolfönstret mer läsbart och användarvänligt kan du använda `Console.Clear()` för att rensa konsolen. Detta är användbart när du vill visa en ny meny eller skapa en ny interaktion med användaren.
Och för att kunna använda svenska tecken i konsolen kan du använda `Console.OutputEncoding = Encoding.Unicode;`.

Lägg till följande kod i början av ditt program:

```csharp
Console.Clear();
Console.InputEncoding = System.Text.Encoding.Unicode;
Console.OutputEncoding = System.Text.Encoding.Unicode;
```

## Strängkonkatenering och variabelsubstitution

Det finns flera sätt att sätta samma strängar i C#:

1. **Med + operatorn**: 
```csharp
string hälsning = "Hej " + namn + "!";
```

2. **Med `$` (interpolerade strängar)**: 
```csharp
string hälsning = $"Hej {namn}!";
```

Båda metoderna ovan kommer att ge samma resultat. Interpolerade strängar är dock mer läsbara och flexibla, särskilt när det finns många variabler som ska infogas i strängen.

## Exempelprogram

1. **Skapa en ny mapp** i VS Code och döp den till `KonsolInteraktion`.
2. **Öppna en terminal** inuti mappen.
3. Skriv `dotnet new console` för att skapa en ny konsolapp.
4. Öppna `Program.cs` och skriv följande kod:

```csharp
Console.WriteLine("Välkommen till konsolinteraktion i C#!");

Console.Write("Ange ditt namn: ");
string namn = Console.ReadLine();

// Strängkonkatenering
Console.WriteLine("Hej " + namn + "!");

// Variabelsubstitution med interpolerade strängar
Console.WriteLine($"Hej {namn} igen! Hoppas du har en bra dag.");
```

5. Kör programmet genom att skriva `dotnet run` i terminalen.

Resultat:

```text
Välkommen till konsolinteraktion i C#!
Ange ditt namn: Adam
Hej Adam!
Hej Adam igen! Hoppas du har en bra dag.
```

## Färger i konsolen

Du kan ändra färgen på texten i konsolen genom att använda `Console.ForegroundColor` och `Console.BackgroundColor`. Här är några exempel på hur du kan använda färger i konsolen:

```csharp
Console.ForegroundColor = ConsoleColor.Red;
Console.WriteLine("Detta är en röd text.");

Console.BackgroundColor = ConsoleColor.Yellow;
Console.ForegroundColor = ConsoleColor.Blue;

Console.WriteLine("Detta är en blå text på gul bakgrund.");
Console.ResetColor();  // Återställ färgerna till standard
``` 

### Exempelprogram: Enkel användarregistrering

1. **Skapa en ny mapp** i VS Code och döp den till `Registrering`.
2. **Öppna en terminal** inuti mappen.
3. Skriv `dotnet new console` för att skapa en ny konsolapp.
4. Öppna `Program.cs` och skriv följande kod:

```csharp
// Välkomstmeddelande
Console.Clear();

// Ser till att svenska tecken fungerar i konsolen
Console.InputEncoding = System.Text.Encoding.Unicode;
Console.OutputEncoding = System.Text.Encoding.Unicode;
Console.WriteLine("Välkommen till användarregistrering!");

// Ändra textfärg
Console.ForegroundColor = ConsoleColor.Green;

Console.Write("Ange ditt förnamn: ");
string förnamn = Console.ReadLine();

Console.Write("Ange ditt efternamn: ");
string efternamn = Console.ReadLine();

// Ändra textfärg
Console.ForegroundColor = ConsoleColor.Yellow;

// Skriv ut en sammanfattning av användarens inmatning
Console.WriteLine($"Hej, du heter {förnamn} {efternamn}");

Console.resetColor();
```

5. Kör programmet genom att skriva `dotnet run` i terminalen.

Resultat:
```text
Välkommen till användarregistrering!
Ange ditt förnamn: Adam
Ange ditt efternamn: Andersson
Hej, du heter Adam Andersson.
```

### Exempelprogram: byta färg på texten

1. **Skapa en ny mapp** i VS Code och döp den till `TextFarg`.
2. **Öppna en terminal** inuti mappen.
3. Skriv `dotnet new console` för att skapa en ny konsolapp.
4. Öppna `Program.cs` och skriv följande kod:

```csharp
// Välkomstmeddelande
Console.Clear();
Console.InputEncoding = System.Text.Encoding.Unicode;
Console.OutputEncoding = System.Text.Encoding.Unicode;
Console.WriteLine("Välkommen till textfärgsprogrammet!");

Console.Write("Vilken färg vill du ha på texten? (röd, grön, blå): ");
string färg = Console.ReadLine();

// Ändra textfärg baserat på användarens inmatning
if (färg == "röd")
{
    Console.ForegroundColor = ConsoleColor.Red;
}

Console.WriteLine("Detta är en text i vald färg.");
Console.ResetColor();
```

5. Kör programmet genom att skriva `dotnet run` i terminalen.

Resultat:
```text
Välkommen till textfärgsprogrammet!
Vilken färg vill du ha på texten? (röd, grön, blå): röd
Detta är en text i vald färg.
```

## Uppgifter

### Uppgift 1

* Skapa ett konsolprojekt **FavoritFarg**.
* Skriv ett program som frågar användaren efter deras favoritfärg och sedan skriver ut en hälsning med den färgen.

**Exempel på körning:**

```text
Ange din favoritfärg: blå
Hej! Din favoritfärg är blå.
```

### Uppgift 2

* Skapa ett konsolprojekt **GissaOrdet**.
* Fråga användaren efter ett ord.
* Kolla om ordet är ett specifikt ord, t.ex. "Taylor".
* Om ordet är "Taylor", skriv ut "Rätt svar!" i grön text.
* Annars, skriv ut "Fel svar!" i röd text.

**Exempel på körning:**

```text
Ange ett ord: Taylor
Rätt svar!

Ange ett ord: Swift
Fel svar!
```

### Uppgift 3

* Skapa ett konsolprojekt **Lucktext**.
* Skapa en digital lucktext. Hämta gärna inspiration från nätet, t.ex:
  * http://www.redkid.net/madlibs/
  * http://www.rinkworks.com/crazytales/
* Använd `Console.WriteLine()`, `Console.Write()` och `Console.ReadLine()`
* Be användaren ange **minst 4 ord** som behövs för lucktexten.
* Låt användaren fylla i orden först – skriv ut den ifyllda lucktexten när alla orden är valda.
* Låt minst en klasskamrat testa din lucktext & titta på koden.

**Extra utmaningar**: 

* Använd färger för att göra lucktexten mer intressant.\
  Tips: `Console.ForegroundColor = ConsoleColor.Red;`
* Låt användaren skriva in fler ord än vad som behövs och välj sedan vilka ord som ska användas i lucktexten.
* Kolla att användaren inte skriver in tomt ord.
* Kolla att användaren inte skriver in samma ord flera gånger.

**Exempel på körning:**

```text
Välkommen till vår lucktext!
Ange ditt namn: Adam
Ange ett adjektiv: snabb
Ange en kroppsdel: hand
Ange en plats: skola

En gång fanns det en snabb Adam som hade en hand som var så stor att han kunde bära hela skolan.
Tack för att du deltog i vår lucktext!

Ange ditt namn: Alice
Ange ett adjektiv: glad
Ange en kroppsdel: fot
Ange en plats: park

En gång fanns det en glad Alice som hade en fot som var så stor att han kunde bära hela parken.
Tack för att du deltog i vår lucktext!
```