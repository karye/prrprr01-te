---
description: Hur man skriver C#-kod
---

# Grundläggande syntax i programmering: tilldelning och kodblock

I denna sektion ska vi utforska några fundamentala koncept i programmeringssyntax - nämligen kodblock och tilldelning. Att behärska dessa koncept är avgörande när vi börjar skriva våra egna program. Låt oss dyka rakt in!

## Tilldelning

Tilldelning är processen att ge ett värde till en variabel. I C# görs detta med `=` operatorn.

Här är ett exempel på en tilldelning:

```csharp
// Tilldela variabeln x värdet 5
int x = 5;
```
I detta fall skapas en variabel med namnet `x` av typen `int` (heltal), och tilldelas värdet `5`.

Här är några fler exempel på tilldelningar:

```csharp
// Tilldela variabeln minDecimal ett decimaltal
decimal minDecimal = 4.5m;

// Tilldela en sträng-variabel värde "Hello, world!"
string minString = "Hello, world!";

// Tilldela en bool-variabel värdet true
bool minBool = true;

// Tilldela en array-variabel ett antal värden
int[] minArray = [1, 2, 3, 4, 5];
```

I dessa exempel tilldelas varje variabel ett initialt värde. Dessa värden kan sedan ändras över tid när programmet körs.

Förståelse av dessa koncept är fundamentalt för att skriva och läsa C#-kod. Med detta som grund kommer vi i nästa lektion att dyka djupare in i mer avancerade koncept som loopar, villkor och funktioner. Lycka till på din programmeringsresa!

## Kodblock

Ett kodblock i C# och många andra programmeringsspråk definieras av uppsättningar av måsvingar `{}`. All kod som skrivs inom dessa måsvingar tillhör samma block.

Ett kodblock kan vara en del av en metod, en loop, ett villkorligt uttryck, en klass, etc. Det används för att gruppera flera kodrader till en enhet som utförs tillsammans.

Ett kodblock skapas genom att skriva `{` och `}` på varsin rad, med all kod som ska köras inuti dessa måsvingar. Här är ett exempel på ett kodblock:

```csharp
{
    // Kodblock
}
```

Och här är några fler exempel på kodblock i olika kontexter:

```csharp
if (true)
{
    // Detta är ett kodblock för ett if-villkor
}

for (int i = 0; i < 10; i++)
{
    // Detta är ett kodblock för en for-loop
}
```
I båda exemplen ovan, all kod inuti `{}` kommer att köras om det föregående villkoret är sant.

