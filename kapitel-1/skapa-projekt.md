---
description: Börja koda
---

# Hur man kodar i VS Code

I den här guiden kommer vi att gå igenom hur man skapar ett grundläggande programmeringsprojekt i VS Code, ett populärt verktyg för kodredigering. Vi kommer att skapa mappstrukturer, initialisera grundkod, och lära oss hur man kör program i terminalen.

## Skapa mappstruktur

När du börjar ett nytt projekt är det viktigt att hålla ditt arbete organiserat. En bra början är att skapa en mappstruktur som kommer att hålla ditt arbete snyggt och strukturerat.

* Skapa mappar för varje avsnitt:

![](../.gitbook/assets/image-57.png)

* I varje avsnitt, skapa en mapp för projektet:

![](../.gitbook/assets/image-58.png)

* Öppna mappen för projektet i VS Code:

![](../.gitbook/assets/image-59.png)

## Skapa grundkoden

### Skapa en mapp och navigera till mappen i terminalen

Första steget för att skapa grundkoden är att skapa en ny mapp. Vi kommer att kalla den `HelloWorld`. När mappen är skapad, högerklicka på den och välj alternativet **"Open in Integrated Terminal"**.

![](<../.gitbook/assets/image-61 (1).png>)

Detta öppnar en terminal som redan är navigerad till din nya mapp.

### Skapa grundkoden

Nu när du är i din nya mapp, är det dags att skapa grundkoden för ditt projekt. I terminalen, skriv följande kommando:

```powershell
dotnet new console
```

Detta kommando kommer att skapa en grundläggande struktur för ett konsolprogram i din mapp.

I Dotnet 8 ser det ut så här:

![alt text](../.gitbook/assets/image-113.png)

### Kompilera och kör programmet

Efter att du har skrivit din kod, är nästa steg att kompilera och köra programmet. Detta gör du genom att skriva följande kommando i terminalen:

```powershell
dotnet run
```

Detta kommando kommer att kompilera din kod (översätta den till maskinkod) och sedan köra det resulterande programmet.

![alt text](../.gitbook/assets/image-114.png)

Grattis! Du har nu skapat, kompilerat och kört ditt första program i VS Code! Fortsätt experimentera, lära och bygga nya saker. Lycka till!
