Jag förstår. Nedan följer en mer detaljerad förklaring av varje del i koden och hur vi lägger in metoder i Main(). 

## Steg 1: Initialisering

Först behöver vi initialisera fönstret med hjälp av Raylib. Vi skapar en ny fil med namnet "Program.cs" och lägger till följande kod:

```csharp
using Raylib_cs;

class Program
{
    static void Main(string[] args)
    {
        // Definiera fönstrets storlek
        const int fönsterBredd = 800;
        const int fönsterHöjd = 600;

        // Initiera fönstret
        Raylib.InitWindow(fönsterBredd, fönsterHöjd, "Pong Spelet");
    }
}
```

Vi börjar med att importera Raylib-biblioteket med `using Raylib_cs;`. Vi definierar sedan en Main-metod, vilket är den första metoden som körs när programmet startar. I Main-metoden definierar vi storleken på vårt fönster och initialiserar fönstret med den storleken och ett titelnamn "Pong Spelet".

## Steg 2: Skapa spelplanen

För att skapa spelplanen behöver vi rita en linje i mitten av skärmen. Vi skapar en ny metod utanför Main som heter `RitaSpelplan`. Denna metod tar in bredden och höjden på fönstret och ritar en vit linje rakt ner i mitten av fönstret.

```csharp
private static void RitaSpelplan(int fönsterBredd, int fönsterHöjd)
{
    // Rita mittpunkten på spelplanen
    Raylib.DrawLine(fönsterBredd / 2, 0, fönsterBredd / 2, fönsterHöjd, Color.WHITE);
}
```

## Steg 3: Skapa paddlarna

Nu behöver vi skapa två "paddlar" - rektanglar som spelarna kommer att använda för att slå tillbaka bollen. Dessa rektanglar kommer att styras av spelarna med hjälp av tangentbordet. Vi skapar två metoder, en för att skapa en paddel och en för att rita ut den:

```csharp
Rectangle paddel = new Rectangle()
{
    x = x,
    y = y,
    width = bredd,
    height = höjd
};

private static void RitaPaddel(Rectangle paddel)
{
    Raylib.DrawRectangleRec(paddel, Color.WHITE);
}
```

I Main-metoden, innan vi går in i spelets huvudslinga (även känd som "spelloopen"), skapar vi två paddlar, en för varje spelare:

```csharp
// Skapa spelarens paddel
Rectangle spelarePaddel = SkapaPaddel(60, fönsterHöjd / 2, 20, 80);

// Skapa AI paddeln
Rectangle aiPaddel = SkapaPaddel(fönsterBredd - 80, fönsterH öjd / 2, 20, 80);
```

I `Main` metoden ser det nu ut så här:

```csharp
using Raylib_cs;

class Program
{
    static void Main(string[] args)
    {
        // Definiera fönstrets storlek
        const int fönsterBredd = 800;
        const int fönsterHöjd = 600;

        // Initiera fönstret
        Raylib.InitWindow(fönsterBredd, fönsterHöjd, "Pong Spelet");
        
        // Skapa spelarens paddel
        Rectangle spelarePaddel = new Rectangle(60, fönsterHöjd / 2, 20, 80);

        // Skapa AI paddeln
        Rectangle aiPaddel = new Rectangle(fönsterBredd - 80, fönsterHöjd / 2, 20, 80);

        // Huvudloopen för spelet
        while (!Raylib.WindowShouldClose())
        {
            // Uppdatering av spelet här...
            // Vi kommer att fylla i detta i kommande steg.

            // Börja att rita
            Raylib.BeginDrawing();
            
            // Rensa skärmen till mörkblått
            Raylib.ClearBackground(Color.DARKBLUE);

            // Rita spelplanen
            RitaSpelplan(fönsterBredd, fönsterHöjd);
            
            // Rita ut paddlarna
            RitaPaddel(spelarePaddel);
            RitaPaddel(aiPaddel);

            // Sluta att rita
            Raylib.EndDrawing();
        }

        // Stäng av fönstret när spelet är över
        Raylib.CloseWindow();
    }
    
    private static void RitaSpelplan(int fönsterBredd, int fönsterHöjd)
    {
        // Rita mittpunkten på spelplanen
        Raylib.DrawLine(fönsterBredd / 2, 0, fönsterBredd / 2, fönsterHöjd, Color.WHITE);
    }
    
    private static void RitaPaddel(Rectangle paddel)
    {
        Raylib.DrawRectangleRec(paddel, Color.WHITE);
    }
}
```

Nu skapar vi våra paddlar direkt i `Main` metoden och sparar dem i variablerna `spelarePaddel` och `aiPaddel`. Dessa paddlar skapas som rektanglar med positionen, bredden och höjden som vi definierar. Vi ritar sedan ut dessa paddlar i vår spelloop med metoden `RitaPaddel`.

Denna struktur är lite enklare och mer direkt. Skapandet av paddlarna sker nu i `Main` metoden istället för i en separat metod.

## Steg 4: Flytta paddlarna

Vi skapar en metod för att uppdatera paddlarnas position. Denna metod tar in en paddel, en hastighet, två tangenter och fönstrets höjd. Metoden returnerar en paddel.

```csharp
// Metod för att uppdatera paddlarnas position
static Rectangle UppdateraPaddel(Rectangle paddel, float hastighet, KeyboardKey upp, KeyboardKey ner, float fönsterHöjd)
{
    if (Raylib.IsKeyDown(upp))
    {
        paddel.y -= hastighet;
    }
    if (Raylib.IsKeyDown(ner))
    {
        paddel.y += hastighet;
    }

    // Hålla paddeln inom skärmen
    if (paddel.y < 0)
    {
        paddel.y = 0;
    }
    if (paddel.y > fönsterHöjd - paddel.height)
    {
        paddel.y = fönsterHöjd - paddel.height;
    }

    return paddel;
}
```

Denna metod tar en paddel (som är en `Rectangle`), en hastighet (som är ett `float`-värde), två tangenter för upp och ner (som är `KeyboardKey`-värden), samt fönstrets höjd (som är ett `float`-värde). Metoden uppdaterar paddelns y-position baserat på om upp- eller ner-tangenten är nedtryckt, och håller sedan paddeln inom skärmen. Till sist returnerar den den uppdaterade paddeln.

För att använda denna metod för att styra spelarens och AI-paddelns position, skulle vi ersätta den tidigare koden för att uppdatera paddlarnas position med följande kod i vår huvudloop:

```csharp
// Uppdatera paddlarnas position
spelarePaddel = UppdateraPaddel(spelarePaddel, paddelHastighet, KeyboardKey.KEY_UP, KeyboardKey.KEY_DOWN, fönsterHöjd);
aiPaddel = UppdateraPaddel(aiPaddel, paddelHastighet, KeyboardKey.KEY_W, KeyboardKey.KEY_S, fönsterHöjd);
```

Observera att vi här antar att AI-paddeln styrs av tangenter W (för upp) och S (för ner). Du kan ändra detta beroende på hur du vill att AI-paddeln ska styras.

## Steg 5: Skapa bollen

Nästa steg är att skapa bollen. Vi kommer att skapa bollen som en cirkel med en viss position, radie och hastighet. Vi kommer att lägga till följande kod före huvudloopen:

```csharp
// Skapa bollen
Circle boll = new Circle();
boll.radius = 8;
boll.center = new Vector2(fönsterBredd / 2, fönsterHöjd / 2);
Vector2 bollHastighet = new Vector2(-5.0f, -5.0f);
```

I ovanstående kodblock skapar vi en ny `Circle` och sparar den i variabeln `boll`. Vi ger bollen en radie av `8` och placerar den i mitten av fönstret. Vi skapar också en `Vector2` som representerar bollens hastighet i x- och y-riktningarna. Initialt ger vi bollen en hastighet av `-5.0` i både x- och y-riktningarna, vilket betyder att den kommer att börja röra sig uppåt och till vänster.

Nu lägger vi till logik för att uppdatera bollens position och få den att studsa när den träffar kanten av skärmen eller någon av paddlarna. Vi lägger till följande kod i speluppdateringsavsnittet i huvudloopen:

```csharp
// Uppdatera bollens position
static Rectangle UppdateraBollPosition(Rectangle boll, Vector2 bollHastighet)
{
    boll.x += bollHastighet.X;
    boll.y += bollHastighet.Y;

    return boll;
}

// Om bollen träffar någon av paddlarna, ändra dess riktning
static Vector2 KontrolleraBollKollision(Rectangle boll, Rectangle spelarePaddel, Rectangle aiPaddel, Vector2 bollHastighet, float fönsterHöjd, float fönsterBredd)
{
    // Om bollen kolliderar med toppen eller botten av skärmen, invertera dess Y-hastighet
    if (boll.y < 0 || boll.y > fönsterHöjd - boll.height)
    {
        bollHastighet.Y *= -1;
    }

    // Om bollen kolliderar med spelarens eller AI:s paddel, invertera dess X-hastighet
    if (Raylib.CheckCollisionRecs(boll, spelarePaddel) || Raylib.CheckCollisionRecs(boll, aiPaddel))
    {
        bollHastighet.X *= -1;
    }

    // Om bollen går utanför skärmen till vänster eller höger, återställ den till mitten och invertera dess X-hastighet
    if (boll.x < 0 || boll.x > fönsterBredd)
    {
        boll.x = fönsterBredd / 2 - boll.width / 2;
        boll.y = fönsterHöjd / 2 - boll.height / 2;
        bollHastighet.X *= -1;
    }

    return bollHastighet;
}

// Rita ut bollen
Raylib.DrawCircleV(boll.center, boll.radius, Color.WHITE);
```

I ovanstående kodblock uppdaterar vi först bollens position genom att addera dess hastighet till dess nuvarande position. Vi kontrollerar sedan om bollen har träffat toppen eller botten av skärmen. Om den har det, multiplicerar vi dess y-hastighet med `-1` för att få den att ändra riktning.

Vi kontrollerar sedan om bollen har kolliderat med någon av paddlarna. Vi gör detta genom att skapa en rektangel som representerar bollen (eftersom `CheckCollisionRecs` metoden behöver rektanglar som input) och kontrollera om den har kolliderat med någon av paddlarna. Om det har skett en kollision, multiplicerar vi bollens x-hastighet med `-1` för att få den att ändra riktning.

Slutligen ritar vi ut bollen i den position den nu befinner sig i.

Det är allt vi behöver för att skapa ett grundläggande Pong-spel. Du kan nu köra koden och spela Pong! Du kan utöka detta exempel på många sätt, till exempel genom att lägga till poängräkning, förbättra AI-paddelns beteende, eller lägga till coola ljudeffekter och grafik. Lycka till med din spelutveckling!

Spelloopen ser nu ut så här:

```csharp
// Huvudloopen för spelet
while (!Raylib.WindowShouldClose())
{
    // Uppdatera spelarens och AI-paddlarnas position
    spelarePaddel = UppdateraPaddel(spelarePaddel, paddelHastighet, KeyboardKey.KEY_UP, KeyboardKey.KEY_DOWN, fönsterHöjd);
    aiPaddel = UppdateraPaddel(aiPaddel, paddelHastighet, KeyboardKey.KEY_W, KeyboardKey.KEY_S, fönsterHöjd);

    // Uppdatera bollens position
    boll = UppdateraBollPosition(boll, bollHastighet);

    // Kontrollera om bollen har kolliderat med någon av paddlarna
    bollHastighet = KontrolleraBollKollision(boll, spelarePaddel, aiPaddel, bollHastighet, fönsterHöjd, fönsterBredd);

    // Rensa skärmen
    Raylib.BeginDrawing();
    Raylib.ClearBackground(Color.BLACK);

    // Rita ut spelplanen
    Raylib.DrawRectangle(0, 0, fönsterBredd, fönsterHöjd, Color.WHITE);

    // Rita ut paddlarna
    Raylib.DrawRectangleRec(spelarePaddel, Color.WHITE);
    Raylib.DrawRectangleRec(aiPaddel, Color.WHITE);

    // Rita ut bollen
    Raylib.DrawCircleV(boll.center, boll.radius, Color.WHITE);

    // Visa allt vi har ritat ut på skärmen
    Raylib.EndDrawing();
}
```

## Utmaningar

### Utmaning 1 - Lägg till poängräkning

* Lägg till poängräkning i spelet. Du kan göra detta genom att skapa två nya variabler, en för spelarens poäng och en för AI:ns poäng. Du kan sedan öka poängen för den spelare som lyckas få bollen att gå utanför skärmen till vänster eller höger. Du kan rita ut poängen på skärmen genom att använda `Raylib.DrawText` metoden.

### Utmaning 2 - Lägg till en meny

* Lägg till en meny i spelet. Du kan göra detta genom att skapa en ny variabel som håller reda på vilket läge spelet befinner sig i. Du kan sedan rita ut olika saker på skärmen beroende på vilket läge spelet befinner sig i. Du kan till exempel rita ut en meny om spelet befinner sig i menyläget, och rita ut spelet om det befinner sig i spelläget.