# Skapa en cell

Till spelet "Game of Life" behöver vi en funktion som skapar en cell. Funktionen ska ta emot en tvådimensionell array som representerar spelets tillstånd, och en position i rutnätet. Funktionen ska sätta cellen på den angivna positionen till levande.

## Steg för steg

### Steg 1: Skapa en metod

Skapa en metod som heter `SkapaCell` som tar emot en tvådimensionell array av heltal, och två heltal som representerar en position i rutnätet. Metoden ska inte returnera något.

### Steg 2: Sätt cellen till levande

I metoden ska du sätta cellen på den angivna positionen till levande. Du kan göra det genom att sätta värdet på den angivna positionen i arrayen till 1.

### Steg 3: Anropa metoden

Anropa metoden från `Main`-metoden. Du kan använda följande kod för att skapa en cell på positionen (5, 5):

```csharp
