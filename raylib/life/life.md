# Game of Life

## Spelloopen 

### Steg 1: Skapa ett nytt C# projekt

Börja med att skapa ett nytt konsolprojekt i C#.
I detta projekt använder vi C#-bindningarna till Raylib, kända som Raylib-cs. 

### Steg 2: Importera Raylib i ditt program

För att kunna använda Raylib behöver du importera det i din `Program.cs`-fil. Du gör det genom att lägga till `using Raylib_cs;` överst i din fil.

```csharp
using Raylib_cs;
using System;
```

### Steg 4: Skapa grundstrukturen för spelet

Först behöver du skapa en grundläggande spelloop. Den ska fortsätta att köras tills du stänger fönstret. I den här loopen behöver du två huvudsaker: uppdatera spelets tillstånd och rita spelets tillstånd. 

Såhär kan det se ut:

```csharp
using Raylib_cs;
using System;

const int skarmBredd = 800;
const int skarmHojd = 450;

Raylib.InitWindow(skarmBredd, skarmHojd, "Game of Life");

// Skapa ett initialt tillstånd för spelet
int[,] rutnat = new int[skarmBredd, skarmHojd];
// Initialisera rutnätet här

while (!Raylib.WindowShouldClose())
{
    // Uppdatera spelets tillstånd
    rutnat = UppdateraSpel(rutnat);

    // Rita spelets tillstånd
    Raylib.BeginDrawing();
    Raylib.ClearBackground(Color.BLACK);

    for (int x = 0; x < skarmBredd; x++)
    {
        for (int y = 0; y < skarmHojd; y++)
        {
            Color farg = rutnat[x, y] == 0 ? Color.BLACK : Color.WHITE;
            Raylib.DrawPixel(x, y, farg);
        }
    }

    Raylib.EndDrawing();
}

Raylib.CloseWindow();

static int[,] UppdateraSpel(int[,] rutnat)
{
    // Denna metod ska implementera spelreglerna och returnera det nya spelets tillstånd
    // Din kod går här
    return rutnat;
}
```

Här är variabelnamnen och kommentarerna översatta till svenska:

- `skarmBredd` och `skarmHojd` används för att ange fönstrets storlek i pixlar.
- `Raylib.InitWindow(skarmBredd, skarmHojd, "Game of Life")` öppnar ett nytt fönster med angiven bredd och höjd.
- `rutnat` är en tvådimensionell array som representerar spelets tillstånd, där varje cell kan vara antingen levande (representerad av 1) eller död (representerad av 0).
- `UppdateraSpel(rutnat)` är en metod som uppdaterar spelets tillstånd baserat på reglerna i Game of Life.
- `Raylib.BeginDrawing()` och `Raylib.EndDrawing()` används för att börja och sluta ritningen av det aktuella tillståndet i spelet.
- `Raylib.DrawPixel(x, y, farg)` ritas en pixel vid positionen (x, y) med angiven färg.
- `Raylib.CloseWindow()` stänger fönstret när spelet är över.

### Steg 5: Implementera uppdateringsmetoden

Nu behöver du fylla i `UpdateSpel`-metoden. Den här metoden bör ta det nuvarande tillståndet av spelet som input och returnera det nya tillståndet av spelet baserat på reglerna i Game of Life.

I denna metod kommer du att behöva iterera över varje cell i rutnätet, räkna antalet levande grannar och bestämma om cellen ska vara levande eller död i nästa generation baserat på reglerna:

1. En levande cell med mindre än två levande grannar dör av ensamhet.
2. En levande cell med två eller tre levande grannar överlever.
3. En levande cell med mer än tre levande grannar dör av överpopulation.
4. En död cell med exakt tre levande grannar blir en levande cell.

### Steg 6: Initialisera spelets tillstånd

Slutligen behöver du sätta ett initialt tillstånd för spelet. Du kan göra det direkt i `Main`-metoden innan spelloopen börjar. Du kanske vill börja med en enkel konfiguration, till exempel ett litet antal levande celler i mitten av skärmen, för att se om spelet fungerar som förväntat. Senare kan du experimentera med mer komplexa initiala tillstånd.

Observera att du kan behöva ändra skärmens bredd och höjd samt storleken på ditt rutnät beroende på vilken upplösning du vill ha för ditt spel.

Det är allt! Du borde nu ha en grundläggande version av Game of Life som kör i Raylib och C#. Du kan lägga till fler metoder, till exempel möjligheten att pausa spelet eller att ändra tillståndet för celler med musklick.

Självklart kan jag göra det. Först ska vi implementera en metod som räknar antalet levande grannar till en cell. Sedan ska vi implementera `UppdateraSpel`-metoden, som använder den här metoden för att uppdatera varje cell baserat på spelets regler.

## Metoderna för livsregler

### Steg 1: Implementera en metod för att räkna antalet levande grannar

Först behöver vi en metod för att räkna antalet levande grannar till en cell. Varje cell i rutnätet har upp till 8 grannar: en ovan, en nedan, en till höger, en till vänster, och fyra diagonalgrannar.

Här är en metod som gör det:

```csharp
static int AntalLevandeGrannar(int[,] rutnat, int x, int y)
{
    int grannar = 0;

    for (int dx = -1; dx <= 1; dx++)
    {
        for (int dy = -1; dy <= 1; dy++)
        {
            int nx = x + dx;
            int ny = y + dy;

            // Kolla om positionen ligger inom rutnätet
            if (nx >= 0 && nx < rutnat.GetLength(0) && ny >= 0 && ny < rutnat.GetLength(1))
            {
                grannar += rutnat[nx, ny];
            }
        }
    }

    // Ta bort cellen själv från antalet grannar
    grannar -= rutnat[x, y];

    return grannar;
}
```
Låt oss bryta ner `AntalLevandeGrannar`-metoden i mindre steg.

`AntalLevandeGrannar` tar in två parametrar, `rutnat` och koordinaterna `x` och `y`. Målet med metoden är att räkna antalet levande grannar till den cell som är placerad på position (x, y) i `rutnat`.

```csharp
static int AntalLevandeGrannar(int[,] rutnat, int x, int y)
{
    int grannar = 0;
```
Här skapar vi en variabel `grannar` och sätter den till 0. Detta är den variabel som vi kommer att använda för att räkna antalet levande grannar.

```csharp
    for (int dx = -1; dx <= 1; dx++)
    {
        for (int dy = -1; dy <= 1; dy++)
        {
            int nx = x + dx;
            int ny = y + dy;
```

Detta är en dubbel loop som går igenom alla celler i omgivningen runt den givna cellen, inklusive cellen själv. `dx` och `dy` är offset från den givna cellens position, så (x + dx, y + dy) blir positionen för den cell vi tittar på i varje steg av loopen. Med andra ord, `dx` och `dy` går från -1 till 1, vilket innebär att vi kontrollerar cellen ovanför, cellen under, cellerna till vänster och höger samt diagonala celler.

```csharp
            if (nx >= 0 && nx < rutnat.GetLength(0) && ny >= 0 && ny < rutnat.GetLength(1))
            {
                grannar += rutnat[nx, ny];
            }
        }
    }
```
Detta är det villkor som ser till att vi inte hamnar utanför rutnätets gränser när vi tittar på grannarna. Om den cell vi tittar på är inom rutnätet lägger vi till dess värde (1 om den är levande, 0 om den är död) till vår `grannar`-variabel.

```csharp
    grannar -= rutnat[x, y];
```

Efter att ha räknat alla celler, inklusive cellen själv, drar vi bort värdet av den ursprungliga cellen eftersom vi bara är intresserade av grannarna.

```csharp
    return grannar;
}
```
Slutligen returnerar vi antalet levande grannar. 

Om vi har en 3x3 matris som ser ut så här:

```
0 1 0
1 1 1
0 0 0
```

och vi kallar `AntalLevandeGrannar(rutnat, 1, 1)`, där cellen vid position (1,1) är den mellersta cellen, kommer vi att räkna antalet 1:or runt den mellersta cellen. I det här fallet är det 3. Notera att vi inte inkluderar den mellersta cellens värde i antalet grannar, så även om den mellersta cellen också är en 1, räknar vi inte den.

### Steg 2: Implementera `UppdateraSpel`-metoden

Nu kan vi implementera `UppdateraSpel`-metoden. För att göra detta korrekt, behöver vi skapa en kopia av det nuvarande tillståndet av spelet, eftersom varje cells nästa tillstånd beror på det nuvarande tillståndet av dess grannar. Om vi ändrade tillståndet för cellerna i det ursprungliga rutnätet medan vi itererade över det, skulle vissa celler ha sitt nästa tillstånd bestämt av andra cellers nästa tillstånd istället för deras nuvarande tillstånd.

Hela metoden ser ut så här:

```csharp
static int[,] UppdateraSpel(int[,] rutnat)
{
    int bredd = rutnat.GetLength(0);
    int hojd = rutnat.GetLength(1);

    int[,] nyRutnat = new int[bredd, hojd];

    for (int x = 0; x < bredd; x++)
    {
        for (int y = 0; y < hojd; y++)
        {
            int grannar = AntalLevandeGrannar(rutnat, x, y);

            if (rutnat[x, y] == 1)
            {
                // Regler för levande celler
                if (grannar < 2 || grannar > 3)
                {
                    nyRutnat[x, y] = 0;
                }
                else
                {
                    nyRutnat[x, y] = 1;
                }
            }
            else
            {
                // Regler för döda celler
                if (grannar == 3)
                {
                    nyRutnat[x, y] = 1;
                }
            }
        }
    }

    return nyRutnat;
}
```

Nu ska vi ta det steg för steg.

```csharp
static int[,] UppdateraSpel(int[,] rutnat)
{
```
Här definieras metoden `UppdateraSpel` som tar en tvådimensionell array, `rutnat`, som argument och returnerar en ny tvådimensionell array. Arrayen `rutnat` representerar spelets nuvarande tillstånd.

```csharp
    int bredd = rutnat.GetLength(0);
    int hojd = rutnat.GetLength(1);
```
Här skapas två variabler, `bredd` och `hojd`, som representerar bredden och höjden på `rutnat`. Vi får dessa värden genom att kalla på `GetLength`-metoden på `rutnat` med lämpliga argument.

```csharp
    int[,] nyRutnat = new int[bredd, hojd];
```
Här skapas en ny tvådimensionell array, `nyRutnat`, som har samma storlek som `rutnat`. Detta kommer att bli det nya tillståndet av spelet efter att vi har applicerat Game of Life-reglerna på varje cell i `rutnat`.

```csharp
    for (int x = 0; x < bredd; x++)
    {
        for (int y = 0; y < hojd; y++)
        {
            int grannar = AntalLevandeGrannar(rutnat, x, y);
```
Här sker en dubbel loop där vi går igenom varje cell i `rutnat`. För varje cell räknar vi antalet levande grannar genom att kalla på `AntalLevandeGrannar`-metoden.

```csharp
            if (rutnat[x, y] == 1)
            {
                // Regler för levande celler
                if (grannar < 2 || grannar > 3)
                {
                    nyRutnat[x, y] = 0;
                }
                else
                {
                    nyRutnat[x, y] = 1;
                }
            }
            else
            {


                // Regler för döda celler
                if (grannar == 3)
                {
                    nyRutnat[x, y] = 1;
                }
            }
```
Här tillämpar vi Game of Life-reglerna på varje cell. Om cellen är levande och har färre än 2 eller fler än 3 levande grannar, dör den i `nyRutnat`. Om cellen är levande och har 2 eller 3 levande grannar, fortsätter den att leva i `nyRutnat`. Om cellen är död och har exakt 3 levande grannar, föds den i `nyRutnat`.

```csharp
        }
    }

    return nyRutnat;
}
```
Till sist, efter att vi har gått igenom varje cell i `rutnat` och tillämpat Game of Life-reglerna på den, returnerar vi `nyRutnat` som det nya tillståndet för spelet.

Nu kan du sätta upp ett initialt tillstånd för rutnätet i `Main`-metoden och se Game of Life i åtgärd! Du kan börja med en enkel konfiguration, till exempel en liten grupp av levande celler i mitten, och sedan experimentera med mer komplexa initiala tillstånd.

## Exempel på initiala tillstånd

Initiala tillstånd i Game of Life kan vara mycket enkla eller mycket komplexa. En enkel startpunkt kan vara en liten grupp av levande celler i mitten av rutnätet. Här är ett exempel på hur du kan skapa en sådan konfiguration:

**Glider:** Detta är ett mönster som "glider" över rutnätet över tid. Gliderns initiala tillstånd ser ut så här:

   ```
   . . #  
   # . #
   . # #
   ```

För att sätta dessa initiala tillstånd kan du skapa en metod som tar ett rutnät, en position, och en sträng som representerar tillståndet. Här är ett exempel:

```csharp
static void SattTillstand(int[,] rutnat, int startX, int startY, string[] tillstand)
{
    for (int y = 0; y < tillstand.Length; y++)
    {
        for (int x = 0; x < tillstand[y].Length; x++)
        {
            if (tillstand[y][x] == '#')
            {
                rutnat[startX + x, startY + y] = 1;
            }
        }
    }
}
```

Du kan sedan använda denna metod för att sätta initiala tillstånd i ditt `Main`-metod. Här är ett exempel på hur du kan skapa en glider:

```csharp
string[] glider = new string[]
{
    "..#",
    "#.#",
    ".##"
};

SattTillstand(rutnat, 10, 10, glider);
```

Denna kod skapar en glider vid position (10, 10) på rutnätet. Observera att du bör kontrollera att positionen och tillståndet inte går utanför gränserna för rutnätet innan du kör denna kod.

## Fler exempel på initiala tillstånd

1. **Small Exploder:** Detta är en liten grupp av celler som 'exploderar' efter ett par generationer.

```csharp
string[] smallExploder = new string[]
{
    ".#.",
    "#.#",
    "###",
    ".#."
};
```

2. **Lightweight Spaceship (LWSS):** Denna konfiguration simulerar ett rymdskepp som rör sig horisontellt över brädet.

```csharp
string[] lightweightSpaceship = new string[]
{
    "#..#.",
    "....#",
    "#...#",
    ".####"
};
```

3. **R-pentomino:** Trots sin enkelhet leder denna konfiguration till ett ovanligt långt och kaotiskt förlopp.

```csharp
string[] rPentomino = new string[]
{
    ".##",
    "##.",
    ".#."
};
```

4. **Diehard:** Detta är ett exempel på en konfiguration som dör efter 130 generationer, trots att det inte har några initiala celler som är omgivna av tre celler och därför skulle kunna återfödas.

```csharp
string[] diehard = new string[]
{
    "......#.",
    ".##.....",
    "#..#....",
    "........",
    "........"
};
```

5. **Acorn:** En liten initialkonfiguration som snabbt växer till ett stort komplex av levande celler.

```csharp
string[] acorn = new string[]
{
    ".#......",
    "...#....",
    "#..###..",
    "........",
    "........"
};
```

Observera återigen att dessa konfigurationer kansbli behöver omges av döda celler (representerade av punkter '.') för att fylla ut resten av brädet om ditt bräd är större än konfigurationerna. 
