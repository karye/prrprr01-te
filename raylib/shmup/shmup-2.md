# Skott

## Målsättning

Vi ritar ut ett skott som flyger uppåt på skärmen. Vi använder tangenten `Space` för att skjuta. Skottet ska försvinna när det når skärmen.

## Skapa ett skott

På samma sätt som vi skapade ett rymdskepp så skapar vi ett skott. Vi använder en rektangel för att representera skottets position och storlek. Vi använder `float` för att representera skottets hastighet.

```csharp
using Raylib_cs;
using System.Numerics;

public static class Program
{
    // Spelets variabler
    static int skarmBredd = 800;
    static int skarmHojd = 450;

    // Skeppets data
    static Texture2D skeppTextur;
    static Rectangle skeppRektangel;

    // Skotten data
    static Rectangle skottRektangel;

    // Hastigheten med vilken skeppet flyger
    static int skeppHastighet = 200;

    static void Main()
    {
        Raylib.InitWindow(skarmBredd, skarmHojd, "Shmup");
        Raylib.SetTargetFPS(60);

        // Spelarens data
        skeppTextur = Raylib.LoadTexture("resurser/skepp.png");

        // Rektangeln som representerar skeppets position och storlek
        // Vi ritar ut skeppet i mitten av skärmen
        skeppRektangel = new Rectangle(skarmBredd / 2, skarmHojd - skeppTextur.height, skeppTextur.width, skeppTextur.height);

        // Rektangeln som representerar skottets position och storlek
        // Vi ritar ut skottet utanför skärmen
        skottRektangel = new Rectangle(0, 0, 10, 50);

        while (!Raylib.WindowShouldClose())
        {
            Raylib.BeginDrawing();
            Raylib.ClearBackground(Color.RAYWHITE);

            UppdateraSpel();
            RitaSpel();

            Raylib.EndDrawing();
        }
    }

    public static void UppdateraSpel()
    {
        // Vi flyttar spelaren med hjälp av pil-tangenterna
        if (Raylib.IsKeyDown(KeyboardKey.KEY_LEFT))
        {
            skeppRektangel.x -= skeppHastighet * Raylib.GetFrameTime();
        }
        else if (Raylib.IsKeyDown(KeyboardKey.KEY_RIGHT))
        {
            skeppRektangel.x += skeppHastighet * Raylib.GetFrameTime();
        }
    }

    public static void RitaSpel()
    {
        // Rita ut skeppet
        Raylib.DrawTexture(skeppTextur, (int)skeppRektangel.x, (int)skeppRektangel.y, Color.WHITE);

        // Rita ut skottet
        Raylib.DrawRectangle((int)skottRektangel.x, (int)skottRektangel.y, (int)skottRektangel.width, (int)skottRektangel.height, Color.RED);
    }
}
```

## Skottets rörelse

Vi flyttar skottet med samma hastighet som vi flyttar spelaren. Vi använder `skottPosition.Y -= skottHastighet * Raylib.GetFrameTime();` för att flytta skottet uppåt. Vi använder `GetFrameTime()` för att få fram hur mycket tid som har gått sedan förra uppdateringen. Vi multiplicerar med `skottHastighet` för att få fram hur mycket skottet ska flytta sig.

```csharp
public static void UppdateraSkott()
{
    skottPosition.Y -= skottHastighet * Raylib.GetFrameTime();
}
```

## Skottet försvinner

Om vi trycker för snabbt på mellanslag försvinner skottet. Detta beror på att vi bara har ett skott som börjar så fort vi trycker på mellanslag. Vi kan undvika det genom att skapa en bool-variabel som håller koll om skottet är synligt eller inte.

```csharp
using Raylib_cs;
using System.Numerics;

public static class Program
{
    // Spelets variabler
    static int skarmBredd = 800;
    static int skarmHojd = 450;

    // Skeppets data
    static Texture2D skeppTextur;
    static Rectangle skeppRektangel;

    // Skotten data
    static Rectangle skottRektangel;
    static int skottHastighet = 500;
    static bool skottSynligt = false;

    ..

    static void UpptateraSkott()
    {
        if (Raylib.IsKeyPressed(KeyboardKey.KEY_SPACE))
        {
            skottRektangel.x = skeppRektangel.x + skeppRektangel.width / 2;
            skottRektangel.y = skeppRektangel.y;
            skottSynligt = true;
        }

        if (skottSynligt)
        {
            skottRektangel.y -= 200 * Raylib.GetFrameTime();
        }

        if (skottRektangel.y < 0)
        {
            skottSynligt = false;
        }
    }
    ...
}
```

## Ljud

Vi kan lägga till ett ljud när vi skjuter. Vi kan använda `LoadSound` för att ladda in ljudet. Det är viktigt att aktivera med `InitAudioDevice` innan vi kan använda ljud.

```csharp
class Program
{
    /*  Skapa alla variabler (globala) */
    ...

    // Skottets variabler
    static Texture2D texturSkott;
    static Rectangle skottRektangel;
    static Sound skottLjud;
    static bool synligt = false;

    static void Main(string[] args)
    {
        Console.WriteLine("Hello SHMUP!");
        Raylib.InitWindow(800, 600, "SHMUP");
        Raylib.InitAudioDevice();
        Raylib.SetTargetFPS(60);

        // Skapa skeppet
        ...

        // Skapa skott
        skottLjud = Raylib.LoadSound(@"./resurser/laser.wav");

        ...

    }
}
```

Vi kan spela ljudet när vi skjuter med `PlaySound`. Vi kan även använda `IsSoundPlaying` för att se om ljudet fortfarande spelas.

```csharp
// Lyssnar på tangentbordets knappar (func)
static void Input()
{
    ...

    // Lyssna på mellanslag (för att skjuta)
    skjuter = false;
    if (Raylib.IsKeyDown(KeyboardKey.KEY_SPACE) && !synligt)
    {
        skjuter = true;
        skottRektangel.x = skeppRektangel.x;
        skottRektangel.y = skeppRektangel.y;

        // Spela upp ljudet
        Raylib.PlaySound(skottLjud);
    }
}
```

## Utmaningar

Vi kan förbättra skottet genom att använda en lista. Vi kan lägga till skottet i en lista när vi skjuter. Vi kan sedan flytta alla skott i listan. Vi kan även ta bort skottet från listan när det når skärmen.

```csharp
using Raylib_cs;
using System.Numerics;
using System.Collections.Generic;

public static class Program
{
    ...

    static void UppdateraSpel()
    {
        // Vi flyttar spelaren med hjälp av pil-tangenterna
        if (Raylib.IsKeyDown(KeyboardKey.KEY_LEFT))
        {
            skeppRektangel.x -= skeppHastighet * Raylib.GetFrameTime();
        }
        else if (Raylib.IsKeyDown(KeyboardKey.KEY_RIGHT))
        {
            skeppRektangel.x += skeppHastighet * Raylib.GetFrameTime();
        }

        UpptateraSkott();
    }

    static void UpptateraSkott()
    {
        if (Raylib.IsKeyPressed(KeyboardKey.KEY_SPACE) && !skottSynligt)
        {
            // Skjut skottet
            skottRektangel.x = skeppRektangel.x + skeppRektangel.width / 2 - 2;
            skottRektangel.y = skeppRektangel.y - 5;
        }

        // Kolla om skottet är synligt
        if (skottRektangel.y < 0)
        {
            skottSynligt = false;
        }
        else
        {
            skottSynligt = true;
        }

        // Uppdatera skottets position
        skottRektangel.y -= skottHastighet * Raylib.GetFrameTime();
    }

    static void RitaSpel()
    {
        Raylib.BeginDrawing();
        Raylib.ClearBackground(Color.RAYWHITE);

        // Rita ut skeppet
        Raylib.DrawTexture(skeppTextur, (int)skeppRektangel.x, (int)skeppRektangel.y, Color.WHITE);

        // Rita ut skotten
        for (i = 0; i < skottLista.Count; i++)
        {
            Raylib.DrawRectangle((int)skottLista[i].x, (int)skottLista[i].y, (int)skottLista[i].width, (int)skottLista[i].height, Color.RED);
        }

        Raylib.EndDrawing();
    }
}
```

## Uppgifter

* Rita ut en "blixt" när skottet skjuts.