# Shmup

Ett shmup-spel, eller "shoot 'em up", är en genre av dator- och tv-spel som fokuserar på att skjuta och förstöra fiender, oftast genom att kontrollera ett litet rymdskepp, flygplan eller liknande fordon. Shmup-spel är vanligtvis snabba, actionfyllda och kräver snabba reflexer från spelaren.

I dessa spel flyger eller rör sig spelaren oftast i en 2D-värld och har möjlighet att skjuta projektiler eller vapen för att eliminera inkommande fiender och undvika deras attacker. Shmup-spel kan delas in i flera subgenrer, inklusive:

* Vertikalt rullande shmups: Spelaren rör sig uppåt på skärmen, och fiender kommer från toppen av skärmen.
* Horisontellt rullande shmups: Spelaren rör sig åt höger eller vänster på skärmen, och fiender kommer från sidorna av skärmen.
* Rymdräknare: Spelaren kan röra sig fritt i alla riktningar, men spelplanen är begränsad.
* Bullet hell (även kallat "danmaku"): En subgenre som kännetecknas av en mycket stor mängd fiendeprojektiler på skärmen samtidigt, vilket skapar en hög svårighetsgrad och kräver att spelaren utvecklar en strategi för att navigera genom projektilerna.

Några klassiska exempel på shmup-spel inkluderar Space Invaders, Galaga, Gradius, R-Type och Touhou-serien.

## Raylib och shmup-spel

För att bygga ett enkelt rymd-shmup-spel i Raylib med C#, kan du följa dessa steg:

1. Skapa ett nytt C#-projekt och installera Raylib-cs NuGet-paketet.
1. Importera nödvändiga bibliotek.
1. Definiera globala variabler och konstanter för spelet, såsom skärmstorlek, spelarens och fiendernas positioner och storlekar, hastigheter och andra nödvändiga värden.
1. Skapa en huvudloop som kör spelet.
1. Skapa metoder för att flytta spelaren, skapa och flytta fiender, hantera kollisioner, rita objekt på skärmen, och hantera spelets logik.
1. Implementera spelets logik i huvudloopen.
1. Här är ett enkelt exempel på ett rymd-shmup-spel med Raylib i C#:

```csharp
using System;
using System.Collections.Generic;
using Raylib_cs;
using System.Numerics;

// Skärmstorlek
public static int screenWidth = 800;
public static int screenHeight = 450;

// Spelarens egenskaper
public static Vector2 playerPosition = new Vector2(screenWidth / 2, screenHeight - 50);
public static Vector2 playerSize = new Vector2(50, 50);
public static float playerSpeed = 200.0f;
public static List<Vector2> bullets = new List<Vector2>();
public static float bulletSpeed = 400.0f;

// Fienders egenskaper
public static List<Vector2> enemies = new List<Vector2>();
public static Vector2 enemySize = new Vector2(40, 40);
public static float enemySpeed = 100.0f;
public static float spawnInterval = 1.0f;
public static float timeSinceLastSpawn = 0.0f;

static void Main()
{
    Raylib.InitWindow(screenWidth, screenHeight, "Rymd Shmup");
    Raylib.SetTargetFPS(60);

    while (!Raylib.WindowShouldClose())
    {
        float dt = Raylib.GetFrameTime();

        // Uppdatera spelarens position
        UpdatePlayer(dt);

        // Uppdatera och rita skott
        UpdateBullets(dt);

        // Hantera fiender
        UpdateEnemies(dt);

        // Rita allt på skärmen
        DrawGame();
    }

    Raylib.CloseWindow();
}

static void UpdatePlayer(float dt)
{
    if (Raylib.IsKeyDown(KeyboardKey.KEY_LEFT) && playerPosition.X > 0)
    {
        playerPosition.X -= playerSpeed * dt;
    }
    if (Raylib.IsKeyDown(KeyboardKey.KEY_RIGHT) && playerPosition.X + playerSize.X < screenWidth)
    {
        playerPosition.X += playerSpeed * dt;
    }
    if (Raylib.IsKeyDown(KeyboardKey.KEY_UP) && playerPosition.Y > 0)
    {
        playerPosition.Y -= playerSpeed * dt;
    }
    if (Raylib.IsKeyDown(KeyboardKey.KEY_DOWN) && playerPosition.Y + playerSize.Y < screenHeight)
    {
        playerPosition.Y += playerSpeed * dt;
    }

    // Skjut en kula
    if (Raylib.IsKeyPressed(KeyboardKey.KEY_SPACE))
    {
    Vector2 bulletPosition = new Vector2(playerPosition.X + playerSize.X / 2, playerPosition.Y);
    bullets.Add(bulletPosition);
    }
}

static void UpdateBullets(float dt)
{
    for (int i = 0; i < bullets.Count; i++)
    {
        bullets[i] = new Vector2(bullets[i].X, bullets[i].Y - bulletSpeed * dt);

        // Ta bort skottet om den är utanför skärmen
        if (bullets[i].Y < -10)
        {
            bullets.RemoveAt(i);
            i--;
        }
    }
}

static void UpdateEnemies(float dt)
{
    timeSinceLastSpawn += dt;

    if (timeSinceLastSpawn >= spawnInterval)
    {
        Vector2 enemyPosition = new Vector2(Raylib.GetRandomValue(0, screenWidth - (int)enemySize.X), -enemySize.Y);
        enemies.Add(enemyPosition);
        timeSinceLastSpawn = 0.0f;
    }

    for (int i = 0; i < enemies.Count; i++)
    {
        enemies[i] = new Vector2(enemies[i].X, enemies[i].Y + enemySpeed * dt);

        // Ta bort fiende om den är utanför skärmen
        if (enemies[i].Y > screenHeight)
        {
            enemies.RemoveAt(i);
            i--;
            continue;
        }

        // Kollisionsdetektering mellan fiender och skott
        for (int j = 0; j < bullets.Count; j++)
        {
            Rectangle enemyRect = new Rectangle(enemies[i].X, enemies[i].Y, enemySize.X, enemySize.Y);
            Rectangle bulletRect = new Rectangle(bullets[j].X, bullets[j].Y, 5, 10);

            if (Raylib.CheckCollisionRecs(enemyRect, bulletRect))
            {
                enemies.RemoveAt(i);
                bullets.RemoveAt(j);
                i--;
                break;
            }
        }
    }
}

static void DrawGame()
{
    Raylib.BeginDrawing();
    Raylib.ClearBackground(Color.BLACK);

    // Rita spelaren
    Raylib.DrawRectangle((int)playerPosition.X, (int)playerPosition.Y, (int)playerSize.X, (int)playerSize.Y, Color.BLUE);

    // Rita skott
    foreach (Vector2 bullet in bullets)
    {
        Raylib.DrawRectangle((int)bullet.X, (int)bullet.Y, 5, 10, Color.YELLOW);
    }

    // Rita fiender
    foreach (Vector2 enemy in enemies)
    {
        Raylib.DrawRectangle((int)enemy.X, (int)enemy.Y, (int)enemySize.X, (int)enemySize.Y, Color.RED);
    }

    Raylib.EndDrawing();
}
```

> **Obs!** Om du vill läsa mer om Raylib och C# kan du kolla in [denna](https://www.raylib.com/cheatsheet/cheatsheet.html) cheatsheet.
Inspirerat av [Making a Shmup av Lazy Devs](https://www.youtube.com/watch?v=81WM_cjp9fo&list=PLea8cjCua_P3Sfq4XJqNVbd1vsWnh7LZd&index=1)