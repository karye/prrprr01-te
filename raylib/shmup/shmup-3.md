# Gränsnitt

## Målsättning 

Vi ritar ut små hjärtan för att visa spelarens liv. Vi ritar ut en text för att visa spelarens poäng.

## Kontrollpanel

Vi skapar en kontrollpanel som vi kan använda för att visa spelarens liv och poäng.
Vi använder ett hjärta som symbol för liv och en text för poäng.

Vi skapar en variabel för att hålla reda på antal liv som spelaren har kvar.

```csharp
    // Kontrollpanel data
    static Texture2D hjartaBild;
    static int antalLiv = 3;
    static int poäng = 0;
    ...

    hjartaBild = Raylib.LoadTexture(@"resources\hjarta.png");
    ...
```

Vi skapar en metod som ritar ut hjärtan för att visa antal liv kvar.

```csharp
    // Rita ut hjärtan för att visa antal liv kvar
    static void RitaKontrollpanel()
    {
        for (int i = 0; i < antalLiv; i++) 
        {
            Raylib.DrawTexture(hjartaBild, hjartaPosition.X + (i * hjartaBild.width), hjartaPosition.Y, Color.WHITE);
        }

        Raylib.DrawText(poäng.ToString(), poängPosition.X, poängPosition.Y, 20, Color.WHITE);
    }
```

Vi ritar ut hjärtan i `RitaSpel`-metoden.

```csharp
    static void RitaSpel()
    {
        ...

        // Rita ut hjärtan för att visa antal liv kvar och poängen
        RitaKontrollpanel();

        ...
    }
```

## Utmaningar

