# Stjärnor

## Målsättning 

Vi ritar ut stjärnor i bakgrunden för att skapa en illusion av att spelaren rör sig i rymden. Vi använder en slumpgenerator för att slumpmässigt placera ut stjärnorna.

## Skapa stjärnor

Vi skapar en lista av stjärnor. Varje stjärna representeras av en rektangel. Vi använder `float` för att representera stjärnans hastighet.

```csharp
using Raylib_cs;
using System.Numerics;

public static class Program
{
    // Spelets variabler
    static int skarmBredd = 800;
    static int skarmHojd = 450;

    // Skeppets data
    static Texture2D skeppTextur;
    static Rectangle skeppRektangel;

    // Skotten data
    static Rectangle skottRektangel;

    // Hastigheten med vilken skeppet flyger
    static int skeppHastighet = 200;

    // Stjärnornas data
    static int antalStjarnor = 100;
    static Rectangle[] stjarnor;
    static float[] stjarnorHastighet;

    static void Main()
    {
        Raylib.InitWindow(skarmBredd, skarmHojd, "Shmup");
        Raylib.SetTargetFPS(60);

        // Spelarens data
        skeppTextur = Raylib.LoadTexture("resurser/skepp.png");

        // Rektangeln som representerar skeppets position och storlek
        // Vi ritar ut skeppet i mitten av skärmen
        skeppRektangel = new Rectangle(skarmBredd / 2, skarmHojd - skeppTextur.height, skeppTextur.width, skeppTextur.height);

        // Rektangeln som representerar skottets position och storlek
        // Vi ritar ut skottet utanför skärmen
        skottRektangel = new Rectangle(0, 0, 10, 50);

        // Stjärnornas data
        stjarnor = new Rectangle[antalStjarnor];
        stjarnorHastighet = new float[antalStjarnor];

        while (!Raylib.WindowShouldClose())
        {
            Raylib.BeginDrawing();
            Raylib.ClearBackground(Color.RAYWHITE);

            UppdateraSpelare();
            UppdateraSkott();
            UppdateraStjarnor();

            RitaSpelare();
            RitaSkott();
            RitaStjarnor();

            Raylib.EndDrawing();
        }

        Raylib.CloseWindow();
    }

    // Vi flyttar stjärnorna nedåt
    static void UppdateraStjarnor()
    {
        for (int i = 0; i < antalStjarnor; i++)
        {
            stjarnor[i].y += stjarnorHastighet[i] * Raylib.GetFrameTime();
        }
    }

    // Vi ritar ut spelaren
    static void RitaStjarnor()
    {
        for (int i = 0; i < antalStjarnor; i++)
        {
            Raylib.DrawRectangleRec(stjarnor[i], Color.WHITE);
        }
    }
}
```

## Slumpa stjärnornas position

Vi vill att stjärnorna ska slumpmässigt placeras ut på skärmen. Vi använder en slumpgenerator för att slumpmässigt placera ut stjärnorna.

```csharp
static void UppdateraStjarnor()
{
    for (int i = 0; i < antalStjarnor; i++)
    {
        stjarnor[i].y += stjarnorHastighet[i] * Raylib.GetFrameTime();

        // Om stjärnan har åkt ut ur skärmen
        if (stjarnor[i].y > skarmHojd)
        {
            // Placera ut stjärnan på en slumpad position
            stjarnor[i].x = Raylib.GetRandomValue(0, skarmBredd);
            stjarnor[i].y = Raylib.GetRandomValue(-100, 0);
        }
    }
}
```

## Slumpa stjärnornas storlek

Vi vill att stjärnorna ska ha olika storlekar. Vi använder en slumpgenerator för att slumpmässigt bestämma stjärnornas storlek.

```csharp
static void UppdateraStjarnor()
{
    for (int i = 0; i < antalStjarnor; i++)
    {
        stjarnor[i].y += stjarnorHastighet[i] * Raylib.GetFrameTime();

        // Om stjärnan har åkt ut ur skärmen
        if (stjärnor[i].y > skarmHojd)
        {
            // Placera ut stjärnan på en slumpad position
            stjarnor[i].x = Raylib.GetRandomValue(0, skarmBredd);
            stjarnor[i].y = Raylib.GetRandomValue(-100, 0);

            // Slumpa stjärnans storlek
            stjarnor[i].width = Raylib.GetRandomValue(1, 3);
            stjarnor[i].height = stjarnor[i].width;
        }
    }
}
```

## Slumpa stjärnornas hastighet

Vi vill att stjärnorna ska ha olika hastigheter. Vi använder en slumpgenerator för att slumpmässigt bestämma stjärnornas hastighet.

```csharp
static void UppdateraStjarnor()
{
    for (int i = 0; i < antalStjarnor; i++)
    {
        stjarnor[i].y += stjarnorHastighet[i] * Raylib.GetFrameTime();

        // Om stjärnan har åkt ut ur skärmen
        if (stjärnor[i].y > skarmHojd)
        {
            // Placera ut stjärnan på en slumpad position
            stjarnor[i].x = Raylib.GetRandomValue(0, skarmBredd);
            stjarnor[i].y = Raylib.GetRandomValue(-100, 0);

            // Slumpa stjärnans storlek
            stjarnor[i].width = Raylib.GetRandomValue(1, 3);
            stjarnor[i].height = stjarnor[i].width;

            // Slumpa stjärnans hastighet
            stjarnorHastighet[i] = Raylib.GetRandomValue(50, 200);
        }
    }
}
```

## Parallax effekt

Vi vill att stjärnorna ska verka vara långt borta. Vi gör detta genom att låta stjärnorna flytta sig långsammare än spelaren. Vi multiplicerar stjärnornas hastighet med ett tal som är mindre än 1.

```csharp
static void UppdateraStjarnor()
{
    for (int i = 0; i < antalStjarnor; i++)
    {
        stjarnor[i].y += stjarnorHastighet[i] * Raylib.GetFrameTime();

        // Om stjärnan har åkt ut ur skärmen
        if (stjärnor[i].y > skarmHojd)
        {
            // Placera ut stjärnan på en slumpad position
            stjarnor[i].x = Raylib.GetRandomValue(0, skarmBredd);
            stjarnor[i].y = Raylib.GetRandomValue(-100, 0);

            // Slumpa stjärnans storlek
            stjarnor[i].width = Raylib.GetRandomValue(1, 3);
            stjarnor[i].height = stjarnor[i].width;

            // Slumpa stjärnans hastighet
            stjarnorHastighet[i] = Raylib.GetRandomValue(50, 200);
        }
    }
}
```

### Parallax bakgrund med Camera2D

Att skapa en parallax bakgrund med stjärnor kan vara enkelt att göra med hjälp av Raylib. Här är en metod för att skapa en parallax bakgrund med stjärnor:

1. Skapa en ny lista för stjärnorna

```csharp
static List<(Vector2 position, float hastighet)> stjarnor = new List<(Vector2, float)>();
```

1. Fyll listan med slumpmässigt placerade stjärnor med slumpmässiga hastigheter

```csharp
for (int i = 0; i < 100; i++)
{
    stjarnor.Add((new Vector2(Rand.Next(0, skarmBredd), Rand.Next(0, skarmHojd)), Rand.Next(10, 200)));
}
```

1. Uppdatera stjärnornas position baserat på deras hastighet och rita ut dem på skärmen

```csharp
static void RitaStjarnor()
{
    foreach (var (position, hastighet) in stjarnor)
    {
        position.Y += hastighet * Raylib.GetFrameTime();
        if (position.Y > skarmHojd)
        {
            position.Y = 0;
            position.X = Rand.Next(0, skarmBredd);
        }
        Raylib.DrawCircle((int)position.X, (int)position.Y, 1, Color.WHITE);
    }
}
```

1. Rita ut stjärnorna flera gånger med olika hastigheter för att skapa parallax effekten. För att göra det, upprepa bara ritningen av stjärnorna med olika hastigheter, och öka hastigheten på stjärnorna ju längre bakgrundslagret är.

```csharp
Raylib.ClearBackground(Color.DARKBLUE);

// Rita ut bakgrundslagret med stjärnor som rör sig långsamt
Raylib.BeginMode2D(new Camera2D { zoom = new Vector2(0.2f, 0.2f) });
RitaStjarnor();
Raylib.EndMode2D();

// Rita ut mellanliggande lagret med stjärnor som rör sig medelhögt
Raylib.BeginMode2D(new Camera2D { zoom = new Vector2(0.4f, 0.4f) });
RitaStjarnor();
Raylib.EndMode2D();

// Rita ut förgrundslagret med stjärnor som rör sig snabbt
Raylib.BeginMode2D(new Camera2D { zoom = new Vector2(0.8f, 0.8f) });
RitaStjarnor();
Raylib.EndMode2D();
```

Med denna metod kommer stjärnorna att röra sig med olika hastigheter och skapa en parallax effekt, som ger en känsla av djup i bakgrunden. Du kan experimentera med olika hastigheter, antal och storlek på stjärnorna för att skapa den bakgrund du vill ha.