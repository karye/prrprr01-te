# Rymdskeppet

## Målsättning

Vi skapar ett rymdskepp som kan flyga runt på skärmen. Vi använder en bild för att representera skeppet, och vi använder tangenterna `A` och `D` för att styra skeppet åt vänster och höger.  
Rymdskeppet ska inte kunna åka utanför skärmen.

## Skapa ett rymdskepp

Vi skapar ett rymdskepp genom att ladda in en bild och sedan skapa en rektangel som representerar skeppets position och storlek. Vi använder `int` för att representera skeppets hastighet.

Vi använder `Raylib.GetFrameTime()` för att få fram hur mycket tid som har gått sedan förra uppdateringen. Detta gör att vi kan använda samma hastighet på alla datorer, oavsett hur snabb de är.

```csharp
using Raylib_cs;
using System.Numerics;

public static class Program
{
    // Spelets variabler
    static int skarmBredd = 800;
    static int skarmHojd = 450;

    // Skeppets data
    static Texture2D skeppTextur;
    static Rectangle skeppRektangel;

    // Hastigheten med vilken skeppet flyger
    static int skeppHastighet = 200;

    static void Main()
    {
        Raylib.InitWindow(skarmBredd, skarmHojd, "Shmup");
        Raylib.SetTargetFPS(60);

        // Spelarens data
        skeppTextur = Raylib.LoadTexture("resurser/skepp.png");

        // Rektangeln som representerar skeppets position och storlek
        // Vi ritar ut skeppet i mitten av skärmen
        skeppRektangel = new Rectangle(skarmBredd / 2, skarmHojd - skeppTextur.height, skeppTextur.width, skeppTextur.height);

        while (!Raylib.WindowShouldClose())
        {
            Raylib.BeginDrawing();
            Raylib.ClearBackground(Color.DARKBLUE);

            RitaSpel();

            Raylib.EndDrawing();
        }
    }

    static void RitaSpel()
    {
        // Rita ut skeppet
        Raylib.DrawTexture(skeppTextur, (int)skeppRektangel.x, (int)skeppRektangel.y, Color.WHITE);

        // Rita ut positionen för skeppet
        Raylib.DrawText($"Skeppets position: {(int)skeppRektangel.x}, {(int)skeppRektangel.y}", 10, 10, 20, Color.WHITE);
    }
}
```

## Styra skeppet

Vi kan styra skeppet genom att ändra på `skeppRektangel.x` beroende på om spelaren trycker på `A` eller `D`. Vi använder `Raylib.IsKeyDown()` för att kolla om spelaren trycker på en tangent.

```csharp
// Kolla om spelaren trycker på A eller D
public static void UppdateraSpel()
{
    // Vi flyttar spelaren med hjälp av pil-tangenterna
    if (Raylib.IsKeyDown(KeyboardKey.KEY_A))
    {
        skeppRektangel.x -= skeppHastighet * Raylib.GetFrameTime();
    }
    else if (Raylib.IsKeyDown(KeyboardKey.KEY_D))
    {
        skeppRektangel.x += skeppHastighet * Raylib.GetFrameTime();
    }
}
```

I Main-metoden måste vi anropa `UppdateraSpel()` innan vi ritar ut spelet.

```csharp
while (!Raylib.WindowShouldClose())
{
    Raylib.BeginDrawing();
    Raylib.ClearBackground(Color.DARKBLUE);

    UppdateraSpel();
    RitaSpel();

    Raylib.EndDrawing();
}
```

## Begränsa skeppets rörelse

* Vi kan använda `Math.Clamp` för att begränsa spelarens position till skärmen:

```csharp
// Begränsa spelarens position till skärmen, dvs mellan x = 0 och x = skärmens bredd - skeppets bredd
skeppRektangel.x = Math.Clamp(skeppRektangel.x, 0, skarmBredd - skeppRektangel.width);
```