# Fienden

## Målsättning 

Vi ritar ut en fiende som följer en viss rörelse. Fiended kommer uppifrån och rör sig neråt. När den når botten av skärmen kommer den att återvända till början och börja om.  

## Skapa en fiende

Vi skapar en fiende på samma sätt som vi skapade ett rymdskepp och ett skott. Vi använder en rektangel för att representera fiendens position och storlek. Vi använder `float` för att representera fiendens hastighet.

```csharp
using Raylib_cs;
using System.Numerics;

public static class Program
{
    public static int skarmBredd = 800;
    public static int skarmHojd = 450;

    // Spelarens data
    public static texture2D spelareBild = Raylib.LoadTexture("/resurser/skepp.png");
    public static Vector2 spelarePosition = new Vector2(skarmBredd / 2, skarmHojd - 50);
    public static Vector2 spelareStorlek = new Vector2(50, 50);
    public static float spelareHastighet = 200.0f;

    // Skottets data
    public static Vector2 skottPosition = new Vector2(0, 0);
    public static Vector2 skottStorlek = new Vector2(10, 10);
    public static float skottHastighet = 500.0f;

    // Fiendens data
    public static texture2D fiendeBild = Raylib.LoadTexture("/resurser/fiende.png");
    public static Vector2 fiendePosition = new Vector2(0, 0);
    public static Vector2 fiendeStorlek = new Vector2(50, 50);
    public static float fiendeHastighet = 100.0f;

    static void Main()
    {
        Raylib.InitWindow(skarmBredd, skarmHojd, "Shmup");
        Raylib.SetTargetFPS(60);

        while (!Raylib.WindowShouldClose())
        {
            Raylib.BeginDrawing();
            Raylib.ClearBackground(Color.RAYWHITE);

            UppdateraSpelare();
            UppdateraSkott();
            UppdateraFiende();

            RitaSpelare();
            RitaSkott();
            RitaFiende();

            Raylib.EndDrawing();
        }

        Raylib.CloseWindow();
    }

    // Vi rör oss neråt med hjälp av hastigheten
    public static void UppdateraFiende()
    {
        fiendePosition.Y += fiendeHastighet * Raylib.GetFrameTime();
    }

    // Rita ut fienden
    public static void RitaFiende()
    {
        Raylib.DrawTexture(fiendeBild, (int)fiendePosition.X, (int)fiendePosition.Y, Color.WHITE);
    }
}
```

## Träffa fienden

När fienden träffas av ett skott så ska den försvinna. Vi kan använda en variabel för att hålla reda på om fienden är träffad eller inte. Vi kan använda en `if`-sats för att kontrollera om fienden är träffad eller inte. Om fienden är träffad så ska vi återställa fiendens position till början.

```csharp
using Raylib_cs;
using System.Numerics;

public static class Program
{
    public static int skarmBredd = 800;
    public static int skarmHojd = 450;

    // Spelarens data
    public static texture2D spelareBild = Raylib.LoadTexture("/resurser/skepp.png");
    public static Vector2 spelarePosition = new Vector2(skarmBredd / 2, skarmHojd - 50);
    public static Vector2 spelareStorlek = new Vector2(50, 50);
    public static float spelareHastighet = 200.0f;

    // Skottets data
    public static Vector2 skottPosition = new Vector2(0, 0);
    public static Vector2 skottStorlek = new Vector2(10, 10);
    public static float skottHastighet = 500.0f;

    // Fiendens data
    public static texture2D fiendeBild = Raylib.LoadTexture("/resurser/fiende.png");
    public static Vector2 fiendePosition = new Vector2(0, 0);
    public static Vector2 fiendeStorlek = new Vector2(50, 50);
    public static float fiendeHastighet = 100.0f;

    // Variabel för att hålla reda på om fienden är träffad eller inte
    public static bool fiendeTräffad = false;

    static void Main()
    {
        Raylib.InitWindow(skarmBredd, skarmHojd, "Shmup");
        Raylib.SetTargetFPS(60);

        while (!Raylib.WindowShouldClose())
        {
            Raylib.BeginDrawing();
            Raylib.ClearBackground(Color.RAYWHITE);

            UppdateraSpelare();
            UppdateraSkott();
            UppdateraFiende();

            RitaSpelare();
            RitaSkott();
            RitaFiende();

            Raylib.EndDrawing();
        }

        Raylib.CloseWindow();
    }

    // Vi rör oss neråt med hjälp av hastigheten
    public static void UppdateraFiende()
    {
        fiendePosition.Y += fiendeHastighet * Raylib.GetFrameTime();

        // Om fienden är träffad så återställer vi dess position
        if (fiendeTräffad)
        {
            fiendePosition = new Vector2(0, 0);
            fiendeTräffad = false;
        }
    }

    // Rita ut fienden
    public static void RitaFiende()
    {
        Raylib.DrawTexture(fiendeBild, (int)fiendePosition.X, (int)fiendePosition.Y, Color.WHITE);
    }
}
```

## Poäng

Vi vill att spelaren ska få poäng när fienden träffas. Vi kan använda en variabel för att hålla reda på poängen. Vi kan använda en `if`-sats för att kontrollera om fienden är träffad eller inte. Om fienden är träffad så ska vi öka poängen med ett värde.

```csharp
using Raylib_cs;
using System.Numerics;

public static class Program
{
    public static int skarmBredd = 800;
    public static int skarmHojd = 450;

    // Spelarens data
    public static texture2D spelareBild = Raylib.LoadTexture("/resurser/skepp.png");
    public static Vector2 spelarePosition = new Vector2(skarmBredd / 2, skarmHojd - 50);
    public static Vector2 spelareStorlek = new Vector2(50, 50);
    public static float spelareHastighet = 200.0f;

    // Skottets data
    public static Vector2 skottPosition = new Vector2(0, 0);
    public static Vector2 skottStorlek = new Vector2(10, 10);
    public static float skottHastighet = 500.0f;

    // Fiendens data
    public static texture2D fiendeBild = Raylib.LoadTexture("/resurser/fiende.png");
    public static Vector2 fiendePosition = new Vector2(0, 0);
    public static Vector2 fiendeStorlek = new Vector2(50, 50);
    public static float fiendeHastighet = 100.0f;

    // Variabel för att hålla reda på om fienden är träffad eller inte
    public static bool fiendeTräffad = false;

    // Variabel för att hålla reda på poängen
    public static int poäng = 0;

    static void Main()
    {
        Raylib.InitWindow(skarmBredd, skarmHojd, "Shmup");
        Raylib.SetTargetFPS(60);

        while (!Raylib.WindowShouldClose())
        {
            Raylib.BeginDrawing();
            Raylib.ClearBackground(Color.RAYWHITE);

            UppdateraSpelare();
            UppdateraSkott();
            UppdateraFiende();

            RitaSpelare();
            RitaSkott();
            RitaFiende();

            Raylib.EndDrawing();
        }

        Raylib.CloseWindow();
    }

    // Vi rör oss neråt med hjälp av hastigheten
    public static void UppdateraFiende()
    {
        fiendePosition.Y += fiendeHastighet * Raylib.GetFrameTime();

        // Om fienden är träffad så ökar vi poängen
        if (fiendeTräffad)
        {
            poäng += 1;
            fiendePosition = new Vector2(0, 0);
            fiendeTräffad = false;
        }
    }
}
```

## Poängtext

Vi vill att poängen ska visas på skärmen. Vi skapar en metod som ritar ut poängen på skärmen.

```csharp
    // Rita ut poängen
    public static void RitaPoäng()
    {
        Raylib.DrawText(poäng.ToString(), 10, 10, 20, Color.BLACK);
    }
```

Vi ritar ut poängen i `Main`-metoden.

```csharp
            Raylib.BeginDrawing();
            Raylib.ClearBackground(Color.RAYWHITE);

            RitaSpelare();
            RitaSkott();
            RitaFiende();
            RitaPoäng();

            Raylib.EndDrawing();
```

## Gränsnittet

Vi slår ihop "antal liv" och "antal poäng" till ett gränsnitt. Vi skapar en metod som ritar ut gränsnittet.

```csharp
    // Rita ut gränsnittet
    public static void RitaGränsnitt()
    {
        Raylib.DrawText("Liv: " + liv.ToString(), 10, 10, 20, Color.BLACK);
        Raylib.DrawText("Poäng: " + poäng.ToString(), 10, 40, 20, Color.BLACK);
    }
```

Vi ritar ut gränsnittet i `Main`-metoden.

```csharp
            Raylib.BeginDrawing();
            Raylib.ClearBackground(Color.RAYWHITE);

            RitaSpelare();
            RitaSkott();
            RitaFiende();
            RitaGränsnitt();

            Raylib.EndDrawing();
```

