# Input

## Lyssna på tangenter

### Nytt projekt

Skapa ett nytt Raylib-projekt.\
Och `using Raylib\_cs` i program.cs.

```powershell
dotnet new console
dotnet add package Raylib-cs
```

### En spelare

Skapa en rektangel som föreställer en spelare:

```csharp
using System;
using Raylib_cs;

// Starta grafikmotorn
Raylib.InitWindow(800, 600, "Mitt Raylib fönster");

// Bestäm skärmuppdatering
Raylib.SetTargetFPS(60);

// Skapa rektanglar
Rectangle spelare = new Rectangle(100, 100, 50, 50);

// Animationsloopen
while (!Raylib.WindowShouldClose())
{
    /* Rita ut grafiken */
    // Börja rita
    Raylib.BeginDrawing();

    // Töm ritytan
    Raylib.ClearBackground(Color.DARKBLUE);

    // Rita ut spelare & fiende
    Raylib.DrawRectangleRec(spelare, Color.RED);
    
    // Ritat ut på fönstret
    Raylib.EndDrawing();
}
```

### Tangenter

I programmet kan vi reagera på input från tangentbordet med `Raylib.IsKeyDown(..)`

```csharp
// Animationsloopen
while (!Raylib.WindowShouldClose())
{
    /* Rita ut grafiken */
    // Börja rita
    Raylib.BeginDrawing();

    // Töm ritytan
    Raylib.ClearBackground(Color.DARKBLUE);

    // Rita ut spelare & fiende
    Raylib.DrawRectangleRec(spelare, Color.RED);
    Raylib.DrawRectangleRec(fiende, Color.GOLD);

    // Ritat ut på fönstret
    Raylib.EndDrawing();

    /* Interaktion med användare */
    // Lyssna på input för spelaren
    if (Raylib.IsKeyDown(KeyboardKey.KEY_RIGHT))
    {
        spelare.x += 5;
    }
}
```

Nu kan du flytta spelare till höger.

### Förflyttning i x och y

Vi lägger in if-satser för att kunna förflytta åt alla håll:

```csharp
// Animationsloopen
while (!Raylib.WindowShouldClose())
{
    /* Rita ut grafiken */
    // Börja rita
    Raylib.BeginDrawing();

    // Töm ritytan
    Raylib.ClearBackground(Color.DARKBLUE);

    // Rita ut spelare & fiende
    Raylib.DrawRectangleRec(spelare, Color.RED);
    Raylib.DrawRectangleRec(fiende, Color.GOLD);

    // Ritat ut på fönstret
    Raylib.EndDrawing();

    /* Interaktion med användare */
    // Lyssna på input för spelaren
    if (Raylib.IsKeyDown(KeyboardKey.KEY_RIGHT))
    {
        spelare.x += 5;
    }
    if (Raylib.IsKeyDown(KeyboardKey.KEY_LEFT))
    {
        spelare.x -= 5;
    }
    if (Raylib.IsKeyDown(KeyboardKey.KEY_DOWN))
    {
        spelare.y += 5;
    }
    if (Raylib.IsKeyDown(KeyboardKey.KEY_UP))
    {
        spelare.y -= 5;
    }
}
```

### Uppgift 3

Lägg till en fiende som förflyttas med tangenterna **ADWS**.

## Förflytta en bild

Infoga en bild, tex [**ufo.png**](https://www.flaticon.com/free-icons/ufo) i en mapp **resurser**:

```csharp
// Starta grafikmotorn
Raylib.InitWindow(800, 600, "Mitt Raylib fönster");

// Bestäm skärmuppdatering
Raylib.SetTargetFPS(60);

// Ladda in bilden
Texture2D ufo = Raylib.LoadTexture(@"./resurser/ufo.png");

// Skapa en rektangel
Rectangle spelare = new Rectangle(100, 100, ufo.width, ufo.height);

...
```

Tricket är nu att rita ut bilden med rektangelns x och y:

```csharp
// Animationsloopen
while (!Raylib.WindowShouldClose())
{
    /* Rita ut grafiken */
    // Börja rita
    Raylib.BeginDrawing();

    // Töm ritytan
    Raylib.ClearBackground(Color.DARKBLUE);

    // Rita ut spelare
    Raylib.DrawTexture(ufo, (int)spelare.x, (int)spelare.y, Color.WHITE);

    // Ritat ut på fönstret
    Raylib.EndDrawing();
    
    /* Interaktion med användare */
    ...
}
```

![](../../.gitbook/assets/image-9.png)

### Uppgift 4

* Använd en bild till fienden
* Hur gör man för att inte lämnar spelplanen?

## Mer info

* Läs mer om hur man kan [skala och rotera bilden](https://csharp.progdocs.se/csharp-ref/grafik/raylib/bilder-och-texturer#drawtextureex)
