# Rita bilder

## Nytt projekt

Skapa ett nytt Raylib-projekt:

```powershell
dotnet new console
dotnet add package Raylib-cs
```

### Importera Raylib-cs

Ange **using Raylib\_cs** i program.cs:

```csharp
using System;
using Raylib_cs;

Console.WriteLine("Hej Raylib!");
```

## Resurser

Skapa en mapp **resurser** och ladda ned bilder i den från tex:
* Hitta fria ikoner på [flaticon.com](https://www.flaticon.com/free-icons/moon)

![En liten måne](../../.gitbook/assets/full-moon.png)

### Ladda in bilder

I programmet laddar man in bilder med Raylib.LoadTexture():

```csharp
// Starta grafikmotorn
Raylib.InitWindow(800, 600, "Mitt Raylib fönster");

// Bestäm skärmuppdatering
Raylib.SetTargetFPS(60);

// Ladda in bilden
Texture2D månenLitenTexture = Raylib.LoadTexture(@"./resurser/moon.png");
Texture2D månenStorTexture = Raylib.LoadTexture(@"./resurser/moon_PNG35.png");
Texture2D solenTexture = Raylib.LoadTexture(@"./resurser/sun.png");

// Animationsloopen
while (!Raylib.WindowShouldClose())
{
    // Börja rita
    Raylib.BeginDrawing();

    // Töm ritytan
    Raylib.ClearBackground(Color.DARKBLUE);

    // Rita ut bilder
    Raylib.DrawTexture(månenLitenTexture, 100, 100, Color.WHITE);
    Raylib.DrawTexture(månenStorTexture, 300, 100, Color.WHITE);
    Raylib.DrawTexture(solenTexture, 500, 0, Color.WHITE);

    // Ritat ut på fönstret
    Raylib.EndDrawing();
}
```

Resultatet blir:

![](../../.gitbook/assets/image-11.png)

## Mer info

* Läs mer om [bilder och texturer](https://csharp.progdocs.se/csharp-ref/grafik/raylib/bilder-och-texturer)
