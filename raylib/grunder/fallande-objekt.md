# Fallande objekt

Nu ska vi skapa ett program som ritar snöflingor som faller nedåt på skärmen. Vi kommer att använda Raylib för att rita snöflingorna, och C# för att skriva koden.

## Skapa ett nytt Raylib-projekt

Vi börjar med att skapa ett nytt Raylib-projekt. För att göra det öppnar du din terminal och skriver följande kommandon:

```powershell
dotnet new console
dotnet add package Raylib-cs
```

Detta skapar ett nytt konsolprojekt och lägger till Raylib-cs paketet till det.

### Grundkoden

Vi börjar med följande grundkod för vårt Raylib-projekt:

```csharp
using System;
using Raylib_cs;

const int fönsterB = 800;
const int fönsterH = 600;

Raylib.InitWindow(fönsterB, fönsterH, "Snöflingor");
Raylib.SetTargetFPS(60);

// Här lägger vi till våra egna variabler och objekt

while (!Raylib.WindowShouldClose())
{
    // Här uppdaterar vi våra variabler
    
    Raylib.BeginDrawing();
    Raylib.ClearBackground(Color.DARKBLUE);

    Raylib.DrawText("Mitt första fönster!", 190, 200, 20, Color.GRAY);

    Raylib.EndDrawing();
}
```

## Skapa en snöflinga

Vi börjar med att skapa en enda snöflinga. Vi kommer att representera snöflingan som en rektangel.

### Slumpad position

För att placera snöflingan på ett slumpmässigt ställe på skärmen, behöver vi skapa en rektangel och tilldela den en x-position med hjälp av `Random`-klassen:

```csharp
Rectangle snö = new Rectangle(0, 0, 10, 10);

// Placera snöflingan på ett slumpmässigt ställe
snö.x = Random.Shared.Next(20, fönsterB - 20);
```

### Rita snöflingan

För att rita snöflingan, lägger vi till följande kod i vår rit-loop:

```csharp
Raylib.BeginDrawing();
Raylib.ClearBackground(Color.DARKBLUE);

// Rita snöflingan
Raylib.DrawRectangleRec(snö, Color.WHITE);

Raylib.EndDrawing();
```

Detta kommer att rita en vit snöflinga på en mörkblå bakgrund.

### Animera snöflingan

För att få snöflingan att falla, behöver vi uppdatera dess y-position i varje loop:

```csharp
// Uppdatera snöflingans position
snö.y += 1;
```

Då kommer snöflingan att röra sig nedåt på skärmen.

### Repetera fallet

För att få snöflingan att börja om från toppen när den har nått botten av skärmen, lägger vi till följande kod i vår uppdaterings-loop:

```csharp
// Om snöflingan har nått botten av skärmen, börja om från toppen
if (snö.y > fönsterH)
{
    snö.y = 0;
}
```

## Skapa många snöflingor

För att skapa flera snöflingor, kan vi använda en array av rektanglar.

### Skapa en array med snöflingor

Först behöver vi skapa en array att hålla våra snöflingor:

```csharp
Rectangle[] flingor = new Rectangle[10];
```

Detta skapar en array som kan hålla 10 snöflingor.

### Fyll arrayen med snöflingor

För att fylla arrayen med snöflingor, använder vi en `for`-loop:

```csharp
// Fyll på med flingor
// Lägg ut dem slumpmässigt
for (int i = 0; i < flingor.Length; i++)
{
    int x = generator.Next(20, fönsterB - 20);
    int y = generator.Next(0, fönsterH) - 700;
    int storlek = generator.Next(2, 7);
    flingor[i] = new Rectangle(x, y, storlek, storlek);
}
```

Detta kommer att placera varje snöflinga på ett slumpmässigt ställe på skärmen, och ge den en slumpmässig storlek.

### Rita alla snöflingor

För att rita alla snöflingor, behöver vi loopa genom vår array och rita varje snöflinga individuellt:

```csharp
Raylib.BeginDrawing();
Raylib.ClearBackground(Color.DARKBLUE);

// Rita varje snöflinga
for (int i = 0; i < flingor.Length; i++)
{
    Raylib.DrawRectangleRec(flingor[i], Color.WHITE);
}

Raylib.EndDrawing();
```

## Uppgifter

### Uppgift 10

Nu vill vi att snöflingorna ska börja om från toppen när de når botten av skärmen. Detta är din uppgift att lösa. Använd samma princip som vi använde för en enskild snöflinga tidigare i guiden. Du behöver lägga till en kontroll i din uppdaterings-loop som kontrollerar om någon av snöflingorna har nått botten av skärmen, och om så är fallet, flytta den tillbaka till toppen.