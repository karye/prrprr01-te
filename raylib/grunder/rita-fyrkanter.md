# Rita fyrkanter

## Koordinatsystemet

För att rita i vårt Raylib-fönster behöver vi förstå hur koordinatsystemet fungerar. I ett traditionellt matematiskt koordinatsystem ligger origo (0,0) i mitten, med positiva X-värden till höger och positiva Y-värden uppåt. Men på datorskärmar är origo istället i det övre vänstra hörnet, med positiva X-värden till höger och positiva Y-värden nedåt.

![](../../.gitbook/assets/image-14.png)

## Rita en fyrkant

Nu ska vi prova att rita en fyrkant på skärmen. Raylib har en inbyggd funktion för detta, `Raylib.DrawRectangle(x, y, bredd, höjd, färg)`, där `(x, y)` är koordinaterna för fyrkantens övre vänstra hörn, `bredd` och `höjd` bestämmer storleken på fyrkanten och `färg` bestämmer dess färg. Vi kommer att använda `Color.MAGENTA` för vår fyrkant.

```csharp
// Animationsloopen
while (!Raylib.WindowShouldClose())
{
    // Börja rita
    Raylib.BeginDrawing();

    // Töm ritytan
    Raylib.ClearBackground(Color.BEIGE);

    // Här skapar vi grafik
    // Rita fyrkanter
    Raylib.DrawRectangle(100, 100, 200, 100, Color.MAGENTA);

    // Ritat ut på fönstret
    Raylib.EndDrawing();
}
```

Efter att ha kört koden ska du se en magentafärgad rektangel på skärmen.

## Rita flera fyrkanter

Vi kan enkelt lägga till fler fyrkanter genom att kalla på `DrawRectangle` flera gånger. Varje gång kommer det att rita en ny rektangel på skärmen.

```csharp
// Animationsloopen
while (!Raylib.WindowShouldClose())
{
    // Börja rita
    Raylib.BeginDrawing();

    // Töm ritytan
    Raylib.ClearBackground(Color.BEIGE);

    // Här ritar vi ut saker
    // Rita fyrkanter
    Raylib.DrawRectangle(100, 100, 200, 100, Color.MAGENTA);
    Raylib.DrawRectangle(450, 100, 100, 100, Color.RED);
    Raylib.DrawRectangle(250, 350, 100, 200, Color.GOLD);

    // Ritat ut på fönstret
    Raylib.EndDrawing();
}
```
Nu borde du se tre fyrkanter i olika färger och storlekar.

## Uppgift

Nu har vi kommit till vår första uppgift. Vi vill att du ska skapa ett nytt projekt där du ritar Sveriges flagga:

![](../../.gitbook/assets/image-17.png)

Tänk på att flaggan består av ett blått fält med ett gult kors. Det vertikala korset börjar en bit till vänster från mitten och det horisontala korset börjar en bit upp från

 mitten.

## Mer info

För att lära dig mer om hur man ritar geometriska former i Raylib, kolla in dokumentationen [här](https://csharp.progdocs.se/csharp-ref/grafik/raylib/geometriska-former).