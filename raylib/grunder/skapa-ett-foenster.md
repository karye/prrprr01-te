# Skapa ett Raylib-fönster

## Skapa ett projekt med Raylib-cs

Vi kommer att använda Raylib-cs, som du kan hitta på [NuGet](https://www.nuget.org/packages/Raylib-cs). 

För att börja skapar vi ett vanligt C# konsolprojekt som vi kallar **Grunder**. Sedan lägger vi till Raylib-cs i projektet. Det gör vi genom att använda dessa kommandon i terminalen:

```powershell
dotnet new console
dotnet add package Raylib-cs
```

Nu har vi skapat ett nytt konsolprojekt och lagt till Raylib-cs som en beroende till det projektet.

## Importera Raylib-cs

För att använda Raylib-cs i vår kod måste vi importera det. Detta görs genom att lägga till `using Raylib_cs` högst upp i vår huvudfil, `program.cs`. Här är en enkel kodsnutt som visar hur det kan se ut:

```csharp
using System;
using Raylib_cs;

Console.WriteLine("Hej Raylib!");
```
I den här koden importeras både standardbiblioteket System och Raylib-cs. Vi skriver också ut en hälsning till konsolen.

## Skapa ett grafikfönster

Nästa steg är att skapa ett grafikfönster. Vi kommer att använda Raylib för att skapa ett fönster som är 800x600 pixlar. Vi gör detta genom att kalla på metoden `Raylib.InitWindow()` och ge den bredden, höjden och titeln på fönstret som argument:

```csharp
Raylib.InitWindow(800, 600, "Mitt Raylib fönster");
```
När du kör detta program kommer ett fönster att visas kort och sedan försvinna igen. Detta beror på att programmet avslutas direkt efter att fönstret har skapats.

## Animationsloopen

För att hålla fönstret öppet behöver vi en loop. Detta kallas ofta en "animationsloop" eller "spelloop". Dessutom behöver vi säga till Raylib att vi vill börja rita till skärmen, och sedan rita en bakgrundsfärg. Här är koden för det:

```csharp
// Starta grafikmotorn
Raylib.InitWindow(800, 600, "Mitt Raylib fönster");

// Bestäm skärmuppdatering
Raylib.SetTargetFPS(60);

// Animationsloopen
while (!Raylib.WindowShouldClose())
{
    // Börja rita
    Raylib.BeginDrawing();

    // Töm ritytan
    Raylib.ClearBackground(Color.BEIGE);

    // Ritat ut på fönstret
    Raylib.EndDrawing();
}
```
Nu när du kör programmet kommer fönstret att stanna öppet tills du stänger det manuellt. Fönstret uppdateras 60 gånger per sekund, vilket är inställt av `Raylib.SetTargetFPS(60)`. 

