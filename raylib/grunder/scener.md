# Hur man skapar scener

## Nytt Projekt

För att börja, skapa ett nytt Raylib-projekt och lägg till biblioteket "Raylib_cs" i program.cs.

```powershell
dotnet new console
dotnet add package Raylib-cs
```

## Grundkoden

Vi kommer att använda följande mall för att skapa vårt spel:

```csharp
using System;
using Raylib_cs;

// Initialisering
//-------------------------------------------------------------
const int fönsterB = 800;
const int fönsterH = 600;

Raylib.InitWindow(fönsterB, fönsterH, "raylib [cs]");
Raylib.SetTargetFPS(60);

// TODO: Infoga variabler och objekt här
//------------------------------------------------------------

// Animationsloopen
while (!Raylib.WindowShouldClose())
{
    // Updatering
    //--------------------------------------------------------
    // TODO: Uppdatera variabler här
    //--------------------------------------------------------

    // Rita
    //--------------------------------------------------------
    Raylib.BeginDrawing();
    Raylib.ClearBackground(Color.WHITE);

    Raylib.DrawText("Mitt första fönster!", 190, 200, 20, Color.GRAY);

    Raylib.EndDrawing();
    //-------------------------------------------------------
}
```

## "Game state" variabler

Vi behöver variabler för att hålla koll på hur det går i spelet, t.ex. scen, tid:

```csharp
// Initialisering
//--------------------------------------------------------------------------------------
const int fönsterB = 800;
const int fönsterH = 600;

Raylib.InitWindow(fönsterB, fönsterH, "raylib [cs]");
Raylib.SetTargetFPS(60);

// Game state variabler
float sek = 60;
string scen = "Scen 0";
//--------------------------------------------------------------------------------------
```

## Skapa scener - alternativ 1

Scener används för att organisera olika delar av spelet. I det här fallet ska vi skapa två scener, "Scen 0" och "Slut".

```csharp
// Updatering
//----------------------------------------------------------------------------------
// TODO: Uppdatera variabler här
//----------------------------------------------------------------------------------

// Rita
//----------------------------------------------------------------------------------
Raylib.BeginDrawing();
Raylib.ClearBackground(Raylib.RayWhite);

if (scen == "Scen 0")
{    
    // Rita ut objekt
    Raylib.DrawText(scen, 10, 10, 50, Color.RED);
    
    // Rita ut tiden
    Raylib.DrawText($"Tid {(int)sek}", 600, 0, 50, Color.RED);
}
else if (scen == "Slut")
{
    Raylib.DrawText("Game Over!", 200, 200, 50, Color.RED);
}

Raylib.EndDrawing();
//----------------------------------------------------------------------------------
```

## Timer och scenbyten

Här skapar vi en timer på 60 sekunder som avslutar "Scen 0" och tar oss till "Game Over".

```csharp
if (scen == "Scen 0")
{    
    // Rita ut objekt
    Raylib.DrawText(scen, 10, 10, 50, Color.RED);
    
    // Rita ut tiden
    Raylib.DrawText($"Tid {(int)sek}", 600, 0, 50, Color.RED);

    // Räkna ned tiden k

var
    sek -= Raylib.GetFrameTime();
    if (sek <= 0)
    {
        scen = "Slut";
    }
}
```

## Skapa scener - alternativ 2

Ett annat sätt att organisera scener är att använda en `switch`-sats. Med denna struktur kan du lägga till så många scener du vill och du kan lätt navigera mellan dem:

```csharp
using System;
using Raylib_cs;

public class Program
{
    static void Main()
    {
        // Initialisering
        //-------------------------------------------------------------
        const int fönsterB = 800;
        const int fönsterH = 600;

        Raylib.InitWindow(fönsterB, fönsterH, "Spelet");
        Raylib.SetTargetFPS(60);

        // TODO: Infoga variabler och objekt här
        //------------------------------------------------------------
        string aktuellScen = "intro";

        while (!Raylib.WindowShouldClose())
        {
            switch (aktuellScen)
            {
                case "intro":
                    Raylib.BeginDrawing();
                    Raylib.ClearBackground(Color.RAYWHITE);

                    // Här ritar vi ut introscenen

                    Raylib.EndDrawing();
                    break;

                case "scen1":
                    Raylib.BeginDrawing();
                    Raylib.ClearBackground(Color.RAYWHITE);
                    
                    // Här ritar vi ut spelscenen 1

                    Raylib.EndDrawing();
                    break;

                case "scen2":
                    Raylib.BeginDrawing();
                    Raylib.ClearBackground(Color.RAYWHITE);
                    
                    // Här ritar vi ut spelscenen 2

                    Raylib.EndDrawing();
                    break;

                case "slut":
                    Raylib.BeginDrawing();
                    Raylib.ClearBackground(Color.RAYWHITE);

                    // Här ritar vi ut spelscenen 3

                    Raylib.EndDrawing();
                    break;

                case "gameover":
                    Raylib.BeginDrawing();
                    Raylib.ClearBackground(Color.RAYWHITE);

                    // Här ritar vi ut slutscenen

                    Raylib.EndDrawing();
                    break;

                default:
                    // Hantera fel
                    break;
            }
        }

        Raylib.CloseWindow();
    }

}
```	

## Uppgifter

### Uppgift 8

Lägg till en scen till så att blir: "Scen 0", "Scen 1" och "Slut".\
Spelaren har 60 sekunder i "Scen 0" och 45 sekunder i "Scen 1".

För att göra det, kan du lägga till ytterligare en `case` till `switch`-satsen. Dessutom, du behöver en ny variabel för att hålla koll på tiden för "Scen 1".

### Uppgift 9

Slutför spelets alla scener:

* Välj bilder för spelaren och alien
* Gör ändringar mellan varje scen: ändra färger, byt bilder, ändra hastighet...
* Visa slutpoäng på i slutscenen

För att infoga bilder, behöver du lägga till koden nedan i initialiseringsdelen av spelet. Var noga med att ersätta `"./resurser/alien3.png"` och `"./resurser/ufo3.png"` med sökvägen till dina faktiska bildfiler.

```csharp
// Ladda in bilder
Texture2D alienBild = Raylib.LoadTexture(@"./resur

ser/alien3.png");
Texture2D spelareBild = Raylib.LoadTexture(@"./resurser/ufo3.png");

// Skapa en spelare och en alien
Rectangle spelare = new Rectangle(generator.Next(1, 300), generator.Next(1, 550), spelareBild.width, spelareBild.height);
Rectangle alien = new Rectangle(generator.Next(300, 750), generator.Next(1, 550), alienBild.width, alienBild.height);
```

För att animera objekten, lägg till följande kod i uppdateringsdelen av din game loop. Här checkar vi kollisionen mellan spelaren och alien, och om det är en träff så slumpar vi en ny position för alien och ökar poängen.

```csharp
// Kolla om spelare träffar alien
if (Raylib.CheckCollisionRecs(spelare, alien))
{
    // Slumpa fram en ny position
    alien.x = generator.Next(300, 750);
    alien.y = generator.Next(1, 550);
    poäng++;
}

// Lyssna på tangenter
if (Raylib.IsKeyDown(KeyboardKey.KEY_RIGHT))
{
    spelare.x += hastighet;
}
if (Raylib.IsKeyDown(KeyboardKey.KEY_LEFT))
{
    spelare.x -= hastighet;
}
if (Raylib.IsKeyDown(KeyboardKey.KEY_DOWN))
{
    spelare.y += hastighet;
}
if (Raylib.IsKeyDown(KeyboardKey.KEY_UP))
{
    spelare.y -= hastighet;
}
```

Slutligen, för att rita ut objekten och visa poängen, lägg till följande kod i ritningsdelen av din game loop.

```csharp
// Rita
//----------------------------------------------------------------------------------
Raylib.BeginDrawing();
Raylib.ClearBackground(Color.DARKBLUE);

// Rita ut objekten
Raylib.DrawTexture(alienBild, (int)alien.x, (int)alien.y, Color.WHITE);
Raylib.DrawTexture(spelareBild, (int)spelare.x, (int)spelare.y, Color.WHITE);

Raylib.DrawText($"Poäng {poäng}", 50, 50, 50, Color.ORANGE);

Raylib.EndDrawing();
//----------------------------------------------------------------------------------
```