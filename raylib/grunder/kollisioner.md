# Kollisioner

## Nytt projekt

Skapa ett nytt Raylib-projekt.\
Och `using Raylib_cs` i program.cs.

```powershell
dotnet new console
dotnet add package Raylib-cs
```

### Grundkoden

Vi använder den här mallen:

```csharp
using System;
using Raylib_cs;

// Initiera grafikmotorn
Raylib.InitWindow(800, 600, "Mitt Raylib fönster");
Raylib.SetTargetFPS(60);

/* Skapa objekt */
// ...

// Animationsloopen
while (!Raylib.WindowShouldClose())
{
    /* Rita ut grafiken */
    // Börja rita
    Raylib.BeginDrawing();
    Raylib.ClearBackground(Color.DARKBLUE);

    // Rita ut objekt
    // ...
    
    // Ritat ut på fönstret
    Raylib.EndDrawing();
    
    /* Interaktion med användare */
    // ...
}
```

## Infoga objekten

Vi infogar två objekt, spelaren och fienden:

```csharp
/* Skapa objekt */
// Skapa en spelare och en fiende
Rectangle spelare = new Rectangle(100, 100, 48, 40);
Rectangle fiende = new Rectangle(300, 300, 64, 64);
```

Sen ritar vi ut objekten:

```csharp
// Animationsloopen
while (!Raylib.WindowShouldClose())
{
    /* Rita ut grafiken */
    // Börja rita
    Raylib.BeginDrawing();
    Raylib.ClearBackground(Color.DARKBLUE);

    // Rita ut objekten
    Raylib.DrawRectangleRec(fiende, Color.BLACK);
    Raylib.DrawRectangleRec(spelare, Color.RED);

    // Ritat ut på fönstret
    Raylib.EndDrawing();
    
    /* Interaktion med användaren */
    //     ...
}
```

### Upptäcka kollision

Vi lägger till ett block för att upptäcka kollision mellan objekten:

```csharp
// Animationsloopen
while (!Raylib.WindowShouldClose())
{
    /* Rita ut grafiken */
    // ...

    /* Kollision */
    // Kolla om spelare träffar fiende
    if (Raylib.CheckCollisionRecs(spelare, fiende))
    {
        Raylib.DrawText("Kollision", 100, 50, 50, Color.ORANGE);
    }
    
    /* Interaktion med användaren */
    //     ...
}
```

Resultat blir:

![](../../.gitbook/assets/image-7.png)

## Använda slumpen

För att göra det roligare kan vi slumpa fram positioner.

### Spawna startpositioner

Vi slumpar fram vart spelaren och fiende spawnar fram:

```csharp
static void Main(string[] args)
{
    // Initiera grafikmotorn
    Raylib.InitWindow(800, 600, "Mitt Raylib spel");
    Raylib.SetTargetFPS(60);

    // Skapa slumpgeneratorn
    Random generator = new Random();

    /* Skapa objekt */
    // Skapa en spelare och en alien
    Rectangle spelare = new Rectangle(generator.Next(1, 300), generator.Next(1, 550), 48, 40);
    Rectangle fiende = new Rectangle(generator.Next(300, 750), generator.Next(1, 550), 64, 64);

    ...
```

### Uppgift 6

Efter kollision då spelaren träffat fienden, spawna fienden om slumpmässigt.

## Game state variabel för poäng

Vi vill hålla koll på antalet gånger det blir träff och ge poäng för varje.\
För varje gången kollision sker ökar poängen!

```csharp
static void Main(string[] args)
{
    // Initiera grafikmotorn
    Raylib.InitWindow(800, 600, "Mitt Raylib spel");
    Raylib.SetTargetFPS(60);

    Random generator = new Random();
    int poäng = 0;
    
    // ...
    
        /* Kollision */
        // Kolla om spelare träffar fiende
        if (Raylib.CheckCollisionRecs(spelare, fiende))
        {
            fiende.x = generator.Next(300, 750);
            fiende.y = generator.Next(1, 550);
            poäng++;
        }
```

![](../../.gitbook/assets/image-8.png)

### Uppgift 7

Byt ut rektanglarna med bilder/ikoner.

