# Rita cirklar och text

## Rita cirklar

För att rita cirklar i Raylib, kommer vi att använda `Raylib.DrawCircle(x, y, radie, färg)` funktionen. `(x, y)` är cirkelns mitt, `radie` bestämmer storleken på cirkeln och `färg` bestämmer dess färg.

För att skapa våra egna färger använder vi `new Color(röd, grön, blå, opacitet)`. Värdena för röd, grön och blå kan vara mellan 0 och 255, och opacitet bestämmer genomskinligheten av färgen (255 är helt opak, 0 är helt transparent).

I vårt exempel kommer vi att skapa en gyllene cirkel:

```csharp
// Skapa en guldfärg
Color GoldenRod = new Color(218, 165, 32, 255);

// Här ritar vi ut saker
Raylib.DrawCircle(200, 200, 100, GoldenRod);
```
Efter att ha kört koden borde du se en gyllene cirkel i mitten av fönstret.

## Rita text

För att lägga till text till vår applikation använder vi `Raylib.DrawText(text, x, y, storlek, färg)`. `text` är strängen vi vill rita, `(x, y)` är startpositionen för texten, `storlek` är textens storlek och `färg` bestämmer dess färg.

```csharp
// Ritar ut en text
Raylib.DrawText("En gyllene sol", 400, 100, 40, Color.BLACK);
```
Texten "En gyllene sol" kommer att visas på skärmen i svart färg.

## Snyggare text

Vi kan också använda egna typsnitt för vår text. För att göra det behöver vi först ladda in typsnittet med `Raylib.LoadFont("Roboto-Medium.ttf")`. Kom ihåg att ladda ner och placera typsnittet i samma mapp som din kod.

```csharp
// Ladda in fonten
Font roboto = Raylib.LoadFont("Roboto-Medium.ttf");

// Ritar ut en text med fonten Roboto
Raylib.DrawTextEx(roboto, "En gyllene sol", new Vector2(400, 100), 40, 0, Color.BLACK);
```
Nu kommer "En gyllene sol" att visas med Roboto-typsnitt.

## Uppgift 2

För din andra uppgift vill vi att du skapar ett nytt projekt för att rita Royal Air Force's Ensign flagga:

![](../../.gitbook/assets/image-6.png)

Använd `Color.SKYBLUE` för bakgrundsfärgen. Du kommer att behöva använda både rektanglar och cirklar för att skapa flaggan. Ta en titt på flaggans design och tänk på hur du kan bryta ner den i mindre delar.

## Mer info

För att lära dig mer om att rita text och använda typsnitt i Raylib, besök Raylib's dokumentation [här](https://csharp.progdocs.se/csharp-ref/grafik/raylib/text).