# Raylib

Raylib är ett enkelt och lättanvänt bibliotek för att skapa 2D- och 3D-spel. Det är utvecklat i ren C och kan användas i en mängd olika programmeringsspråk, inklusive C#, genom sitt API. Raylib är känt för sin enkelhet, tydliga syntax och för att det är mycket pedagogiskt.

Ett 2D-bibliotek som Raylib hjälper utvecklare att skapa spel genom att tillhandahålla verktyg för att hantera viktiga spelkomponenter såsom fönsterhantering, grafik, ljud, input (tangenter, mus, kontroller), och mycket mer.

## Skapa ett Raylib-projekt

Att skapa ett Raylib-projekt i .NET är relativt enkelt:

1. Skapa en ny projektmapp. Du kan göra detta via terminalen (kommandotolken eller terminalen i din kod-editor). 

2. Inuti din nya projektmapp, skapa en ny konsolapplikation med kommandot `dotnet new console`. Detta kommer att generera en grundläggande applikationsstruktur med en `Program.cs`-fil och en `.csproj`-fil.

3. Nu behöver du lägga till Raylib-cs-paketet som en beroende till ditt projekt. Detta gör du genom att köra kommandot `dotnet add package Raylib-cs`. 

Dina terminalkommandon kommer att se ut ungefär så här:

```powershell
dotnet new console MittRaylibProjekt
cd MittRaylibProjekt
dotnet add package Raylib-cs
```

## Grundkoden

Här är en grundläggande kodmall för ett Raylib-projekt i C#. Den innehåller en minimal uppsättning instruktioner för att skapa ett spel med Raylib:

```csharp
using Raylib_cs;

// Initiera fönstret
Raylib.InitWindow(512, 480, "Raylib");

// Ange målframerate (uppdateringshastighet)
Raylib.SetTargetFPS(60);

// Skapa och initiera spelobjekt här, som spelare, fiender, poängsystem, osv.

while (!Raylib.WindowShouldClose()) // Spelets huvudloop, körs tills fönstret ska stängas
{
    // Starta rita
    Raylib.BeginDrawing();
    
    // Rensa bakgrunden
    Raylib.ClearBackground(Color.DARKGREEN);

    // Uppdatera och rita spelobjekt här

    // Sluta rita
    Raylib.EndDrawing();
}
```

## Hur Raylib fungerar

Animeringsloopen, även känd som spel-loopen eller huvudloopen, är hjärtat av de flesta interaktiva spel och applikationer. Denna loop körs kontinuerligt under programmets livstid och ansvarar för att uppdatera spelets tillstånd och rita spelets grafik.

Här är en grundläggande struktur av hur en animationsloop kan se ut:

```csharp
while (!Raylib.WindowShouldClose())
{
    // Uppdatera spelets tillstånd
    // ...

    // Ritar spelets grafik
    Raylib.BeginDrawing();
    
    // ... rita här ...

    Raylib.EndDrawing();
}
```

Låt oss gå igenom detta steg för steg.

**1. Loop-kontroll**

Först och främst måste vi ha något som kontrollerar loopen. I det här fallet är det uttrycket `!Raylib.WindowShouldClose()`. Detta returnerar `true` så länge som användaren inte har bett om att stänga programmet (till exempel genom att klicka på krysset på fönstret). Så länge detta uttryck returnerar `true`, kommer loopen att fortsätta köras.

**2. Uppdatera spelets tillstånd**

Detta är platsen där du skulle lägga kod för att uppdatera spelets tillstånd. Detta kan inkludera saker som att uppdatera spelarens position baserat på input, flytta fiender, hantera kollisioner, och så vidare.

**3. Rita spelets grafik**

Efter att du har uppdaterat spelets tillstånd, är det dags att rita spelets aktuella tillstånd till skärmen. All ritningskod bör ligga mellan `Raylib.BeginDrawing()` och `Raylib.EndDrawing()`. Det kan innebära att rita spelaren, fiender, bakgrunder, HUD (Head-Up Display, till exempel poängräknare eller hälsomätare), och allt annat som behöver visas på skärmen.

Denna loop kommer att köra så många gånger per sekund som möjligt (eller enligt en specificerad bildfrekvens om du har ställt in en sådan med `Raylib.SetTargetFPS()`). Varje genomgång av loopen kallas en "frame", och därför kallas processen för att uppdatera spelets tillstånd och rita dess grafik ibland för "framerate".

Det är värt att notera att det är viktigt att balansera mängden arbete som utförs i varje genomgång av loopen. Om du försöker göra för mycket arbete (till exempel genom att utföra mycket komplexa beräkningar eller rita för mycket grafik), kan loopen ta för lång tid att köra, vilket kan leda till att spelet hakar upp sig eller känns långsamt.

## VS Code snippet

VS Code ger dig möjligheten att skapa egna kodfragment, kallade "snippets". Dessa snippets kan snabbt generera kodstrukturer som du ofta använder. Här är en grundläggande snippet för att skapa ett Raylib-fönster i C#:

1. Öppna VS Code.
2. Klicka på `File` -> `Preferences` -> `User Snippets`.
3. Välj `csharp`.
4. Lägg

 till följande kod:

```json
"Raylib grundkod": {
    "prefix": "raylib",
    "body": [
        "Raylib.InitWindow(512, 480, \"Raylib\");",
        "Raylib.SetTargetFPS(60);",
        "\n$1\n",
        "while (!Raylib.WindowShouldClose())",
        "{",
        "\tRaylib.BeginDrawing();",
        "\tRaylib.ClearBackground(Color.WHITE);",
        "\t$2",
        "\tRaylib.EndDrawing();",
        "}"
    ],
    "description": "Grundläggande struktur för ett Raylib-spel i C#"
}
```

När du har skapat denna snippet, kan du skriva `raylib` i en C#-fil, och sedan trycka `Tab` eller `Enter` för att automatiskt generera koden ovan. `$1` och `$2` är platser där din markör kommer att flyttas till när du trycker `Tab` efter att ha infogat koden. Du kan ersätta dessa med lämplig kod för ditt spel.

## Raylib-dokumentation

Här är några bra resurser att hänvisa till när du arbetar med Raylib och C#:

1. **Raylib officiella webbplats**: [Raylib.com](https://www.raylib.com/)
   - Här kan du hitta allt från grundläggande tutorials till avancerade exempel. Raylib-dokumentationen är mycket omfattande och lätt att förstå.

2. **Raylib GitHub-repository**: [github.com/raysan5/raylib](https://github.com/raysan5/raylib)
   - Det här är källkoden för Raylib. Om du vill veta exakt hur ett specifikt API-funktion fungerar, kan du söka efter den i detta repo.

3. **Raylib-cs GitHub-repository**: [github.com/ChrisDill/Raylib-cs](https://github.com/ChrisDill/Raylib-cs)
   - Detta är C#-bindningen för Raylib. Om du vill använda Raylib med C#, behöver du detta paket. Det är också bra att kolla i detta repo för exempel på hur man använder Raylib med C#.

4. **Raylib-cs NuGet-paket**: [nuget.org/packages/Raylib-cs/](https://www.nuget.org/packages/Raylib-cs/)
   - Detta är paketet du lägger till i ditt .NET-projekt för att kunna använda Raylib.
