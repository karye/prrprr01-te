# Space Invaders

## Steg 1: Skapa ett fönster

Först, vi behöver skapa ett fönster där vårt spel kommer att köras. Raylib ger oss ett antal metoder för att göra detta:

```csharp
using Raylib_cs;

class Program
{
    static void Main(string[] args)
    {
        const int fönsterBredd = 800;
        const int fönsterHöjd = 600;

        // Starta ett fönster med specificerad bredd och höjd.
        Raylib.InitWindow(fönsterBredd, fönsterHöjd, "Space Invaders");

        // Huvudslingan för spelet. Fortsätter tills fönstret stängs.
        while (!Raylib.WindowShouldClose())
        {
            Raylib.BeginDrawing();
            Raylib.ClearBackground(Color.BLACK);
            // Spellogik och ritning kommer här
            Raylib.EndDrawing();
        }
        
        // Stänga fönstret och OpenGL-kontexten
        Raylib.CloseWindow();
    }
}
```

## Steg 2: Skapa Spelaren**

Nu vill vi lägga till en spelare i botten av skärmen. Spelaren kommer att vara en rektangel som vi kan röra sig åt vänster och höger. Vi använder `Rectangle`-strukturen från Raylib för att representera spelaren. 

```csharp
// Skapa en rektangel för spelaren
Rectangle spelare = new Rectangle(fönsterBredd / 2, fönsterHöjd - 20, 60, 10);
Color spelareFärg = Color.GREEN;

// Rita spelaren
Raylib.DrawRectangleRec(spelare, spelareFärg);
```

## Steg 3: Skapa Spelarens Rörelse

Vi vill kunna röra spelaren åt vänster och höger. För att göra detta skapar vi en metod `UppdateraSpelare()` som tar in spelarens rektangel och uppdaterar `x`-positionen baserat på användarens input.

```csharp
static void UppdateraSpelare(ref Rectangle spelare, int hastighet, int fönsterBredd)
{
    if (Raylib.IsKeyDown(KeyboardKey.KEY_LEFT) && spelare.x > 0)
    {
        spelare.x -= hastighet;
    }
    if (Raylib.IsKeyDown(KeyboardKey.KEY_RIGHT) && spelare.x < fönsterBredd - spelare.width)
    {
        spelare.x += hastighet;
    }
}
```

Denna metod bör kallas i varje iteration av spelets huvudslinga.

Bra fråga, låt oss ordna kodstrukturen i `Main`-metoden. Den kod som skapas för spelaren behöver placeras på rätt ställe i vår huvudslinga.

```csharp
using Raylib_cs;

class Program
{
    static void Main(string[] args)
    {
        const int fönsterBredd = 800;
        const int fönsterHöjd = 600;
        const int hastighet = 5;

        Raylib.InitWindow(fönsterBredd, fönsterHöjd, "Space Invaders");

        // Skapa en rektangel för spelaren
        Rectangle spelare = new Rectangle(fönsterBredd / 2, fönsterHöjd - 20, 60, 10);
        Color spelareFärg = Color.GREEN;

        // Huvudslingan för spelet
        while (!Raylib.WindowShouldClose())
        {
            // Uppdatera spelaren
            UppdateraSpelare(ref spelare, hastighet, fönsterBredd);
            
            Raylib.BeginDrawing();
            
            Raylib.ClearBackground(Color.BLACK);

            // Rita spelaren
            Raylib.DrawRectangleRec(spelare, spelareFärg);
            
            Raylib.EndDrawing();
        }
        
        Raylib.CloseWindow();
    }

    static void UppdateraSpelare(ref Rectangle spelare, int hastighet, int fönsterBredd)
    {
        if (Raylib.IsKeyDown(KeyboardKey.KEY_LEFT) && spelare.x > 0)
        {
            spelare.x -= hastighet;
        }
        if (Raylib.IsKeyDown(KeyboardKey.KEY_RIGHT) && spelare.x < fönsterBredd - spelare.width)
        {
            spelare.x += hastighet;
        }
    }
}
```

Här är vad vi gör:

1. Efter att vi har skapat fönstret, definierar vi `spelare` och `spelareFärg`-variablerna.

2. Inuti spelets huvudslinga, innan `Raylib.BeginDrawing()`, kallar vi `UppdateraSpelare()` för att ändra spelarens position baserat på användarinput.

3. Mellan `Raylib.BeginDrawing()` och `Raylib.EndDrawing()`, ritar vi ut spelaren med `Raylib.DrawRectangleRec()`.

Det är viktigt att uppdatera spelarens position innan vi börjar rita, så att vi ritar ut spelaren på den uppdaterade positionen. 

Observera att vi använder `ref` nyckelordet när vi skickar `spelare` till `UppdateraSpelare()`. Detta är för att vi vill ändra spelarens faktiska värden, inte bara kopior av dem.

Självklart, låt oss dyka djupare in i `UppdateraSpelare`-metoden.

```csharp
static void UppdateraSpelare(ref Rectangle spelare, int hastighet, int fönsterBredd)
{
    if (Raylib.IsKeyDown(KeyboardKey.KEY_LEFT) && spelare.x > 0)
    {
        spelare.x -= hastighet;
    }
    if (Raylib.IsKeyDown(KeyboardKey.KEY_RIGHT) && spelare.x < fönsterBredd - spelare.width)
    {
        spelare.x += hastighet;
    }
}
```

Denna metod tar tre argument:

- `ref Rectangle spelare`: Här är `spelare` ett objekt av typen `Rectangle` som representerar vår spelare på skärmen. `ref` nyckelordet används för att säkerställa att vi faktiskt ändrar den ursprungliga `spelare`-variabeln, inte bara en kopia av den.

- `int hastighet`: Detta är hastigheten som spelaren rör sig när vi trycker på höger- eller vänsterpil.

- `int fönsterBredd`: Detta är bredden på vårt spelvindu. Vi behöver denna för att förhindra att spelaren rör sig utanför fönstret.

I metoden kontrollerar vi om vänster- eller högerpiltangenten är nedtryckt med hjälp av `Raylib.IsKeyDown(KeyboardKey.KEY_LEFT)` eller `Raylib.IsKeyDown(KeyboardKey.KEY_RIGHT)`.

Om vänsterpiltangenten är nedtryckt och spelaren inte redan är på den vänstra kanten av skärmen (det vill säga `spelare.x > 0`), subtraherar vi `hastighet` från `spelare.x`. Detta flyttar spelaren till vänster.

Om högerpiltangenten är nedtryckt och spelaren inte redan är på den högra kanten av skärmen (det vill säga `spelare.x < fönsterBredd - spelare.width`), adderar vi `hastighet` till `spelare.x`. Detta flyttar spelaren till höger.

Exempel: 

Antag att vårt fönster är 800 pixlar brett, och spelaren är en rektangel som är 60 pixlar bred. Om spelaren för närvarande är vid `x`-position 400 (mitten av fönstret), och högerpiltangenten är nedtryckt, skulle `spelare.x` öka med `hastighet`. Om `hastighet` är 5, skulle `spelare.x` bli 405. Om vi sedan ritar spelaren igen skulle den vara 5 pixlar till höger än tidigare.

På samma sätt, om spelaren var vid `x`-position 10 och vänsterpiltangenten var nedtryckt, skulle `spelare.x` minska med `hastighet` till 5, och spelaren skulle ritas 5 pixlar till vänster. Men om spelaren var vid `x`-position 0 och vänsterpiltangenten var nedtryckt, skulle `spelare.x` inte ändras alls, eftersom `spelare.x > 0`-kontrollen skulle

 förhindra det. På detta sätt kan vi förhindra att spelaren går utanför vänsterkanten av skärmen. Ett liknande resonemang gäller för att förhindra att spelaren går utanför högerkanten av skärmen.

 Självklart, låt oss börja med att skapa en enskild fiende och animera den.

## Steg 5: Skapa en Fiende

För att skapa en fiende behöver vi definiera en rektangel och en färg för fienden. Vi kan göra det precis som vi gjorde för spelaren.

```csharp
Rectangle fiende = new Rectangle(fönsterBredd / 2, 10, 40, 20);
Color fiendeFärg = Color.RED;
```

Vi placerar fienden i mitten av skärmen, 10 pixlar ner från toppen. Vi gör fienden 40 pixlar bred och 20 pixlar hög.

För att rita fienden, vi kommer att lägga till följande kod mellan `Raylib.BeginDrawing()` och `Raylib.EndDrawing()`:

```csharp
Raylib.DrawRectangleRec(fiende, fiendeFärg);
```

Nu borde du se en röd rektangel i toppen av skärmen.

## Steg 6: Animerar Fienden

För att animerar fienden, vi kommer att lägga till kod i spelets huvudslinga som ändrar `fiende.x` (fiendens horisontella position) för varje iteration av slingan. Detta kommer att få fienden att röra sig åt sidan.

```csharp
const int fiendeHastighet = 2;

// I huvudslingan, före BeginDrawing()
fiende.x += fiendeHastighet;

if (fiende.x > fönsterBredd - fiende.width || fiende.x < 0)
{
    fiendeHastighet = -fiendeHastighet;
    fiende.y += fiende.height;
}
```

Här lägger vi till `fiendeHastighet` till `fiende.x` för varje iteration av slingan. Detta får fienden att röra sig åt höger. 

Vi kontrollerar också om fienden har nått kanten av skärmen med `if (fiende.x > fönsterBredd - fiende.width || fiende.x < 0)`. Om fienden har nått kanten av skärmen, byter vi riktning på `fiendeHastighet` (genom att multiplicera den med -1) och flyttar fienden nedåt.

Nu borde du se en fiende som rör sig åt sidan och nedåt när den når kanten av skärmen.



## Steg 7: Skapa Flera Fiender

Vi kan skapa flera fiender genom att skapa en array av `Rectangle`-objekt. Låt oss skapa en array med 10 fiender:

```csharp
const int antalFiender = 10;
Rectangle[] fiender = new Rectangle[antalFiender];
```

Här är `antalFiender` konstanten som definierar antalet fiender i spelet. `fiender` är en array som innehåller `antalFiender` rektanglar.

För att placera varje fiende på skärmen, kan vi använda en loop:

```csharp
const int fiendeBredd = 40;
const int fiendeHöjd = 20;

for (int i = 0; i < antalFiender; i++)
{
    fiender[i] = new Rectangle(i * (fiendeBredd + 10), 10, fiendeBredd, fiendeHöjd);
}
```

I detta exempel startar vi vår loop med `i` lika med 0, och fortsätter tills `i` är mindre än `antalFiender`. För varje iteration av loopen, skapar vi en ny `Rectangle` och lagrar den i `fiender[i]`.

Vi placerar varje fiende bredvid varandra genom att multiplicera `i` med `(fiendeBredd + 10)`. Detta flyttar varje nästa fiende 10 pixlar till höger från föregående fiende.

Nu har vi skapat och placerat våra fiender på skärmen. Men vi måste också rita dem.

## Steg 8: Rita Flera Fiender

För att rita våra fiender, vi kan använda en liknande loop inuti `Raylib.BeginDrawing()` och `Raylib.EndDrawing()`:

```csharp
Color fiendeFärg = Color.RED;

for (int i = 0; i < antalFiender; i++)
{
    Raylib.DrawRectangleRec(fiender[i], fiendeFärg);
}
```

Nu borde du se en rad med 10 röda fiender längst upp på skärmen. 

Detta är hur vi kan skapa och rita flera fiender med hjälp av en array och loopar. Nästa steg skulle vara att animera dessa fiender, vilket vi kan göra med hjälp av en liknande strategi som vi använde för att animera en enskild fiende.

## Steg 9: Animera Flera Fiender

Precis som vi har animerat en enda fiende tidigare, kan vi också animera alla fienderna i vår array med hjälp av en loop. Vi vill att alla fienderna ska röra sig åt sidan tills den yttersta fienden träffar kanten av skärmen, då de alla ska byta riktning och flytta neråt. För att uppnå detta kommer vi att skapa en variabel `fiendeHastighet` och uppdatera positionen för varje fiende i vår huvudslinga:

```csharp
int fiendeHastighet = 2;

// I huvudslingan, före BeginDrawing()
for (int i = 0; i < antalFiender; i++)
{
    fiender[i].x += fiendeHastighet;

    if (fiender[i].x > fönsterBredd - fiendeBredd || fiender[i].x < 0)
    {
        fiendeHastighet = -fiendeHastighet;
        for (int j = 0; j < antalFiender; j++)
        {
            fiender[j].y += fiendeHöjd;
        }
        break;
    }
}
```

Här går vi igenom varje fiende och ökar dess `x`-position baserat på `fiendeHastighet`. Om någon av fienderna träffar sidorna av skärmen, vänder vi på `fiendeHastighet` (genom att multiplicera den med -1) och flyttar alla fienderna en rad ner.

## Steg 10: Skapa Projektiler

Nu när vi har vår spelare och fiender, behöver vi en metod för spelaren att interagera med fienderna. För detta kommer vi att skapa en projektil som spelaren kan avfyra. Precis som tidigare, kommer vi att representera vår projektil som en rektangel.

```csharp
Rectangle projektil = new Rectangle(spelare.x + spelare.width / 2, spelare.y, 5, 10);
Color projektilFärg = Color.GREEN;
bool projektilAktiv = false;
```

Vi börjar med att placera vår projektil i mitten av spelaren. `projektilAktiv` är en boolean som berättar om projektilen för närvarande är aktiv (dvs. om den flyger uppåt) eller inte.

För att avfyra en projektil behöver vi lägga till några regler i vår huvudslinga:

```csharp
// I huvudslingan, före BeginDrawing()
if (Raylib.IsKeyPressed(KeyboardKey.KEY_SPACE) && !projektilAktiv)
{
    projektil.x = spelare.x + spelare.width / 2;
    projektil.y = spelare.y;
    projektilAktiv = true;
}

if (projektilAktiv)
{
    projektil.y -= 10;
    if (projektil.y < 0)
    {
        projektilAktiv = false;
    }
}

Raylib.DrawRectangleRec(projektil, projektilAktiv ? projektilFärg : Color.BLANK);
```

Här gör vi så att när spelaren trycker på mellanslag, om projektilen inte redan är aktiv, startas projektilen från spelarens position och blir aktiv. Om projektilen är aktiv, rör den sig uppåt. Om projektilen når toppen av skärmen, blir den inaktiv.

Nu när vi har lagt till allt detta, har vi ett grundläggande Space Invaders-spel! Du kan flytta spelaren åt sidorna och avfyra en projektil för att försöka träffa fienderna. Vi har dock inte lagt till någon kollisionsdetektering, så projektilen passerar bara igenom fienderna just nu. Det skulle vara nästa steg för att utveckla detta spel vidare.