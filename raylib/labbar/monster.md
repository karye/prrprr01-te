# Metoder som ändrar 2:2

## Starta projektet "JagaMonster-3"

För att komma igång med projektet "JagaMonster-3", följ dessa steg:

1. Skapa en ny projektmapp med namnet **JagaMonster-3**.
2. Öppna ett terminalfönster och navigera till den nya mappen.
3. Skapa ett nytt Raylib-projekt genom att köra följande kommandon:

```powershell
dotnet new console
dotnet add package Raylib-cs
```

4. Skapa en mapp i ditt projekt med namnet **resurser**.
5. Lägg de bilder som ska användas i spelet i denna mapp. 

I detta projekt kommer vi att bygga vidare på koden från det tidigare projektet.

## Infoga ett monster i spelet

För att införa ett monster i spelet behöver vi först definiera några variabler för monstret. Dessa inkluderar `monsterTex`, som innehåller texturinformation för monstret, och `monsterRec`, som bestämmer var på skärmen monstret kommer att ritas ut.

Efter att ha definierat dessa variabler kan vi skapa metoder som ritar ut monstret och placerar det slumpmässigt på skärmen.

### Skapa figurer och lägg till dem på skärmen

```csharp
// Skapa figurer
heroRec = SpawnaHero(heroRec);
monsterRec = SpawnaMonster(monsterRec);

// Animationsloopen
while (!Raylib.WindowShouldClose())
{
    // Starta ritning
    Raylib.BeginDrawing();
    Raylib.ClearBackground(Color.WHITE);

    // Rita bakgrund
    RitaBakgrund(bakgrundTex);

    // Rita ut objekt
    RitaHus(husTex);
    RitaVatten(vattenTex);

    // Rita ut figurer
    RitaHero(heroTex, heroRec);
    RitaMonster(monsterTex, monsterRec);

    // Animera figurer
    heroRec = AnimeraHero(heroRec);
    monsterRec = AnimeraMonster(monsterRec);

    // Avsluta ritning
    Raylib.EndDrawing();
}
```

### Variabler för monster och dess rörelse

```csharp
// Variabler för monstret
Texture2D monsterTex = Raylib.LoadTexture(@"resurser/monster.png");
Rectangle monsterRec = new Rectangle(10, 60, monsterTex.width, monsterTex.height);
int monsterVx = generator.Next(1, 5);
int monsterVy = generator.Next(1, 5);
```

### Animera monstret

Monstret kommer att röra sig med en konstant hastighet som definieras av `monsterVx` och `monsterVy`.

```csharp
// Monstret rör sig med en konstant hastighet
monsterRec.x += monsterVx;
monsterRec.y += monsterVy;
```

## Begränsa monstrets rörelse till spelplanen

För att förhindra att monstret rör sig utanför spelplanen, kan vi justera dess hastighetsvariabler, `monsterVx` och `monsterVy`, så att de ändrar riktning när de når kanten av spelplanen.

```csharp
static (Rectangle, int, int) AnimeraMonster(Rectangle monsterRec, int vx, int vy

)
{
    // Förflyttning
    monsterRec.x += vx;
    monsterRec.y += vy;

    // Ändrar monstret riktning om det hamnar utanför spelplanen
    if (monsterRec.x < 32)
    {
        monsterRec.x -= vx; // Backa tillbaka
        vx = -vx;
    }
    return (monsterRec, vx, vy);
}
```

## Införa poäng och liv i spelet

Det är dags att göra spelet mer utmanande genom att införa konceptet med poäng och liv. Spelarens poäng kan öka när de lyckas undvika monstret, och antalet liv kan minska när spelaren kolliderar med monstret. Vi kan skapa variabler för att hålla koll på spelarens poäng och liv.

## Uppgifter

För att utöka spelets funktionalitet kan du genomföra följande uppgifter:

### Uppgift 1

Skapa en metod som håller koll på och uppdaterar spelarens poäng och liv. Denna metod kan kallas varje gång spelaren interagerar med monstret eller undviker en kollision.

### Uppgift 2

Lägg till fler monster i spelet. Varje nytt monster ska placeras på en slumpmässig plats på skärmen och röra sig med en slumpmässig hastighet. Detta kommer att göra spelet mer utmanande och underhållande.

### Uppgift 3

Införa ljud och musik för att förbättra spelupplevelsen. Du kan använda Raylibs metoder för att spela upp ljud när spelaren tjänar poäng, förlorar liv, eller när ett monster dyker upp på skärmen.