# Metoder som ändrar 1:2

## Resultat

Det förväntade resultatet efter att ha fullföljt alla steg i detta avsnitt:
![](../../.gitbook/assets/image-18.png)

### Nytt projekt JagaMonster-2

* Skapa en ny projektmapp som heter **JagaMonster-2**.
* Inuti den mappen, initiera ett nytt Raylib-projekt med dessa kommandon:

```powershell
dotnet new console
dotnet add package Raylib-cs
```

### Bilder

* Skapa en ny undermapp som heter **resurser**.
* Flytta de bilder du vill använda i spelet till den mappen.

## Koden

Vi börjar med koden från föregående avsnitt:

```csharp
static void Main(string[] args)
{
    Console.WriteLine("Hello objekt!");

    // Initiera Raylib
    Raylib.InitWindow(512, 480, "Jaga monster");
    Raylib.SetTargetFPS(30);

    /** Initiering **/
    // Läs in bilder
    Texture2D bakgrundTex = Raylib.LoadTexture(@"resurser/background.png");
    Texture2D husTex = Raylib.LoadTexture(@"resurser/house.png");
    Texture2D vattenTex = Raylib.LoadTexture(@"resurser/pond.png");

    // Animationsloopen
    while (!Raylib.WindowShouldClose())
    {
        // Starta ritning
        Raylib.BeginDrawing();
        Raylib.ClearBackground(Color.WHITE);

        /** Rita ut i fönstret **/
        // Rita bakgrund
        RitaBakgrund(bakgrundTex);

        // Rita ut hus och sjö
        RitaHus(husTex);
        RitaVatten(vattenTex);

        // Stäng av ritning
        Raylib.EndDrawing();
    }
}
```

### Infoga figuren hero

För att introducera en spelbar karaktär, "hero", i spelet, kommer vi att göra följande:

* Ladda in bilden:

```csharp
/** Initialisering **/
// Läs in figuren hero
Texture2D heroTex = Raylib.LoadTexture(@"resurser/hero.png");
```

* Skapa en kollisionsrektangel:

```csharp
/** Initialisering **/
...
Rectangle heroRec = new Rectangle(10, 60, heroTex.width, heroTex.height);
```

* Och rita ut den i animationsloopen med hjälp av en metod:

```csharp
/** Rita ut i fönstret **/
// Rita ut figuren "hero"
RitaHero(heroRec);
```

## Metoder

### Skapa en metod

Vi skapar en separat metod för att rita ut "hero":

```csharp
class Program
{
    static void Main(string[] args)
    {
        Console.WriteLine("Hej objekt!");
        ...
    }
    
    /// <summary>
    /// Rita ut hero
    /// </summary>
    static void RitaHero(Texture2D heroTex, Rectangle heroRec)
    {
        Raylib.DrawTexture(heroTex, (int)heroRec.x, (int)heroRec.y, Color.WHITE);
    }
}
```

### Använda metoden

Vi använder sedan metoden **RitaHero()** i animationslo

open:

```csharp
class Program
{
    static void Main(string[] args)
    {
        Console.WriteLine("Hello objekt!");
        ...

        // Animationsloopen
        while (!Raylib.WindowShouldClose())
        {
            // Starta ritning
            Raylib.BeginDrawing();
            Raylib.ClearBackground(Color.WHITE);

            /** Rita ut i fönstret **/
            // Rita ut bakgrund
            RitaBakgrund(bakgrundTex);
            
            // Rita ut hus och sjö
            RitaHus(husTex);
            RitaVatten(vattenTex);

            // Rita ut figuren "hero"
            RitaHero(heroTex, heroRec);

            // Stäng av ritning
            Raylib.EndDrawing();
        }
    }
}
```

## Slumpa spawn positionen

Som det är nu spawnas figuren "hero" på position x=0, y=0 varje gång spelet startas. Vi kan göra detta mer dynamiskt genom att använda en slumpgenerator för att bestämma "hero":s startposition.

```csharp
class Program
    {
        static Random generator = new Random();

        static void Main(string[] args)
        {
        ...
        }
    }
```

### Placering på spelplanen

Så här kan det se ut när "hero" spawnas på en slumpmässig position:

![](../../.gitbook/assets/image-20.png)

### Metod som returnerar ett värde

Vi skapar en metod **SpawnaHero()**, som slumpar fram en position för x och y:

```csharp
static Rectangle SpawnaHero(Rectangle heroRec)
{
    heroRec.x = generator.Next(32, 485);
    heroRec.y = generator.Next(32, 450);
    return heroRec;
}
```

OBS! Eftersom vi ändrar på `heroRec`, måste vi returnera den.

Vi kör sedan metoden i början av spelet:

```csharp
    /** Initialisering **/
    ...
    
    // Spawna figuren "hero"
    heroRec = SpawnaHero(heroRec);
    
    // Animationsloopen
    while (!Raylib.WindowShouldClose())
    {
    ...
    }
```

## Styra figuren hero

Vi vill att spelaren ska kunna styra figuren "hero" med piltangenterna. Till detta behöver vi en metod. Eftersom "hero":s position ändras när vi trycker på piltangenterna, måste metoden returnera de nya positionsvärdena.

```csharp
static Rectangle AnimeraHero(Rectangle heroRec)
    {
        // Hämta tangentbordets inmatning
        if (Raylib.IsKeyDown(KeyboardKey.KEY_LEFT))
        {
            heroRec.x -= 5;
        }
        if (Raylib.IsKeyDown(KeyboardKey.KEY_RIGHT))
        {
            heroRec.x += 5;
        }
        if (Raylib.IsKeyDown(KeyboardKey.KEY_UP))
        {
            heroRec.y -= 5;
        }
        if (Raylib.IsKeyDown(KeyboardKey.KEY_DOWN))
        {
            heroRec.y += 5;
        }

        // Returna rektangeln med de ändrade värdena
        return heroRec;
    }
```

Metoden **AnimeraHero()** lägger vi till i animationsloopen:

```csharp
// Animationslo

open
while (!Raylib.WindowShouldClose())
{
    ...

    // Animera figuren "hero"
    heroRec = AnimeraHero(heroRec);

    // Stäng av ritning
    Raylib.EndDrawing();
}
```

### Begränsa rörelse till spelplanen

Vi vill förhindra att "hero" rör sig utanför spelplanen, dvs:

* När x < 32px
* När x > 485px
* När y < 32px
* När y > 450px

#### Vänsterkanten

![](../../.gitbook/assets/image-21.png)

Vi lägger till dessa villkor en efter en, börjar med när "hero" försöker röra sig åt vänster:

```csharp
// Hämta tangentbordets inmatning
if (Raylib.IsKeyDown(KeyboardKey.KEY_LEFT))
{
    // Flytta ett steg till vänster
    hero.rec.x -= 5;

    // Om figuren hamnar för nära kanten av skärmen, ska den flytta tillbaka
    if (hero.rec.x < 32)
    {
        hero.rec.x += 5;
    }
}
```

Här är 32px för att inte låta "hero" gå in i trädkanten och 5px är den steglängd som vi har valt.

#### Högerkanten

![](../../.gitbook/assets/image-22.png)

Högerkanten ligger vid 485px, men vi måste också räkna in figurens bredd på 32px:

```csharp
if (Raylib.IsKeyDown(KeyboardKey.KEY_RIGHT))
{
    // Flytta ett steg till höger
    hero.rec.x += 5;

    // Om figuren hamnar för nära kanten av skärmen, ska den flytta tillbaka
    if (hero.rec.x > 485 - 32)
    {
        hero.rec.x -= 5;
    }
}
```

## Uppgifter

### Uppgift 1

* Utöka programmet så att figuren "hero" inte kan gå utanför toppkanten och bottenkanten av spelplanen. Använd samma princip som du använde för vänster- och högerkanten.

![](../../.gitbook/assets/image-23.png)

### Uppgift 2

* Lägg till en skatt på spelplanen, placerad slumpmässigt varje gång spelet startas. Använd en bild för skatten - du kan hitta passande bilder på [flaticon.com](https://www.flaticon.com/search?word=treasure). Utveckla spelet ytterligare genom att låta "hero" samla skatten och ge poäng för detta.