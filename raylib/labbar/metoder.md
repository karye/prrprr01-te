# Metoder som ritar

## Resultat

Det förväntade resultatet efter att ha fullföljt alla steg i detta avsnitt:
![](../../.gitbook/assets/image-24.png)


### Nytt projekt JagaMonster-1

* Börja med att skapa en ny projektmapp som heter **JagaMonster-1**.
* Inuti den mappen, initiera ett nytt Raylib-projekt med dessa kommandon:

```powershell
dotnet new console
dotnet add package Raylib-cs
```

### Bilder

* Skapa en ny undermapp som heter **resurser**.
* Flytta de bilder du vill använda i spelet till den mappen.

## Koden

Grundkoden för att starta **Raylib** ser ut så här:

```csharp
// Initiering av Raylib
Raylib.InitWindow(512, 480, "Jaga monster");
Raylib.SetTargetFPS(30);

/** Initiering **/
...

// Animationsloopen
while (!Raylib.WindowShouldClose())
{
    // Starta ritning
    Raylib.BeginDrawing();
    Raylib.ClearBackground(Color.WHITE);
    
    /** Rita ut i fönstret **/
    ...

    // Stäng av ritning
    Raylib.EndDrawing();
}
```

### Infoga bilder

* Ladda in bilden:

```csharp
/** Initiering **/
// Ladda bakgrunden
Texture2D bakgrundTex = Raylib.LoadTexture(@"resurser/background.png");
```

* Sedan ritar vi ut bilden i animationsloopen med hjälp av en metod:

```csharp
/** Rita ut i fönstret **/
// Rita ut bakgrund
RitaBakgrund(bakgrundTex);
```

## Metoder

### Skapa en metod

Vi skapar en separat metod för att rita ut bakgrunden:

```csharp
// Some code
class Program
{
    static void Main(string[] args)
    {
        Console.WriteLine("Hello objekt!");
        ...
    }
    
    /// <summary>
    /// Rita ut bakgrunden
    /// </summary>
    static void RitaBakgrund(Texture2D texture)
    {
        // Rita ut bilden från position x=0, y=0
        Raylib.DrawTexture(texture, 0, 0, Color.WHITE);
    }
}
```

### Använda metoden

Vi använder sedan metoden **RitaBakgrund()** i animationsloopen:

```csharp
class Program
{
    static void Main(string[] args)
    {
        Console.WriteLine("Hello objekt!");
        ...

        // Animationsloopen
        while (!Raylib.WindowShouldClose())
        {
            // Starta ritning
            Raylib.BeginDrawing();
            Raylib.ClearBackground(Color.WHITE);

            // Rita bakgrund
            RitaBakgrund(bakgrundTex);

            // Stäng av ritning
            Raylib.EndDrawing();
        }
    }

    /// <summary>
    /// Rita ut bakgrunden
    /// </summary>
    static void RitaBakgrund(Texture2D texture)
    {
        Raylib.DrawTexture(texture, 0, 0, Color.WHITE);
    }
}
```

## Uppgifter

Nu när vi har grundstrukturen för vårt spel, låt oss utö

ka det med några mer komplexa funktioner.

### Uppgift 1

* Ladda ner en bild av ett hus från [https://www.flaticon.com](https://www.flaticon.com/premium-icon/house\_2163350?term=house\&page=1\&position=7\&page=1\&position=7\&related\_id=2163350\&origin=search). Välj storlek 32px.
* Skapa en metod som heter **RitaHus()**.
* Använd metoden för att rita ut huset vid positionen x=400, y=400.

Bilden visar hur spelet ska se ut efter att ha slutfört Uppgift 1:
![](../../.gitbook/assets/image-25.png)

### Uppgift 2

* Ladda ner en bild av en [sjö](https://www.flaticon.com/free-icon/pond\_4251896?term=lake\&page=1\&position=10\&page=1\&position=10\&related\_id=4251896\&origin=search).
* Skapa en metod som heter **RitaVatten()**.
* Använd metoden för att rita ut sjön vid positionen x=300, y=100.

Bilden visar hur spelet ska se ut efter att ha slutfört Uppgift 2:
![](../../.gitbook/assets/image-26.png)

### Uppgift 3

* Fundera på om man kan slå ihop alla tre metoderna **Rita..()** till en. Kan du hitta ett sätt att generalisera metoden så att den kan användas för att rita vilken bild som helst vid vilken position som helst? Alla tre metoderna är 99% lika.
