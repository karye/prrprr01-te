---
description: Innehåll i kursen programmering 1
---

# Kursinnehåll

## Centralt innehåll

### Undervisningen i kursen ska behandla följande centrala innehåll:

* **Grundläggande programmering** i ett eller flera programspråk varav minst ett av språken är textbaserat.
* Programmering och dess olika användningsområden ur ett socialt perspektiv inklusive genus, kultur och socioekonomisk bakgrund.
* Programmeringens möjligheter och begränsningar utifrån datorns funktionssätt.
* **Strukturerat** arbetssätt för problemlösning och programmering.
* Grundläggande **kontrollstrukturer**, konstruktioner och datatyper.
* Arbetsmetoder för förebyggande av programmeringsfel, testning, felsökning och rättning av kod.
* Grundläggande **datastrukturer** och **algoritmer**.
* Gränssnitt för **interaktion** mellan program och användare.
* Normer och värden inom programmering, till exempel läsbarhet, dokumentation, testbarhet, rena gränssnitt och nyttan av standard.

## Konkretisering

### Kod

* [ ] Inmatning och utmatning
* [ ] Kommentarer
* [ ] Variabler och datatyper
* [ ] Villkor
* [ ] Loopar
* [ ] Listor & arrayer
* [ ] Metoder

### Teori

* [ ] Historia
* [ ] Begrepp
* [ ] “Goda vanor” – principer och tekniker
* [ ] Problemlösning
* [ ] Felsökning
* [ ] Planering
* [ ] Algoritmer
* [ ] Samhällsperspektiv

