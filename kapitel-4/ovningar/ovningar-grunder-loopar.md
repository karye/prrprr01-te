# Övning: grunder med loopar, inmatning och variabler 1:2

Den här övningen innehåller små och enkla steg för att träna på att läsa in tal, använda loopar, och jobba med variabler och textutmatning. Följ varje steg i ordning och testa att koden fungerar innan du går vidare.

## 1. Läs in ett tal och skriv ut det
Läs in ett tal från användaren och skriv ut samma tal.

### Instruktioner:
1. Använd `Console.ReadLine` för att läsa in talet.
2. Omvandla texten till ett heltal med `int.Parse`.
3. Skriv ut talet med `Console.WriteLine`.

### Exempel på körning:
```
Ange ett tal: 15
Du angav: 15
```

## 2. Läs in ett tal 5 gånger
Läs in ett tal från användaren 5 gånger och skriv ut varje tal direkt.

### Instruktioner:
1. Skapa en **for-loop** som kör 5 gånger.
2. Läs in ett tal från användaren i varje loopvarv.
3. Skriv ut talet direkt efter inmatningen.

### Tips:
- En for-loop som kör 5 gånger:
```csharp
for (int i = 1; i <= 5; i++)
{
    // Kod här
}
```

### Exempel på körning:
```
Ange ett tal: 3
Du angav: 3
Ange ett tal: 7
Du angav: 7
Ange ett tal: 12
Du angav: 12
Ange ett tal: 5
Du angav: 5
Ange ett tal: 9
Du angav: 9
```

## 3. Skriv ut alla tal från 10 till 20
Skriv ut alla tal från 10 till 20 med hjälp av en for-loop.

### Instruktioner:
1. Skapa en for-loop som startar på 10 och slutar på 20.
2. Skriv ut varje tal med `Console.WriteLine`.

### Tips:
- Starta loopen med `i = 10` och sluta när `i <= 20`.

### Exempel på körning:
```
10
11
12
...
20
```

## 4. Skriv ut varannat tal från 10 till 20
Utöka den förra övningen så att endast varannat tal skrivs ut.

### Instruktioner:
1. Ändra for-loopen så att `i` ökas med 2 varje gång: `i += 2`.
2. Skriv ut talet som vanligt.

### Exempel på körning:
```
10
12
14
16
18
20
```

## 5. Skapa en variabel före loopen
Lägg till en variabel som räknar hur många tal som skrivs ut i loopen.

### Instruktioner:
1. Skapa en variabel `antal` före loopen och sätt den till 0.
2. Inuti loopen, öka variabeln med 1 varje gång ett tal skrivs ut.
3. Efter loopen, skriv ut hur många tal som skrevs ut.

### Tips:
- Lägg till i variabeln med `antal++`.

### Exempel på körning:
```
10
12
14
16
18
20
Antal tal som skrevs ut: 6
```

## 6. Skriv ut text
Låt användaren ange en text och skriv ut samma text 5 gånger.

### Instruktioner:
1. Läs in en text från användaren med `Console.ReadLine`.
2. Använd en for-loop som kör 5 gånger.
3. Skriv ut texten i varje loopvarv.

### Exempel på körning:
```
Ange en text: Hej!
Hej!
Hej!
Hej!
Hej!
Hej!
```

## 7. Extra utmaning: kombinera variabler och text
Låt användaren ange ett tal och en text. Skriv sedan ut texten lika många gånger som det angivna talet.

### Instruktioner:
1. Läs in ett tal och en text från användaren.
2. Använd en for-loop där loopen kör lika många gånger som talet.
3. Skriv ut texten i varje loopvarv.

### Exempel på körning:
```
Ange ett tal: 3
Ange en text: Kul!
Kul!
Kul!
Kul!
```