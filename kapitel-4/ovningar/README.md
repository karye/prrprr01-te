# Övningar 

På egen hand får du nu lösa några uppgifter som tränar på for-loopar och tryParse.

Till din hjälp finns en [C#-lathund](https://csharp.progdocs.se/lathund).

## Grundläggande om for-loopar

```csharp
// Skriv ut alla tal från 1 till 10
for (int i = 1; i <= 10; i++)
{
    Console.WriteLine(i);
}

// Skriv ut alla tal från 10 till 1
for (int i = 10; i >= 1; i--)
{
    Console.WriteLine(i);
}

// Skriv ut alla jämna tal från 2 till 10
for (int i = 2; i <= 10; i += 2)
{
    Console.WriteLine(i);
}

// Skriv ut alla udda tal från 1 till 9
for (int i = 1; i <= 9; i += 2)
{
    Console.WriteLine(i);
}
```