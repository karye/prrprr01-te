---
description: Träna på att använda for-loopar i C# för att iterera, summera och skapa mönster.
---

# Mikroövning: for-loopar

I denna övning ska du träna på att använda **for-loopar** för att iterera över siffror, göra beräkningar och skapa mönster. Övningen består av flera steg där du bygger upp funktionaliteten gradvis.

Till din hjälp: [C#-lathund](https://csharp.progdocs.se/lathund)

## 1. Iterera över en sekvens av siffror
* Skapa en `for-loop` som skriver ut alla heltal från **1 till 10**.
  
### Exempel på körning:
```
1
2
3
...
10
```

## 2. Användaren anger start och slut
* Låt användaren ange startvärdet och slutvärdet för loopen

### Exempel på körning:
```
Ange startvärde: 3
Ange slutvärde: 7
3
4
5
6
7
```

## 3. Summera alla siffror i en sekvens
* Skapa en `for-loop` som summerar alla heltal och skriver ut summan.
* Tips: Skapa en variabel för att hålla summan och öka den i varje loop.

### Exempel på körning:
```
Summan av siffrorna 1 till 10 är: 55
```

## 4. Summera alla jämna siffror i en sekvens del 2
* Låt användaren ange startvärdet och slutvärdet
* Skapa en `for-loop` som summerar alla heltal.

### Exempel på körning:
```
Ange startvärde: 2
Ange slutvärde: 8
Summan av siffrorna 2 till 8 är: 35
```

## 5. Skriv ut jämna och udda siffror
* Skapa två separata `for-loopar` som skriver ut:
  * Alla jämna siffror från **1 till 20**.
  * Alla udda siffror från **1 till 20**.
* Tips: Använd `%` (modulo) för att kontrollera om ett tal är jämnt eller udda, dvs `tal % 2 == 0`.

### Exempel på körning:
```
Jämna siffror: 2, 4, 6, 8, ..., 20
Udda siffror: 1, 3, 5, 7, ..., 19
```

## 6. Multiplikationstabell
* Låt användaren ange ett tal.
* Skapa en `for-loop` som skriver ut multiplikationstabellen för det talet (1 till 10).

### Exempel på körning:
```
Ange ett tal: 3
3 x 1 = 3
3 x 2 = 6
...
3 x 10 = 30
```

## 7. Skapa ett mönster med stjärnor
* Använd en `for-loop` för att skriva ut ett pyramidmönster med stjärnor (`*`).

Exempel för 5 rader:
```
*
**
***
****
*****
```

## 8. Skriv ut en julgran
* Låt användaren ange antal rader för julgranen.

Exempel för 5 rader:
```
    *
   ***
  *****
 ********
**********
    **
    **
```

## Valfria förbättringar
1. **Omvänd ordning:** Skriv ut talen från 10 till 1 istället för 1 till 10.
2. **Stjärnmönster baklänges:** Skapa en pyramid som börjar bred och smalnar av:
   ```
   *****
   ****
   ***
   **
   *
   ```
3. **Multiplikationstabell för flera tal:** Låt användaren ange två tal och skriv ut tabellerna för båda.
4. **Fler stjärnmönster:** Låt användaren ange antal rader och skapa motsvarande pyramid.
