---
description: 
---

# Mikroövning: for-loop + TryParse

I denna övning ska du träna på att använda loopar och `TryParse` för att hantera användarens inmatning samt göra beräkningar. Övningen består av flera steg där du bygger upp funktionaliteten gradvis.

Till din hjälp: [C#-lathund](https://csharp.progdocs.se/lathund)

## 1. Skapa en enkel loop
- Skapa en loop som körs **10 gånger**.
- Varje gång loopen körs ska den skriva ut ditt namn, exempelvis:  
```
Alex B
Alex B
...
```

## 2. Lägg till ett nummer framför namnet
- Ändra loopen så att den också skriver ut en stigande siffra framför namnet.

### Exempel på körning:
```
0. Alex B
1. Alex B
2. Alex B
...
```

## 3. Låt användaren bestämma antalet repetitioner
- Läs in ett tal från användaren som anger hur många gånger loopen ska köras.
- Använd en `while`-loop och `TryParse` för att säkerställa att användarens inmatning är en siffra mellan **1 och 10** innan programmet går vidare.

## 4. Gör beräkningar efter loopen
- Efter att loopen har körts klart, be användaren mata in **ytterligare** ett tal.
- Skriv ut resultaten av följande beräkningar på talet:
  - Multiplicera det med 2.
  - Dividera det med 2.
  - Addera 2.
  - Subtrahera 2.

### Exempel på körning:
```
Ange ett tal: 5
Multiplicerat med 2: 10
Dividerat med 2: 2,5
Adderat med 2: 7
Subtraherat med 2: 3
```

## Valfria förbättringar
1. Gör en snyggare meny för användarens val.
2. Visa ett felmeddelande om användaren matar in ett ogiltigt värde (utöver den nuvarande valideringen).
3. Lägg till fler beräkningar, t.ex. kvadraten av talet eller talet upphöjt till tre.
4. Låt användaren välja om hen vill köra programmet igen eller avsluta.