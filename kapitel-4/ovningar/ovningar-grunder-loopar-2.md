# Övning: grunder med loopar, inmatning och variabler 2:2

## 1. Här kommer mina tal!

Skriv ut talen 1 till 5 på en rad med en `for`-loop.

### Instruktioner:
1. Skapa en tom sträng som heter `minaTal`.
2. Använd en `for`-loop som går från 1 till 5.
3. Lägg till varje tal i strängen, separerat med ett kommatecken.
4. Efter loopen, skriv ut texten: "Mina tal: " följt av strängen.

### Tips:
- Kom ihåg att använda `+=` för att lägga till tal till strängen.
- Tänk på att undvika ett kommatecken efter sista talet.

### Exempel på körning:
```
Mina tal: 1, 2, 3, 4, 5
```

## 2. Läs in ett tal 5 gånger
Läs in ett tal från användaren 5 gånger och skriv ut alla talen på en rad.

### Instruktioner:
1. Skapa en tom sträng som heter `allaTal`.
2. Använd en `for`-loop som kör 5 gånger.
3. Läs in ett tal från användaren varje gång och lägg till det i strängen.
4. Efter loopen, skriv ut texten: "Du angav talen: " följt av strängen.

### Tips:
- `for`-loopen ska köras exakt 5 gånger.
- Se till att lägga till ett kommatecken mellan talen, men inte efter sista talet.

### Exempel på körning:
```
Ange ett tal: 3
Ange ett tal: 5
Ange ett tal: 7
Ange ett tal: 1
Ange ett tal: 8
Du angav talen: 3, 5, 7, 1, 8
```

## 3. Skriv ut alla tal 10–20
Skriv ut talen från 10 till 20 med en `for`-loop.

### Instruktioner:
1. Använd en `for`-loop som går från 10 till 20.
2. Lägg till varje tal i en sträng, separerat med ett kommatecken.
3. Efter loopen, skriv ut texten: "Talen från 10 till 20 är: " följt av strängen.

### Tips:
- Starta loopen med `i = 10` och sluta när `i <= 20`.
- Glöm inte att skapa en tom sträng innan loopen börjar.

### Exempel på körning:
```
Talen från 10 till 20 är: 10, 11, 12, ..., 20
```

## 4. Skriv ut varannat tal 10–20
Skriv ut varannat tal från 10 till 20 med en `for`-loop.

### Instruktioner:
1. Använd en `for`-loop som startar på 10 och ökar `i` med 2 varje gång.
2. Lägg till varje tal i en sträng, separerat med ett kommatecken.
3. Efter loopen, skriv ut texten: "Varannat tal från 10 till 20 är: " följt av strängen.

### Tips:
- For-loopen kan skrivas som `for (int i = 10; i <= 20; i += 2)`.

### Exempel på körning:
```
Varannat tal från 10 till 20 är: 10, 12, 14, 16, 18, 20
```

## 5. Skapa en variabel före loopen
Räkna hur många jämna tal det finns mellan 10 och 20 med en `for`-loop.

### Instruktioner:
1. Skapa en variabel `antal` före loopen och sätt den till 0.
2. Använd en `for`-loop som går från 10 till 20.
3. Om talet är jämnt (dela med 2 utan rest), öka variabeln `antal` med 1.
4. Efter loopen, skriv ut texten: "Det finns " följt av antalet jämna tal.

### Tips:
- Ett tal är jämnt om `i % 2 == 0`.

### Exempel på körning:
```
Det finns 6 jämna tal mellan 10 och 20.
```

## 6. Upprepa en text
Låt användaren ange en text och upprepa den 3 gånger med en `for`-loop.

### Instruktioner:
1. Läs in en text från användaren.
2. Använd en `for`-loop som kör 3 gånger.
3. Lägg till texten i en sträng varje gång loopen körs.
4. Efter loopen, skriv ut strängen.

### Tips:
- Använd `+=` för att lägga till text till strängen.

### Exempel på körning:
```
Ange en text: Hej!
Hej!Hej!Hej!
```

## 7. Räkna ned från 10
Skriv ut talen från 10 till 1 på en rad med en `for`-loop.

### Instruktioner:
1. Använd en `for`-loop som går från 10 till 1.
2. Lägg till varje tal i en sträng, separerat med ett kommatecken.
3. Efter loopen, skriv ut texten: "Räkna ned: " följt av strängen.

### Tips:
- En `for`-loop som räknar ned ser ut så här: `for (int i = 10; i >= 1; i--)`.

### Exempel på körning:
```
Räkna ned: 10, 9, 8, 7, ..., 1
```

## 8. Gör en enkel addition i en loop
Låt användaren ange två tal, addera dem 5 gånger och skriv ut summorna.

### Instruktioner:
1. Läs in två tal från användaren.
2. Använd en `for`-loop som kör 5 gånger.
3. Lägg ihop talen varje gång och lägg till summan i en sträng.
4. Efter loopen, skriv ut strängen.

### Tips:
- Talen kan adderas med `int summa = tal1 + tal2`.

### Exempel på körning:
```
Ange ett tal: 3
Ange ett till tal: 7
Summor: 10, 10, 10, 10, 10
```

## 9. Bygg ett ord steg för steg
Låt användaren skriva in en bokstav 5 gånger och bygg ett ord med en `for`-loop.

### Instruktioner:
1. Skapa en tom sträng som heter `ord`.
2. Använd en `for`-loop som kör 5 gånger.
3. Läs in en bokstav från användaren varje gång och lägg till den i strängen.
4. Efter loopen, skriv ut: "Ditt ord är: " följt av strängen.

### Tips:
- Kom ihåg att en sträng byggs ihop med `+=`.

### Exempel på körning:
```
Ange en bokstav: H
Ange en bokstav: e
Ange en bokstav: j
Ange en bokstav: s
Ange en bokstav: a
Ditt ord är: Hejsa
```

## 10. Multiplikationstabell
Låt användaren ange ett tal och skriv ut multiplikationstabellen för det talet med en `for`-loop.

### Instruktioner:
1. Läs in ett tal från användaren.
2. Använd en `for`-loop som går från 1 till 10.
3. Beräkna resultatet av varje multiplikation och lägg till det i en sträng.
4. Efter loopen, skriv ut texten: "Multiplikationstabell: " följt av strängen.

### Tips:
- Multiplikation görs med `*`.

### Exempel på körning:
```
Ange ett tal: 3
Multiplikationstabell: 3, 6, 9, 12, ..., 30
```
