---
description: Skapa en frågesportprogram med minst tre frågor
---

# Uppgift: quiz

![Quiz Time](../.gitbook/assets/image-119.png)

Du ska skapa ett frågesport-program, med **fyra eller fler frågor**. 

## Instruktioner

* Skapa ett konsolprojekt **Quiz**.
* Programmet börjar med att fråga efter användarens **namn**.
* Sedan visas alla frågor i tur och ordning.
  * För varje fråga ska användaren få välja mellan minst **tre olika alternativ**.
  * Tips: det blir mycket enklare om hen får skriva in **a, b eller c** snarare än kompletta svar.
* Använd en **loop** för att få spelet att **börja om** när man spelat klart.
  * Lägg in en fråga – ”namn, vill du spela igen?”

## Förbättringar

* Använd en **loop** på varje fråga för att tvinga användaren att välja **a, b eller c**. Dvs, om användaren skriver in något annat, fråga igen.
* Lägg in lite fin [ASCII-art](https://www.asciiart.eu/) som grafik i spelet.
* Spelet ska hålla redo på **antalet poäng**, i en `int`-variabel. 
  * Rätt svar ger **+1 poäng**, fel svar ger **-1 poäng**.
* När alla frågor är klara ska användaren få veta sitt **resultat**.
  * Antalet poäng man fått avgör vilken text som visas, tex:
    * ”Bra jobbat namn!”
    * ”Bättre lycka nästa gång namn!”
    * ”namn, du är en riktig quiz-mästare!”

## Exempel på körning

```text
Välkommen till frågesporten!
Vad heter du? Maria

Fråga 1: Vilken är huvudstaden i Sverige?
a) Stockholm
b) Göteborg
c) Malmö
Ditt svar (a, b eller c): b
Fel svar!

Fråga 2: Vilket är världens största hav?
a) Atlanten
b) Stilla havet
c) Indiska oceanen
Ditt svar (a, b eller c): b
Rätt svar!

Fråga 3: Vad heter Sveriges kung?
a) Carl XVI Gustaf
b) Gustav Vasa
c) Olof Skötkonung
Ditt svar (a, b eller c): a
Rätt svar!

Du fick totalt 1 poäng!
Bra jobbat, Maria!

Vill du spela igen? (j/n): n
```

## Överkurs

* Lägga in frågorna i **metoder**, som returnerar antalet poäng användaren tjänat.
* Skapa en separat metod som innehåller loopen som avtvingar användaren svaret a, b eller c, och som returnerar användarens svar.

## Definitivt överkurs

* Skapa **en klass** för frågor, som innehåller varje frågas text och svarsalternativ. Samt en metod för att ställa frågan.
* Skriv kod som läser in frågor och svar från en **textfil**.
  * …och lagrar dem i en lista med instanser av ovanstående klass.
  * Använd något standardformat för data, t.ex. csv, json eller xml för lagring av frågorna och svaren.
* Lägg till så att administratören kan välja att gå in i en ”**redigeringsmeny**”, där hen t.ex. kan:
  * Skapa nya frågor
  * Ta bort frågor
  * Ändra existerande frågor

## Exempellösning

Så här kan ett enkelt utkast se ut: [Program.cs](https://github.com/NTISTHLM-TESPEL-PRR01-FACIT/FACIT_Quiz/blob/master/FACIT_Quiz/Program.cs)