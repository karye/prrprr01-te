# While-loopen

![En meny](../.gitbook/assets/image-118.png)

## Menydriven programloop

När vi skapar ett interaktivt program vill vi ofta att användaren ska kunna välja mellan olika alternativ. `While`-loopen kan vara ett effektivt verktyg för att skapa en dynamisk användarmeny. Genom att visa en meny till användaren i en `while`-loop kan vi låta användaren utföra olika åtgärder genom att välja olika alternativ. Vi kan sedan hantera det valda alternativet med hjälp av `if`-satser eller en `switch`-sats.

Exempelvis kan vi skapa ett program som låter användaren välja mellan att omvandla en text till versaler eller gemener. Programmet kommer att fortsätta att visa menyn tills användaren väljer att avsluta programmet.

```csharp
Consol.Clear();
Console.WriteLine("Program för att omvandla text");

// Deklarera en variabel för att lagra användarens val
string val = "";

// Programloopen som snurrar tills användaren väljer att avsluta programmet
while (true)
{
    // Menyn som visas för användaren
    Console.Write("""
    1. Omvandla en text till versaler
    2. Omvandla en text till gemener
    3. Avsluta programmet
    Välj ett av följande alternativen: 
    """);

    // Läs in användarens val
    Console.Write("Ditt val: ");
    val = Console.ReadLine();

    // Hantera användarens val
    if (val == "1")
    {
        Console.WriteLine("Skriv in en text:");
        string text = Console.ReadLine().toUpper();
        Console.WriteLine($"Texten i versaler: {text}");
    }
    else if (val == "2")
    {
        Console.WriteLine("Skriv in en text:");
        string text = Console.ReadLine().toLower();
        Console.WriteLine($"Texten i gemener: {text}");
    }
    else if (val == "3")
    {
        Console.WriteLine("Tack för idag.");
        break;
    }
    else
    {
        Console.WriteLine("Du valde inte ett giltigt alternativ.");
    }
}
```

I konsolen kommer det att se ut som följande:

```text
Detta är ett menyprogram
Det kan användas för att manipulera text

Välj ett av följande alternativ:
1. Omvandla en text till versaler
2. Omvandla en text till gemener
3. Avsluta programmet
Ditt val: 1

Skriv in en text:
hej
Texten i versaler: HEJ

Välj ett av följande alternativ:
1. Omvandla en text till versaler
2. Omvandla en text till gemener
3. Avsluta programmet
Ditt val: 2

Skriv in en text:
HEJ
Texten i gemener: hej

Välj ett av följande alternativ:
1. Omvandla en text till versaler
2. Omvandla en text till gemener
3. Avsluta programmet
Ditt val: 3
Tack för idag.
```

### Användning av switch-sats för att hantera användarens val

En `switch`-sats kan också användas för att hantera användarens val. Detta kan leda till mer lättläst kod, särskilt när det finns flera alternativ att hantera. Dessutom gör det det lättare att lägga till fler alternativ i framtiden. För att avbryta `while`-loopen kan vi inte längre använda `break`, istället behöver vi ändra villkoret i `while`-loopen så att det blir falskt.

```csharp
// Deklarera en variabel för att lagra användarens val
string val = "";
Console.WriteLine("Detta är ett menyprogram");
Console.WriteLine("Det kan användas för att addera och multiplicera tal");

// Initiera en loop som fortsätter tills användaren väljer "3" för att avsluta programmet
while (val != "3")
{
    Console.Write("""
    
    1. Addera två tal
    2. Multiplicera två tal
    3. Avsluta programmet
    Välj ett av alternativen :
    """);

    // Läs in användarens val
    Console.Write("Ditt val: ");
    val = Console.ReadLine();

    // Hantera användarens val med hjälp av en switch-sats
    switch (val)
    {
        case "1":
            Console.WriteLine("Skriv in två tal på var sin rad");
            double tal1 = double.Parse(Console.ReadLine());
            double tal2 = double.Parse(Console.ReadLine());
            Console.WriteLine($"{tal1} + {tal2} = {tal1+tal2}");
            break;

        case "2":
            Console.WriteLine("Skriv in två tal på var sin rad");
            double faktor1 = double.Parse(Console.ReadLine());
            double faktor2 = double.Parse(Console.ReadLine());
            Console.WriteLine($"{faktor1} * {faktor2}={faktor1*faktor2}");
            break;

        case "3":
            Console.WriteLine("Tack för idag.");
            break;

        default:
            Console.WriteLine("Du valde inte ett giltigt alternativ.");
            break;
    }
}
```

## Uppgifter

### Uppgift 1

* Skapa ett nytt konsolprojekt och döp det till **Menyprogram**.
* Skapa ett program med en huvudloop och en meny som låter användaren välja mellan att subtrahera, dividera eller avsluta programmet. 
* Använd en `while`-loop för att fortsätta visa menyn tills användaren väljer att avsluta programmet
* Använd sedan `if-else`-satser för att bestämma vilken kod som ska köras beroende på användarens val i menyn.

Exempel på körning:

```text
Menyprogram
===========
Välj ett av följande alternativ:
1. Subtrahera två tal
2. Dividera två tal
3. Avsluta programmet

Ditt val: 1
Skriv in två tal på var sin rad
5
3
5 - 3 = 2
```

### Uppgift 2

* Skapa ett nytt konsolprojekt och döp det till **Texteditor**.
* Skapa ett program med en huvudloop och en meny som låter användaren skriva och läsa en textfil. Menyalternativen bör vara:
  * Skriv till fil
  * Läs från fil
  * Avsluta programmet

* Tips! För att läsa från en textfil kan du använda följande kod:

```csharp
using System.IO;

// Läs in text från filen
string text = File.ReadAllText("filnamn.txt");

// Skriv text i filen
File.WriteAllText("filnamn.txt", text);
```

Konsolen kan det se ut så här:

```text
Texteditor
==========
Välj ett av följande alternativ:
1. Skriv till fil
2. Läs från fil
3. Avsluta programmet

Ditt val: 1
Skriv in en text: Hej
Texten har sparats i filen text.txt
```

### Uppgift 3

* Skapa ett nytt konsolprojekt och döp det till **MittMenyprogram**.
* Skapa ett program med en huvudloop och en meny. 
* Kom på ett antal alternativ som användaren kan välja mellan. 
* Lägg till ett alternativ som gör att programmet avslutas. 

### Uppgift 4: Enkel kalkylator

* Skapa ett program där användaren kan göra enkla beräkningar.
* Programmet ska visa en meny där användaren kan välja att addera, subtrahera, multiplicera eller dividera två tal.
* Programmet ska fortsätta visa menyn tills användaren väljer att avsluta.

**Exempel på körning:**

```text
Enkel kalkylator
================
1. Addera två tal
2. Subtrahera två tal
3. Multiplicera två tal
4. Dividera två tal
5. Avsluta programmet

Ditt val: 1
Skriv in första talet: 10
Skriv in andra talet: 5
Resultat: 10 + 5 = 15
```

### Uppgift 5: Temperaturkonverterare

* Skapa ett program som omvandlar temperaturer mellan Celsius och Fahrenheit.
* Användaren ska kunna välja att omvandla från Celsius till Fahrenheit eller från Fahrenheit till Celsius.
* Programmet ska fortsätta fråga om konverteringar tills användaren väljer att avsluta.

**Exempel på körning:**

```text
Temperaturkonverterare
======================
1. Celsius till Fahrenheit
2. Fahrenheit till Celsius
3. Avsluta programmet

Ditt val: 1
Skriv in temperaturen i Celsius: 25
Resultat: 25 grader Celsius är 77 grader Fahrenheit
```

### Uppgift 6: Kvadrat och kub

* Skapa ett program där användaren kan mata in ett tal och sedan välja om talet ska kvadreras eller kuberas.
* Programmet ska fortsätta fråga om användaren vill göra fler beräkningar eller avsluta.

**Exempel på körning:**

```text
Kvadrat och kub
===============
1. Kvadrera ett tal
2. Kubera ett tal
3. Avsluta programmet

Ditt val: 1
Skriv in ett tal: 4
Resultat: 4^2 = 16
```

### Uppgift 7: Slumpmässig enkel matteövning
* Skapa ett program som slumpar två tal mellan 1 och 10, och sedan ber användaren att addera, subtrahera, multiplicera eller dividera dessa tal.
* Användaren får feedback på om svaret är rätt eller fel.
* Programmet ska fortsätta generera nya tal tills användaren väljer att avsluta.

**Exempel på körning:**

```text
Matteövningar
=============
1. Slumpa tal och räkna ut svaret
2. Avsluta programmet

Ditt val: 1
Vad är 3 * 7? Skriv in ditt svar: 21
Rätt svar!
```

### Uppgift 8: Slumpa ett lyckotal
* Skapa ett program som slumpmässigt väljer ett "lyckotal" mellan 1 och 100 varje gång användaren väljer att slumpa ett nytt tal.
* Användaren kan fortsätta slumpa nya tal tills de väljer att avsluta programmet.

**Exempel på körning:**

```text
Lyckotal
========
1. Slumpa ett lyckotal
2. Avsluta programmet

Ditt val: 1
Ditt lyckotal är 42.
```
