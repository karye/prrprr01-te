# While-loopen

En `while`-loop används för att upprepa kod ett antal gånger. Loopen fortsätter att upprepa kod så länge ett villkor är sant. När villkoret är falskt avslutas loopen och programmet fortsätter med koden efter loopen.

```csharp
while (villkor)
{
    // Kod som upprepas så länge villkoret är sant
}
```

## Gissa ett tal flera gånger

![alt text](../.gitbook/assets/image-116.png)

I det här exemplet kommer vi att skapa ett program som slumpar ett tal mellan 1 och 6 och låter användaren gissa vilket tal som har slumpats. Programmet kommer att fortsätta att be användaren att gissa tills användaren gissar rätt.

```csharp
// Det här programmet slumpar ett tal mellan 1 och 6
// och låter användaren gissa vilket tal som har slumpats
Console.Clear();
Console.WriteLine("Program för att gissa ett tal mellan 1 och 6");

// Generera ett slumpmässigt heltal mellan 1 och 6
int slumptal = Random.Shared.Next(1, 7);

// En loop som fortsätter tills användaren gissar rätt
while (true)
{
    Console.Write("Gissa ett tal (1-6): ");
    int gissning = int.Parse(Console.ReadLine());

    // Kontrollera om användarens gissning matchar det slumpade talet
    if (gissning == slumptal)
    {
        Console.WriteLine("Rätt svar!");
        break; // Avbryter loopen om gissningen är korrekt
    }
    else
    {
        Console.WriteLine("Fel svar!");
    }
}

Console.WriteLine("Tack för att du spelade!");
```

I konsolen kommer det att se ut som följande:

```text
Gissa ett tal (1-6): 3
Fel svar!
Gissa ett tal (1-6): 4
Fel svar!
Gissa ett tal (1-6): 5
Fel svar!
Gissa ett tal (1-6): 6
Rätt svar!
```

## Gissa ett tal flera gånger med min- och maxvärden

I det här exemplet kommer vi att skapa ett program som låter användaren skriva in ett min- och maxvärde och sedan slumpar ett tal mellan dessa värden. Användaren ska sedan gissa vilket tal som har slumpats. Programmet kommer att fortsätta att be användaren att gissa tills användaren gissar rätt.

```csharp
// Det här programmet slumpar ett tal mellan min och max
// och låter användaren gissa vilket tal som har slumpats
Console.Clear();
Console.WriteLine("Program för att gissa ett tal mellan min och max");

// Läs in min- och maxvärde från användaren
Console.Write("Ange minvärde: ");
int min = int.Parse(Console.ReadLine());
Console.Write("Ange maxvärde: ");
int max = int.Parse(Console.ReadLine());

// Generera ett slumpmässigt heltal mellan min och max
int slumptal = Random.Shared.Next(min, max + 1);

// En loop som fortsätter tills användaren gissar rätt
while (true)
{
    Console.Write($"Gissa ett tal ({min}-{max}): ");
    int gissning = int.Parse(Console.ReadLine());

    // Kontrollera om användarens gissning matchar det slumpade talet
    if (gissning == slumptal)
    {
        Console.WriteLine("Rätt svar!");
        break; // Avbryter loopen om gissningen är korrekt
    }
    else
    {
        Console.WriteLine("Fel svar!");
    }
}

Console.WriteLine("Tack för att du spelade!");
```

I konsolen kommer det att se ut som följande:

```text
Ange minvärde: 1
Ange maxvärde: 10

Gissa ett tal (1-10): 3
Fel svar!
Gissa ett tal (1-10): 5
Fel svar!
Gissa ett tal (1-10): 7
Rätt svar!
```

## Gissa ett tal tills användaren vill sluta

I det här exemplet kommer vi att skapa ett program som slumpar ett tal mellan 1 och 6 och låter användaren gissa vilket tal som har slumpats. Programmet kommer att fortsätta att be användaren att gissa tills användaren vill sluta.

```csharp
// Det här programmet slumpar ett tal mellan 1 och 6
// och låter användaren gissa vilket tal som har slumpats
Console.Clear();
Console.WriteLine("Program för att gissa ett tal mellan 1 och 6");

// Generera ett slumpmässigt heltal mellan 1 och 6
...

// En loop som fortsätter tills användaren vill sluta
while (true)
{
    // Läs in användarens gissning
    ...

    // Kontrollera om användarens gissning matchar det slumpade talet
    ...

    Console.Write("Vill du fortsätta? (j/n): ");
    string fortsatt = Console.ReadLine().ToLower();
    if (fortsatt != "j")
    {
        break; // Avbryter loopen om användaren inte vill fortsätta
    }
}

Console.WriteLine("Tack för att du spelade!");
```

## Inmatningskontroll

Med en loop kan vi tvinga användaren att välja mellan bestämda alternativ:

```csharp
// Det här programmet slumpar ett tal mellan 1 och 6
...

// En loop som fortsätter tills användaren vill sluta
while (true)
{
    // Läs in användarens gissning
    ...

    // Kontrollera om användarens gissning matchar det slumpade talet
    ...

    // Kontrollera att användaren svara rätt
    while (true)
    {
        Console.Write("Vill du avbryta? (j/n): ");
        string fortsatt = Console.ReadLine().ToLower();
        if (fortsatt == "j")
        {
            break; // Avbryter loopen om användaren inte vill fortsätta
        }
        else 
        {
            Console.WriteLine("Jag förstår inte vad du skriver...");
        }
    }
}

Console.WriteLine("Tack för att du spelade!");
```

## Räkna antal gånger

Vi kan enkelt lägga till en variabel som håller reda på antalet försök i gissningsspelet.

Exempelkod med variabeln `försök` för att räkna antal gissningar:

```csharp
// Program för att gissa ett tal och hålla koll på antal gissningar
Console.Clear();
Console.WriteLine("Program för att gissa ett tal mellan 1 och 6.");

// Generera ett slumpmässigt heltal mellan 1 och 6
int slumptal = Random.Shared.Next(1, 7);
int försök = 0; // Håller koll på antal gissningar

while (true)
{
    Console.Write("Gissa ett tal (1-6): ");
    int gissning = int.Parse(Console.ReadLine());

    försök++; // Öka antalet försök varje gång användaren gissar

    if (gissning == slumptal)
    {
        Console.WriteLine($"Rätt svar! Du gissade rätt på {försök} försök.");
        break; // Avbryt loopen om gissningen är korrekt
    }
    else
    {
        Console.WriteLine("Fel svar! Försök igen.");
    }
}

Console.WriteLine("Tack för att du spelade!");
```

### Exempel på körning:

```plaintext
Program för att gissa ett tal mellan 1 och 6.
Gissa ett tal (1-6): 2
Fel svar! Försök igen.
Gissa ett tal (1-6): 5
Fel svar! Försök igen.
Gissa ett tal (1-6): 3
Rätt svar! Du gissade rätt på 3 försök.
Tack för att du spelade!
```

I det här exemplet håller variabeln `försök` reda på hur många gånger användaren gissar, och när användaren gissar rätt, visas antalet försök. Denna funktion kan enkelt läggas till i andra typer av spel eller frågesporter för att ge feedback om hur många försök användaren behövde.

## Uppgifter

### Uppgift 1: Gissa talet

* Skapa ett nytt konsolprojekt **GissaTalet2**.
* Utöka programmet ovan så att användaren får veta om gissningen var för hög eller för låg.

### Uppgift 2: Gissa talet på 5 försök

* Utöka programmet så att användaren bara får gissa 5 gånger innan spelet avslutas.
* Om användaren inte gissar rätt på 5 försök ska programmet avslutas och användaren informeras om det.
