---
description: Kul med slumptal i spel
---
# Utforska slumpmässighet i spel

Många spel involverar någon form av slumpmässighet, vilket ger en unik och oförutsägbar spelupplevelse varje gång. Oavsett om det är att generera slumpmässiga nummer, välja ett kort på måfå, eller positionera objekt slumpmässigt, spelar slumpen en avgörande roll i att hålla spänningen vid liv.

I C# kan vi skapa slumpmässighet med hjälp av `Random`-klassen. Här är ett exempel på hur du kan generera slumpmässiga nummer:

```csharp
// Ett slumptal från 0 till 4
int slumptal1 = Random.Shared.Next(5);

// Ett slumptal från 1 till 9
int slumptal2 = Random.Shared.Next(1, 10);
```

## Uppgift 1: Lyckohjulet

![Lyckohjul](../.gitbook/assets/image-51.webp)

* Skapa ett nytt konsolprojekt **Lyckohjulet**
* Användaren ska kunna välja ett nummer mellan 1 och 10, och datorn kommer slumpmässigt att generera ett nummer i samma intervall.
* Om användarens nummer matchar datorns, vinner de spelet!

Användarens interaktion med programmet kan se ut så här:

1. Användaren startar programmet och blir ombedd att gissa ett nummer mellan 1 och 10.
2. Användaren skriver in sitt valda nummer.
3. Programmet genererar ett slumpmässigt nummer mellan 1 och 10.
4. Programmet jämför användarens nummer med det slumpmässigt genererade numret.
5. Programmet meddelar användaren om de vann (det vill säga om numren matchar) eller förlorade.

Användaren ska ha tre försök att gissa rätt nummer.

## Uppgift 2: Tärningssimulator

![Tärningar](../.gitbook/assets/image-52.png)

* Skapa ett nytt konsolprojekt **Tärningssimulator**
* Användaren ska kunna bestämma hur många tärningar de vill kasta, och hur många sidor varje tärning ska ha.
* Därefter ska programmet simulera tärningskasten och skriva ut resultatet av varje kast.

## Uppgift 3: 21:an

![Black Jack](../.gitbook/assets/image-53.jpg)

* Skapa ett nytt konsolprojekt **BlackJack**
* Reglerna för spelet är enkla:
  * Spelaren och datorn får först två kort var.
  * Spelaren får sedan dra kort tills de antingen stannar eller får en total poängsumma över 21.
  * Datorn drar kort tills de har en total poängsumma över 21.
  * Om Spelaren stannar, drar datorn ett extra kort.
    * Den som kommer närmast 21 utan att överstiga vinner spelet.

* Exempel på kort:
  * Korten 2-10 har samma värde som sitt nummer.
  * Knekt, Dam och Kung har värdet 10.
  * Ess kan ha värdet 1 eller 11, beroende på vad som är mest fördelaktigt för spelaren.

* Extra utmaning:
  * Implementera en enkel grafisk representation av korten med hjälp av ASCII-tecken, se https://raw.githubusercontent.com/anton-bot/Full-Emoji-List/master/Emoji.cs.
  * Användaren ska kunna spela spelet flera gånger utan att behöva starta om programmet.
  * Datorn ska kunna dra kort på ett intelligent sätt, baserat på sina kort och spelarens kort.