﻿---
description: Skapa ett spel av "Sten, sax, påse" mot datorn
---

# Uppgift: spelet sten, sax, påse, ödla, Spock

Du ska skapa ett spel där användaren kan spela "Sten, sax, påse, ödla, Spock" mot datorn.

![](../.gitbook/assets/image-96.png)

"Rock-Paper-Scissors-Lizard-Spock" är en utökad version av det klassiska spelet "sten-sax-påse". Spelet introducerades för en bredare publik genom TV-serien "The Big Bang Theory", men dess ursprung går tillbaka till 2005 när det skapades av David C. Lovelace.

I denna variant finns det fem möjliga val:

1. Sten (Rock)
2. Sax (Scissors)
3. Påse (Paper)
4. Ödla (Lizard)
5. Spock

## Spelets regler

- Sten krossar sax
- Sten krossar ödla
- Sax klipper papper
- Sax halshugger ödla
- Påse täcker sten
- Påse avvisar Spock
- Ödla äter papper
- Ödla förgiftar Spock
- Spock krossar sax
- Spock smälter sten

Varje val vinner över två andra val, förlorar mot två andra och matchar sig själv. Detta gör spelet mer balanserat jämfört med originalversionen, och det ger det också en extra taktisk djup.

## Instruktioner

* Skapa ett konsolprojekt **StenSaxPase**
* Programmet börjar med att slumpmässigt generera datorns val (sten, sax, påse, ödla eller Spock).
* Användaren får sedan göra sitt val.
  * Tips: En enkel lösning är att användaren anger sitt val som ett heltal (1, 2, 3, 4 eller 5).\
    1 = sten, 2 = sax, 3 = påse, 4 = ödla, 5 = Spock
  * En mer avancerad lösning är att användaren skriver in ordet "sten", "sax", "påse", "ödla" eller "Spock".\
    Använd `ToLower()` för att göra om inmatningen till små bokstäver och `Trim()` för att ta bort eventuella mellanslag.\
    Och datorn slumpar fram sitt val från en lista av \["sten", "sax", "påse", "ödla", "Spock"\].
* Jämför datorns och användarens val för att avgöra vem som vinner.
* Informera användaren om resultatet av omgången.
* Använd en **while-loop** för att låta spelet börja om när en omgång är slut.

## Exempellösning

Ett enkelt utkast kan inkludera användning av `Random` för datorns val, `if`- och `else`-satser eller `switch`-satser för att avgöra vinnaren och en `while`-loop för att upprepa spelet:

```csharp
// Spelet sten, sax, påse, ödla, Spock
Console.Clear();
Console.WriteLine("Välkommen till Sten, sax, påse, ödla, Spock!");
Console.WriteLine("""
Spelets regler:
Sten krossar sax
Sten krossar ödla
Sax klipper papper
Sax halshugger ödla
Påse täcker sten
Påse fångar Spock
Ödla äter papper
Ödla förgiftar Spock
Spock krossar sax
Spock smälter sten
""");

// Programloop

    // Slumpa fram datorns val

    // Fråga användaren om val
    Console.WriteLine("Välj: sten, sax, påse, ödla eller Spock: ");

    // Jämför användarens och datorns val

    // Vem vinner?

    // Vill användaren spela igen?

// Avsluta programmet
```

## Förbättringar

* Lägg in lite enkel grafik som Emoji för att illustrera valen (sten, sax, påse, ödla, Spock).
* Använd en **while-loop** för att tvinga användaren att välja sten, sax, påse, ödla eller Spock. Om användaren skriver in något annat, fråga igen.
* Lägg in en poängräkning där användaren och datorn samlar poäng för varje vunnen omgång.

## Överkurs

* Skapa en metod för att genomföra en omgång och returnera resultatet.