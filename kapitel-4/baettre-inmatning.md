# Säkrare inmatning

Det är viktigt att kunna hantera användarinmatning korrekt i dina program. Om en användare skriver in oväntade värden kan det leda till fel eller till och med orsaka programmet att krascha.

## Felsäker inmatning med `TryParse()`

En vanlig situation där detta kan uppstå är när du vill omvandla en sträng till ett numeriskt värde. Om strängen inte kan omvandlas till ett numeriskt värde kommer programmet att kasta ett undantag och avsluta.

Ett sätt att hantera detta är att använda metoden `TryParse()` som finns tillgänglig för numeriska typer som `int`, `double`, `float` och så vidare. `TryParse()` försöker omvandla en sträng till ett numeriskt värde och returnerar en boolean som indikerar om omvandlingen lyckades eller inte.

Här är ett exempel på hur `TryParse()` kan användas för att säkert läsa in ett heltal från användaren:

```csharp
Console.Write("Gissa ett tal (1-6)? ");
string input = Console.ReadLine();

if (int.TryParse(input, out int gissning))
{
    Console.WriteLine("Vad bra att du matade in ett tal!");
}
else
{
    Console.WriteLine("Svara med heltal, tack!");
}
```

I detta exempel kommer `TryParse()` att returnera `true` och sätta värdet på `gissning` om användaren skriver in ett giltigt heltal. Om användaren skriver in något som inte kan omvandlas till ett heltal kommer `TryParse()` att returnera `false` och `gissning` kommer att vara 0.

## Använda `TryParse()` i en loop för säker inmatning

Du kan använda `TryParse()` i en `while`-loop för att fortsätta fråga användaren tills de ger ett giltigt svar. Här är ett exempel på hur det kan se ut:

```csharp
int gissning = 0;
bool ärHeltal = false;

while (true)
{
    // Fråga användaren om ett tal
    Console.Write("Gissa ett tal (1-6)? ");
    string input = Console.ReadLine();

    // Försök omvandla strängen till ett heltal
    ärHeltal = int.TryParse(input, out gissning);

    // Om omvandlingen misslyckades, skriv ut ett felmeddelande
    if (!ärHeltal)
    {
        Console.WriteLine("Svara med heltal, tack!");
    }
    // Om omvandlingen lyckades, avsluta loopen
    else
    {
        break;
    }
}
```

I det här exemplet kommer loopen att fortsätta tills användaren skriver in ett giltigt heltal.

## Tryparse() för andra numeriska typer

`TryParse()` finns tillgänglig för andra numeriska typer som `double`, `float` och `decimal`.
Här är ett exempel på hur du kan använda `TryParse()` för att läsa in ett decimaltal från användaren:

```csharp
double tal = 0;
bool ärDecimaltal = false;

while (true)
{
    // Fråga användaren om ett tal
    Console.Write("Hur lång är du? (m) ");
    string input = Console.ReadLine();

    // Försök omvandla strängen till ett decimaltal
    ärDecimaltal = double.TryParse(input, out tal);

    // Om omvandlingen misslyckades, skriv ut ett felmeddelande
    if (!ärDecimaltal)
    {
        Console.WriteLine("Svara med ett decimaltal, tack!");
    }
    // Om omvandlingen lyckades, avsluta loopen
    else
    {
        break;
    }
}
```

## Uppgifter

### Uppgift 1

* Skapa ett konsolprojekt **SäkrareInmatning**.
* Programmet som ber användaren att mata in ett tal.
* Programmet skall skriva om det inmattade är ett heltal eller decimaltal.

### Uppgift 2

* Skapa ett konsolprojekt **Area**.
* Programmet som ber användaren att mata in två decimaltal: ett för längd och ett för bredd.
* Programmet skall skriva ut arean av en rektangel med de angivna måtten.
* Om användaren matar in något som inte är ett giltigt decimaltal, ska programmet fortsätta att fråga användaren tills den ger ett giltigt svar.

## Förenklad användning av `TryParse()` med en metod

Eftersom det är så vanligt att använda `TryParse()` för att läsa in numeriska värden från användaren kan det vara en bra idé att skapa en metod för att förenkla detta. Här är ett exempel på hur det kan se ut:

```csharp
// Användning av metoden
int tal = ReadInt("Gissa ett tal (1-6)? ");

// Metoden
static int ReadInt(string prompt)
{
    int tal = 0;
    bool ärHeltal = false;

    while (true)
    {
        // Fråga användaren om ett tal
        Console.Write(prompt);
        string input = Console.ReadLine();

        // Försök omvandla strängen till ett heltal
        ärHeltal = int.TryParse(input, out tal);

        // Om omvandlingen misslyckades, skriv ut ett felmeddelande
        if (!ärHeltal)
        {
            Console.WriteLine("Svara med heltal, tack!");
        }
        // Om omvandlingen lyckades, avsluta loopen
        else
        {
            break;
        }
    }

    return tal;
}
```

## Hantering av felaktig inmatning med `try-catch`

Ett annat sätt att hantera felaktig inmatning är att använda en `try-catch`-sats. Denna metod kan användas för att fånga och hantera undantag som kastas när ett fel uppstår. Här är ett exempel på hur det kan se ut:

```csharp
// Fråga användaren om ett tal
Console.Write("Gissa ett tal (1-6)? ");
string input = Console.ReadLine();

// Försök omvandla strängen till ett heltal
try
{
    int gissning = int.Parse(input);
    Console.WriteLine("Vad bra att du matade in ett tal!");
}
// Om omvandlingen misslyckades, skriv ut ett felmeddelande
catch (FormatException)
{
    Console.WriteLine("Svara med heltal, tack!");
}
```

I detta exempel kommer `int.Parse(input)` att kasta ett `FormatException` om `input` inte kan omvandlas till ett heltal. I det här fallet fångar vi undantaget med en `catch`-sats och skriver ut ett felmeddelande till användaren. Observera att `try-catch`-metoden bör användas med försiktighet, eftersom det kan dölja problem i din kod och göra den svårare att felsöka.

Det är också värt att notera att `try-catch` kan ha en negativ inverkan på programmets prestanda, speciellt om undantag kastas ofta. I de flesta fall är det bättre att använda `TryParse()` för att hantera potentiella fel vid konvertering av numeriska värden.


