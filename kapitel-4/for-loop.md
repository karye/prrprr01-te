---
description: Hur man upprepar kod
---

# For-loopen

Vi har sett hur `while`-loopen kan användas för att upprepa kod. I det här kapitlet kommer vi att titta på en annan typ av loop som kan användas för att upprepa kod: `for`-loopen.

När vi på förhand vet hur många gånger vi vill upprepa en viss kod, kan vi använda en `for`-loop. 

## Hur fungerar for-loopen?

En for-loop består av tre delar:

1. **Initieringen**: Här sätts startvärdet för loopvariabeln.
1. **Villkoret**: Detta är ett booleskt uttryck (ett uttryck som antingen är sant eller falskt) som testas före varje loop-iteration. Om uttrycket är sant, körs loopen.
1. **Iterationen**: Detta ändrar värdet på loopvariabeln efter varje iteration.

![Loop](../.gitbook/assets/image-97.png)

Tillsammans ser en for-loop ut så här i kod:

```csharp
for (int i = 0; i < 5; i++)
{
    // kod som ska upprepas
}
```

I detta exempel är int i = 0 **initieringen**, i < 5 är **villkoret**, och i++ är **iterationen**. Loopen fortsätter att köra så länge i är mindre än 5. Efter varje loop ökas i med 1 tack vare i++. När i når värdet 5 är villkoret inte längre sant, och loopen avslutas.

## Exempelprogram - Gissa ett tal

Vi kommer att skapa ett program där användaren uppmanas att gissa ett tal mellan 1 och 6. Om användarens gissning är korrekt, skriver programmet ut meddelandet "Rätt gissning". Om gissningen däremot är felaktig, kommer programmet att meddela "Fel gissning".

### Flödesschema

![Flödesschema](../.gitbook/assets/image-54.png)

### Implementering

Först måste vi slumpa fram ett tal mellan 1 och 6. Detta görs med hjälp av `Random.Shared.Next(1, 7)`.

```csharp
// Slumpa fram ett heltal mellan 1 och 6
int slumptal = Random.Shared.Next(1, 7);

// Be användaren gissa ett tal
Console.Write("Gissa ett tal mellan 1 och 6: ");
int gissning = int.Parse(Console.ReadLine());

// Kontrollera om gissningen är korrekt
if (gissning == slumptal)
{
    Console.WriteLine("Rätt svar!");
}
else
{
    Console.WriteLine("Fel svar!");
}
```

Exempel på programkörning i konsolen:

```text
Gissa ett tal mellan 1 och 6: 3
Fel svar!
```

### Använda for-loopen för att upprepa frågan

Om vi vill ställa samma fråga flera gånger kan vi använda oss av en `for`-loop. I exemplet nedan upprepar vi frågan fem gånger.

```csharp
for (int i = 0; i < 5; i++)
{
    Console.Write("Gissa ett tal mellan 1 och 6: ");
    int gissning = int.Parse(Console.ReadLine());
}
```

Exempel på programkörning i konsolen:

```text
Gissa ett tal mellan 1 och 6: 3
Gissa ett tal mellan 1 och 6: 4
Gissa ett tal mellan 1 och 6: 5
Gissa ett tal mellan 1 och 6: 6
Gissa ett tal mellan 1 och 6: 7
```

## Låt användaren gissa flera gånger

Nu ska vi modifiera programmet så att användaren kan gissa flera gånger. Om användaren gissar rätt, kommer programmet att skriva ut "Rätt gissning". Om gissningen är felaktig, kommer programmet att meddela "Fel gissning". Om användaren gissar fel, kommer programmet att ge användaren en ny chans att gissa.

```csharp
// Slumpa fram ett heltal mellan 1 och 6
int slumptal = Random.Shared.Next(1, 7);

// Ge användaren fem chanser att gissa
for (int i = 0; i < 5; i++) // i++ är samma sak som i = i + 1
{
    // Be användaren att gissa ett tal
    Console.Write("Gissa ett tal mellan 1 och 6: ");
    int gissning = int.Parse(Console.ReadLine());

    // Kontrollera om gissningen är korrekt
    if (gissning == slumptal)
    {
        Console.WriteLine("Rätt svar!");
        break; // Avslutar loopen om användaren gissar rätt!
    }
    else
    {
        Console.WriteLine("Fel svar!");
    }
}

// Användaren har nu fått sina fem gissningschanser
Console.WriteLine("Det här gick inte så bra!");
```

Exempel på programkörning i konsolen:

```text
Gissa ett tal mellan 1 och 6: 3
Fel svar!
Gissa ett tal mellan 1 och 6: 4
Fel svar!
Gissa ett tal mellan 1 och 6: 5
Fel svar!
Gissa ett tal mellan 1 och 6: 6
Rätt svar!
```

Som vi ser ovan, tillåter for-loopen oss att upprepa en specifik kodblock ett bestämt antal gånger, vilket är särskilt användbart i situationer där vi vill att användaren ska ha flera försök, eller när vi behöver iterera över en lista eller en samling av objekt. Genom att förstå hur for-loopen fungerar, kan vi skapa mer dynamiska och interaktiva program.

## Uppgifter

### Uppgift 1

Skapa ett program som ber användaren att mata in ett tal. 
Programmet ska sedan skriva ut alla tal från 1 till det tal som användaren matade in.

Konsolen kan det se ut så här:

```text
Ange ett tal: 5
1
2
3
4
5
```

### Uppgift 2

Skapa ett program som ber användaren att mata in ett tal. 
Programmet ska sedan använda en `for`-loop för att räkna ner från det inmatade talet till noll. För varje iteration ska programmet också skriva ut summan av alla tal som hittills har skrivits ut.

Konsolen kan det se ut så här:

```text
Ange ett tal: 5
5
Summa: 5
4
Summa: 9
3
Summa: 12
2
Summa: 14
1
Summa: 15
0
Summa: 15
```

### Uppgift 3

* Skapa ett program som frågar användaren efter en textsträng och ett heltal `n`. 
* Använd en `for`-loop för att skriva ut textsträngen `n` gånger.

Konsolen kan det se ut så här:

```text
Ange en textsträng: Hej
Ange ett heltal: 3
Hej
Hej
Hej
```

### Uppgift 4

Skapa ett program där användaren först får mata in en startpunkt och en slutpunkt. Därefter ska programmet skriva ut alla jämna tal som finns mellan start- och slutpunkten.
Tips: Använd modulus-operatorn (%) för att kontrollera om ett tal är jämnt eller inte.

Konsolen kan det se ut så här:

```text
Ange startpunkt: 1
Ange slutpunkt: 10
2
4
6
8
10
```

### Uppgift 5

Använd en `for`-loop för att skapa ett program som ritar ut ett rätvinkligt triangel med hjälp av stjärntecken (“*”). Användaren ska kunna ange triangelns höjd.

Konsolen kan det se ut så här:

```text
Höjd: 5
*
**
***
****
*****
```
