# Stränghantering

Stränghantering är en central del av programmering. C# erbjuder en rad inbyggda metoder för att manipulera strängar på olika sätt. I denna del kommer vi att utforska några grundläggande tekniker för stränghantering: att hitta tecken i en sträng, extrahera delsträngar och dela upp en sträng i mindre delar.

![Stränghantering](./../.gitbook/assets/url.png)

## Sök efter tecken eller teckensekvenser i en sträng

Metoden `IndexOf()` är mycket användbar när du behöver hitta ett visst tecken eller en sekvens av tecken inom en sträng. Den här metoden skannar igenom din sträng från start till slut och återger indexpositionen för den första förekomsten av det sökta tecknet eller teckensekvensen. Om det du söker inte finns med i strängen, kommer metoden att returnera -1.

Låt oss se på ett exempel där vi hittar var @-tecknet befinner sig i en e-postadress:

```csharp
string email= "kalle@gmail.se”;

// Hitta positionen för @-tecknet!
int position = email.IndexOf("@");
Console.WriteLine($"@-tecknet finns på position {position}");
```

I konsolen ser vi:

```text
@-tecknet finns på position 5
```

## Extraktion av delsträngar

Metoden `Substring()` möjliggör extraktion av en delsträng från din huvudsträng. Den tar emot två argument: det första anger startpositionen och det andra specificerar antalet tecken som ska inkluderas i din delsträng. Delsträngen som skapas är en helt ny sträng.

Exemplen nedan illustrerar användningen av `Substring()`-metoden för att extrahera domännamnet från en e-postadress med två olika överlag:

```csharp
string email = "kalle@gmail.se";

// Extrahera domännamnet (gmail.se) med två argument
int position = email.IndexOf("@");
string domain = email.Substring(position + 1, email.Length - position - 1);
Console.WriteLine($"Domännamnet är: {domain}");

// Extrahera domännamnet (gmail.se) med ett argument
domain = email.Substring(position + 1);
Console.WriteLine($"Domännamnet är: {domain}");
```

Resultatet i konsolen för båda exemplen blir:

```text
Domännamnet är: gmail.se
```

Oavsett vilken överlag du väljer att använda är både giltiga och valet beror på vad som är mest lämpligt för din specifika situation.

## Att dela upp strängar i mindre delar

För att bryta ned en sträng i flera mindre delsträngar används `Split()`-metoden. Denna metod tar ett tecken eller en teckensekvens som argument och delar upp den ursprungliga strängen vid varje förekomst av det angivna tecknet eller teckensekvensen. Resultatet blir en array av strängar.

Se följande exempel där vi delar en e-postadress i två delar; en del för användarnamnet och en del för domännamnet:

```csharp
string email = "kalle@gmail.se";

// Dela upp strängen vid @-tecknet
string[] parts = email.Split('@');
Console.WriteLine($"Användarnamn: {parts[0]}");
Console.WriteLine($"Domännamn: {parts[1]}");
```

I konsolen ser vi:

```text
Användarnamn: kalle
Domännamn: gmail.se
```

Här har e-postadressen delats upp i två delsträngar vid @-tecknet, där första delen representerar användarnamnet och den andra domännamnet.

## Uppgifter

### Uppgift 1

* Skapa ett konsolprojekt **Stränghantering1**.
* Extrahera alla ord i meningen **"Den mätta dagen, den är aldrig störst. Den bästa dagen är en dag av törst."** som börjar med bokstaven "d". 
* Använd `Split()`-metoden för att dela upp meningen i en array av ord, och använd sedan en `foreach`-loop för att iterera över orden och extrahera de ord som börjar med "d".
* Använd `ToLower()`-metoden för att konvertera varje ord till gemener innan du jämför det med "d".

### Uppgift 2

* Skapa ett konsolprojekt **Stränghantering2**.
* Byt ut alla mellanslag i meningen **"Nog finns det mål och mening i vår färd - men det är vägen, som är mödan värd."** med #-tecken. 
* Använd `Replace()`-metoden för att byta ut alla mellanslag mot #-tecken.
* Använd `Trim()`-metoden för att ta bort mellanslag i början och slutet av strängen innan du byter ut mellanslag i resten av strängen.

### Uppgift 3

* Skapa ett konsolprojekt **Stränghantering3**.
* Extrahera det sista ordet i meningen **"Det bästa målet är en nattlång rast, där elden tänds och brödet bryts i hast."**. 
* Använd `Split()`-metoden för att dela upp meningen i en array av ord,. 
* Använd sedan `Length`-egenskapen för att hitta indexet för det sista ordet i arrayen.

### Uppgift 4

* Skapa ett konsolprojekt **Stränghantering4**.
* Räkna antalet förekomster av bokstaven "e" i meningen **"På ställen, där man sover blott en gång, blir sömnen trygg och drömmen full av sång."**. 
* Använd en `for`-loop för att iterera över varje tecken i strängen och öka en räknare varje gång bokstaven "e" hittas.

### Uppgift 5

* Skapa ett konsolprojekt **Stränghantering5**.
* Extrahera alla ord i meningen **"Bryt upp, bryt upp! Den nya dagen gryr. Oändligt är vårt stora äventyr."** som har mer än tre bokstäver. 
* Använd `Split()`-metoden för att dela upp meningen i en array av ord.
* Använd sedan en `foreach`-loop för att iterera över orden och extrahera de ord som har mer än tre bokstäver.

### Uppgift 6

* Skapa ett konsolprojekt **Stränghantering6**.
* Extrahera alla ord i meningen **"Ja visst gör det ont när knoppar brister."** som innehåller mer än 5 bokstäver. 
* Använd `Split()`-metoden för att dela upp meningen i en array av ord.
* Använd sedan en `foreach`-loop för att iterera över orden och extrahera de ord som har mer än fem bokstäver.
