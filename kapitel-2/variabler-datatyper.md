# Variabler och datatyper

## Variabler

En variabel är ett namn som refererar till ett värde i datorns minne. Värdet kan vara en textsträng, ett heltal, ett decimaltal, ett enskilt tecken, eller ett sant eller falskt värde. 

Variabler kan användas för att lagra data som användaren anger, eller för att lagra resultatet av beräkningar som programmet utför.

### Deklaration av variabler

För att skapa en variabel behöver vi först deklarera den. Detta görs genom att ange variabelns datatyp och namn. Här är ett exempel på hur vi deklarerar en variabel av typen `string` med namnet `namn`:

```csharp
string namn;
```

### Tilldelning av värde till variabler

När vi deklarerat en variabel kan vi tilldela ett värde till den. Detta görs genom att använda tilldelningsoperatorn `=`. Här är ett exempel på hur vi tilldelar värdet `"Kajsa Johansson"` till variabeln `namn`:

```csharp
namn = "Kajsa Johansson";
```

## Förståelse för datatyper

Datatyper i programmering representerar en mängd olika sätt att lagra och hantera data i datorns minne. Varje datatyp har unika egenskaper som definierar dess användning och begränsningar. Att välja rätt datatyp är avgörande för effektiv datalagring och för att försäkra sig om att man kan utföra de rätt operationerna på datan.

De mest grundläggande datatyperna i programmering, som finns i de flesta programmeringsspråk, inkluderar följande:

| Typ | Beskrivning | Exempel |
| :--------- | :------------------------- | :------------------------------- |
| **string** | Används för att lagra en sekvens av tecken eller text. | string namn = "Kajsa Johansson"; |
| **int**    | Används för att lagra heltal, både positiva och negativa. | int antal = 12;                  |
| **double** | Används för att lagra decimaltal, med högre precision än andra flyttalstyper. | double vikt = 35,21;             |
| **char**   | Används för att lagra ett enskilt tecken. | char tecken = 'a';               |
| **bool**   | Används för att lagra ett sant eller falskt värde. | bool ärStor = true;              |

## Exempel på användning av datatyper

### Använda textsträngar med `string`-datatypen

För att demonstrera hur en `string`-datatyp används, låt oss skapa ett nytt projekt i C#. Först vill vi rensa konsolen och skriva ut en introduktion till vårt program. Sedan kan vi använda en `string`-variabel för att lagra ett namn som användaren anger.

```csharp
// Rensa konsolen och introducera programmet
Console.Clear();
Console.Write("Det här programmet räknar ut din ålder vid examen.");

// Läs in användarens namn
Console.Write("Ange ditt namn: ");
string namn = Console.ReadLine();
```

### Använda heltal med `int` och konvertering av sträng till heltal

Att läsa in ett tal från konsolen i C# sker alltid som en textsträng. Därför behöver vi konvertera strängen till ett heltal om vi vill utföra några matematiska operationer på den. För att göra detta kan vi använda metoden `int.Parse()`.

Här är ett exempel på hur vi kan läsa in en ålder som en sträng, konvertera den till ett heltal, lägga till 2 till den (för att simulera ålder vid examen om 2 år), och skriva ut resultatet.

```csharp
// Läs in temperaturen i Celsius som en sträng
Console.Write("Ange temperaturen i Celsius: ");
string celsiusString = Console.ReadLine();

// Konvertera strängen till ett heltal
int celsius = int.Parse(celsiusString);

// Beräkna temperaturen i Fahrenheit
int fahrenheit = (celsius * 9 / 5) + 32;

// Skriv ut resultatet
Console.WriteLine($"Temperaturen i Fahrenheit är: {fahrenheit}");
```

Ett kortare sätt att skriva detta är att använda `int.Parse()` direkt vid inläsningen av åldern:

```csharp
// Läs in temperaturen i Celsius som en sträng och konvertera den till ett heltal
Console.Write("Ange temperaturen i Celsius: ");
int celsius = int.Parse(Console.ReadLine());

// Beräkna temperaturen i Fahrenheit
int fahrenheit = (celsius * 9 / 5) + 32;

// Skriv ut resultatet
Console.WriteLine($"Temperaturen i Fahrenheit är: {fahrenheit}");
```

### Använda decimaltal med `double`-datatypen

För att använda decimaltal i C# använder vi datatypen `double`. Här är ett exempel på hur vi kan använda en `double`-variabel för att lagra en vikt som användaren anger, och sedan skriva ut vikten i kilogram och pund.

```csharp
// Läs in vikten i kilogram som en sträng och konvertera den till ett decimaltal
Console.Write("Ange din vikt i kilogram: ");
double vikt = double.Parse(Console.ReadLine());

// Beräkna vikten i pund
double pund = vikt * 2.20462;

// Skriv ut resultatet
Console.WriteLine($"Din vikt är: {vikt} kg eller {pund} pund");

// Skriv ut resultatet med två decimaler
// :00.00 indikerar betyder två siffror före och två efter decimaltecknet
Console.WriteLine($"Din vikt är: {vikt} kg eller {Math:00.00} pund");
```

## Användning av variabelsubstitution

Variabelsubstitution, eller stränginterpolering, är ett enklare sätt att infoga variabler i en sträng. Istället för att använda `+`-operatorn för att lägga ihop strängar och variabler kan vi använda `${}` för att bädda in variablerna direkt i strängen. Detta gör koden lättare att läsa och skriva.

```csharp
// Ett enklare sätt att skriva ut...
Console.WriteLine($"{namn}, du är {ålder} år när du tar examen.");

// Vilket är enklare att läsa än
Console.WriteLine(namn + ", du är " + ålder + " år när du tar examen.");
```

## Hur funkar operatorerna + / * - ?

I C# fungerar operatorerna `+`, `-`, `*` och `/` på olika sätt beroende på vilken datatyp du arbetar med. När du arbetar med heltal (`int`), får du hela tal som resultat:

```csharp
int summa = 7 + 2;        // Ger 9
int delat = 7 / 2;        // Ger 3, inte 3.5, eftersom vi arbetar med heltal
```

Om du vill få ett decimaltal som resultat, behöver du arbeta med datatypen `double`:

```csharp
double delat = 7.0 / 2;   // Ger 3.5
```

Observera att om du delar två heltal, får du ett heltal som resultat, även om du lagrar resultatet i en `double`-variabel:

```csharp
double delat = 7 / 2;     // Ger 3.0, inte 3.5
```

För att få ett decimaltal som resultat när du delar två heltal, behöver du konvertera minst ett av talen till `double`. Du kan göra detta antingen genom att lägga till `.0` efter talet, eller genom att använda en cast-operator `(double)`:

```csharp
double delat1 = (double)7 / 2;   // Ger 3.5
double delat2 = 7.0 / 2;         // Ger 3.5
double delat3 = 7d / 2;          // Ger 3.5, 'd' indikerar att talet är av typen double
```

## Fler datatyper i C#

Utöver de grundläggande datatyperna som vi har diskuterat ovan, finns det flera andra datatyper i C# som kan vara användbara beroende på vilken typ av data du arbetar med. Här är några exempel på andra datatyper:

| Typ | Beskrivning | Exempel |
| :--------- | :------------------------- | :------------------------------- |
| **float**  | Används för att lagra decimaltal med lägre precision än `double`. | float vikt = 35.21f;            |
| **decimal**| Används för att lagra decimaltal med högre precision än `double`. | decimal pris = 35.21m;          |
| long   | Används för att lagra stora heltal. | long population = 8000000000;   |
| short  | Används för att lagra korta heltal. | short antal = 12;               |
| byte   | Används för att lagra ett heltal mellan 0 och 255. | byte färg = 255;                |
| sbyte  | Används för att lagra ett heltal mellan -128 och 127. | sbyte temperatur = -10;         |
| uint   | Används för att lagra positiva heltal. | uint antal = 12;                |
| ulong  | Används för att lagra stora positiva heltal. | ulong population = 8000000000;  |
| ushort | Används för att lagra korta positiva heltal. | ushort antal = 12;              |
| **char**   | Används för att lagra ett enskilt tecken. | char tecken = 'a';              |
| **bool**   | Används för att lagra ett sant eller falskt värde. | bool ärStor = true;             |
| ...     | ... | ... |

## Uppgifter

### Uppgift 1

* Skapa ett konsolprojekt: **FahrenheitTillCelsius**
* Programmet ska fråga efter temperaturen i Fahrenheit
* Programmet ska sedan presentera temperaturen i Celsius
* Formeln för att konvertera Fahrenheit till Celsius är: `C = (F - 32) * 5 / 9`

### Uppgift 2

* Skapa ett konsolprojekt: **BMI**
* Programmet ska fråga efter användarens vikt i kg och längd i meter
* Programmet ska sedan presentera användarens BMI (Body Mass Index)
* Formeln för att beräkna BMI är: `BMI = vikt / (längd * längd)`
* Tips: Använd datatypen `double` för att lagra vikten, längden och BMI

### Uppgift 3

* Skapa ett konsolprojekt: **Biluthyrning**
* Program för att beräkna kostnaden för att hyra bil hos en biluthyrningsfirma.
* Startavgiften är 500 kr
* Kostnaden per mil är 5 kr
* Kostnad per dag är 100 kr
* Programmet ska fråga efter antal mil och antal dagar
* Programmet ska sedan presentera totala kostnaden

**Exempel på körning:**

```plaintext
Ange antal mil: 200
Ange antal dagar: 5
Total kostnad: 1500 kr
```

### Uppgift 4

* Skapa ett konsolprojekt: **SekTid**
* Programmet ska fråga efter antal sekunder
* Programmet ska sedan presentera hur många timmar, minuter och sekunder det är i antalet sekunder
* Tips: Använd heltalsdivision (/) och modulus (%) för att räkna ut antalet timmar, minuter och sekunder