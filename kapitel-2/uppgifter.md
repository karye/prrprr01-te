---
description: Beräkna Mars position
---

# Uppgift: beräkna Mars position

![Mars](../.gitbook/assets/Mars_-_August_30_2021_-_Flickr_-_Kevin_M._Gill.png)

## Beräkna Mars Position

Målet är att skapa ett enkelt program som beräknar longituden (längdgraden) för planeten Mars baserat på ett givet datum.

## Bakgrund

Mars, den fjärde planeten från solen, har en bana som kan beskrivas matematiskt med vissa formler. Genom att känna till hur många dagar det har gått sedan ett specifikt referensdatum (kallat J2000, som är 1 januari 2000) kan vi beräkna Mars ungefärliga position i sin bana.

![Rektascension](../.gitbook/assets/475px-Ra_and_dec_on_celestial_sphere.png)

## Krav

Skapa ett konsolprojekt **MarsPosition**. Programmet ska uppfylla följande krav:
1. Programmet ska börja med att visa en introduktionstext för användaren.
2. Programmet ska sedan be användaren att ange ett datum (år, månad och dag).
3. Utifrån det angivna datumet ska programmet beräkna hur många dagar det har gått sedan J2000.
4. Använd den givna formeln: `medellongitud = (355.453 + (0.5240207766 * dagarSedanJ2000)) % 360` för att beräkna Mars:s medellongitud.
5. För förenklings skull kommer vi inte att ta hänsyn till andra korrigeringar i denna Uppgift.
6. Programmet ska sedan visa Mars:s beräknade longitud för det angivna datumet.
7. Avsluta med beräkna rektascension i timmar, minuter och sekunder såhär: `rektascension = medellongitud / 15`

## Tips

Om du är osäker på hur du räknar ut antal dagar mellan två datum i C#, kan du använda `TimeSpan`-objektet. När du subtraherar två `DateTime`-objekt får du tillbaka en `TimeSpan`, som representerar tidsintervallet mellan de två datumen.

Här är ett kort exempel:

```csharp
DateTime startDatum = new DateTime(2000, 1, 1);  // Representant för J2000
DateTime slutDatum = new DateTime(2023, 5, 10);  // Ett exempeldatum
int antalDagar = (slutDatum - startDatum).Days;  // Beräkna antal dagar mellan datumen
```

#### Exempel på önskad output:

```text
Beräkna Mars position
Ange ett år: 2023
Ange en månad: 09
Ange en dag: 3
Mars medellongitud för 2023-09-03 är 206,13663448360057 grader.
Mars rektascension för 2023-09-03 är 13h 44m 32s.sekunder
```

I verkligheten kommer Mars:s position att vara något annorlunda, eftersom vi inte tar hänsyn till andra korrigeringar i denna uppgift. På [The Sky Live](https://theskylive.com/mars-info) kan du jämföra med verkliga värden.