---
Beskrivning: En grundläggande genomgång av hur man styr flödet i ett program med villkor.
---

# Villkorsstyrning

## Grundläggande if-sats

En `if-sats` tillåter att ett visst kodblock körs endast när ett visst villkor uppfylls. Om villkoret är sant, körs koden inuti if-satsen. Annars ignoreras den.

![Bild som illustrerar if-satsen](../.gitbook/assets/image-68.png)

För att validera ett lösenord kan en `if-sats` vara användbar:

```csharp
Console.Write("Ange ett lösenord: ");
string lösenord = Console.ReadLine();

if (lösenord == "test123") {
   Console.WriteLine("Du är inloggad");
}
else {
   Console.WriteLine("Fel lösenord");
}
```

## Kedjade if-satser

För mer komplexa villkor kan flera if-satser kedjas ihop. Exempelvis för att avgöra vilken färg en användare anger:

```csharp
Console.Write("Ange en färg (röd, grön, blå): ");
string färg = Console.ReadLine();

if (färg == "röd") {
   Console.WriteLine("Färgen är röd");
}
else if (färg == "grön") {
   Console.WriteLine("Färgen är grön");
}
else if (färg == "blå") {
   Console.WriteLine("Färgen är blå");
}
else {
   Console.WriteLine("Färgen är inte känd");
}
```

## Använda operatorer inom if-satser

För att jämföra värden kan man använda operatorer. Dessa hjälper till att avgöra om en `if-sats` ska exekveras eller inte. Här är de vanligaste operatorerna:

| Operator | Beskrivning |
| :------: | --------------------- |
| == | Lika med |
| != | Inte lika med |
|  < | Mindre än |
|  > | Större än |
| <= | Mindre eller lika med |
| >= | Större eller lika med |

Det är viktigt att jämföra liknande datatyper för att säkerställa korrekt beteende. 

Exempel:

```csharp
// Lotto vinstnummer
int vinstnr = 1234;

Console.Write("Gissa vinstnummer (1-9999): ");
int gissning = int.Parse(Console.ReadLine());

if (gissning == vinstnr) {
    Console.WriteLine("Du gissade rätt!");
}
else if (gissning < vinstnr) {
    Console.WriteLine("Du gissade för lågt.");
}
else if (gissning > vinstnr) {
    Console.WriteLine("Du gissade för högt.");
}
```

## Logiska operatorer

För att kombinera villkor kan logiska operatorer användas:

| Operator | Beskrivning           |
| :------: | --------------------- |
| && | Logiskt OCH |
| \|\| | Logiskt ELLER |
| !| Logiskt INTE |

Exempel:

```csharp
// Lotto vinstnummer
int vinstnr = 1234;

Console.Write("Gissa vinstnummer (1-9999): ");
int gissning = int.Parse(Console.ReadLine());

if (gissning == vinstnr) {
    Console.WriteLine("Du gissade rätt!");
}
else if (gissning < vinstnr && gissning > 0) {
    Console.WriteLine("Du gissade för lågt.");
}
else if (gissning > vinstnr || gissning < 0) {
    Console.WriteLine("Du gissade för högt.");
}
else {
    Console.WriteLine("Ogiltig gissning.");
}
```
## Hantera stora och små bokstäver

Notera att i C#, skiljer man mellan stora och små bokstäver. Det innebär att "Palme" och "palme" betraktas som två ord. Om vi vill ignorera skillnaden mellan stora och små bokstäver i användarens svar, kan vi använda metoden `ToLower()`.

Exempel:

```csharp
// Gissa statsminister
Console.Write("Vem var statsminister i Sverige år 1970? ");
string användarensSvar = Console.ReadLine();

// Omvandla svaret till små bokstäver (gemena)
användarensSvar = användarensSvar.ToLower();

// Nu spelar det ingen roll hur användaren svarar
if (användarensSvar == "palme")
{
    Console.WriteLine("Du svarade rätt!");
}
else
{
    Console.WriteLine("Du svarade fel!");
}
```

## Laborationer

### Laboration - felsökning av lampa

Skapa ett nytt konsolprojekt och döp det till `LabbLampa`.

I den här laborationen ska du skapa ett program som hjälper användaren att felsöka ett potentiellt lampfel genom att ställa en serie frågor. Din uppgift är att komplettera koden nedan med korrekta `if-satser` för att styra flödet i programmet baserat på användarens svar.

![](../.gitbook/assets/image-69.png)

```csharp
Console.Clear();
Console.WriteLine("Välkommen till guiden felsöka lampa!");

// Ställ den första frågan till användaren
Console.Write("Är lampan inkopplad? (j/n): ");
string svar = Console.ReadLine();

// TODO: Lägg till en if-sats som kontrollerar svaret,
// och ställer nästa fråga eller ger en lösning baserat på svaret
if (...) {
   ...
}
else
{
   // TODO: Ställ den andra frågan till användaren
   ...
   string svar2 = Console.ReadLine();

   // TODO: Lägg till en if-sats som kontrollerar svaret på den andra frågan, 
   // och ger en lösning baserat på svaret
   ...
}
```

## Uppgifter

### Uppgift 1

* Skapa ett nytt konsolprojekt och döp det till **EngineeringFlow**.
* Program ska fungera enligt detta flödesschema:

![](../.gitbook/assets/image-70.png)

### Uppgift 2

* Skapa ett konsolprojekt och döp det till **Datorer**.
* Programmet som frågar användaren hur många **datorer** hen äger. Skriptet ska sedan skriva ut korrekt i singular eller plural:

* Det innebär att om användaren har en dator skrivs det ut "**Du har 1 dator**".
* Om användaren har tex 3 datorer skrivs det ut "**Du har 3 datorer**".
* **Programmet får endast innehåll en if-sats**.

### Uppgift 3

* Skapa ett program enligt detta flödesschema:

![](../.gitbook/assets/image-71.png)

### Uppgift 4

* Skapa ett program enligt detta flödesschema:

![](../.gitbook/assets/image-74.png)

Man kan använda sig av emojis [unicode.org](http://www.unicode.org/emoji/charts/full-emoji-list.html) i sin kod:

```csharp
Console.OutputEncoding = Encoding.UTF8;
Console.WriteLine("No Problem! 😆");
```

### Uppgift 5

* Skapa ett program som frågar användaren efter en **månad** och skriver ut antalet dagar i den månaden.

**Exempel på körning:**

```text
Ange en månad: januari
Januari har 31 dagar.

Ange en månad: februari
Februari har 28 dagar.
```

### Uppgift 6

* Skapa ett program som implementerar **valfria åldersregler**.\
Välj 4 åldersgränser och associera dem med något som är tillåtet att göra vid den åldern.

**Tex** kan programmet fråga användaren efter deras ålder och sedan skriva ut vilka fordon de får köra:

| Ålder | Fordon |
| :---: | ----------------- |
| 15 | moped |
| 16 | moped, traktor |
| 18 | moped, traktor, bil |
| 21 | moped, traktor, bil, lastbil |

**Exempel på körning:**

```text
Ange din ålder: 18
Du får köra moped, traktor och bil.

Ange din ålder: 15
Du får köra moped.

Ange din ålder: 22
Du får köra moped, traktor, bil och lastbil.
```

### Uppgift 7

* Skapa ett quiz där användaren får svara på frågor inom ett ämne du själv väljer (t.ex. sport, film, musik eller något annat du gillar). Beroende på hur många frågor användaren svarar rätt på, får de en betygsbedömning eller tilldelas en nivå. Låt eleverna själva välja antal frågor, svarsalternativ och nivåer.

* Instruktioner:
	- Välj ett ämne du tycker om.
	- Skapa minst 5 frågor.
	- Bedöm användarens svar och ge dem en nivå baserat på deras prestation.

**Exempel på bedömning:**

| Rätt antal svar | Nivå |
| :---: | -------- |
| 0–2   | Nybörjare |
| 3–4   | Medelnivå |
| 5     | Expert |

**Exempel på körning:**

```text
Vad heter huvudstaden i Japan?
1. Tokyo
2. Seoul
3. Peking
Ditt svar: 1

Hur många kontinenter finns det?
1. 5
2. 6
3. 7
Ditt svar: 3

Du svarade rätt på 2 av 5 frågor.
Du är på Nybörjar-nivå!
```

### Uppgift 8

* Skapa ett program som ger filmrekommendationer baserat på vad användaren gillar. Fråga användaren om olika preferenser (t.ex. genrer, favoritregissörer, skådespelare eller humör) och rekommendera en film utifrån svaren. Låt eleverna själva skapa listor över filmer och kriterier för vad som rekommenderas.

* Instruktioner:
	- Skapa frågor som hjälper dig att förstå användarens preferenser.
	- Sätt upp regler för hur du väljer en film baserat på svaren.
	- Var kreativ med filmrekommendationerna!

**Exempel på frågor:**

1. Vilken typ av film gillar du bäst? (Action, Komedi, Drama)
2. Gillar du filmer med en lycklig slut? (Ja/Nej)
3. Föredrar du filmer från det senaste decenniet? (Ja/Nej)

**Exempel på körning:**

```text
Vilken typ av film gillar du bäst? (Action, Komedi, Drama): Komedi
Gillar du filmer med en lycklig slut? (Ja/Nej): Ja
Föredrar du filmer från det senaste decenniet? (Ja/Nej): Nej

Vi rekommenderar att du ser "Monty Python and the Holy Grail"!
```


