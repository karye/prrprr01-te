---
description: Övningar på if-satser i C#.
---

# Mikroövningar: Kedjade if-satser med flera variabler 2:2

I dessa övningar ska du träna på att använda kedjade `if`-satser, flera variabler och variabelsubstitution för att lösa olika problem. Exempelkod tillhandahålls för var tredje övning, medan resten är utmaningar utan direkt lösningstips.

Till din hjälp: [C#-lathund](https://csharp.progdocs.se/lathund)

## 1. Kontrollera en ålderskategori
- Skapa en variabel `ålder` som användaren fyller i.
- Kontrollera vilken kategori personen tillhör:
  - Barn: Under 13 år
  - Tonåring: 13-19 år
  - Vuxen: 20 år eller äldre

### Exempel på körning:
```
Ange din ålder: 15
Du är en tonåring. Din ålder är 15.
```

Tips:
```csharp
Console.Write("Ange din ålder: ");
int ålder = int.Parse(Console.ReadLine());
if (ålder < 13)
{
    Console.WriteLine($"Du är ett barn. Din ålder är {ålder}.");
}
else if (ålder >= 13 && ålder <= 19)
{
    Console.WriteLine($"Du är en tonåring. Din ålder är {ålder}.");
}
else
{
    Console.WriteLine($"Du är vuxen. Din ålder är {ålder}.");
}
```

## 2. Kontrollera körrättigheter
- Läs in en ålder.
- Kontrollera om personen får köra följande fordon:
  - Under 15: Ingen körning
  - 15-17: Moped
  - 18 och över: Bil

### Exempel på körning:
```
Ange din ålder: 16
Du får köra moped.
```

## 3. Kontrollera rabatt för biobiljetter
- Läs in en persons ålder och civilstatus (gift/ogift).
- Ge rabatt:
  - 10% rabatt för barn under 12 år.
  - 15% rabatt för pensionärer (65 och över).
  - 5% extra rabatt om personen är gift.

### Exempel på körning:
```
Ange din ålder: 70
Är du gift? ja
Du får 20% rabatt.
```

## 4. Kontrollera veckodag och öppettider
- Skapa en variabel `dag` där användaren anger veckodag.
- Skapa en variabel `tid` där användaren anger klockslag.
- Kontrollera om en butik är öppen:
  - Öppet måndag till fredag mellan 9-18.
  - Stängt på helger.

### Exempel på körning:
```
Ange veckodag: tisdag
Ange klockslag: 10
Butiken är öppen på tisdag klockan 10.
```

Tips:
- Använd `.ToLower()` för att göra strängen gemener vid jämförelse.

```csharp
Console.Write("Ange veckodag: ");
string dag = Console.ReadLine().ToLower();
Console.Write("Ange klockslag (0-23): ");
int tid = int.Parse(Console.ReadLine());
if ((dag == "måndag" || dag == "tisdag" || dag == "onsdag" || dag == "torsdag" || dag == "fredag") && tid >= 9 && tid <= 18)
{
    Console.WriteLine($"Butiken är öppen på {dag} klockan {tid}.");
}
else
{
    Console.WriteLine($"Butiken är stängd på {dag} klockan {tid}.");
}
```

## 5. Beräkna årsskatt
- Läs in en persons inkomst.
- Beräkna skattesats:
  - 10% om inkomsten är under 20 000.
  - 20% om inkomsten är mellan 20 000 och 50 000.
  - 30% om inkomsten är över 50 000.

### Exempel på körning:
```
Ange din inkomst: 45000
Din skatt är 20%.
```

## 6. Kontrollera en skolas nivå
- Läs in en elevs ålder.
- Visa vilken skolnivå eleven går i:
  - Under 7: Förskola
  - 7-15: Grundskola
  - 16-19: Gymnasium
  - Över 19: Högskola/universitet

### Exempel på körning:
```
Ange din ålder: 14
Du går i grundskolan.
```

## 7. Klassificera temperatur
- Skapa en variabel `temp` där användaren anger temperaturen.
- Klassificera:
  - Under 0: Mycket kallt
  - 0-10: Kallt
  - 11-20: Svalt
  - 21 och över: Varmt

### Exempel på körning:
```
Ange temperaturen: 15
Temperaturen 15 är sval.
```

Tips:
- Använd intervallkontroller i `else if`.

```csharp
Console.Write("Ange temperaturen: ");
int temp = int.Parse(Console.ReadLine());
if (temp < 0)
{
    Console.WriteLine($"Temperaturen {temp} är mycket kall.");
}
else if (temp >= 0 && temp <= 10)
{
    Console.WriteLine($"Temperaturen {temp} är kall.");
}
else if (temp >= 11 && temp <= 20)
{
    Console.WriteLine($"Temperaturen {temp} är sval.");
}
else
{
    Console.WriteLine($"Temperaturen {temp} är varm.");
}
```

## 8. Beräkna träningsrekommendation
- Läs in en persons ålder och antal träningspass i veckan.
- Visa rekommendationer:
  - Under 18: Rekommenderas minst 3 pass i veckan.
  - 18-65: Rekommenderas minst 5 pass i veckan.
  - Över 65: Rekommenderas 2 pass i veckan.

### Exempel på körning:
```
Ange din ålder: 25
Hur många pass tränar du i veckan? 4
Du borde träna minst 5 pass i veckan.
```

## 9. Jämför två produkter
- Läs in pris och kvalitet (1-5) för två produkter.
- Visa vilken produkt som ger bäst värde för pengarna (lägre pris och högre kvalitet).

### Exempel på körning:
```
Produkt 1: Pris 500, kvalitet 4
Produkt 2: Pris 400, kvalitet 3
Produkt 1 ger bäst värde för pengarna.
```

## 10. Kontrollera skottår
- Läs in ett år och kontrollera om det är ett skottår:
  - Delbart med 4 men inte med 100 (om inte delbart med 400).

Tips:
- Använd villkoren `(år % 4 == 0 && år % 100 != 0) || (år % 400 == 0)`.

### Exempel på körning:
```
Ange ett år: 2020
2020 är ett skottår.
```

```csharp
Console.Write("Ange ett år: ");
int år = int.Parse(Console.ReadLine());
if ((år % 4 == 0 && år % 100 != 0) || (år % 400 == 0))
{
    Console.WriteLine($"{år} är ett skottår.");
}
else
{
    Console.WriteLine($"{år} är inte ett skottår.");
}
```

## 11. Kontrollera kreditvärdighet
- Läs in inkomst och skulder.
- Klassificera:
  - Bra kreditvärdighet: Hög inkomst och låg skuld.
  - Normal kreditvärdighet: Normal inkomst och skuld.
  - Låg kreditvärdighet: Hög skuld eller låg inkomst.

### Exempel på körning:
```
Inkomst: 50000, skuld: 10000
Din kreditvärdighet är bra.
```

## 12. Kontrollera varukorg
- Läs in två varors priser och en budget.
- Kontrollera om användaren har råd att köpa båda, en eller ingen vara.

### Exempel på körning:
```
Vara 1: 400 kr
Vara 2: 700 kr
Din budget: 1000 kr
Du har råd att köpa endast en av varorna.
```