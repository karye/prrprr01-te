---
description: Övningar på if-satser i C#.
---

# Mikroövningar: Kedjade if-satser med flera variabler 1:2

I denna övning ska du träna på att använda i-satser för att fatta beslut baserat på användarinmatning eller variabler. Du kommer också att använda strängkonkatenering (`+`).

Till din hjälp: [C#-lathund](https://csharp.progdocs.se/lathund)

## 1. Kontrollera om ett tal är större än 10
- Läs in ett heltal från användaren.
- Använd en `if`-sats för att kontrollera om talet är större än 10.
- Visa ett meddelande om talet är större än 10.

### Exempel på körning:
```
Ange ett tal: 12
12 är större än 10.
```

Tips:
```csharp
Console.Write("Ange en siffra: ");
int siffra = int.Parse(Console.ReadLine());
if (siffra > 5)
{
    Console.WriteLine(siffra + " är större än 5.");
}
```

## 2. Kontrollera om en person är myndig
- Läs in en ålder från användaren.
- Använd en `if`-sats för att kontrollera om personen är 18 år eller äldre.
- Visa ett meddelande om personen är myndig.

### Exempel på körning:
```
Ange din ålder: 20
Du är myndig.
```

## 3. Jämför två tal
- Läs in två heltal från användaren.
- Använd en `if`-sats för att visa vilket tal som är störst, eller om de är lika stora.

### Exempel på körning:
```
Ange första talet: 5
Ange andra talet: 8
8 är större än 5.
```

Tips:
```csharp
Console.Write("Skriv ett första tal: ");
int förstaTal = int.Parse(Console.ReadLine());
Console.Write("Skriv ett andra tal: ");
int andraTal = int.Parse(Console.ReadLine());
if (förstaTal == andraTal)
{
    Console.WriteLine("Talen är lika stora.");
}
else
{
    Console.WriteLine("De två talen är olika.");
}
```

## 4. Kontrollera om ett tal är jämnt
- Läs in ett heltal från användaren.
- Använd en `if`-sats för att kontrollera om talet är jämnt, dvs. delbart med 2.
- Visa ett meddelande om talet är jämnt eller inte.

### Exempel på körning:
```
Ange ett tal: 4
4 är ett jämnt tal.

Ange ett tal: 5
5 är ett udda tal.
```

Tips:
```csharp
if (heltal % 3 == 0)
{
    Console.WriteLine(heltal + " är delbart med 3.");
}
```

## 5. Kontrollera lösenord
- Be användaren ange ett lösenord.
- Använd en `if`-sats för att kontrollera om lösenordet är "hemligt".
- Visa ett meddelande om lösenordet är korrekt.

### Exempel på körning:
```
Ange lösenord: hemligt
Lösenordet är korrekt.

Ange lösenord: katt
Fel lösenord.
```

Tips:
```csharp
if (kodord == "öppna")
{
    Console.WriteLine("Rätt kodord!");
}
```

## 6. Kontrollera temperatur
- Läs in en temperatur från användaren.
- Använd en `if`-sats för att kontrollera om temperaturen är under 0 grader.
- Visa ett meddelande om det är minusgrader.

### Exempel på körning:
```
Ange temperaturen: -5
Det är minusgrader.
```

Tips:
```csharp
Console.Write("Temperatur idag: ");
int dagensTemp = int.Parse(Console.ReadLine());
if (dagensTemp > 25)
{
    Console.WriteLine("Det är en varm dag.");
}
```

## 7. Kontrollera bokstav
- Läs in en bokstav från användaren.
- Använd en `if`-sats för att kontrollera om bokstaven är "A".
- Visa ett meddelande om bokstaven är "A".

### Exempel på körning:
```
Ange en bokstav: A
Du skrev bokstaven A.
```

Tips:
```csharp
Console.Write("Vilken bokstav gillar du? ");
string input = Console.ReadLine();
if (input == "B")
{
    Console.WriteLine("Du gillar bokstaven B.");
}
```

## 8. Kontrollera namn
- Be användaren skriva sitt namn.
- Använd en `if`-sats för att kontrollera om namnet är "Anna".
- Visa ett meddelande om det är rätt namn.

### Exempel på körning:
```
Vad heter du? Anna
Hej Anna!
```

Tips:
```csharp
Console.Write("Vad är ditt förnamn? ");
string förnamn = Console.ReadLine();
if (förnamn == "Erik")
{
    Console.WriteLine("Hej Erik!");
}
```

## 9. Kontrollera om tal är positivt eller negativt
- Läs in ett tal från användaren.
- Använd en `if`-sats för att kontrollera om talet är positivt eller negativt.
- Visa ett meddelande baserat på resultatet.

### Exempel på körning:
```
Ange ett tal: -3
-3 är ett negativt tal.
```

Tips:
```csharp
Console.Write("Ange ett heltal: ");
int heltal = int.Parse(Console.ReadLine());
if (heltal == 0)
{
    Console.WriteLine("Talet är noll.");
}
```

## 10. Kontrollera rabatt
- Läs in ett belopp från användaren.
- Använd en `if`-sats för att kontrollera om beloppet är över 500.
- Om beloppet är över 500, visa ett meddelande om att användaren får rabatt.

### Exempel på körning:
```
Ange beloppet: 600
Du får 10% rabatt!
```

Tips:
```csharp
Console.Write("Ange inköpsbelopp: ");
int inköp = int.Parse(Console.ReadLine());
if (inköp < 100)
{
    Console.WriteLine("Beloppet är för litet för rabatt.");
}
```