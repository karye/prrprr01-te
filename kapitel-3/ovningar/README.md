# Övningar 

På egen hand får du nu lösa några uppgifter som tränar på for-loopar och tryParse.

Till din hjälp finns en [C#-lathund](https://csharp.progdocs.se/lathund).

## Grundläggande om if-satser

```csharp
// Skapa en variabel för att lagra användarens ålder
int ålder = 0;
if (ålder < 18)
{
    Console.WriteLine("Du är inte myndig.");
}
else
{
    Console.WriteLine("Du är myndig.");
}

// Kontrollera om ett tal är positivt, negativt eller noll
int tal = 0;
if (tal > 0)
{
    Console.WriteLine("Talet är positivt.");
}
else if (tal < 0)
{
    Console.WriteLine("Talet är negativt.");
}
else
{
    Console.WriteLine("Talet är noll.");
}

// Kontrollera om ett tal ligger inom ett intervall
int värde = 5;
if (värde >= 0 && värde <= 10)
{
    Console.WriteLine("Talet ligger mellan 0 och 10.");
}
else
{
    Console.WriteLine("Talet ligger utanför intervallet.");
}
```