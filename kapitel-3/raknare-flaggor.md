---
Beskrivning: Hur variabler kan användas för att hålla reda på vilka villkor som är uppfyllda.
---

# Räknare och flaggor

Variabler kan också användas för att hålla reda på vilka villkor som är uppfyllda. Detta kan vara användbart när vi vill räkna hur många gånger ett visst villkor är uppfyllt, eller när vi vill hålla reda på om ett villkor är uppfyllt eller inte.

## Räkna antal rätt

![](../.gitbook/assets/image-95.png)

Att skapa ett program som frågar användaren om deras dagliga vanor, och sedan ger feedback baserat på användarens svar. Vi kommer att använda en räknare för att hålla koll på hur många gånger användaren svarar "ja".

Instruktioner:

* Fråga användaren om deras namn och hälsa dem välkommen.
* Deklarera en variabel jaRäknare som initieras till 0.
* För varje fråga, om användaren svarar "ja", öka jaRäknare med 1.
* I slutet av programmet, ge feedback baserat på antalet "ja"-svar användaren gav.

```csharp
// Program för att räkna hur många gånger användaren svarar "ja" på frågor
Console.Write("Hej! Vad heter du? ");
string namn = Console.ReadLine();

// En variabel för att räkna hur många gånger användaren svarar "ja"
int jaRäknare = 0;

Console.Write($"Hej {namn}! Har du borstat tänderna idag? (j/n) ");
string svar = Console.ReadLine().ToLower();
if (svar == "j") 
{
   jaRäknare++;
}

Console.Write("Har du ätit frukost idag? (j/n) ");
svar = Console.ReadLine().ToLower();
if (svar == "j") 
{
   jaRäknare++;
}

Console.Write("Har du motionerat idag? (j/n) ");
svar = Console.ReadLine().ToLower();
if (svar == "j") 
{
   jaRäknare++;
}

Console.WriteLine($"Du svarade ja på {jaRäknare} frågor.");
```

### Uppgifter

#### Uppgift 1: Mer komplex feedback

* Skapa ett nytt konsolprojekt **Halsovanor**.
* I stället för att endast säga "bra jobbat" i det första exemplet, ge användaren en mer detaljerad feedback beroende på hur många "ja"-svar de ger:

1. 0-1 "ja"-svar: "Det verkar som du kan göra några förbättringar i dina dagliga rutiner."
2. 2 "ja"-svar: "Bra jobbat! Du följer några hälsosamma rutiner."
3. 3 "ja"-svar: "Utmärkt! Du har bra dagliga vanor."

## Flaggor

![](../.gitbook/assets/image-94.png)

* Vi skapar ett program som ställer en serie frågor till användaren för att avgöra om de uppfyller vissa kriterier.

Instruktioner:

* Fråga användaren hens namn och hälsa välkommen.
* Deklarera en boolsk variabel ärKvalificerad och sätt den till true.
* För varje fråga, om användaren inte uppfyller kriteriet, sätt ärKvalificerad till false.
* Avsluta programmet med att meddela användaren om hen är kvalificerade eller inte.

```csharp
// Program för att avgöra om en användare är kvalificerad
Console.WriteLine("Hej! Vad heter du?");
string namn = Console.ReadLine();

// En variabel för att hålla reda på om användaren är kvalificerad
bool ärKvalificerad = true;

Console.WriteLine($"Hej {namn}! Vi kommer att ställa några frågor för att se om du kvalificerar dig.");

Console.Write("Är du över 18 år? (j/n) ");
string svar = Console.ReadLine().ToLower();
if (svar != "j") 
{
   ärKvalificerad = false;
}

Console.Write("Har du körkort? (j/n) ");
svar = Console.ReadLine().ToLower();
if (svar != "j") 
{
   ärKvalificerad = false;
}

// Ge feedback till användaren baserat på om de är kvalificerade eller inte
if (ärKvalificerad) 
{
   Console.WriteLine($"Grattis, {namn}, du uppfyller kriterierna!");
}
else 
{
   Console.WriteLine($"Tyvärr, {namn}, du uppfyller inte kriterierna.");
}
```

### Uppgifter

#### Uppgift 2: Utökade kriterier

* Skapa ett nytt konsolprojekt **Kvalificerad**.
* För det andra exemplet, lägg till ytterligare tre frågor som avgör om en användare är kvalificerad eller inte. Dessa kan vara exempelvis:

1. Har du någon erfarenhet av programmering? (j/n)
2. Har du gymnasieexamen? (j/n)
3. Har du erfarenhet av att arbeta i team? (j/n)

* Inkludera dessa nya kriterier i den boolska variabeln `harKompetens`.
* Uppdatera slutmeddelandet för att inkludera dessa nya kriterier.

#### Uppgift 3: Skapa en meny

* Skapa ett nytt konsolprojekt **Menyn**.
* Låt användaren välja mellan att köra det första programmet (räknare) eller det andra programmet (flaggor) genom att visa en meny vid start:

```
Välj ett alternativ:
1. Räkna antal "ja"-svar.
2. Kolla om du är kvalificerad.
```
