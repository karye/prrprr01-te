---
Beskrivning: Variabler kan skapas på olika ställen i koden. I det här kapitlet lär du dig mer om variabler och deras livslängd.
---

# Variablers livslängd

Här ser du exempel på hur variabler kan skapas på olika ställen i koden:

```csharp
// Exempel 1
int antal = 5;

Console.WriteLine(antal);

// Exempel 2
int antal = 5;

if (antal > 0)
{
    Console.WriteLine(antal);
}

// Exempel 3
if (antal > 0)
{
    int antal = 5;
    Console.WriteLine(antal);
}
```

